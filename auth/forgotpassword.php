<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сменить пароль");
?>
<main class="sidebar-version">
	<?$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"WITHOUT_FOOTER"=>"Y",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);?>
    <section class="contact-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6">

<div class="container static-page">
	<div class="row">
		<div class="col-xs-12">
            <?$APPLICATION->IncludeComponent( "bitrix:system.auth.changepasswd", 
                "flat", 
                Array() 
            );
            ?>
		</div>
	</div>
</div>
</section>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>