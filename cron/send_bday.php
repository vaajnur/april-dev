#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
//раз в день
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
//за 5 дней
$resUsers=CUser::GetList($by="id",$order="asc",array("ACTIVE"=>"Y","PERSONAL_BIRTHDAY_DATE"=>date("m-d",strtotime("+5 days"))));	
while($arUsers=$resUsers->Fetch())
{
	$arEvFields=array(
		"EMAIL" => $arUsers["EMAIL"],
		"NAME" => $arUsers["NAME"],
		"LAST_NAME" => $arUsers["LAST_NAME"],
		"COUPON" => $arCoupon["COUPON"]
	); 
	$ev = new CEvent;
	//$ev->Send("EMAIL_MARKETING_CUSTOM","s1",$arEvFields,"Y",74);
}
//в др
$resUsers=CUser::GetList($by="id",$order="asc",array("ACTIVE"=>"Y","PERSONAL_BIRTHDAY_DATE"=>date("m-d")));	
while($arUsers=$resUsers->Fetch())
{
	$arEvFields=array(
		"EMAIL" => $arUsers["EMAIL"],
		"NAME" => $arUsers["NAME"],
		"LAST_NAME" => $arUsers["LAST_NAME"],
		"COUPON" => $arCoupon["COUPON"]
	); 
	$ev = new CEvent; 
	$ev->Send("EMAIL_MARKETING_CUSTOM","s1",$arEvFields,"Y",72);
}
//через 3 дня
$resUsers=CUser::GetList($by="id",$order="asc",array("ACTIVE"=>"Y","PERSONAL_BIRTHDAY_DATE"=>date("m-d",strtotime("-3 days"))));	
while($arUsers=$resUsers->Fetch())
{
	$arEvFields=array(
		"EMAIL" => $arUsers["EMAIL"],
		"NAME" => $arUsers["NAME"],
		"LAST_NAME" => $arUsers["LAST_NAME"],
		"COUPON" => $arCoupon["COUPON"]
	); 
	$ev = new CEvent;
	//$ev->Send("EMAIL_MARKETING_CUSTOM","s1",$arEvFields,"Y",71);
}
?>