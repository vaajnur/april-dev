#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
/*
ТОВАРЫ:
<span style="width: 32%;display: inline-block;text-align:left;">
     <img src="img/3.jpg" width="180" border="0" style="display:block;margin: auto;" alt="" align="center">
     <span style="display: block;width: 180px;margin: 10px auto 0;">Платье</span>
     <span style="display: block;width: 180px;margin:auto;"><b>12th street By Cunthia</b></span>
     <span style="display: block;width: 100%;max-width: 180px;margin: 10px auto 0;">
         <a style="display: inline-block;vertical-align: middle;"><color style="text-decoration: line-through;font-size: 12px;color: #8a8a8a;">130 000 р.</color><br>130 000 р.</a>
         <a href="" style="display: inline-block;vertical-align: middle;float: right;padding: 8px 20px;background: #f6244b;color: white;text-decoration: none;">Купить</a>
     </span>
 </span>
 <span style="width: 32%;display: inline-block;text-align:left;">
     <img src="img/3.jpg" width="180" border="0" style="display:block;margin: auto;" alt="" align="center">
     <span style="display: block;width: 180px;margin: 10px auto 0;">Платье</span>
     <span style="display: block;width: 180px;margin:auto;"><b>12th street By Cunthia</b></span>
     <span style="display: block;width: 100%;max-width: 180px;margin: 10px auto 0;">
         <a style="display: inline-block;vertical-align: middle;"><color style="text-decoration: line-through;font-size: 12px;color: #8a8a8a;">130 000 р.</color><br>130 000 р.</a>
         <a href="" style="display: inline-block;vertical-align: middle;float: right;padding: 8px 20px;background: #f6244b;color: white;text-decoration: none;">Купить</a>
     </span>
 </span>
 <span style="width: 32%;display: inline-block;text-align:left;">
     <img src="img/3.jpg" width="180" border="0" style="display:block;margin: auto;" alt="" align="center">
     <span style="display: block;width: 180px;margin: 10px auto 0;">Платье</span>
     <span style="display: block;width: 180px;margin:auto;"><b>12th street By Cunthia</b></span>
     <span style="display: block;width: 100%;max-width: 180px;margin: 10px auto 0;">
         <a style="display: inline-block;vertical-align: middle;"><color style="text-decoration: line-through;font-size: 12px;color: #8a8a8a;">130 000 р.</color><br>130 000 р.</a>
         <a href="" style="display: inline-block;vertical-align: middle;float: right;padding: 8px 20px;background: #f6244b;color: white;text-decoration: none;">Купить</a>
     </span>
 </span>
 */
//раз в неделю
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
//активные и подтвержденные адреса, подписанные на рубрики
$subscr = CSubscription::GetList(
    array("ID"=>"ASC"),
    array("RUBRIC"=>array(5),  "ACTIVE"=>"Y" )
);
$count=4;
$goods=array();
$advanced_goods=array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y",'>PROPERTY_RAZMER_SKIприDKI_VALUE'=>0,"!DETAIL_PICTURE"=>false);
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $goods[]=$arFields['ID'];
}
$goods=array_unique($goods);
foreach($goods as $good){
     $arSelect = Array("ID", "NAME","DETAIL_PAGE_URL","DETAIL_PICTURE","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE");
     //в фильтре указываем что ищем товары которые были добавлены не более часа назад
     $arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y","ID"=>$good);
     $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
     while($ob = $res->GetNextElement())
     {
         $arFields = $ob->GetFields();
        $arProps=$ob->GetProperties();

         
		 $res_offers = CCatalogSKU::getOffersList(
			$arFields['ID'],
			$arFields['IBLOCK_ID'],
			$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
			$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
			$propertyFilter = array()
		);
         $no_sizes=false;
		foreach($res_offers[$arFields['ID']] as $goodd){
			if(strlen($goodd['PROPERTY_RAZMER_VALUE'])==0){
				$no_sizes=true;
			}
			$offer_id=$goodd['ID'];
            $ar_price = GetCatalogProductPrice($goodd["ID"], 1);
            $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                $ar_price["ID"],
                array(2),
                "N",
                SITE_ID
            );
            $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                    $ar_price["PRICE"],
                    $ar_price["CURRENCY"],
                    $arDiscounts
                );
            $new_price=$discountPrice; 
            $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
            $ar_price["PRICE"] = $new_price; 
		} 
        $advanced_goods[$my_index]['LINK']=$arFields['DETAIL_PAGE_URL'];
        $advanced_goods[$my_index]['IMAGE']=$arFields['DETAIL_PICTURE'];
        $advanced_goods[$my_index]['NAME']=$arFields['NAME'];
        $advanced_goods[$my_index]['DESIGNER']=$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];
		$advanced_goods[$my_index]['ARTICLE']=$arFields['PROPERTY_CML2_ARTICLE_VALUE'];
        $advanced_goods[$my_index]['PRICE']=$ar_price['PRICE']; 
        $advanced_goods[$my_index]['DISCOUNT_PRICE']=$ar_price['DISCOUNT_PRICE']; 
     }
     $my_index++;
 }
$goods='<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
		<tbody>
			<tr style="max-width:850px;display:block;margin:auto;text-align:center;vertical-align:top;">';
$counter=0;
foreach($advanced_goods as $good){
	$counter++;
	if($counter%3==0){
		$goods.='</tr><tr style="max-width:850px;display:block;margin:auto;text-align:center;vertical-align:top;">';
	}
                    $goods.='<td style="display: inline-block;width: 250px;padding: 0 10px 25px;">
				<img src="'.CFile::GetPath($good['IMAGE']).'" style="width: 100%;" alt="">
				<table border="0" cellpadding="0" cellspacing="0" style="width:100%;padding: 20px 0;">
					<tbody>
						<tr>
							<td style="text-align:left;font-family:sans-serif;color:#1f1f1f;font-size:16px;">'.$good['NAME'].' '.$good['DESIGNER'].'</td>';
                            if($good['DISCOUNT_PRICE']!=$good['PRICE']){
                                $goods.='<td style="text-align:right;font-family:sans-serif;color:#c75e03;font-size:14px;text-decoration: line-through;">'.number_format($good['DISCOUNT_PRICE'], 0, ',', ' ').' руб.</td>';
                            }
                            $goods.='</tr>
						<tr>
							<td style="text-align:left;font-family:sans-serif;color:#1f1f1f;font-size:14px;">Артикул: '.$good['ARTICLE'].'</td>
							<td style="text-align:right;font-family:sans-serif;color:#1f1f1f;font-size:16px;">'.number_format($good['PRICE'], 0, ',', ' ').' руб.</td>
						</tr>
					</tbody>
				</table>
				<a href="'.$good['LINK'].'?utm_source=email&utm_medium=cpc&utm_campaign=content" style="background: #ff948b;text-decoration: none;color: white;width: 100%;display: block;line-height: 40px;font-family: sans-serif;font-size: 14px;">КУПИТЬ</a>
			</td>';
}
 $goods.='</tr>
	</tbody>
</table>';
echo $goods;
$html="";
while(($subscr_arr = $subscr->Fetch())){  
   $html.=$subscr_arr["EMAIL"]."- распродажа";
    //отправляем письмо
     $arEventFields = array( 
		"NAME"=>$subscr_arr["NAME"],
        "EMAIL" => "alihachev@arkvision.pro", 
        "GOODS" => $goods
    ); 
    if(count($advanced_goods)>0){
        if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",67)): 
            echo "ok<br>"; 
        endif; 
    }
} 

?>