#!/php
<?php
date_default_timezone_set('UTC');
//
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
?>
<?
$arSelect = Array("ID", "NAME", "PROPERTY_access_token","PROPERTY_account");
$arFilter = Array("IBLOCK_ID"=>7, "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
    $arF=$ob->GetFields();
	 $account=$arF['PROPERTY_ACCOUNT_VALUE'];
	 $get_contents = file_get_contents('https://api.instagram.com/v1/users/self/media/recent/?access_token='.$arF['PROPERTY_ACCESS_TOKEN_VALUE']);

	$json_decode = json_decode($get_contents, true);
	$json_decode = $json_decode['data'];

	foreach ($json_decode as $json_get) 
	{
        
		$el = new CIBlockElement;
		$PROP = array();
        $PROP[46]=array();
		foreach($json_get['tags'] as $tagg){
			if (strpos($tagg, "product_") !== false) {
				$PROP[46][] = intval(str_replace("product_","",$tagg));
			} 
		}
		$PROP[52] = $json_get['link'];
		$PROP[56] = $json_get['created_time'];
		$PROP[53] = $account;
        $PROP[54] = $json_get['likes']['count'];
        $PROP[55] = $json_get['comments']['count'];
		$arLoadProductArray = Array(  
		'MODIFIED_BY' => $GLOBALS['USER']->GetID(), // элемент изменен текущим пользователем  
		'IBLOCK_SECTION_ID' => false, // элемент лежит в корне раздела  
		'IBLOCK_ID' => 6, 
		'NAME' => $json_get['id'],  
		'ID' => $json_get['id'], 
		"PREVIEW_PICTURE"=>CFile::MakeFileArray($json_get['images']['standard_resolution']['url']),
		'ACTIVE' => 'Y'
		);
		$i = 0;
       	$IDPHOTO=0;
		echo $json_get['id'];
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_ID"=>6, "NAME"=>$json_get['id']);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			echo "- found";
		   	$IDPHOTO=$arFields['ID'];  $i++;
		}
	   //echo $i;
		if($i==0){
			echo "- add<br>";
			$PRODUCT_ID = $el->Add($arLoadProductArray);
            CIBlockElement::SetPropertyValues($PRODUCT_ID, 6,$json_get['link'], "postlink");
            CIBlockElement::SetPropertyValues($PRODUCT_ID, 6,$json_get['created_time'], "date");
            CIBlockElement::SetPropertyValues($PRODUCT_ID, 6,$account, "account");
            CIBlockElement::SetPropertyValues($PRODUCT_ID, 6,$json_get['likes']['count'], "likes");
            CIBlockElement::SetPropertyValues($PRODUCT_ID, 6,$json_get['comments']['count'], "comments");
            if(count($PROP[46])>0){
                CIBlockElement::SetPropertyValues($PRODUCT_ID, 6,$PROP[46], "good");
            }
            
            }
        else{
			echo "- update<br>";
            $GGG="";
            $arSelect2 = Array("ID", "NAME","ACTIVE","PROPERTY_good");
            $arFilter2 = Array("IBLOCK_ID"=>6,"ID"=>$IDPHOTO);
            $res2 = CIBlockElement::GetList(Array(), $arFilter2, false, Array("nPageSize"=>1), $arSelect2);
            while($ob2 = $res2->GetNextElement())
            {
                $arFields2 = $ob2->GetFields();
                $GGG=$arFields2['PROPERTY_GOOD_VALUE'];
               
            }
           // $el->Update($IDPHOTO, $arLoadProductArray);
            CIBlockElement::SetPropertyValues($IDPHOTO, 6,$json_get['link'], "postlink");
            CIBlockElement::SetPropertyValues($IDPHOTO, 6,$json_get['created_time'], "date");
            CIBlockElement::SetPropertyValues($IDPHOTO, 6,$account, "account");
            CIBlockElement::SetPropertyValues($IDPHOTO, 6,$json_get['likes']['count'], "likes");
            CIBlockElement::SetPropertyValues($IDPHOTO, 6,$json_get['comments']['count'], "comments");
            if(count($PROP[46])>0){
                CIBlockElement::SetPropertyValues($IDPHOTO, 6,$PROP[46], "good");
            }
            //CIBlockElement::SetPropertyValues($IDPHOTO, 30,$GGG, "good");
        }
            
	}
}
?>