#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
//раз в неделю
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
$goods=array();
$advanced_goods=array();
$arSelect = Array();
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y","!DETAIL_PICTURE"=>false);
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
$html='<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
  <channel>
    <title>aprilmoscow.ru</title>
    <link>https://aprilmoscow.ru</link>
    <description>Магазин Апрель</description>';
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $rsResult = CIBlockSection::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$arFields["IBLOCK_SECTION_ID"]), false, array("UF_GOOGLE_ID"));
    while($arResult = $rsResult->GetNext())
    {
        $google_id=$arResult['UF_GOOGLE_ID'];
    }
    $res_offers = CCatalogSKU::getOffersList(
        $arFields['ID'],
        $arFields['IBLOCK_ID'],
        $skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
        $fields = array("PROPERTY_RAZMER","PROPERTY_TSVET","NAME","PRICE"),
        $propertyFilter = array()
    );
    foreach($res_offers[$arFields['ID']] as $good){
        $ar_price = GetCatalogProductPrice($good["ID"], 1);
        $offer_color=$good['PROPERTY_TSVET_VALUE'];
        $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
            $ar_price["ID"],
            $USER->GetUserGroupArray(),
            "N",
            SITE_ID
        );
        $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                $ar_price["PRICE"],
                $ar_price["CURRENCY"],
                $arDiscounts
            );
        $new_price=$discountPrice; 
        if($new_price!=$ar_price["PRICE"]){
            $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
            $ar_price["PRICE"] = $new_price;
        } 
    }
    $nav = CIBlockSection::GetNavChain($arFields["IBLOCK_ID"], $arFields["IBLOCK_SECTION_ID"]);
    $category="";
    while ($arNav=$nav->GetNext()):
        $category.=$arNav["NAME"].'/';
    endwhile;
    $category=trim($category,"/");
    $html.='<item>
    <g:id>'.$arFields['ID'].'</g:id>
    <g:title>'.htmlspecialchars($arFields['NAME']).' '.htmlspecialchars($arProps['CML2_MANUFACTURER']['VALUE']).' '.htmlspecialchars($arProps['CML2_ARTICLE']['VALUE']).'</g:title>
    <g:description>';
    if(strlen($arProps['SOSTAV']['VALUE'])>0){
		$arProps['SOSTAV']['VALUE'] = str_replace("%", "% ", $arProps['SOSTAV']['VALUE']);
		$returnValue = preg_replace('/([а-я])([0-9])/', '$1,$2', $arProps['SOSTAV']['VALUE'] , -1, $count);
		$html.='Состав:'.htmlspecialchars($returnValue).";";
	}
	foreach ($arProps['CML2_ATTRIBUTES']['VALUE'] as $key=>$attribute){
        $html.=htmlspecialchars($attribute).":".htmlspecialchars($arProps['CML2_ATTRIBUTES']['DESCRIPTION'][$key]).";";
     }
    $html.='</g:description>
    <g:google_product_category>'.$google_id.'</g:google_product_category>
    <g:product_type>'.htmlspecialchars($category).'</g:product_type>
    <g:link>https://aprilmoscow.ru'.htmlspecialchars($arFields['DETAIL_PAGE_URL']).'</g:link>
    <g:image_link>https://aprilmoscow.ru'.CFIle::GetPath($arFields['DETAIL_PICTURE']).'</g:image_link>';
    foreach($arProps['MORE_PHOTO']['VALUE'] as $k=>$v)
	{
        $html.='<g:additional_image_link>https://aprilmoscow.ru'.htmlspecialchars(CFIle::GetPath($v)).'</g:additional_image_link>';
    }
    $html.='<g:condition>new</g:condition>
    <g:availability>in stock</g:availability>';
    if($ar_price["PRICE"]!=$ar_price["DISCOUNT_PRICE"]&&strlen($ar_price["DISCOUNT_PRICE"])>0){
        $html.='<g:price>'.$ar_price["DISCOUNT_PRICE"].' RUB</g:price><g:sale_​price>'.$ar_price["PRICE"].' RUB</g:sale_​price>';
    }
    else{
        $html.='<g:price>'.$ar_price["PRICE"].' RUB</g:price>';
    } 
    $html.='<g:brand>'.$arProps['CML2_MANUFACTURER']['VALUE'].'</g:brand>
  </item>';
}
$html.='  </channel>
</rss>';
// Открыть текстовый файл
$f = fopen($_SERVER['DOCUMENT_ROOT']."/upload/googlemerchant.xml", "w");

// Записать строку текста
fwrite($f, $html); 

// Закрыть текстовый файл
fclose($f); 
?>