#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
function basket($uid){
    $goods = array();
    $dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(
                "FUSER_ID" => $uid,
                "CAN_BUY"=>"Y",
                "ORDER_ID" => "NULL"
            ),
        false,
        false,
        array("ID", "CALLBACK_FUNC", "MODULE", 
              "PRODUCT_ID", "QUANTITY", "DELAY", 
              "CAN_BUY", "PRICE", "WEIGHT")
    );
    while ($arItems = $dbBasketItems->Fetch())
    {
        $mxResult = CCatalogSku::GetProductInfo(
            $arItems["PRODUCT_ID"]
       );
       if (is_array($mxResult))
        {
             $product_id=$mxResult['ID'];
        }
        else{
                $product_id=$arItems["PRODUCT_ID"];
        }
        $goods[] = $product_id;
    }
    return $goods;
}
function goods($arFilter,$mail_template){
    $advanced_goods=array();
    $arSelect = Array("ID",'IBLOCK_ID', "NAME","DETAIL_PAGE_URL","DETAIL_PICTURE","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE");
    //в фильтре указываем что ищем товары которые были добавлены не более часа назад
    $res = CIBlockElement::GetList(Array("id"=>"desc"), $arFilter, false, Array(), $arSelect);
    $my_index=0;
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arProps=$ob->GetProperties();
        $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arFields['ID'], 'CATALOG_GROUP_ID' => 1)); 
	    if ($arPrice = $rsPrices->Fetch()) $price = floor($arPrice["PRICE"]);
        $discount_price="";
        $arDiscounts = CCatalogDiscount::GetDiscountByProduct($arFields['ID']);
        if($arDiscounts[0]['VALUE_TYPE']=="P"&&strlen($arDiscounts[0]["COUPON"])==0){
                $discount_price=$price-($arDiscounts[0]['VALUE']/100)*$price;
        }
        if($arDiscounts[0]['VALUE_TYPE']=="F"&&strlen($arDiscounts[0]["COUPON"])==0){
            $discount_price=$price-$arDiscounts[0]['VALUE'];
        }
        if(strlen($discount_price)>0)
           $discount_price=$discount_price;
        else{
            $discount_price=$price;
        }
		$res_offers = CCatalogSKU::getOffersList(
			$arFields['ID'],
			$arFields['IBLOCK_ID'],
			$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
			$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
			$propertyFilter = array()
		);
		$no_sizes=false;
		foreach($res_offers[$arFields['ID']] as $good){
			if(strlen($good['PROPERTY_RAZMER_VALUE'])==0){
				$no_sizes=true;
			}
			$offer_id=$good['ID'];
            $ar_price = GetCatalogProductPrice($good["ID"], 1);
            $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                $ar_price["ID"],
                array(2),
                "N",
                SITE_ID
            );
            $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                    $ar_price["PRICE"],
                    $ar_price["CURRENCY"],
                    $arDiscounts
                );
            $new_price=$discountPrice; 
            $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
            $ar_price["PRICE"] = $new_price; 
		}
        $advanced_goods[$my_index]['LINK']=$arFields['DETAIL_PAGE_URL'];
        $advanced_goods[$my_index]['IMAGE']=$arFields['DETAIL_PICTURE'];
        $advanced_goods[$my_index]['NAME']=$arFields['NAME'];
        $advanced_goods[$my_index]['DESIGNER']=$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];
        $advanced_goods[$my_index]['ARTICLE']=$arFields['PROPERTY_CML2_ARTICLE_VALUE'];
        $advanced_goods[$my_index]['DISCOUNT_PRICE']=$ar_price['DISCOUNT_PRICE']; 
        $advanced_goods[$my_index]['PRICE']=$ar_price['PRICE']; 
        $my_index++;
    }
    $goods='<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
		<tbody>
			<tr style="max-width:850px;display:block;margin:auto;text-align:center;vertical-align:top;">';
	$counter=0;
    foreach($advanced_goods as $good){
		$counter++;
		if($counter%3==0){
			$goods.='</tr><tr style="max-width:850px;display:block;margin:auto;text-align:center;vertical-align:top;">';
		}
                        $goods.='<td style="display: inline-block;width: 250px;padding: 0 10px 25px;">
					<img src="'.CFile::GetPath($good['IMAGE']).'" style="width: 100%;" alt="">
					<table border="0" cellpadding="0" cellspacing="0" style="width:100%;padding: 20px 0;">
						<tbody>
							<tr>
								<td style="text-align:left;font-family:sans-serif;color:#1f1f1f;font-size:16px;">'.$good['NAME'].' '.$good['DESIGNER'].'</td>';
                                if($good['DISCOUNT_PRICE']!=$good['PRICE']){
                                    $goods.='<td style="text-align:right;font-family:sans-serif;color:#c75e03;font-size:14px;text-decoration: line-through;">'.number_format($good['DISCOUNT_PRICE'], 0, ',', ' ').' руб.</td>';
                                }
                                $goods.='<tr>
								<td style="text-align:left;font-family:sans-serif;color:#1f1f1f;font-size:14px;">Артикул: '.$good['ARTICLE'].'</td>
								<td style="text-align:right;font-family:sans-serif;color:#1f1f1f;font-size:16px;">'.number_format($good['PRICE'], 0, ',', ' ').' руб.</td>
							</tr>
						</tbody>
					</table>
                    <a href="'.$good['LINK'].'?utm_source=email&utm_medium=cpc&utm_campaign=content'.$mail_template.'" style="background: #ff948b;text-decoration: none;color: white;width: 100%;display: block;line-height: 40px;font-family: sans-serif;font-size: 14px;">КУПИТЬ</a>
				</td>';
    }
	 $goods.='</tr>
		</tbody>
	</table>';
    return $goods;
}
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
$results = $DB->Query("SELECT * FROM `b_email_marketing_custom`"); 
//проверка существования записи с сегодняшней датой и этим товаром
$html="";
while ($row = $results->Fetch())
{
    /*if($row['email']!=="alihachev@arkvision.pro")
        continue;*/
    $html.=$row['email'].":"; 
    //$row['email']="alihachev@arkvision.pro"; 
    $days_ago=(strtotime("now")-$row['date_of_sub'])/86400;
    $html.="-". $days_ago." дней<br>";
    //при отсутствии заказов
    //письмо 1.1 приходит сразу при подписке, тут будут письма из 1 и 2 цепочки
    if($row['count_orders']==0){
        //для пользователей которые не добавили товар в корзину стимулируем 1 заказ
        if($row['add_to_cart']==0){
            //письмо 1.2 с просмотренными товарами и категориями
            if($days_ago>1&&$days_ago<2&&$row['sended']==0){
                $goods="";
                if(strlen($row['viewed_goods'])>0){
                    $goods=explode(";",$row['viewed_goods']);
                    $goods=array_unique($goods);
                    $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$goods,"ACTIVE"=>"Y"),"_1_2");
                    
                }
                else if(strlen($row['viewed_categories'])>0){
                    $goods=explode(";",$row['viewed_categories']);
                    $goods=array_unique($goods);
                    $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"IBLOCK_SECTION_ID"=>$goods,"ACTIVE"=>"Y"),"_1_2");
                   
                }
                //отправляем письмо, ставим метку, что sended=1
                if(strlen($goods)>0){
                    $html.="письмо 1.2";
                     $arEventFields = array( 
                        "EMAIL" => $row['email'], 
                        "GOODS" => $goods
                    ); 
                    if (CModule::IncludeModule("main")): 
                      if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",53)): 
                          $html.="ok<br>"; 
                       endif; 
                    endif;
                }
            }
            if($days_ago>2&&$days_ago<3&&$row['sended']==0){
                $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y"),"_1_3");
                 $html.="письмо 1.3";
                     $arEventFields = array( 
                        "EMAIL" => $row['email'], 
                        "GOODS" => $goods
                    ); 
                    if (CModule::IncludeModule("main")): 
                      if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",54)): 
                          $html.="ok<br>"; 
                       endif; 
                    endif;
            }
            
        }
        //забытая корзина
        else{
            //тут письмо 2.1
            if($days_ago>1&&$days_ago<2&&$row['sended']==0){
                $goods=basket($row['fuser_id']);
                $count_goods=count($goods);
                $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$goods,"ACTIVE"=>"Y"),"_2_1");
                //отправляем письмо 2.1
                if($count_goods>0){
                    $html.="письмо 2.1";
                     $arEventFields = array( 
                        "EMAIL" => $row['email'], 
                        "GOODS" => $goods
                    ); 
                    if (CModule::IncludeModule("main")): 
                      if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",57)): 
                          $html.="ok<br>"; 
                       endif; 
                    endif;
               }
            }
            //ну или хз смотря как промокод сколько будет действовать но письмо 2.2
            if($days_ago>6&&$days_ago<7&&$row['sended']==0){
                $goods=basket($row['fuser_id']);
                $count_goods=count($goods);
                $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$goods,"ACTIVE"=>"Y"),"_2_2");
                //отправляем письмо 2.2
                if($count_goods>0){
                    $html.="письмо 2.2";
                     $arEventFields = array( 
                        "EMAIL" => $row['email'], 
                        "GOODS" => $goods
                    ); 
                    if (CModule::IncludeModule("main")): 
                      if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",58)): 
                          $html.="ok<br>"; 
                       endif; 
                    endif;
               }
            }
        }
    }
    //стимулирование повторного заказа письма 1* и 2*
    if($row['count_orders']==1&&$row['repeat_enter']==1){
        //для пользователей которые не добавили товар в корзину стимулируем 1 заказ
        if($row['add_to_cart']==0){
            //письмо 1.2 с просмотренными товарами и категориями
            if($days_ago>1&&$days_ago<2&&$row['sended']==0){
                $goods="";
                if(strlen($row['viewed_goods'])>0){
                    $goods=explode(";",$row['viewed_goods']);
                    $goods=array_unique($goods);
                    $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$goods,"ACTIVE"=>"Y"),"_1_2*");
                    
                }
                else if(strlen($row['viewed_categories'])>0){
                    $goods=explode(";",$row['viewed_categories']);
                    $goods=array_unique($goods);
                    $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"IBLOCK_SECTION_ID"=>$goods,"ACTIVE"=>"Y"),"_1_2*");
                   
                }
                //отправляем письмо, ставим метку, что sended=1
                if(strlen($goods)>0){
                    $html.="письмо 1.2*";
                     $arEventFields = array( 
                        "EMAIL" => $row['email'], 
                        "GOODS" => $goods
                    ); 
                    if (CModule::IncludeModule("main")): 
                      /* if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",55)): 
                          //$html.="ok<br>"; 
                       endif; */
                    endif;
                }
            }
            if($days_ago>2&&$days_ago<3&&$row['sended']==0){
                $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y"),"_1_3*");
                 $html.="письмо 1.3*";
                     $arEventFields = array( 
                        "EMAIL" => $row['email'], 
                        "GOODS" => $goods
                    ); 
                    if (CModule::IncludeModule("main")): 
                      /* if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",56)): 
                          //$html.="ok<br>"; 
                       endif; */
                    endif;
            }
            
        }
        //забытая корзина
        else{
            //тут письмо 2.1*
            if($days_ago>1&&$days_ago<2&&$row['sended']==0){
                $goods=basket($row['fuser_id']);
                $count_goods=count($goods);
                $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$goods,"ACTIVE"=>"Y"),"_2_1*");
                //отправляем письмо 2.1*
                if($count_goods>0){
                    $html.="письмо 2.1*";
                     $arEventFields = array( 
                        "EMAIL" => $row['email'], 
                        "GOODS" => $goods
                    ); 
                    if (CModule::IncludeModule("main")): 
                       if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",59)): 
                          $html.="ok<br>"; 
                       endif; 
                    endif;
               }
            }
            //ну или хз смотря как промокод сколько будет действовать но письмо 2.2*
            if($days_ago>6&&$days_ago<7&&$row['sended']==0){
                $goods=basket($row['fuser_id']);
                $count_goods=count($goods);
                $goods=goods(Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$goods,"ACTIVE"=>"Y"),"_2_2*");
                //отправляем письмо 2.2*
                if($count_goods>0){
                    $html.="письмо 2.2*";
                     $arEventFields = array( 
                        "EMAIL" => $row['email'], 
                        "GOODS" => $goods
                    ); 
                    if (CModule::IncludeModule("main")): 
                       if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",60)): 
                          $html.="ok<br>"; 
                       endif; 
                    endif;
               }
            }
        }
    }
}
//считаем кол-во заказов после повторного заказа просим обратную связь
$arFilter_countorders = Array(
     "STATUS_ID"=>"F"
 );
$rsSales_countorders = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter_countorders);
 $countorders=0;
while ($arSales_countorders = $rsSales_countorders->Fetch())
{
   /* $days_ago=(strtotime("now")- strtotime($arSales_countorders['DATE_STATUS']))/86400;
   $rsUser = CUser::GetByID($arSales_countorders['USER_ID']);
    $arUser = $rsUser->Fetch();
    $days_ago=intval($days_ago);
    if($days_ago==1){
        //письмо 4.1
        $html.="письмо 4.1 - ".$arUser['EMAIL'];
        $arEventFields = array( 
            "EMAIL" => $arUser['EMAIL']
        ); 
        if (CModule::IncludeModule("main")): 
          if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",63)): 
              //$html.="ok<br>"; 
           endif;
        endif;
    } */
}
//каждые 90 дней запрашиваем д.р. если не указан, это надо 1 раз в сутки
$cUser = new CUser; 
$sort_by = "ID";
$sort_ord = "ASC";
$arFilter = array( 
   "ACTIVE" => 'Y', 
);
$dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
while ($arUser = $dbUsers->Fetch()) 
{
    $arUser['EMAIL']="alihachev@arkvision.pro";
   $days_ago=(strtotime("now")- strtotime($arUser["DATE_REGISTER"]))/86400;
   $days_ago=intval($days_ago);
    if($days_ago%90==0&&strlen($arUser["PERSONAL_BIRTHDAY"])==0){
        //письмо с напоминанием заполнить ДР
        $html.="<br><br>".$arUser['ID']." - письмо напомнить о др";
        $arEventFields = array( 
            "EMAIL" => $arUser['EMAIL']
        ); 
        if (CModule::IncludeModule("main")): 
           /*if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",66)): 
              //$html.="ok<br>"; 
           endif; */
        endif;
    }
   
}
echo $html;