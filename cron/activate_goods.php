#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
//активные и подтвержденные адреса, подписанные на рубрики
$arSelect = Array("ID","NAME","CATALOG_QUANTITY","QUANTITY");
//в фильтре указываем что ищем товары которые были добавлены не более часа назад
$arFilter = Array("IBLOCK_ID"=>21);

$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    print_r($arFields);
    $el = new CIBlockElement;
    $active="N";
    if($arFields['CATALOG_QUANTITY']>0)
        $active="Y";
    $arLoadProductArray = Array("ACTIVE" => $active);
    $PRODUCT_ID = $arFields['ID'];  // изменяем элемент с кодом (ID) 2
    $res2 = $el->Update($PRODUCT_ID, $arLoadProductArray);
}

?>