#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
/* НОВИНКИ ДЕЛИМ ПО ДИЗАЙНЕРАМ
ПРИМЕР:
<tr>
     <td style="width: 100%;display:block;text-align:center;"><span style="    font-size: 20px;color: #f6244b;margin: 30px 0 20px;display: block;">GRAVETEIGHT</span></td>
</tr>
<tr>
    <td style="text-align: center;">
        <span style="width: 49%;display: inline-block;">
            <img src="img/3.jpg" width="180" border="0" style="display:block;margin: auto;" alt="" align="center">
            <span style="display: block;width: 180px;margin: 10px auto 0;">Платье</span>
            <span style="display: block;width: 180px;margin:auto;"><b>12th street By Cunthia</b></span>
            <span style="display: block;width: 100%;max-width: 180px;margin: 10px auto 0;">
                <a style="display: inline-block;vertical-align: middle;padding: 8px 0;">130 000 р.</a>
                <a href="" style="display: inline-block;vertical-align: middle;float: right;padding: 8px 20px;background: #f6244b;color: white;text-decoration: none;">Купить</a>
            </span>
        </span>
        <span style="width: 49%;display: inline-block;">
            <img src="img/3.jpg" width="180" border="0" style="display:block;margin: auto;" alt="" align="center">
            <span style="display: block;width: 180px;margin: 10px auto 0;">Платье</span>
            <span style="display: block;width: 180px;margin:auto;"><b>12th street By Cunthia</b></span>
            <span style="display: block;width: 100%;max-width: 180px;margin: 10px auto 0;">
                <a style="display: inline-block;vertical-align: middle;padding: 8px 0;">130 000 р.</a>
                <a href="" style="display: inline-block;vertical-align: middle;float: right;padding: 8px 20px;background: #f6244b;color: white;text-decoration: none;">Купить</a>
            </span>
        </span>
    </td>
</tr> 
*/
//раз в неделю выполнять
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
//активные и подтвержденные адреса, подписанные на рубрики
$subscr = CSubscription::GetList(
    array("ID"=>"ASC"),
    array("RUBRIC"=>array(6),  "ACTIVE"=>"Y" )
);
$count=4;
$goods=array();
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y",">DATE_CREATE"=>date("d.m.Y H:i:s",strtotime("-30 days")),"!DETAIL_PICTURE"=>false);
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $goods[]=$arFields['ID'];
}
$goods=array_unique($goods);
$advanced_goods=array();
$my_index=0;
foreach($goods as $good){
    $arSelect = Array("ID", "NAME","DETAIL_PAGE_URL","DETAIL_PICTURE","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE");
    //в фильтре указываем что ищем товары которые были добавлены не более часа назад
   $arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y","ID"=>$good);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(""), $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arProps=$ob->GetProperties();
        $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arFields['ID'], 'CATALOG_GROUP_ID' => 1)); 
        if ($arPrice = $rsPrices->Fetch()) $price = floor($arPrice["PRICE"]);
        $discount_price="";
        $arDiscounts = CCatalogDiscount::GetDiscountByProduct($arFields['ID']);
        if($arDiscounts[0]['VALUE_TYPE']=="P"&&strlen($arDiscounts[0]["COUPON"])==0){
                $discount_price=$price-($arDiscounts[0]['VALUE']/100)*$price;
        }
        if($arDiscounts[0]['VALUE_TYPE']=="F"&&strlen($arDiscounts[0]["COUPON"])==0){
            $discount_price=$price-$arDiscounts[0]['VALUE'];
        }
        if(strlen($discount_price)>0)
           $discount_price=$discount_price;
        else{
            $discount_price=$price;
        }
		$res_offers = CCatalogSKU::getOffersList(
			$arFields['ID'],
			$arFields['IBLOCK_ID'],
			$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
			$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
			$propertyFilter = array()
		);
		$no_sizes=false;
		foreach($res_offers[$arFields['ID']] as $good){
			if(strlen($good['PROPERTY_RAZMER_VALUE'])==0){
				$no_sizes=true;
			}
			$offer_id=$good['ID'];
            $ar_price = GetCatalogProductPrice($good["ID"], 1); 
            $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                $ar_price["ID"],
                array(2),
                "N",
                SITE_ID
            );
            $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                    $ar_price["PRICE"],
                    $ar_price["CURRENCY"],
                    $arDiscounts
                );
            $new_price=$discountPrice; 
            $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
            $ar_price["PRICE"] = $new_price;  
		}
        $advanced_goods[$my_index]['LINK']=$arFields['DETAIL_PAGE_URL'];
        $advanced_goods[$my_index]['IMAGE']=$arFields['DETAIL_PICTURE'];
        $advanced_goods[$my_index]['NAME']=$arFields['NAME'];
        $advanced_goods[$my_index]['DESIGNER']=$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];
		$advanced_goods[$my_index]['ARTICLE']=$arFields['PROPERTY_CML2_ARTICLE_VALUE'];
        $advanced_goods[$my_index]['PRICE']=$ar_price['PRICE']; 
        $advanced_goods[$my_index]['DISCOUNT_PRICE']=$ar_price['DISCOUNT_PRICE']; 
    }
    $my_index++;
}
$goods='<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
		<tbody>
			<tr style="max-width:850px;display:block;margin:auto;text-align:center;vertical-align:top;">';
$counter=0;
foreach($advanced_goods as $good){
	$counter++;
	if($counter%3==0){
		$goods.='</tr><tr style="max-width:850px;display:block;margin:auto;text-align:center;vertical-align:top;">';
	}
                    $goods.='<td style="display: inline-block;width: 250px;padding: 0 10px 25px;">
				<img src="'.CFile::GetPath($good['IMAGE']).'" style="width: 100%;" alt="">
				<table border="0" cellpadding="0" cellspacing="0" style="width:100%;padding: 20px 0;">
					<tbody>
						<tr>
							<td style="text-align:left;font-family:sans-serif;color:#1f1f1f;font-size:16px;">'.$good['NAME'].' '.$good['DESIGNER'].'</td>';
                            if($good['DISCOUNT_PRICE']!=$good['PRICE']){
                                $goods.='<td style="text-align:right;font-family:sans-serif;color:#c75e03;font-size:14px;text-decoration: line-through;">'.number_format($good['DISCOUNT_PRICE'], 0, ',', ' ').' руб.</td>';
                            }
                            $goods.='</tr>
						<tr>
							<td style="text-align:left;font-family:sans-serif;color:#1f1f1f;font-size:14px;">Артикул: '.$good['ARTICLE'].'</td>
							<td style="text-align:right;font-family:sans-serif;color:#1f1f1f;font-size:16px;">'.number_format($good['PRICE'], 0, ',', ' ').' руб.</td>
						</tr>
					</tbody>
				</table>
				<a href="'.$good['LINK'].'?utm_source=email&utm_medium=cpc&utm_campaign=content" style="background: #ff948b;text-decoration: none;color: white;width: 100%;display: block;line-height: 40px;font-family: sans-serif;font-size: 14px;">КУПИТЬ</a>
			</td>';
}
 $goods.='</tr>
	</tbody>
</table>';
$designer="";
$html="";
while(($subscr_arr = $subscr->Fetch())){  
   $html.=$subscr_arr["EMAIL"]."- новинки";
    //отправляем письмо
     $arEventFields = array( 
		 "NAME"=>$subscr_arr["NAME"],
        "EMAIL" => $subscr_arr["EMAIL"], 
        "GOODS" => $goods
    );
    if(count($advanced_goods)>0){
        if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",68)): 
        echo "ok<br>"; 
        endif; 
    }
} 
?>