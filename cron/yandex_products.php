#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
//раз в неделю
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
$goods=array();
$advanced_goods=array();
$html='<?xml version="1.0" encoding="UTF-8"?>
<yml_catalog date="'.date("Y-m-d H:i").'">
    <shop>
    <name>Магазин Апрель</name>
    <company>ООО "АПРЕЛЬ ФЭШН"</company>
    <url>https://aprilmoscow.ru</url>
    <currencies>
      <currency id="RUR" rate="1"/>
    </currencies>
    <categories>';
$arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID);
$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
while ($arSection = $rsSections->Fetch())
{
    $html.='<category id="'.$arSection['ID'].'"';
    if($arSection['IBLOCK_SECTION_ID']>0)
        $html.=' parentId="'.$arSection['IBLOCK_SECTION_ID'].'"';
    $html.='>'.$arSection['NAME'].'</category>';
}
$html.='</categories><offers>';
$arSelect = Array();
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y","!DETAIL_PICTURE"=>false);
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $rsResult = CIBlockSection::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$arFields["IBLOCK_SECTION_ID"]), false, array("UF_GOOGLE_ID"));
    while($arResult = $rsResult->GetNext())
    {
        $google_id=$arResult['UF_GOOGLE_ID'];
    }
    $res_offers = CCatalogSKU::getOffersList(
        $arFields['ID'],
        $arFields['IBLOCK_ID'],
        $skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
        $fields = array("PROPERTY_RAZMER","PROPERTY_TSVET","NAME","PRICE"),
        $propertyFilter = array()
    );
    foreach($res_offers[$arFields['ID']] as $good){
        $ar_price = GetCatalogProductPrice($good["ID"], 1);
        $offer_color=$good['PROPERTY_TSVET_VALUE'];
        $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
            $ar_price["ID"],
            $USER->GetUserGroupArray(),
            "N",
            SITE_ID
        );
        $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                $ar_price["PRICE"],
                $ar_price["CURRENCY"],
                $arDiscounts
            );
        $new_price=$discountPrice; 
        if($new_price!=$ar_price["PRICE"]){
            $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
            $ar_price["PRICE"] = $new_price;
        } 
    }
    $nav = CIBlockSection::GetNavChain($arFields["IBLOCK_ID"], $arFields["IBLOCK_SECTION_ID"]);
    $category="";
    while ($arNav=$nav->GetNext()):
        $category.=$arNav["NAME"].'/';
    endwhile;
    $category=trim($category,"/");
    $html.='<offer id="'.$arFields['ID'].'" available="true">
    
    <name>'.htmlspecialchars($arFields['NAME']).' '.htmlspecialchars($arProps['CML2_MANUFACTURER']['VALUE']).' '.htmlspecialchars($arProps['CML2_ARTICLE']['VALUE']).'</name>
    <description>';
    if(strlen($arProps['SOSTAV']['VALUE'])>0){
		$arProps['SOSTAV']['VALUE'] = str_replace("%", "% ", $arProps['SOSTAV']['VALUE']);
		$returnValue = preg_replace('/([а-я])([0-9])/', '$1,$2', $arProps['SOSTAV']['VALUE'] , -1, $count);
		$html.='Состав:'.htmlspecialchars($returnValue).";";
	}
	foreach ($arProps['CML2_ATTRIBUTES']['VALUE'] as $key=>$attribute){
        $html.=htmlspecialchars($attribute).":".htmlspecialchars($arProps['CML2_ATTRIBUTES']['DESCRIPTION'][$key]).";";
     }
    $html.='</description>
    <categoryId>'.$arFields["IBLOCK_SECTION_ID"].'</categoryId>
    <url>https://aprilmoscow.ru'.htmlspecialchars($arFields['DETAIL_PAGE_URL']).'</url>
    <picture>https://aprilmoscow.ru'.CFIle::GetPath($arFields['DETAIL_PICTURE']).'</picture>';
    foreach($arProps['MORE_PHOTO']['VALUE'] as $k=>$v)
	{
        $html.='<picture>https://aprilmoscow.ru'.htmlspecialchars(CFIle::GetPath($v)).'</picture>';
    }
    if(strlen($arProps['SOSTAV']['VALUE'])>0){
		$arProps['SOSTAV']['VALUE'] = str_replace("%", "% ", $arProps['SOSTAV']['VALUE']);
		$returnValue = preg_replace('/([а-я])([0-9])/', '$1,$2', $arProps['SOSTAV']['VALUE'] , -1, $count);
		$html.='<param name="Состав">'.htmlspecialchars($returnValue).'</param>';
	}
	foreach ($arProps['CML2_ATTRIBUTES']['VALUE'] as $key=>$attribute){
        $html.='<param name="'.htmlspecialchars($attribute).'">'.htmlspecialchars($arProps['CML2_ATTRIBUTES']['DESCRIPTION'][$key]).'</param>';
     }
    if($ar_price["PRICE"]!=$ar_price["DISCOUNT_PRICE"]&&strlen($ar_price["DISCOUNT_PRICE"])>0){
        $html.='<price>'.$ar_price["DISCOUNT_PRICE"].' RUB</price><oldprice>'.$ar_price["PRICE"].' RUB</oldprice>';
    }
    else{
        $html.='<price>'.$ar_price["PRICE"].' RUB</price>';
    } 
    $html.='<currencyId>RUR</currencyId>';
    $html.='<vendor>'.$arProps['CML2_MANUFACTURER']['VALUE'].'</vendor><vendorCode>'.$arProps['CML2_ARTICLE']['VALUE'].'</vendorCode><pickup>true</pickup>
  </offer>';
}
$html.='</offers>
</shop>
</yml_catalog>';
// Открыть текстовый файл
$f = fopen($_SERVER['DOCUMENT_ROOT']."/upload/yandexmarket.xml", "w");

// Записать строку текста
fwrite($f, $html); 

// Закрыть текстовый файл
fclose($f); 
?>