<?php
date_default_timezone_set('UTC');
//
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");
?>
<?
$arSelect = Array("ID", "NAME","PROPERTY_TAGS","PROPERTY_CML2_ARTICLE","IBLOCK_ID","PROPERTY_CML2_MANUFACTURER");
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, Array(), $arSelect);
$csv_array=array();
$ids=array();
$articles=array();
while($ob = $res->GetNextElement())
{ 
    $arFields = $ob->GetFields();
    
     if(isset($arFields['PROPERTY_TAGS_VALUE'])){
        $arSelect_b = Array("ID", "NAME");
        $arFilter_b = Array("IBLOCK_ID"=>22,"ID"=>$arFields['PROPERTY_TAGS_VALUE']);
        $res_b = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter_b, false, Array(), $arSelect_b);
        $tags="";
        while($ob_b = $res_b->GetNextElement())
        { 
            $arFields_b = $ob_b->GetFields();
            $tags.="'".$arFields_b['NAME']."' ";
        }
        $name=$arFields['NAME'];
        $article=$arFields['PROPERTY_CML2_ARTICLE_VALUE'];
        if(strlen($tags)>0){
            $csv_array[$arFields['ID']]['ID']=$arFields['ID'];
            $csv_array[$arFields['ID']]['NAME']=$name;
            $csv_array[$arFields['ID']]['BRAND']=$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];
            $csv_array[$arFields['ID']]['ARTICLE']=$article;
            $csv_array[$arFields['ID']]['TAGS'].=$tags; 
        }
            
    }
}
$handle = fopen("goods.csv", "w"); 
fputcsv($handle, explode(";", "ID;Название;Артикул;Тэги;"), ";");
foreach ($csv_array as $value) { //Проходим массив
    $value=$value['ID']." - ".$value['NAME']." ".$value['BRAND']." - ".$value['ARTICLE']." - ".$value['TAGS'].";"; 
    echo $value."<br>";
    //Записываем, 3-ий параметр - разделитель поля
    //fputcsv($handle, explode(";", $value), ";"); 
}
fclose($handle); //Закрываем