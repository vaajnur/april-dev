#!/php
<?php
date_default_timezone_set('UTC');
//
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
?>
<?php
$arFilter = array('IBLOCK_ID' => $arParentSection['CATALOG_IBLOCK_ID']); // выберет потомков без учета активности
$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
while ($arSect = $rsSect->GetNext())
{
    $bs = new CIBlockSection;

    $arSelect = Array("ID", "NAME");
    $arFields = Array(
        "ACTIVE" => "N",
     );
     $res = $bs->Update($arSect['ID'], $arFields);
     $arFilter = Array("SECTION_ID"=>$arSect['ID'],"ACTIVE"=>"Y","INCLUDE_SUBSECTIONS" => "Y","!DETAIL_PICTURE"=>false);
     $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
     $activeElements=0;
     while($ob = $res->GetNextElement())
     {
         $arFields_tag=$ob->GetFields();
        $res_offers = CCatalogSKU::getOffersList(
            $arFields_tag['ID'],
            CATALOG_IBLOCK_ID,
            $skuFilter = array("CATALOG_AVAILABLE"=>"Y","ACTIVE"=>"Y"),
            $fields = array("PROPERTY_RAZMER","PROPERTY_TSVET","NAME","PRICE"),
            $propertyFilter = array()
        );
        foreach($res_offers[$arFields_tag['ID']] as $good){
            $bs = new CIBlockSection;
            $arFields = Array(
                "ACTIVE" => "Y",
            );
            $ress = $bs->Update($arSect['ID'], $arFields);
        }
     }
}
?>