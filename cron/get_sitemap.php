<?php
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
$pass = 'autositemappass';

if(
    isset($_GET["key"])
    && $_GET["key"] === $pass
    ||
    isset($argv[1])
    && $argv[1] === $pass
){
   $host = COption::GetOptionString("main", "server_name");  
   $link = 'https://'.$host.'/cron/seo_sitemap_run.php';
   $schet = 0; //просто счетчик для ограничения
   $nextStep = 0;
   $ns = '';
   $id = 1; //ID выгрузки
   
   while($schet <= 15){
      $dataStep = array(
         'lang' => 'ru', 
         'action' => 'sitemap_run',
         'ID' => $id,
         'value' => $nextStep,
         'pid' => $id,
         'NS' => $ns,
         'key' => $pass
      );
            
      $dataStepQuery = http_build_query($dataStep);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$link);
      curl_setopt($ch, CURLOPT_POST, 1);
      echo $dataStepQuery;
      curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStepQuery);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $server_output = curl_exec ($ch);
      curl_close ($ch);
      $contStart = $server_output;
      echo  $contStart;  
      if(preg_match('/runSitemap/', $contStart)){

         //выбираем данные
         preg_match('/runSitemap\(1,\s([0-9]+),/', $contStart, $arNextStep);
         if(intval($arNextStep[1]) > 0){
            $nextStep = intval($arNextStep[1]);
            preg_match('/{(.*)}/', $contStart, $arPregNs);
            $ns = '';
            if(strlen($arPregNs[0]) > 0 && 
               strpos($arPregNs[0], '{') !== false &&
               strpos($arPregNs[0], '}') !== false
               ){
                  $nsJson = str_replace("'", '"', $arPregNs[0]);
                  $ns = json_decode($nsJson, true);
            }
         }
      }else{
         $schet = 15;
      }
      $schet++;
   }
}
?>