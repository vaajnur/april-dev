#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale"); 
?>
<?
CModule::IncludeModule('sale');
function getOrderPropValue($ORDER_ID, $PROPS_ID)
{
	$db_order = CSaleOrder::GetList(
		array(),
		array("ID" => $ORDER_ID)
	);
	while ($arOrder = $db_order->Fetch())
	{
		$db_props = CSaleOrderProps::GetList(
			array("SORT" => "ASC"),
			array(
		"PERSON_TYPE_ID" => $arOrder["PERSON_TYPE_ID"],
		)
		);
		while ($arProps = $db_props->Fetch())
		{
			$db_vals = CSaleOrderPropsValue::GetList(
				array(),
				array(
				"ORDER_ID" => $ORDER_ID,
				"ORDER_PROPS_ID" => $PROPS_ID
			)
			);
			if ($arVals = $db_vals->Fetch())
				return $arVals["VALUE"];
		}
	}
}
$db_orders = CSaleOrder::GetList(
	array(),
	array("STATUS_ID"=>"D")
);
while ($arOrders = $db_orders->Fetch())
{
    
	if(date("d.m.Y")==getOrderPropValue($arOrders['ID'], 11)){
		$rsUser = CUser::GetByID($arOrders['USER_ID']);
		$arUser = $rsUser->Fetch();
		if($arUser["UF_SMS"]){
			$phone = getOrderPropValue($arOrders['ID'], 3); //номер телефона в формате 79169999999
			//Данные для смс
			$rs = CIBlockElement::GetList (Array(),Array("IBLOCK_ID" => 18, "ID"=>	6754),
			false,Array (),$arSelect);
            //для доставки курьером выберем другой шаблон
            if($arOrders['DELIVERY_ID']==2)
                $rs = CIBlockElement::GetList (Array(),Array("IBLOCK_ID" => 18, "ID"=>	6756),
			false,Array (),$arSelect);
			$props_sms=$rs->GetNext();
			$message=$props_sms['PREVIEW_TEXT'];
			$message=str_replace("#ORDER_ID#",$arOrders['ID'],$message);
			$message=str_replace("#NAME#",$arUser["NAME"],$message);
            AddMessage2Log($message);
           
            $sessionId=file_get_contents("https://integrationapi.net/rest/user/sessionid?login=AprilFashion&password=Lempicka369");
            $sessionId=trim($sessionId,'"');
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://integrationapi.net/rest/Sms/Send");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,"SessionId=".$sessionId."&DestinationAddress=".$phone."&Data=".$message."&Validity=0&SourceAddress=APRIL");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  
			$server_output = curl_exec ($ch);
			curl_close ($ch);
            
		}
	} 
}

?>
