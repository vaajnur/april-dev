#!/php
<?
date_default_timezone_set('UTC');
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
//раз в день
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
//активные и подтвержденные адреса, подписанные на рубрики
$subscr = CSubscription::GetList(
    array("ID"=>"ASC"),
    array("ACTIVE"=>"Y","FORMAT"=>$post_arr["SUBSCR_FORMAT"])
);
while(($subscr_arr = $subscr->Fetch())){
    if(date("d.m.Y")==explode(" ",$subscr_arr['DATE_CONFIRM'])[0]){
        $arEvFields=array(
            "EMAIL" => $subscr_arr["EMAIL"],
        ); 
        $ev = new CEvent;
        $ev->Send("EMAIL_MARKETING_CUSTOM","s1",$arEvFields,"Y",74);
    }
}
?>