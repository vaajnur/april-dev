<?php
date_default_timezone_set('UTC');
//
$root = $_SERVER['DOCUMENT_ROOT'] = realpath(dirname(__FILE__) . '/../');
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('SITE_ID', 's1');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");
?>
<?
$arSelect = Array("ID", "NAME","PROPERTY_TAGS","PROPERTY_CML2_ARTICLE","IBLOCK_ID","PROPERTY_CML2_MANUFACTURER");
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID);
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, Array(), $arSelect);
$csv_array=array();
$ids=array();
$articles=array();
while($ob = $res->GetNextElement())
{ 
    $arFields = $ob->GetFields();
    $name=$arFields['NAME'];
    $article=$arFields['PROPERTY_CML2_ARTICLE_VALUE'];
    $csv_array[$arFields['ID']]['ID']=$arFields['ID'];
    $csv_array[$arFields['ID']]['NAME']=$name;
    $csv_array[$arFields['ID']]['BRAND']=$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];
    $csv_array[$arFields['ID']]['ARTICLE']=$article;
    if(strpos($arFields['PROPERTY_CML2_MANUFACTURER_VALUE'],"Joseph")!==false){
        // Выберем все скидки для данного товара
        $dbProductDiscounts = CCatalogDiscount::GetList(
            array("SORT" => "ASC"),
            array(
                    "+PRODUCT_ID" => $arFields['ID'],
                ),
            false,
            false,
            array(
                    "ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO", 
                    "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE", 
            "VALUE", "CURRENCY", "PRODUCT_ID"
                )
            );
        while ($arProductDiscounts = $dbProductDiscounts->Fetch())
        {
            $ELEMENT_ID = $arFields['ID'];  // код элемента
            $PROPERTY_CODE = "RAZMER_SKIDKI";  // код свойства
            $PROPERTY_VALUE = "";  // значение свойства
            // Установим новое значение для данного свойства данного элемента
            CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
           //CCatalogDiscount::Delete($arProductDiscounts['ID']);
        }
        echo $arFields['ID']."<br>";
    }
}