<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
$APPLICATION->SetTitle("");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
CModule::IncludeModule("iblock");
CModule::IncludeModule("main");
CModule::IncludeModule("sale");
?><?
global $USER;
//проверяем является ли заход на сайт повторным после заказа

if(isset($_COOKIE['BITRIX_SM_USER_EMAIL'])){
    $rsUsers = CUser::GetList($by="", $order="", array('=EMAIL' => $_COOKIE['BITRIX_SM_USER_EMAIL']));
    while ($rsUser = $rsUsers->Fetch()) 
    {
        $uid=$rsUser["ID"] . "<br />";
    }                          
    $arFilter_countorders = Array(
      "USER_EMAIL" => $uid,
      "STATUS_ID"=>"F"
      );
     $rsSales_countorders = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter_countorders);
      $countorders=0;
     while ($arSales_countorders = $rsSales_countorders->Fetch())
     {
        $countorders++;
     }
    if($countorders==1){
        $arAr = Array(
            "repeat_enter"=>"1"
         );
        $DB->Update("b_email_marketing_custom", $arAr, "WHERE `email`='".$_COOKIE['BITRIX_SM_USER_EMAIL']."'", $err_mess.__LINE__);
    }
}
if($_GET['logout']=="Y")
	$USER->Logout();
//достаем из инфоблока контактные данные чтобы использовать в шаблоне
$arSelect = Array("ID", "NAME","PROPERTY_PHONE","PROPERTY_PHONE_2","PROPERTY_EMAIL","PROPERTY_ADDRESS","PROPERTY_TIME","PROPERTY_INN","PROPERTY_OGRN","PROPERTY_INSTALINK","PROPERTY_FACEBOOKLINK","PROPERTY_PHONESMS","PROPERTY_PHONE_ADMIN","PROPERTY_SMS_ON");
$arFilter = Array("IBLOCK_ID"=>8, "ACTIVE"=>"Y","ID"=>"3278");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>4), $arSelect);
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$GLOBALS['contacts']=$arFields;
	$GLOBALS['contacts']['PROPERTY_PHONE_VALUE_TEL']=str_replace("-","",str_replace(" ","",$GLOBALS['contacts']['PROPERTY_PHONE_VALUE']));
	$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE_TEL']=str_replace("-","",str_replace(" ","",$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE']));
}
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-KMS3K28');</script>
	<!-- End Google Tag Manager -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<?$APPLICATION->ShowHead();?>
	<link rel="icon" type="image/png" href="/img/icons/favicon.png" />

    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/reset.css" rel="stylesheet">
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/slick-theme.css" rel="stylesheet">
    <link href="/css/flexboxgrid.min.css" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">
    <link href="/css/lightslider.min.css" rel="stylesheet">
    <link href="/css/zoomove.min.css" rel="stylesheet">
    <link href="/css/owl.carousel.min.css" rel="stylesheet">
    <link href="/css/simple-scrollbar.css" rel="stylesheet">
    <link href="/css/selectric.css" rel="stylesheet">
    <link href="/css/tippy.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/mobile.css" rel="stylesheet">
    <link href="/css/jquery.fancybox.css" rel="stylesheet">
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

   	<script src="https://www.google.com/recaptcha/api.js"  async defer></script>
    <!--[if !IE]><!-->
    <script>
        if (false) {
            document.documentElement.className += ' ie10';
        }
    </script>
    <!--<![endif]-->
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1401429223415363'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=1401429223415363&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMS3K28"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="panel"><?$APPLICATION->ShowPanel();?></div>

<div class="pusher">

        <div class="mobile-menu">
            <!-- <div class="open-hamburger">
                <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/menu.svg"); ?>
            </div> -->

            <div class="menu-button js-menu-button">
                <div class="menu-button__holder">
                  <div class="menu-button__line menu-button__line_top"></div>
                  <div class="menu-button__line menu-button__line_middle"></div>
                  <div class="menu-button__line menu-button__line_bottom"></div>
                </div>
            </div>

            <div class="text-center logotype">
                <a href="/">
                    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/apr-logo.svg"); ?>
                </a>
            </div>

            <div>
                <p>Звоните: <a href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE_TEL'];?>" class="dashed"><?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE'];?></a>
                <!-- <a href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE_TEL'];?>" class="dashed"><?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE'];?></a> -->
                </p>
                <div class="header-options row middle-xs end-xs">
                    <? if($USER->IsAuthorized()){?>
                    <?
                    $arSelect = Array();
                    $arFilter = Array("IBLOCK_ID"=>12,"CREATED_BY"=>$USER->GetID());
                    $res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
                    while($ob = $res->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        $arProps=$ob->GetProperties();
                        if($arProps['GOOD']['VALUE']>0){
                            $rsItems = CIBlockElement::GetList(array(),array('IBLOCK_ID' =>CATALOG_IBLOCK_ID,'ID' => $arProps['GOOD']['VALUE']),false,false,array('ID'));
                            if ($arItem = $rsItems->GetNext())
                            {
                                // есть такой элемент
                                //ищем торговые предложения
                                $res_offers = CCatalogSKU::getOffersList(
                                    $arItem['ID'],
                                    CATALOG_IBLOCK_ID,
                                    $skuFilter = array("CATALOG_AVAILABLE"=>"Y","ACTIVE"=>"Y"),
                                    $fields = array("PROPERTY_RAZMER","CATALOG_QUANTITY","PROPERTY_TSVET","NAME","PRICE"),
                                    $propertyFilter = array()
                                );
                                $no_sizesw=true;
                                $cnt_offers=0;
                                foreach($res_offers[$arItem['ID']] as $good){
                                    $cnt_offers++;
                                    if(strlen($good['PROPERTY_RAZMER_VALUE'])>0&&$good['CATALOG_QUANTITY']>0){
                                        $no_sizesw=false;
                                    }
                                }
                                if( $no_sizesw===false)
                                    $ids_wishes[]=$arProps['GOOD']['VALUE'];
                            }
                            
                        }
                    }
                    $ids_wishes=array_unique($ids_wishes);
                    ?>
                    <div class="header-option no-margin-mob">
                        <a href="/personal/wishlist/"></a>
                        <span><?=count($ids_wishes);?></span>
                        <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/star.svg"); ?>
                    </div>
                    <?}?>
                    <div style='display: flex; align-items: center;'>
                        <div class="search-field open-search">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="594" height="612" viewBox="0 0 594 612">
                              <path data-name="Forma 1" d="M412.533,399.505c39.134-42.355,63.549-99.068,63.549-161.525C476.082,106.965,369.448,0,238.041,0S0,106.965,0,237.98,106.634,475.96,238.041,475.96a236.9,236.9,0,0,0,142.537-47.38l177,176.959A21.271,21.271,0,0,0,572.662,612a20.668,20.668,0,0,0,15.079-6.461c8.259-8.256,8.259-21.895,0-30.51ZM43.085,237.98c0-107.325,87.245-194.907,194.956-194.907C345.393,43.073,433,130.3,433,237.98S345.752,432.887,238.041,432.887,43.085,345.305,43.085,237.98Z"></path>
                            </svg>
                            </span>
                            <input type="text">
                            <ul class="results" style=""></ul>
                        </div>

                        <div class="header-option 1">
                            <a href="/basket/"></a>
                            <span><?=CSaleBasket::GetList(false, ["FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL"], [], false, ['ID']);?></span>
                            <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/cart.svg"); ?>
                        </div>

                    </div>
                    
                </div>
            </div>
            
        </div>


        <!-- <div class="mobile-menu">
            <div class="text-center logotype">
                <a href="/">
                    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/apr-logo.svg"); ?>
                </a>
            </div>
            <p>Звоните: <a href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE_TEL'];?>" class="dashed"><?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE'];?></a> -->

            <!-- <a href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE_TEL'];?>" class="dashed"><?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE'];?></a> -->

            <!-- </p>
            <div class="header-options row middle-xs end-xs">
                <? if($USER->IsAuthorized()){?>
                <?
                $arSelect = Array();
                $arFilter = Array("IBLOCK_ID"=>12,"CREATED_BY"=>$USER->GetID());
                $res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
                while($ob = $res->GetNextElement())
                {
                    $arFields = $ob->GetFields();
                    $arProps=$ob->GetProperties();
                    if($arProps['GOOD']['VALUE']>0){
                        $rsItems = CIBlockElement::GetList(array(),array('IBLOCK_ID' =>CATALOG_IBLOCK_ID,'ID' => $arProps['GOOD']['VALUE']),false,false,array('ID'));
                        if ($arItem = $rsItems->GetNext())
                        {
                            // есть такой элемент
                            //ищем торговые предложения
                            $res_offers = CCatalogSKU::getOffersList(
                                $arItem['ID'],
                                CATALOG_IBLOCK_ID,
                                $skuFilter = array("CATALOG_AVAILABLE"=>"Y","ACTIVE"=>"Y"),
                                $fields = array("PROPERTY_RAZMER","CATALOG_QUANTITY","PROPERTY_TSVET","NAME","PRICE"),
                                $propertyFilter = array()
                            );
                            $no_sizesw=true;
                            $cnt_offers=0;
                            foreach($res_offers[$arItem['ID']] as $good){
                                $cnt_offers++;
                                if(strlen($good['PROPERTY_RAZMER_VALUE'])>0&&$good['CATALOG_QUANTITY']>0){
                                    $no_sizesw=false;
                                }
                            }
                            if( $no_sizesw===false)
                                $ids_wishes[]=$arProps['GOOD']['VALUE'];
                        }
                        
                    }
                }
                $ids_wishes=array_unique($ids_wishes);
                ?>
                <div class="header-option no-margin-mob">
                    <a href="/personal/wishlist/"></a>
                    <span><?=count($ids_wishes);?></span>
                    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/star.svg"); ?>
                </div>
                <?}?>
                <div class="header-option">
                    <a href="/basket/"></a>
                    <span><?=CSaleBasket::GetList(false, ["FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL"], [], false, ['ID']);?></span>
                    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/cart.svg"); ?>
                </div>
                <div class="open-hamburger">
                    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/menu.svg"); ?>
                </div>
            </div>
        </div> -->


        <div class="hamburger-menu">
            <div class="hamburger-menu-header">
                <div class="logotype">
                    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/apr-logo.svg"); ?>
                </div>
                <!-- <div class="sign-in">
                   <? if (!$USER->IsAuthorized()){?>
                    <a href="javascript:void(0);" class="link-enter">Вход</a>
                    <i>|</i>
                    <a href="javascript:void(0);" class="link-register">Регистрация</a>
                    <?} else{?>
                    <a class="" href="/personal/profile/">Личный кабинет</a>
	                <i>|</i>
	                <a href="/?logout=Y" class="open-exit-popup">Выйти</a>
                    <?}?>
                </div> -->
                <div class="close-icon close-hamburger">
                    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/close.svg"); ?>
                </div>
            </div>
            <div class="hamburger-menu-body">
                <form action="" class="">
                    <div class="search-field">
                        <span><?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/search.svg"); ?></span>
                        <input type="text">
                        
                    </div>
                </form>
                <!--<ul class="mm-parent">
                    <p>Главное меню</p>
                    <li><a class="current">Новинки</a></li>
                    <li>
                        <span><a>Женское</a><?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/arrow-mobile.svg"); ?></span>
                        <ul class="zhenskoe">
                            <p>
                                <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/arrow-mobile.svg"); ?>Женское</p>
                            <li>
                                <span><a>Одежда</a><?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/arrow-mobile.svg"); ?></span>
                                <ul>
                                    <p>
                                        <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/arrow-mobile.svg"); ?>Одежда</p>
                                    <li><a href="">Болеро</a></li>
                                    <li><a href="">Болеро</a></li>
                                    <li><a class="mobile-see-all">Смотреть все</a></li>
                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li><a>Обувь</a></li>
                    <li><a>SALE</a></li>
                    <li><a>Аксессуары</a></li>
                    <li><a>Дизайнеры</a></li>
                    <li><a>InstaShop</a></li>
                    <li><a>April Style</a></li>
                    <li><a>Доставка и оплата</a></li>
                    <li><a>Контакты</a></li>
                </ul>-->
                 <? $APPLICATION->IncludeComponent("april:menu","links_mobile",Array(
						"ROOT_MENU_TYPE" => "main1",
						"HEAD_TEXT" => "Y",
						"MAX_LEVEL" => "3",
						"CHILD_MENU_TYPE" => "main1",
						"USE_EXT" => "Y",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "Y",
						"MENU_CACHE_TYPE" => "Y",
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => ""
						)
				);?>
                <?$APPLICATION->IncludeComponent("april:menu","links_mobile",Array(
					"ROOT_MENU_TYPE" => "mainsections_mobile",
					"MAX_LEVEL" => "3",
					"CHILD_MENU_TYPE" => "mainsections_mobile",
					"USE_EXT" => "Y",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "Y",
					"MENU_CACHE_TYPE" => "Y",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => ""
					)
				);?>
                <?$APPLICATION->IncludeComponent("april:menu","links_mobile",Array(
						"ROOT_MENU_TYPE" => "main2",
						"MAX_LEVEL" => "3",
						"CHILD_MENU_TYPE" => "main2",
						"USE_EXT" => "Y",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "Y",
						"MENU_CACHE_TYPE" => "Y",
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => ""
						)
				); ?>
            </div>
            <p class="hamburgers-phones">Звоните: 
                <a href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE_TEL'];?>"><?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE'];?></a>
                <!-- <a href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE_TEL'];?>"><?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE'];?></a></p> -->
        </div>

        <div class="content">
            <div class="footer-liner">
                <div class='container-fluid relative desktop-menu no-padding new-header'>
                    <div class="row between-xs top-xs header-top middle-xs <?=$APPLICATION->getcurpage()!='/'?' standing ':'';?>">
                        <div class="col-sm-5 no-padding">
                            <div class="menu-button js-menu-button">
                                <div class="menu-button__holder">
                                  <div class="menu-button__line menu-button__line_top"></div>
                                  <div class="menu-button__line menu-button__line_middle"></div>
                                  <div class="menu-button__line menu-button__line_bottom"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 text-center logotype">
                            <a href="/">
                                <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/apr-logo.svg"); ?>
                            </a>
                        </div>
                        <div class="col-sm-5 text-right no-padding relative">
                            <div class='row middle-xs end-xs'>
                                <div class=''>

                                    <!-- <div class="sign-in"> -->
                                  <!--<a href="/blog/" class="link-enter">April Style</a> <i>|</i>-->
                                  <!--  <? if(!$USER->IsAuthorized()){?>
                                    <a href="javascript:void(0);" class="link-enter">Вход</a>
                                    <i>|</i>
                                    <a href="javascript:void(0);" class="link-register">Регистрация</a>
                                    <?} else{?>
                                    <a href="/personal/profile/" class="">Личный кабинет</a>
                                    <i>|</i>
                                    <a href="javascript:void(0);" class="link-exit">Выход</a>
                                    <?}?>
                                    </div> -->


                                    <div class='contact-block' style='display: none;'>
                                        <span>
                                            <svg width="14" height="15" viewBox="0 0 14 15" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M11.0703 8.68472C10.7833 8.38585 10.4371 8.22606 10.0701 8.22606C9.70618 8.22606 9.357 8.38289 9.05814 8.68176L8.12306 9.61388C8.04612 9.57245 7.96919 9.53398 7.89521 9.49551C7.78868 9.44225 7.68807 9.39194 7.60226 9.33868C6.72637 8.78237 5.93037 8.05739 5.16692 7.11935C4.79704 6.65182 4.54847 6.25826 4.36797 5.85878C4.61061 5.63684 4.8355 5.40603 5.05448 5.1841C5.13733 5.10125 5.22019 5.01543 5.30304 4.93258C5.92445 4.31117 5.92445 3.50629 5.30304 2.88488L4.49521 2.07704C4.40348 1.98531 4.30878 1.89062 4.22001 1.79593C4.04247 1.61246 3.85604 1.42308 3.6637 1.24553C3.37667 0.961461 3.03341 0.810547 2.6724 0.810547C2.31139 0.810547 1.96222 0.961461 1.66631 1.24553C1.66335 1.24849 1.66335 1.24849 1.66039 1.25145L0.6543 2.26643C0.275536 2.64519 0.0595218 3.10681 0.0121762 3.64241C-0.0588421 4.50647 0.19564 5.31134 0.390941 5.83806C0.870314 7.13119 1.58642 8.32963 2.65465 9.61388C3.95073 11.1615 5.51018 12.3836 7.29155 13.2447C7.97215 13.5672 8.88059 13.949 9.89556 14.0141C9.9577 14.017 10.0228 14.02 10.082 14.02C10.7655 14.02 11.3396 13.7744 11.7894 13.2861C11.7923 13.2802 11.7983 13.2772 11.8012 13.2713C11.9551 13.0849 12.1326 12.9162 12.3191 12.7357C12.4463 12.6144 12.5765 12.4872 12.7037 12.354C12.9967 12.0492 13.1506 11.6941 13.1506 11.3302C13.1506 10.9632 12.9937 10.6111 12.6949 10.3152L11.0703 8.68472ZM12.1297 11.8007C12.1267 11.8007 12.1267 11.8036 12.1297 11.8007C12.0143 11.9249 11.8959 12.0374 11.7687 12.1617C11.5763 12.3451 11.381 12.5375 11.1976 12.7535C10.8987 13.0731 10.5466 13.224 10.0849 13.224C10.0406 13.224 9.99321 13.224 9.94882 13.221C9.06997 13.1648 8.25326 12.8215 7.64073 12.5286C5.96588 11.7178 4.49521 10.5667 3.2731 9.10787C2.26405 7.89168 1.58938 6.76722 1.14255 5.55991C0.867355 4.82309 0.766746 4.24902 0.811132 3.70751C0.840723 3.36129 0.973883 3.07426 1.21949 2.82866L2.22854 1.8196C2.37354 1.68348 2.52741 1.6095 2.67832 1.6095C2.86475 1.6095 3.01566 1.72195 3.11035 1.81664C3.11331 1.8196 3.11627 1.82256 3.11923 1.82552C3.29973 1.99419 3.47136 2.16878 3.65187 2.3552C3.7436 2.44989 3.83829 2.54458 3.93298 2.64223L4.74081 3.45007C5.05448 3.76373 5.05448 4.05372 4.74081 4.36739C4.655 4.4532 4.57214 4.53902 4.48633 4.62187C4.23777 4.87635 4.00104 5.11308 3.7436 5.34389C3.73768 5.34981 3.73176 5.35277 3.7288 5.35869C3.47432 5.61317 3.52167 5.86174 3.57493 6.0304C3.57789 6.03928 3.58085 6.04816 3.58381 6.05704C3.7939 6.566 4.08981 7.04538 4.53959 7.61648L4.54255 7.61944C5.35926 8.62554 6.22036 9.4097 7.17023 10.0104C7.29155 10.0873 7.41584 10.1495 7.5342 10.2087C7.64073 10.2619 7.74134 10.3122 7.82715 10.3655C7.83899 10.3714 7.85082 10.3803 7.86266 10.3862C7.96327 10.4365 8.05796 10.4602 8.15561 10.4602C8.40122 10.4602 8.55509 10.3063 8.60539 10.256L9.6174 9.24399C9.71801 9.14338 9.87781 9.02206 10.0642 9.02206C10.2477 9.02206 10.3986 9.13746 10.4903 9.23807C10.4933 9.24103 10.4933 9.24103 10.4963 9.24399L12.1267 10.8745C12.4315 11.1763 12.4315 11.487 12.1297 11.8007Z" />
                                        <path d="M7.56677 3.33544C8.34205 3.46564 9.04632 3.83256 9.60855 4.39479C10.1708 4.95702 10.5348 5.66129 10.6679 6.43657C10.7005 6.63187 10.8691 6.76799 11.0615 6.76799C11.0851 6.76799 11.1059 6.76503 11.1295 6.76207C11.3485 6.72656 11.4935 6.51943 11.458 6.30045C11.2982 5.36242 10.8543 4.50724 10.1767 3.82961C9.49906 3.15197 8.64388 2.70811 7.70585 2.54832C7.48687 2.51281 7.28269 2.6578 7.24423 2.87382C7.20576 3.08983 7.34779 3.29993 7.56677 3.33544Z" />
                                        <path d="M13.9939 6.18468C13.7306 4.64004 13.0026 3.23447 11.8841 2.11593C10.7655 0.997388 9.35997 0.26945 7.81532 0.00609026C7.59931 -0.032378 7.39513 0.115577 7.35666 0.331591C7.32115 0.550564 7.46615 0.754742 7.68512 0.79321C9.06406 1.02698 10.3217 1.68094 11.3219 2.67815C12.322 3.67833 12.973 4.93595 13.2068 6.31489C13.2393 6.51019 13.408 6.6463 13.6004 6.6463C13.624 6.6463 13.6447 6.64334 13.6684 6.64039C13.8844 6.60784 14.0324 6.4007 13.9939 6.18468Z" />
                                    </svg>
                                        </span>

                                        <span>
                                            <a class="fixedmob" href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE_TEL'];?>" class="h3">
                                                <?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE'];?>
                                            </a>
                                        </span>
                                    </div>
                                    
                                    <!-- <br><br>
                                    <a href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE_TEL'];?>" class="h3"><?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE'];?></a> -->
                                    <!-- <br> -->
                                    <p class="header-border"></p>
                                    <div class='contact-block'>
                                        <span>
                                            <svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0)">
                                                <path d="M12.0114 1.98851C10.7291 0.706131 9.02445 0 7.21079 0C7.21068 0 7.21025 0 7.21015 0C6.31314 0.000106812 5.439 0.173782 4.61217 0.516327C3.78523 0.858871 3.04439 1.35416 2.41004 1.98851C1.12776 3.27078 0.421632 4.97571 0.421632 6.78915C0.421632 7.87062 0.682573 8.94536 1.177 9.90292L0.0358292 13.163C-0.0445998 13.393 0.0123307 13.6429 0.184724 13.8152C0.305315 13.9359 0.464037 14 0.627245 14C0.697206 14 0.767915 13.9883 0.836916 13.9641L4.09702 12.823C5.05458 13.3175 6.12932 13.5784 7.21079 13.5784C9.02423 13.5784 10.7291 12.8722 12.0114 11.5899C13.2937 10.3076 13.9999 8.60271 13.9999 6.78926C13.9999 4.97571 13.2938 3.27078 12.0114 1.98851ZM11.4308 11.0093C10.3036 12.1366 8.80484 12.7572 7.21079 12.7572C6.2356 12.7572 5.26682 12.5161 4.40912 12.0596C4.2536 11.9769 4.06978 11.9627 3.90476 12.0205L0.942873 13.0571L1.97959 10.0952C2.03737 9.92995 2.02306 9.74612 1.94039 9.59071C1.48398 8.73323 1.24269 7.76445 1.24269 6.78915C1.24269 5.1951 1.86348 3.69632 2.99066 2.56914C4.11774 1.44206 5.6163 0.821274 7.21025 0.82106H7.21079C8.80495 0.82106 10.3036 1.44185 11.4308 2.56914C12.5581 3.69632 13.1789 5.19499 13.1789 6.78915C13.1789 8.38332 12.5581 9.8821 11.4308 11.0093Z" />
                                                <path d="M9.73394 7.39565C9.42162 7.08333 8.91341 7.08333 8.60109 7.39565L8.25951 7.73723C7.41132 7.27505 6.72495 6.58868 6.26267 5.74049L6.60425 5.39891C6.91668 5.08659 6.91668 4.57838 6.60425 4.26607L5.68375 3.34557C5.37143 3.03325 4.86322 3.03325 4.55091 3.34557L3.81455 4.08192C3.39275 4.50372 3.37171 5.22887 3.75527 6.12384C4.08831 6.90089 4.69777 7.75496 5.47141 8.52859C6.24504 9.30223 7.09911 9.9117 7.87616 10.2447C8.30138 10.427 8.68814 10.5179 9.02236 10.5179C9.39161 10.5179 9.69666 10.4069 9.91808 10.1855L10.6544 9.44899V9.4491C10.8058 9.29774 10.8891 9.09662 10.8891 8.88268C10.8891 8.66862 10.8058 8.4675 10.6544 8.31625L9.73394 7.39565ZM9.33745 9.60483C9.21708 9.7252 8.84687 9.7674 8.1997 9.49011C7.51632 9.19724 6.75357 8.64951 6.05204 7.94797C5.3505 7.24643 4.80288 6.4838 4.51 5.80042C4.23261 5.15324 4.2748 4.78293 4.39518 4.66255L5.11743 3.94029L6.00953 4.83249L5.58196 5.26016C5.38863 5.45349 5.34195 5.74669 5.46596 5.98979C6.02811 7.09209 6.90792 7.97189 8.01021 8.53404C8.25342 8.65805 8.54662 8.61148 8.73995 8.41805L9.16751 7.99048L10.0597 8.88268L9.33745 9.60483Z" />
                                                </g>
                                                <defs>
                                                <clipPath id="clip0">
                                                <rect width="14" height="14" />
                                                </clipPath>
                                                </defs>
                                                </svg>

                                        </span>

                                        <span>
                                            <a class="fixedmob" href="https://wa.me/79264810639" target="_blank">+7 926 481-06-39</a>
                                        </span>
                                    </div>

                                    <div class='contact-block'>
                                        <span>
                                            <svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M-9.53674e-07 11.4269V2.57391C-9.53674e-07 2.41 0.0630941 2.26094 0.165624 2.14834C0.167176 2.1465 0.168918 2.14476 0.170533 2.14292C0.286271 2.0187 0.450724 1.94043 0.633483 1.94043H13.3665C13.5491 1.94043 13.7133 2.01851 13.829 2.14245C13.8308 2.14447 13.8327 2.14641 13.8345 2.14843C13.9369 2.26104 14 2.41006 14 2.57391V11.4269C14 11.5908 13.9369 11.7399 13.8343 11.8525C13.8329 11.8542 13.8313 11.8558 13.8299 11.8574C13.7141 11.9819 13.5495 12.0603 13.3665 12.0603H0.633483C0.450724 12.0603 0.286271 11.9821 0.170533 11.8578C0.168918 11.856 0.167176 11.8543 0.165624 11.8524C0.0630941 11.7398 -9.53674e-07 11.5908 -9.53674e-07 11.4269ZM10.8947 8.83936C11.0208 8.7182 11.2213 8.72229 11.3425 8.84848L13.3665 10.9564V3.04437L9.56802 7.00038L10.5941 8.06904C10.7153 8.1952 10.7112 8.39573 10.585 8.51688C10.5236 8.57583 10.4446 8.60516 10.3657 8.60516C10.2825 8.60516 10.1993 8.57257 10.1371 8.50779L9.12889 7.45773L7.66722 8.98002C7.49022 9.16437 7.25327 9.26588 7 9.26588C6.74673 9.26588 6.50978 9.16433 6.33278 8.98002L4.87111 7.45773L1.05998 11.4269H12.94L10.8856 9.2872C10.7644 9.16104 10.7685 8.96051 10.8947 8.83936ZM6.78971 8.54127C6.84613 8.60003 6.92081 8.6324 7 8.6324C7.07919 8.6324 7.15384 8.60003 7.21028 8.54127L12.94 2.57391H1.06001L6.78971 8.54127ZM4.43198 7.00038L0.633483 3.04437L0.63342 10.9565L4.43198 7.00038Z"/>
                                                </svg>
                                        </span>

                                        <span>
                                            <a class="fixedmob" href="mailto:love@aprilmoscow.ru" target="_blank">love@aprilmoscow.ru</a>
                                        </span>
                                    </div>
                                    
                            </div>
                            <div class=''>
                                <div class="header-options row middle-xs end-xs">
                                <div class="search-field">
                                    <span><svg xmlns="http://www.w3.org/2000/svg" width="594" height="612" viewBox="0 0 594 612">
                                      <path data-name="Forma 1" d="M412.533,399.505c39.134-42.355,63.549-99.068,63.549-161.525C476.082,106.965,369.448,0,238.041,0S0,106.965,0,237.98,106.634,475.96,238.041,475.96a236.9,236.9,0,0,0,142.537-47.38l177,176.959A21.271,21.271,0,0,0,572.662,612a20.668,20.668,0,0,0,15.079-6.461c8.259-8.256,8.259-21.895,0-30.51ZM43.085,237.98c0-107.325,87.245-194.907,194.956-194.907C345.393,43.073,433,130.3,433,237.98S345.752,432.887,238.041,432.887,43.085,345.305,43.085,237.98Z"></path>
                                    </svg>
                                    </span>
                                                                        <input type="text">
                                                                        <ul class="results" style="display:none;">
                                                                            
                                                                        </ul>
                                                                    </div>
                                    <div class="header-option top-cart">
                                                                        
                                            
                                        <!-- Линк перекрывает иконку, просто записать нужную ссылку -->
                                        <a href="/basket/"></a>
                                        <!-- counter, выводит количество -->
                                                        <span>0</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" enable-background="new 0 0 26 26">
                                        <path d="M 13 0 C 10.79359 0 9.3169988 0.99198407 8.65625 2.28125 C 7.9955012 3.5705159 8 4.9833333 8 6 A 1.0001 1.0001 0 1 0 10 6 C 10 5.0166667 10.073249 3.9294841 10.4375 3.21875 C 10.801751 2.5080159 11.30641 2 13 2 C 14.685043 2 15.193853 2.5216977 15.5625 3.25 C 15.931147 3.9783023 16 5.0675439 16 6 A 1.0001 1.0001 0 1 0 18 6 C 18 5.0324561 18.000103 3.6404477 17.34375 2.34375 C 16.687397 1.0470523 15.214957 0 13 0 z M 4.1875 8 C 3.7875 8 3.4125 8.3125 3.3125 8.8125 L 0.1875 24.6875 C 0.0875 25.3875 0.4 26 1 26 L 25 26 C 25.6 26 25.9125 25.3875 25.8125 24.6875 L 22.6875 8.8125 C 22.5875 8.3125 22.2125 8 21.8125 8 L 4.1875 8 z M 8.5 9 C 9.3 9 10 9.7 10 10.5 C 10 11.3 9.3 12 8.5 12 C 7.7 12 7 11.3 7 10.5 C 7 9.7 7.7 9 8.5 9 z M 17.5 9 C 18.3 9 19 9.7 19 10.5 C 19 11.3 18.3 12 17.5 12 C 16.7 12 16 11.3 16 10.5 C 16 9.7 16.7 9 17.5 9 z" color="#322a35" overflow="visible" enable-background="accumulate" font-family="Bitstream Vera Sans"></path>
                                    </svg>
                                        <div class="drop">
                                            <div class="header">
                                                <div class="price">
                                                    <span>В корзине  0 товаров</span>
                                                    <span>Сумма 0 ₽</span>
                                                </div>
                                                <button onclick="location.replace('/basket/')" class="btn">Перейти в корзину</button>
                                            </div>
                                            <div class="scroll-area">
                                                        </div>
                                        </div>
                                    </div>
                                                                </div>
                            </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- в мобилке весь блок ниже скрывается через css, если можно отловить момент когда у юзера меньше 768px по ширине и грузить нужную шапку из отдельного шаблона - будет круто  -->
                <div class="container-fluid relative desktop-menu no-padding" style='display: none;'>
                    <div class="row between-xs top-xs header-top">
                        <div class="col-sm-5 no-padding">
                            <ul class="top-menu">
                                <?$APPLICATION->IncludeComponent("april:menu","links",Array(
									"ROOT_MENU_TYPE" => "top",
									"MAX_LEVEL" => "3",
									"CHILD_MENU_TYPE" => "top",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "Y",
									"MENU_CACHE_TYPE" => "Y",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => ""
									)
								);?>
                            </ul>
                            <p>Доставка от <span>15000 руб.</span> осуществляется бесплатно.<br>Бесплатный возврат.</p>
                        </div>
                        <div class="col-sm-2 text-center logotype">
                           	<a href="/">
                            	<?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/apr-logo.svg"); ?>
                            </a>
                        </div>
                        <div class="col-sm-5 text-right no-padding relative">
                            <div class="sign-in">
                              <!--<a href="/blog/" class="link-enter">April Style</a> <i>|</i>-->
                               <? if(!$USER->IsAuthorized()){?>
                                <a href="javascript:void(0);" class="link-enter">Вход</a>
                                <i>|</i>
                                <a href="javascript:void(0);" class="link-register">Регистрация</a>
                                <?} else{?>
                                <a href="/personal/profile/" class="">Личный кабинет</a>
                                <i>|</i>
                                <a href="javascript:void(0);" class="link-exit">Выход</a>
                                <?}?>
                            </div>
                            <a class="fixedmob" href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE_TEL'];?>" class="h3"><?=$GLOBALS['contacts']['PROPERTY_PHONE_VALUE'];?></a>
                            <!-- <br><br>
                            <a href="tel:<?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE_TEL'];?>" class="h3"><?=$GLOBALS['contacts']['PROPERTY_PHONE_2_VALUE'];?></a> -->
							<!-- <br> -->
							<p class="header-border"></p>
					<a class="wup h3" href="https://wa.me/79264810639" target="_blank">
						<span class="watsup"></span>
						<span class="watsuptext">+7 926 481-06-39<span>
					</a>
                        </div>
                    </div>
                    <div class="header-bottom">
                        <ul class="main-menu text-center">
                           <?$APPLICATION->IncludeComponent("april:menu","links",Array(
									"ROOT_MENU_TYPE" => "main1",
									"MAX_LEVEL" => "3",
									"CHILD_MENU_TYPE" => "main1",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "Y",
									"MENU_CACHE_TYPE" => "Y",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => ""
									)
							);?>
                            <?$APPLICATION->IncludeComponent(
                                "april:menu", 
                                "links", 
                                array(
                                    "ROOT_MENU_TYPE" => "mainsections",
                                    "MAX_LEVEL" => "2",
                                    "CHILD_MENU_TYPE" => "mainsections",
                                    "USE_EXT" => "Y",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(
                                    ),
                                    "COMPONENT_TEMPLATE" => "links"
                                ),
                                false
                            );?>
                            <?$APPLICATION->IncludeComponent("april:menu","links",Array(
									"ROOT_MENU_TYPE" => "main2",
									"MAX_LEVEL" => "3",
									"CHILD_MENU_TYPE" => "main2",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "Y",
									"MENU_CACHE_TYPE" => "Y",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => ""
									)
							);?>
                        </ul>
                        <div class="header-options row middle-xs end-xs">

                                <div class="search-field">
                                    <span><?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/search.svg"); ?></span>
                                    <input type="text">
                                    <ul class="results" style="display:none;">
                                        
                                    </ul>
                                </div>
                                <? if($USER->IsAuthorized()){?>
                                <div class="wishlister">
                                    <div class="header-option">
                                    <!-- Линк перекрывает иконку, просто записать нужную ссылку -->
                                    <a href="/personal/wishlist/"></a>
                                    <!-- counter, выводит количество -->
                                    
                                    <span><?=count($ids_wishes);?></span>
                                    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/star.svg"); ?>
                                    <? if(count($ids_wishes)>0){?>
                                    <div class="drop">
                                        <div class="header">
                                            <button onclick="location.replace('/personal/wishlist/')" class="btn">Перейти в WishList</button>
                                        </div>
                                        <div class="scroll-area">
                                           <?
                                            foreach($ids_wishes as $id){
                                                $arSelect = Array("PROPERTY_CML2_ARTICLE","DETAIL_PAGE_URL","DETAIL_PICTURE","IBLOCK_SECTION_ID","PROPERTY_color","PROPERTY_CML2_MANUFACTURER","CODE");
                                                $arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ID"=>$id);
                                                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
                                                if($ob = $res->GetNextElement()){
                                                    $ar_res = $ob->GetFields();
                                                    $ar_res_props=$ob->GetProperties();
													$res_offers = CCatalogSKU::getOffersList(
														$ar_res['ID'],
														CATALOG_IBLOCK_ID,
														$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
														$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
														$propertyFilter = array()
													);
													foreach($res_offers[$ar_res['ID']] as $good){
                                                        $ar_price = GetCatalogProductPrice($good["ID"], 1); 
                                                        $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                                                            $ar_price["ID"],
                                                            $USER->GetUserGroupArray(),
                                                            "N",
                                                            SITE_ID
                                                        );
                                                        $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                                                                $ar_price["PRICE"],
                                                                $ar_price["CURRENCY"],
                                                                $arDiscounts
                                                            );
                                                        $new_price=$discountPrice; 
                                                        if($new_price!=$ar_price["PRICE"]){
                                                            $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
                                                            $ar_price["PRICE"] = $new_price;
                                                        }
													}
                                                    ?>
                                                    <?$myImg= CFile::ResizeImageGet($ar_res["DETAIL_PICTURE"], array('width'=>300, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL , true);?>
                                                    <div class="item" product-id="<?=$ar_res['ID'];?>">
                                                        <div onclick="delwish(<?=$ar_res['ID'];?>)" class="delete">
                                                            <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/close.svg"); ?>
                                                        </div>
                                                        <div class="img" style="background-image: url(<?=strlen($myImg['src'])>0?$myImg['src']:"/img/prod-sample.jpg";?>)"></div>
                                                        <span>
                                                            <a href="<?=$ar_res['DETAIL_PAGE_URL'];?>">
                                                                <span><?=explode(" ",$ar_res['NAME'])[0];?></span>
                                                        <span><?=$ar_res_props['CML2_MANUFACTURER']['VALUE'];?></span>
                                                        </a>
                                                        </span>
                                                        <div class="price">
                                                            <? if(strlen($ar_price["DISCOUNT_PRICE"])>0){?><span class="old"><?=number_format($ar_price["DISCOUNT_PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span><?}?>
                        <span><?=number_format($ar_price['PRICE'],0,"."," ");?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
                                                        </div>
                                                    </div>
                                                    <?
                                            }}?>
                                        </div>
                                    </div>
                                    <?}?>
                                    </div>
                                </div>
                                <?}?>
                                <div class="header-option top-cart">
                                <? $APPLICATION->IncludeComponent("april:sale.basket.basket.line", "top_cart_wc", Array(
                                    "PATH_TO_BASKET" => "/personal/basket.php",	// Страница корзины
                                    "PATH_TO_PERSONAL" => "/personal/",	// Персональный раздел
                                    "SHOW_PERSONAL_LINK" => "Y",	// Отображать ссылку на персональный раздел
                                    ),
                                    false
                                );?>
                                </div>
                            </div>
                    </div>
                </div>