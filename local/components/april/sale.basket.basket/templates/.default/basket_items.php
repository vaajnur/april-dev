<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0):
?>
<?
?>
<div class="col-xs-12 col-sm-8 col-lg-9 xs-order-1">
	<p class="h1">Эти вещи скоро будут Ваши:</p>
	<div class="cart-items">
        
        <? foreach ($arResult["GRID"]["ROWS"] as $k => $arItem): 
        $arSelect = Array();
		$mxResult = CCatalogSku::GetProductInfo(
		$arItem["PRODUCT_ID"]
		);
		if (is_array($mxResult))
		{
		 $product_id=$mxResult['ID'];
		}
		else{
			$product_id=$arItem["PRODUCT_ID"];
		}
        //в фильтре указываем что ищем товары которые были добавлены не более часа назад
        $arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$product_id);
        $res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arProps=$ob->GetProperties();
            
        }
        $nav = CIBlockSection::GetNavChain($arFields["IBLOCK_ID"], $arFields["IBLOCK_SECTION_ID"]);
        $category="";
        while ($arNav=$nav->GetNext()):
            $category.=$arNav["NAME"].'/';
        endwhile;
        $category=trim($category,"/");
        ?>
        <div class="item item<?=$arItem['ID'];?>" product-id="<?=$arItem['ID'];?>">
            <?$myImg= CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>300, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL , true);?>
			<div class="cart-img img" style="background-image: url(<?=strlen($myImg['src'])>0?$myImg['src']:"/img/prod-sample.jpg";?>)">
				<!-- <img src="../img/prod-sample.jpg" alt=""> -->
				<div class="status">SALE</div>
                <? if(strlen( $arProps['RAZMER_SKIDKI']['VALUE'])>0){?><div class="status">SALE</div><?}?>
			    <? if(strtotime($arFields['DATE_CREATE'])>=strtotime(date("d.m.Y"))-5184000&&strlen( $arProps['RAZMER_SKIDKI']['VALUE'])==0){?><div class="status">NEW</div><?}?>
				<div class="remove" id-gtm="<?=$arFields['ID'];?>" name-gtm="<?=explode(" ",$arFields['NAME'])[0];?> <?=$arProps['CML2_MANUFACTURER']['VALUE'];?> <?=$arProps['CML2_ARTICLE']['VALUE'];?>" gtm-brand="<?=$arProps['CML2_MANUFACTURER']['VALUE'];?>" price-gtm="<?=number_format($arItem['PRICE'],0,".","");?>" category-gtm="<?=$category;?>" >
					<?php include($_SERVER["DOCUMENT_ROOT"]."/img/icons/close.svg"); ?>
				</div>
			</div>
			<div class="cart-body">
				<div class="cart-name">
					<a href="<?=$arFields['DETAIL_PAGE_URL'];?>">
						<span><?=$arFields['NAME'];?></span>
						<span><?=$arProps['CML2_MANUFACTURER']['VALUE'];?></span>
					</a>
				</div>
				<div class="cart-description">
					<div class="cart-count-size">
							<div class="group">
									<h4 class="cart-subtitle">Размер</h4>	
                                    <?
                                    $res_offers = CCatalogSKU::getOffersList(
                                        $product_id,
                                        CATALOG_IBLOCK_ID,
                                        $skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
                                        $fields = array("PROPERTY_RAZMER","NAME","PRICE"),
                                        $propertyFilter = array()
                                    );
                                    ?>
									<select name="" id="select-size" <? if(count($res_offers[$product_id])==1) echo"disabled";?>>
										<?
										foreach($res_offers[$product_id] as $good){
											?><option <? if($arItem['PROPS'][0]["VALUE"]==$good['PROPERTY_RAZMER_VALUE']) echo "selected";?> value="<?=$good['PROPERTY_RAZMER_VALUE'];?>"><?=$good['PROPERTY_RAZMER_VALUE'];?></option><?
										}
										?>
									</select>
								</div>
								<div class="counter">
									<h4 class="cart-subtitle">Количество</h4>
									<span class="minus">-</span>
									<input type="text" value="<?=$arItem['QUANTITY'];?>">
									<span class="plus">+</span>
								</div>
					</div>
                    <div class="price">
						<h4 class="cart-subtitle">Цена</h4>
						<div>
                        <? if($arItem['PRICE']+$arItem['DISCOUNT_PRICE']>$arItem['PRICE']){?><span class="old"><?=number_format($arItem['PRICE']+$arItem['DISCOUNT_PRICE'], 0, ',', ' ');?> <?php include($_SERVER["DOCUMENT_ROOT"]."/img/icons/rouble.svg"); ?></span><?}
                         else if(strlen($arProps['RAZMER_SKIDKI']['VALUE'])>0){?><span class="old"><?=number_format($arItem['PRICE']+$arItem['PRICE']*$arProps['RAZMER_SKIDKI']['VALUE']/100, 0, ',', ' ');?> <?php include($_SERVER["DOCUMENT_ROOT"]."/img/icons/rouble.svg"); ?></span><?}?>
							<span><?=number_format($arItem['PRICE'], 0, ',', ' ');?> <?php include($_SERVER["DOCUMENT_ROOT"]."/img/icons/rouble.svg"); ?></span>
						</div>
					</div>
					<div class="price">
						<h4 class="cart-subtitle">Стоимость</h4>
						<div>
                                <? if($arItem['PRICE']*$arItem['QUANTITY']+$arItem['DISCOUNT_PRICE']*$arItem['QUANTITY']>$arItem['PRICE']*$arItem['QUANTITY']){?><span class="old"><?=number_format($arItem['PRICE']*$arItem['QUANTITY']+$arItem['DISCOUNT_PRICE']*$arItem['QUANTITY'], 0, ',', ' ');?> <?php include($_SERVER["DOCUMENT_ROOT"]."/img/icons/rouble.svg"); ?></span><?}
                                else if(strlen($arProps['RAZMER_SKIDKI']['VALUE'])>0){?><span class="old"><?=number_format($arItem['PRICE']*$arItem['QUANTITY']+$arItem['PRICE']*$arItem['QUANTITY']*$arProps['RAZMER_SKIDKI']['VALUE']/100, 0, ',', ' ');?> <?php include($_SERVER["DOCUMENT_ROOT"]."/img/icons/rouble.svg"); ?></span><?}?>
							<span><?=number_format($arItem['PRICE']*$arItem['QUANTITY'], 0, ',', ' ');?> <?php include($_SERVER["DOCUMENT_ROOT"]."/img/icons/rouble.svg"); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
        <? endforeach; ?>
	</div>
</div>
<div class="col-xs-12 col-sm-4 col-lg-3 xs-order-2 aside-cart">
    <?$APPLICATION->IncludeComponent(
        "april:sale.order.ajax", 
        "visual", 
        array(
            "ADDITIONAL_PICT_PROP_8" => "-",
            "ALLOW_AUTO_REGISTER" => "Y",
            "ALLOW_NEW_PROFILE" => "Y",
            "ALLOW_USER_PROFILES" => "Y",
            "BASKET_IMAGES_SCALING" => "standard",
            "BASKET_POSITION" => "after",
            "COMPATIBLE_MODE" => "Y",
            "DELIVERIES_PER_PAGE" => "8",
            "DELIVERY_FADE_EXTRA_SERVICES" => "Y",
            "DELIVERY_NO_AJAX" => "Y",
            "DELIVERY_NO_SESSION" => "Y",
            "DELIVERY_TO_PAYSYSTEM" => "d2p",
            "DISABLE_BASKET_REDIRECT" => "N",
            "MESS_DELIVERY_CALC_ERROR_TEXT" => "Вы можете продолжить оформление заказа, а чуть позже менеджер магазина свяжется с вами и уточнит информацию по доставке.",
            "MESS_DELIVERY_CALC_ERROR_TITLE" => "Не удалось рассчитать стоимость доставки.",
            "MESS_FAIL_PRELOAD_TEXT" => "Вы заказывали в нашем интернет-магазине, поэтому мы заполнили все данные автоматически. Обратите внимание на развернутый блок с информацией о заказе. Здесь вы можете внести необходимые изменения или оставить как есть и нажать кнопку \"#ORDER_BUTTON#\".",
            "MESS_SUCCESS_PRELOAD_TEXT" => "Вы заказывали в нашем интернет-магазине, поэтому мы заполнили все данные автоматически. Если все заполнено верно, нажмите кнопку \"#ORDER_BUTTON#\".",
            "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
            "PATH_TO_AUTH" => "/auth/",
            "PATH_TO_BASKET" => "/basket/",
            "PATH_TO_PAYMENT" => "/payment/",
            "PATH_TO_PERSONAL" => "index.php",
            "PAY_FROM_ACCOUNT" => "Y",
            "PAY_SYSTEMS_PER_PAGE" => "8",
            "PICKUPS_PER_PAGE" => "5",
            "PRODUCT_COLUMNS_HIDDEN" => array(
                0 => "PROPERTY_MATERIAL",
            ),
            "PRODUCT_COLUMNS_VISIBLE" => array(
                0 => "PREVIEW_PICTURE",
                1 => "PROPS",
            ),
            "PROPS_FADE_LIST_1" => array(
                0 => "17",
                1 => "19",
            ),
            "SEND_NEW_USER_NOTIFY" => "Y",
            "SERVICES_IMAGES_SCALING" => "standard",
            "SET_TITLE" => "Y",
            "SHOW_BASKET_HEADERS" => "N",
            "SHOW_COUPONS_BASKET" => "Y",
            "SHOW_COUPONS_DELIVERY" => "Y",
            "SHOW_COUPONS_PAY_SYSTEM" => "Y",
            "SHOW_DELIVERY_INFO_NAME" => "Y",
            "SHOW_DELIVERY_LIST_NAMES" => "Y",
            "SHOW_DELIVERY_PARENT_NAMES" => "Y",
            "SHOW_MAP_IN_PROPS" => "N",
            "SHOW_NEAREST_PICKUP" => "N",
            "SHOW_ORDER_BUTTON" => "final_step",
            "SHOW_PAY_SYSTEM_INFO_NAME" => "Y",
            "SHOW_PAY_SYSTEM_LIST_NAMES" => "Y",
            "SHOW_STORES_IMAGES" => "Y",
            "SHOW_TOTAL_ORDER_BUTTON" => "Y",
            "SKIP_USELESS_BLOCK" => "Y",
            "TEMPLATE_LOCATION" => "popup",
            "TEMPLATE_THEME" => "site",
            "USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
            "USE_CUSTOM_ERROR_MESSAGES" => "Y",
            "USE_CUSTOM_MAIN_MESSAGES" => "N",
            "USE_PREPAYMENT" => "N",
            "USE_YM_GOALS" => "N",
            "COMPONENT_TEMPLATE" => "visual",
            "USE_PRELOAD" => "Y",
            "SHOW_PAYMENT_SERVICES_NAMES" => "Y",
            "ADDITIONAL_PICT_PROP_2" => "-",
            "ADDITIONAL_PICT_PROP_3" => "-",
            "ADDITIONAL_PICT_PROP_4" => "-",
            "DISPLAY_IMG_WIDTH" => "90",
            "DISPLAY_IMG_HEIGHT" => "90",
            "ADDITIONAL_PICT_PROP_9" => "-"
        ),
        false
    );?>
</div>
	<input type="hidden" id="column_headers" value="<?=htmlspecialcharsbx(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
<?

endif;