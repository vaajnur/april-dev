<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult=array("ERROR"=>array());
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");

$arResult["POST"]["ORDER_ID"]=intval($_REQUEST["order_id"]);

$resOrders = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), array());
while($arOrders=$resOrders->GetNext())
{
	$arResult["ORDERS"][$arOrders["ID"]]=array(
		"ID"=>$arOrders["ID"],
		"USER_NAME"=>$arOrders["USER_NAME"],
		"USER_LAST_NAME"=>$arOrders["USER_LAST_NAME"],
		"PRICE"=>$arOrders["PRICE"],
		"DATE_INSERT"=>$arOrders["DATE_INSERT"],
	);
}

$arResult["CUR_ORDER"]=array();
if($arResult["POST"]["ORDER_ID"]!=0)
{
	if($arResult["POST"]["ORDER_ID"]>0)
		$arResult["ORDER_ID_PF"]=checkOrderId($arResult["POST"]["ORDER_ID"]);
	$arResult["CUR_ORDER"]["ID"]=$arResult["POST"]["ORDER_ID"];
	$resOrderProps=CSaleOrderPropsValue::GetOrderProps($arResult["POST"]["ORDER_ID"]);
	while($arOrderProps=$resOrderProps->Fetch())
	{
		switch($arOrderProps["CODE"]){
			case "FIO":
				$arResult["CUR_ORDER"]["NAME"]=$arOrderProps["VALUE"];
			break;
			case "PHONE":
				$arResult["CUR_ORDER"]["PHONE"]=$arOrderProps["VALUE"];
			break;
			case "EMAIL":
				$arResult["CUR_ORDER"]["EMAIL"]=$arOrderProps["VALUE"];
			break;
			case "ADDRESS":
				$arResult["CUR_ORDER"]["ADDRESS"]=$arOrderProps["VALUE"];
			break;
		}
	}
	
	$arResult["CUR_ORDER"]["ITEMS"]=array();
	$resBasketItems = CSaleBasket::GetList(array(),array("ORDER_ID" => $arResult["POST"]["ORDER_ID"]),false,false,array("ID","PRODUCT_ID","NAME","PRICE","QUANTITY","DISCOUNT_PRICE"));
	while($arBasketItems=$resBasketItems->GetNext())
	{
		$arResult["CUR_ORDER"]["ITEMS"][$arBasketItems["ID"]]=array(
			"NAME"=>$arBasketItems["NAME"],
			"PRICE"=>round($arBasketItems["PRICE"],0),
			"QUANTITY"=>$arBasketItems["QUANTITY"],
			"DISCOUNT_PRICE"=>$arBasketItems["DISCOUNT_PRICE"],
		);
		$resBasketItemProps = CSaleBasket::GetPropsList(array(),array("BASKET_ID" => $arBasketItems["ID"]));
		while ($arBasketItemProps = $resBasketItemProps->Fetch())
		{
			$arResult["CUR_ORDER"]["ITEMS"][$arBasketItems["ID"]]["PROPS"][$arBasketItemProps["CODE"]]=$arBasketItemProps["VALUE"];
		}
		$resItem=CIBlockElement::GetList(array(),array("ID"=>$arBasketItems["PRODUCT_ID"],"IBLOCK_ID"=>21),false,false,array("NAME","PROPERTY_CML2_ARTICLE","PROPERTY_TSVET"));
		while($arItem=$resItem->GetNext())
		{
			$arResult["CUR_ORDER"]["ITEMS"][$arBasketItems["ID"]]["COLOR"]=$arItem["PROPERTY_TSVET_VALUE"];
			$arResult["CUR_ORDER"]["ITEMS"][$arBasketItems["ID"]]["ARTICLE"]=$arItem["PROPERTY_CML2_ARTICLE_VALUE"];
		}
	}
	
	if(isset($_REQUEST["send"]))
	{
		$arResult["POST"]["MAN_ORDER_ID"]=trim($_REQUEST["man_order_id"]);
		$arResult["POST"]["NAME"]=trim($_REQUEST["name"]);
		$arResult["POST"]["PHONE"]=trim($_REQUEST["phone"]);
		$arResult["POST"]["EMAIL"]=trim($_REQUEST["email"]);
		$arResult["POST"]["ADDRESS"]=trim($_REQUEST["address"]);
		$arResult["POST"]["CITY"]=trim($_REQUEST["city"]);
		$arResult["POST"]["REGION"]=trim($_REQUEST["region"]);
		$arResult["POST"]["DELIVERY"]=trim($_REQUEST["delivery"]);
		$arResult["POST"]["DELIVERY_TIME"]=trim($_REQUEST["time_delivery"]);
		$arResult["POST"]["DELIVERY_DATE"]=trim($_REQUEST["date_delivery"]);
		$arResult["POST"]["DELIVERY_DATE_FORMATTED"]=(MakeTimeStamp(trim($_REQUEST["date_delivery"]))>0?date("Ymd",MakeTimeStamp(trim($_REQUEST["date_delivery"]))):"");
		$arResult["POST"]["BARCODE"]=trim($_REQUEST["barcode"]);
		$arResult["POST"]["ITEMS"]=array();
		$arResult["POST"]["FULLPRICE"]=0;
		$arResult["POST"]["FULLINSPRICE"]=0;
		$arResult["POST"]["FULLDESC"]="";
		foreach($_REQUEST["itemname"] as $kitem=>$item)
		{
			if(strlen(trim($item))>0)
			{
				$arResult["POST"]["ITEMS"][$kitem]=array(
					"NAME"=>trim($_REQUEST["itemname"][$kitem]),
					"PRICE"=>doubleval($_REQUEST["itemprice"][$kitem]),
					"QUANTITY"=>doubleval($_REQUEST["itemquantity"][$kitem]),
					"COLOR"=>trim($_REQUEST["itemcolor"][$kitem]),
					"ARTICLE"=>trim($_REQUEST["itemarticle"][$kitem]),
					"SIZE"=>trim($_REQUEST["itemsize"][$kitem])
				);
				$arResult["POST"]["FULLPRICE"]+=doubleval($_REQUEST["itemprice"][$kitem]);
				$arResult["POST"]["FULLINSPRICE"]+=doubleval($_REQUEST["iteminsprice"][$kitem]);
				$arResult["POST"]["FULLDESC"].=($arResult["POST"]["FULLDESC"]!=""?", ":"").trim($_REQUEST["itemarticle"][$kitem])." ".trim($_REQUEST["itemname"][$kitem])." (".trim($_REQUEST["itemsize"][$kitem]).") ".trim($_REQUEST["itemcolor"][$kitem]);
			}
		}
		if(strlen($arResult["POST"]["NAME"])<=0)
			$arResult["ERROR"][]="Не заполнено имя";
		if(strlen($arResult["POST"]["PHONE"])<=0)
			$arResult["ERROR"][]="Не заполнен телефон";	
		if(strlen($arResult["POST"]["ADDRESS"])<=0)
			$arResult["ERROR"][]="Не заполнен адрес";
		if(count($arResult["POST"]["ITEMS"])<=0)
			$arResult["ERROR"][]="Отсутствуют товары";
		if(strlen($arResult["POST"]["MAN_ORDER_ID"])<=0&&$arResult["POST"]["ORDER_ID"]<=0)
			$arResult["ERROR"][]="Не заполнен номер заказа";
		if(strlen(preg_replace("/\d/is","",$arResult["POST"]["MAN_ORDER_ID"]))<=0&&$arResult["POST"]["ORDER_ID"]<=0)
			$arResult["ERROR"][]="В номере заказа должны присутствовать латинские буквы (например 'n123')";
		if(strlen($arResult["POST"]["CITY"])<=0)
			$arResult["ERROR"][]="Не заполнен город";
		
		$arResult["ORDER_ID_PF"]=checkOrderId(($arResult["POST"]["ORDER_ID"]>0?$arResult["POST"]["ORDER_ID"]:$arResult["POST"]["MAN_ORDER_ID"]));
		
		$arResult["POST"]["PHONE"]=preg_replace("/\D/is", "", $arResult["POST"]["PHONE"]);
		if(substr($arResult["POST"]["PHONE"],0,1)!=8)
		{
			if(substr($arResult["POST"]["PHONE"],0,1)==7)
				$arResult["POST"]["PHONE"]=substr($arResult["POST"]["PHONE"], 1);
			$arResult["POST"]["PHONE"]="8".$arResult["POST"]["PHONE"];
		}
		
		if(count($arResult["ERROR"])<=0)
		{
			$login="april.moscow";
			$pswd="7702752511";
			$location="http://api.spsr.ru/waExec/WAExec";
			$SID="";
			$PCN="7702752511";
			
			//LOGIN
			$xmlText="<?xml version=\"1.0\"?>";
			$xmlText.='<root xmlns="http://spsr.ru/webapi/usermanagment/login/1.0">'; 
			$xmlText.='<p:Params Name="WALogin" Ver="1.0" xmlns:p="http://spsr.ru/webapi/WA/1.0" />'; 
			$xmlText.='<Login Login="'.$login.'" Pass="'.$pswd.'" UserAgent="MyGarderob" />'; 
			$xmlText.='</root>'; 
			
			$result = sendXml($xmlText,$location);
			preg_match('/<Login SID="(.*?)"/i',$result,$matches);
			$SID=$matches[1];
			if(strlen($SID)<=0)
				$arResult["ERROR"][]="Ошибка. Невозможна авторизация.";
			//LOGIN
			
			
			if(count($arResult["ERROR"])<=0)
			{
				//SEND
				$xmlText="<?xml version=\"1.0\"?>";
				$xmlText.='<root xmlns="http://spsr.ru/webapi/xmlconverter/1.3">';
				$xmlText.='<Params xmlns="http://spsr.ru/webapi/WA/1.0" Ver="1.3" Name="WAXmlConverter"/>';
				$xmlText.='<Login SID="'.$SID.'"/>';
				$xmlText.='<XmlConverter>';
				$xmlText.='<GeneralInfo ContractNumber="'.$PCN.'" >';
				$xmlText.='<Invoice Action="N" ShipRefNum="'.$arResult["ORDER_ID_PF"].'" PickUpType="W" ProductCode="PelOn" FullDescription="'.$arResult["POST"]["FULLDESC"].'" PiecesCount="1" '.($arResult["POST"]["DELIVERY_DATE_FORMATTED"]!=""?'DeliveryDate="'.$arResult["POST"]["DELIVERY_DATE_FORMATTED"].'"':"").' DeliveryTime="'.$arResult["POST"]["DELIVERY_TIME"].'" InsuranceType="INS" InsuranceSum="'.number_format($arResult["POST"]["FULLINSPRICE"],2,".","").'" CODGoodsSum="'.number_format($arResult["POST"]["FULLPRICE"]+$arResult["POST"]["DELIVERY"],2,".","").'" CODDeliverySum="0">';
				$xmlText.='<Receiver Country="РФ" Region="'.$arResult["POST"]["REGION"].'" City="'.$arResult["POST"]["CITY"].'" Address="'.$arResult["POST"]["ADDRESS"].'" ContactName="'.$arResult["POST"]["NAME"].'" Phone="'.$arResult["POST"]["PHONE"].'" Comment="" />';
				$xmlText.='<AdditionalServices COD="1" PartDelivery="1" TryOn="1"/>';
				$xmlText.='<Pieces>';
				$xmlText.='<Piece Description="16"'.($arResult["POST"]["BARCODE"]!=""?' PieceID="'.$arResult["POST"]["BARCODE"].'"':"").'>';
				foreach($arResult["POST"]["ITEMS"] as $kitem=>$item)
				{
					$xmlText.='<SubPiece ProductCode="'.$item["ARTICLE"].'" Description="'.$item["NAME"].' '.$item["ARTICLE"].' '.$item["COLOR"].' '.$item["SIZE"].'" Cost="'.number_format($item["PRICE"],2,".","").'" Quantity="'.intval($item["QUANTITY"]).'"/>';					
				}
				//if($arResult["POST"]["DELIVERY"]>0)
				//{
					$xmlText.='<SubPiece Description="Доставка" Cost="'.number_format(doubleval($arResult["POST"]["DELIVERY"]),2,".","").'"/>';	
				//}
				$xmlText.='</Piece>';	
				$xmlText.='</Pieces>';
				$xmlText.='</Invoice>';
				$xmlText.='</GeneralInfo>';
				$xmlText.='</XmlConverter>';
				$xmlText.='</root>';
				//echo htmlspecialchars($xmlText); die();
				$result = sendXml($xmlText,$location);
				preg_match('/<Result RC="(.*?)"/i',$result,$matches);
				if($matches[1]=="0")
				{
					preg_match('/GCNumber="(.*?)"/i',$result,$matches);
					$arResult["MESSAGES"][]="Номер заказа: ".$matches[1];
					preg_match('/InvoiceNumber="(.*?)"/i',$result,$matches);
					$arResult["MESSAGES"][]="Номер накладной СПСР-Экспресс: ".$matches[1];
				}else{
					$arResult["ERROR"][]="Ошибка при отправке данных спср.<br/>".htmlspecialchars($result);				
				}
				//echo htmlspecialchars($xmlText);
				//SEND
				
				
				//LOGOUT
				$xmlText="<?xml version=\"1.0\"?>";
				$xmlText.='<root xmlns="http://spsr.ru/webapi/usermanagment/logout/1.0" >'; 
				$xmlText.='<p:Params Name="WALogout" Ver="1.0" xmlns:p="http://spsr.ru/webapi/WA/1.0" />'; 
				$xmlText.='<Logout Login="'.$login.'" SID="'.$SID.'" /> '; 
				$xmlText.='</root>';
				$result = sendXml($xmlText,$location);
				//LOGOUT
			}
		}
	}
}

function sendXml($xmlText,$location)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,  $location);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,   $xmlText );

	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml')); 
	$result = curl_exec($ch);
	
	curl_close($ch);
	
	return $result;
}

function checkOrderId($ordId)
{
	$login="7702382511GAR";
	$pswd="N0!2382511";
	$location="http://api.spsr.ru/waExec/WAExec";
	$SID="";
	$PCN="7702382511";
	
	//LOGIN
	$xmlText="<?xml version=\"1.0\"?>";
	$xmlText.='<root xmlns="http://spsr.ru/webapi/usermanagment/login/1.0">'; 
	$xmlText.='<p:Params Name="WALogin" Ver="1.0" xmlns:p="http://spsr.ru/webapi/WA/1.0" />'; 
	$xmlText.='<Login Login="'.$login.'" Pass="'.$pswd.'" UserAgent="MyGarderob" />'; 
	$xmlText.='</root>'; 
	
	$result = sendXml($xmlText,$location);
	preg_match('/<Login SID="(.*?)"/i',$result,$matches);
	$SID=$matches[1];
	if(strlen($SID)<=0)
		$arResult["ERROR"][]="Ошибка. Невозможна авторизация.";
	//LOGIN
	
	//CHECK
	$norm=false;
	$ordIdPostfix=1;
	while(!$norm)
	{
		$xmlText="<?xml version=\"1.0\"?>";
		$xmlText.="<root xmlns=\"http://spsr.ru/webapi/DataEditManagment/GetInvoiceInfo/1.1\">"; 
		$xmlText.="<p:Params Name=\"WAGetInvoiceInfo\" xmlns:p=\"http://spsr.ru/webapi/WA/1.0\" Ver=\"1.1\"/>"; 
		$xmlText.="<Login SID=\"".$SID."\" Login=\"".$login."\" ICN=\"".$PCN."\"/>";
		$xmlText.="<InvoiceInfo GCInvoiceNumber=\"".$ordId.($ordIdPostfix>1?"v".$ordIdPostfix:"")."\"/>";
		$xmlText.="</root>";
		$result = sendXml($xmlText,$location);
		//echo htmlspecialchars($result);
		if(preg_match('/<NotFound>/i',$result))
		{
			$norm=true;
		}else{
			$ordIdPostfix++;
		}
		if($cntrrr++>1000)
		{
			$ordIdPostfix=1;
			break;
		}
	}
	//CHECK
	
	//LOGOUT
	$xmlText="<?xml version=\"1.0\"?>";
	$xmlText.='<root xmlns="http://spsr.ru/webapi/usermanagment/logout/1.0" >'; 
	$xmlText.='<p:Params Name="WALogout" Ver="1.0" xmlns:p="http://spsr.ru/webapi/WA/1.0" />'; 
	$xmlText.='<Logout Login="'.$login.'" SID="'.$SID.'" /> '; 
	$xmlText.='</root>';
	$result = sendXml($xmlText,$location);
	//LOGOUT
	
	return $ordId.($ordIdPostfix>1?"v".$ordIdPostfix:"");
}

//printer($arResult);
$this->IncludeComponentTemplate();
?>