<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? CJSCore::Init(array("jquery"));?>
<? foreach($arResult["ERROR"] as $error){?>
	<? ShowError($error)?>
<? }?>
<? foreach($arResult["MESSAGES"] as $message){?>
	<? ShowNote($message)?>
<? }?>
<? if($arResult["ORDER_ID_PF"]!=""){?>
	<? ShowNote("ID заказа: ".$arResult["ORDER_ID_PF"])?>
<? }?>
<form method="post">
	<table>
		<tr>
			<td>Заказ</td>
			<td>
				<select onchange="jQuery(this).closest('form').submit()" name="order_id">
					<? if($arResult["POST"]["ORDER_ID"]==0){?>
						<option value="-1"> ~ Выберите заказ из списка ~ </option>
					<? }?>
					<option value="-1">Ввести заказ вручную</option>
					<? foreach($arResult["ORDERS"] as $order){?>
						<option value="<?=$order["ID"]?>" <?=$order["ID"]==$arResult["POST"]["ORDER_ID"]?"selected":""?>>№<?=$order["ID"]?>. <?=$order["USER_LAST_NAME"]?> <?=$order["USER_NAME"]?> - <?=$order["DATE_INSERT"]?></option>
					<? }?>
				</select>
			</td>
		</tr>
		<? if($arResult["POST"]["ORDER_ID"]<0){?>
			<tr>
				<td>Номер заказа *</td>
				<td>
					<input size="50" name="man_order_id" type="text" value="<?=$arResult["POST"]["MAN_ORDER_ID"]?>" />
				</td>
			</tr>
		<? }?>
		<? if($arResult["POST"]["ORDER_ID"]!=0){?>
			<tr>
				<td>Фамилия Имя *</td>
				<td>
					<input size="50" name="name" type="text" value="<?=$arResult["POST"]["NAME"]!=""?$arResult["POST"]["NAME"]:$arResult["CUR_ORDER"]["NAME"]?>" required="required" />
				</td>
			</tr>
			<tr>
				<td>Город *</td>
				<td style="position:relative;">
					<input size="50" name="city_show" type="text" value="<?=$arResult["POST"]["CITY"]!=""?$arResult["POST"]["CITY"]:"Москва"?>" class="city_input" />
					<input size="50" name="city" type="hidden" value="<?=$arResult["POST"]["CITY"]!=""?$arResult["POST"]["CITY"]:"Москва"?>" class="city_input_val" />
					<div class="cities_list" style="display:none;position:absolute;width:250px;max-height:350px;overflow-x: hidden;overflow-y: auto;left:0;top: 30px;background:rgba(255,255,255,0.9);border:1px solid #cccccc;padding: 5px;">
					</div>
					<input size="50" name="region" type="text" readonly="readonly" value="<?=$arResult["POST"]["REGION"]!=""?$arResult["POST"]["REGION"]:"Московская обл."?>" class="region_input" />	
					<script>
						jQuery(document).ready(function(){
							jQuery(".city_input").keyup(function(){
								var citList=jQuery(this).siblings(".cities_list");
								if(jQuery(this).val().length>1)
								{
									jQuery.ajax({
										url:"<?=$component->__path?>/get_cities.php",
										dataType:"json",
										type:"post",
										data:{
											CITY:jQuery(this).val()
										},
										success:function(data){
											if(data.error==0||data.cities.length==0)
											{
												var cityListHtml="";
												for(var qq=0;qq<data.cities.length;qq++)
												{
													cityListHtml+=(qq>0?"<br/>":"");
													cityListHtml+="<a href='javascript:void(0)'><span class='citN'>"+data.cities[qq]+"</span> - <span class='regN'>"+data.regions[qq]+"</span></a>";
												}
												citList.html(cityListHtml).show();
											}else{
												citList.html("").hide();
												citList.siblings(".city_input_val").val("");
												jQuery(this).siblings(".region_input").val("");
											}
										},
										error: function(data)
										{
											console.log(data);
										}
									});
								}else{
									citList.html("").hide();
									jQuery(this).siblings(".city_input_val").val("");
									jQuery(this).siblings(".region_input").val("");
								}
							});
							$(".cities_list").on("click", "a", function(){
								jQuery(this).closest(".cities_list").siblings(".city_input").val(jQuery(this).find(".citN").text());
								jQuery(this).closest(".cities_list").siblings(".city_input_val").val(jQuery(this).find(".citN").text());
								jQuery(this).closest(".cities_list").siblings(".region_input").val(jQuery(this).find(".regN").text());
								jQuery(this).closest(".cities_list").hide();
							});
							jQuery(".city_input").blur(function(){
								jQuery(this).val(jQuery(this).siblings(".city_input_val").val());
							});
						});

					</script>
				</td>
			</tr>
			<tr>
				<td>Адрес *</td>
				<td>
					<input size="50" name="address" type="text" value="<?=$arResult["POST"]["ADDRESS"]!=""?$arResult["POST"]["ADDRESS"]:$arResult["CUR_ORDER"]["ADDRESS"]?>" required="required" />
				</td>
			</tr>
			<tr>
				<td>Телефон *</td>
				<td>
					<input size="50" name="phone" type="text" value="<?=$arResult["POST"]["PHONE"]!=""?$arResult["POST"]["PHONE"]:$arResult["CUR_ORDER"]["PHONE"]?>" required="required" />
				</td>
			</tr>
			<tr>
				<td>Стоимость доставки</td>
				<td>
					<input size="50" name="delivery" type="text" value="<?=$arResult["POST"]["DELIVERY"]?>" />
				</td>
			</tr>
			<tr>
				<td>Штрихкод</td>
				<td>
					<input size="50" maxlength="12" name="barcode" type="text" value="<?=$arResult["POST"]["BARCODE"]?>" />
				</td>
			</tr>
			<tr>
				<td>Комментарий</td>
				<td>
					<input size="50" name="comment" type="text" value="" />
				</td>
			</tr>       
			<tr>
				<td>Дата доставки</td>
				<td>
					<?$APPLICATION->IncludeComponent("bitrix:main.calendar","",Array(
					     "SHOW_INPUT" => "Y",
					     "FORM_NAME" => "",
					     "INPUT_NAME" => "date_delivery",
					     "INPUT_NAME_FINISH" => "",
					     "INPUT_VALUE" => $arResult["POST"]["DELIVERY_DATE"],
					     "INPUT_VALUE_FINISH" => "", 
					     "SHOW_TIME" => "N",
					     "HIDE_TIMEBAR" => "Y"
						)
					);?>
				</td>
			</tr>
			<tr>
				<td>Время доставки</td>
				<td>
					<select name="time_delivery">
						<option <?=$arResult["POST"]["DELIVERY_TIME"]=="WD"?"selected='selected'":""?> value="WD">с 9:00 до 18:00</option>
						<option <?=$arResult["POST"]["DELIVERY_TIME"]=="AM"?"selected='selected'":""?> value="AM">с с 9:00 до 14:00</option>
						<option <?=$arResult["POST"]["DELIVERY_TIME"]=="PM"?"selected='selected'":""?> value="PM">с 14:00 до 18:00</option>
						<option <?=$arResult["POST"]["DELIVERY_TIME"]=="AM1"?"selected='selected'":""?> value="AM1">с 9:00 до 13:00</option>
						<option <?=$arResult["POST"]["DELIVERY_TIME"]=="PM1"?"selected='selected'":""?> value="PM1">с 13:00 до 17:00</option>
						<option <?=$arResult["POST"]["DELIVERY_TIME"]=="PM2"?"selected='selected'":""?> value="PM2">с 17:00 до 21:00</option>
						<option <?=$arResult["POST"]["DELIVERY_TIME"]=="WD1"?"selected='selected'":""?> value="WD1">с 9:00 до 21:00</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Товары
				</td>
				<td>
					<table id="basket_items">
						<tr>
							<th>
								Название *
							</th>
							<th>
								Артикул
							</th>
							<th>
								Цвет
							</th>
							<th>
								Размер
							</th>
							<th>
								Оценочная стоимость
							</th>
							<th>
								Цена
							</th>
							<th>
								Кол-во
							</th>
							<th>
								Удалить
							</th>
						</tr>
						
						<? $arType="CUR_ORDER"?>					
						<? if(isset($arResult["POST"]["ITEMS"])){?>
							<? $arType="POST"?>
						<? }?>
						<? foreach($arResult[$arType]["ITEMS"] as $item){?>
							<? for($qq=0;$qq<intval($item["QUANTITY"]);$qq++){?>
								<tr>
									<td>
										<input name="itemname[]" value="<?=$item["NAME"]?>" size="20"/>
									</td>
									<td>
										<input name="itemarticle[]" value="<?=$item["ARTICLE"]?>" size="10"/>
									</td>
									<td>
										<input name="itemcolor[]" value="<?=$item["COLOR"]?>" size="10"/>
									</td>
									<td>
										<input name="itemsize[]" value="<?=$item["PROPS"]["SIZE"]?>" size="5"/>
									</td>
									<td>
										<input name="iteminsprice[]" value="<?=$item["PRICE"]?>" size="10"/>
									</td>
									<td>
										<input name="itemprice[]" value="<?=$item["PRICE"]?>" size="10"/>
									</td>
									<td>
										<input name="itemquantity[]" value="1" size="5"/>
									</td>
									<td>
										<input type="button" value="Удалить" onclick="if(confirm('Удалить?')) jQuery(this).closest('tr').remove();return false;"/>
									</td>
								</tr>
							<? }?>
						<? }?>
						
					</table>
					<input type="button" value="Добавить товар" onclick="jQuery('#basket_items').append('<tr><td><input name=\'itemname[]\' value=\'\' size=\'20\'/></td><td><input name=\'itemarticle[]\' value=\'\' size=\'10\'/></td><td><input name=\'itemcolor[]\' value=\'\' size=\'10\'/></td><td><input name=\'itemsize[]\' value=\'\' size=\'5\'/></td><td><input name=\'iteminsprice[]\' value=\'\' size=\'10\'/></td><td><input name=\'itemprice[]\' value=\'\' size=\'10\'/></td><td><input name=\'itemquantity[]\' value=\'\' size=\'5\'/></td><td><input type=\'button\' value=\'Удалить\' onclick=\'if(confirm(&quot;Удалить?&quot;)) jQuery(this).closest(&quot;tr&quot;).remove();return false;\'/></td></tr>');"/>
				</td>
			</tr>
				<td colspan="2"><hr/></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" value="Отправить" name="send" />
				</td>
			</tr>
		<? }?>
	</table>
</form>
<? //printer($arResult);?>