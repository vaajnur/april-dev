<?
$arReturn=array("error"=>0,"cities"=>array());
if(strlen(trim($_REQUEST["CITY"]))>1)
{
	$location="http://api.spsr.ru/waExec/WAExec";
	
	$xmlText='<root xmlns="http://spsr.ru/webapi/Info/GetCities/1.0">'; 
	$xmlText.='<p:Params Name="WAGetCities" Ver="1.0" xmlns:p="http://spsr.ru/webapi/WA/1.0" />'; 
	$xmlText.='<GetCities CityName="'.trim($_REQUEST["CITY"]).'" CountryName="россия"/>'; 
	$xmlText.='</root>';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,  $location);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,   $xmlText );

	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml')); 
	$result = curl_exec($ch);
	
	curl_close($ch);
	
	preg_match_all('/CityName="(.*?)"\sRegionName="(.*?)"/is',$result,$matches);
	$arReturn["cities"]=$matches[1];
	$arReturn["regions"]=$matches[2];
	exit( json_encode($arReturn));
}
?>