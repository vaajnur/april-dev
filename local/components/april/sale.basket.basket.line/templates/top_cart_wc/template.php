<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	
		<?

		if (!function_exists('declOfNumCart')) {
		function declOfNumCart($number, $titles,$forward=array("","",""))  
			{  
				$cases = array (2, 0, 1, 1, 1, 2);  
				return $forward[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ]." ".$number." ".$titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];  
			} 
		} ?>

    <!-- Линк перекрывает иконку, просто записать нужную ссылку -->
    <a href="/basket/"></a>
    <!-- counter, выводит количество -->
    <?
		if (IntVal($arResult["NUM_PRODUCTS"])>0)
		{
			if (CModule::IncludeModule("sale"))
			{
				$arBasketItems = array();
				$dbBasketItems = CSaleBasket::GetList(
						array(
							"NAME" => "ASC",
							"ID" => "ASC"
							),
						array(
							"CAN_BUY" => "Y",
							"FUSER_ID" => CSaleBasket::GetBasketUserID(),
							"LID" => SITE_ID,
							"ORDER_ID" => "NULL"
							),
						false,
						false,
						array()
					);
                $cntbsk=0;
				while ($arItems = $dbBasketItems->Fetch())
				{
                    $cntbsk++;
					if (strlen($arItems["CALLBACK_FUNC"]) > 0)
					{
						CSaleBasket::UpdatePrice($arItems["ID"],
						$arItems["QUANTITY"]);
						$arItems = CSaleBasket::GetByID($arItems["ID"]);
					}
					$arBasketItems[] = $arItems;
				}
			}
			$summ = substr(intval($arResult["TOTAL_PRICE"]), 0, -4);
			?>
                <span><?=$cntbsk;?></span>
			<?
		}
		else 
		{
			?>
				<span>0</span>
			<?
		}
		?>
    <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/cart.svg"); ?>
    <div class="drop">
        <div class="header">
            <div class="price">
                <span>В корзине <?=declOfNumCart(intval($cntbsk), array('товар', 'товара', 'товаров')); ?></span>
                <span>Сумма <?=number_format(str_replace("руб.","",(str_replace(" ","",$arResult["TOTAL_PRICE"]))),0,"."," ");?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
            </div>
            <button onclick="location.replace('/basket/')" class="btn">Перейти в корзину</button>
        </div>
        <div class="scroll-area">
            <? 

            $cntbsk=0;
            foreach ($arBasketItems as $arItem){
                $cntbsk++;
                //if($cntbsk>$max) continue;
				$mxResult = CCatalogSku::GetProductInfo(
				$arItem["PRODUCT_ID"]
				);
				if (is_array($mxResult))
				{
				 $product_id=$mxResult['ID'];
				}
				else{
					$product_id=$arItem["PRODUCT_ID"];
				}
                $arSelect = Array("PROPERTY_CML2_ARTICLE","DETAIL_PAGE_URL","IBLOCK_ID","DETAIL_PICTURE","IBLOCK_SECTION_ID","PROPERTY_color","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE","CODE");
                $arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ID"=>$product_id);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array());
                if($ob = $res->GetNextElement()){
                $ar_res = $ob->GetFields();
                $ar_res_props=$ob->GetProperties();
                ?>
                <?$myImg= CFile::ResizeImageGet($ar_res["DETAIL_PICTURE"], array('width'=>300, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL , true);?>
                <div class="item" product-id="<?=$arItem['ID'];?>">
					<?
					 $ar_price = GetCatalogProductPrice($arItem["PRODUCT_ID"], 1); 
					 $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
						 $ar_price["ID"],
						 $USER->GetUserGroupArray(),
						 "N",
						 SITE_ID
					 );
					 $discountPrice = CCatalogProduct::CountPriceWithDiscount(
							 $ar_price["PRICE"],
							 $ar_price["CURRENCY"],
							 $arDiscounts
						 );
					 $new_price=$discountPrice; 
					 if($new_price!=$ar_price["PRICE"]){
						 $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
						 $ar_price["PRICE"] = $new_price;
					 }
					 $nav = CIBlockSection::GetNavChain($ar_res["IBLOCK_ID"], $ar_res["IBLOCK_SECTION_ID"]);
					$category="";
					while ($arNav=$nav->GetNext()):
						$category.=$arNav["NAME"].'/';
					endwhile;
					$category=trim($category,"/");  
					 ?>
                    <div class="delete" id-gtm="<?=$ar_res['ID'];?>" name-gtm="<?=explode(" ",$arItem['NAME'])[0];?> <?=$ar_res_props['CML2_MANUFACTURER']['VALUE'];?> <?=$ar_res_props['CML2_ARTICLE']['VALUE'];?>" gtm-brand="<?=$ar_res_props['CML2_MANUFACTURER']['VALUE'];?>" price-gtm="<?=number_format($ar_price['PRICE'],0,".","");?>" category-gtm="<?=$category;?>" >
                        <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/close.svg"); ?>
                    </div>
                    <div class="img" style="background-image: url(<?=strlen($myImg['src'])>0?$myImg['src']:"/img/prod-sample.jpg";?>)"></div>
                    <span>
                        <a href="<?=$ar_res['DETAIL_PAGE_URL'];?>">
                            <span><?=explode(" ",$arItem['NAME'])[0];?></span>
                    <span><?=$ar_res_props['CML2_MANUFACTURER']['VALUE'];?></span>
                    </a>
                    </span>
					
                    <div class="price">
                        <? if(strlen($ar_price["DISCOUNT_PRICE"])>0){?><span class="old"><?=number_format($ar_price["DISCOUNT_PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span><?}?>
                        <span><?=number_format($arItem['PRICE'],0,"."," ");?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
                    </div>
                </div>
                <?
            }}?>
        </div>
    </div>
