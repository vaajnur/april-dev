<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

/**
 * @global CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arParams
 * @var array $arResult
 * @var array $arCurSection
 */
?>

<div class="container-fluid sidebar-version">
	<div class="row">
		
        <?$APPLICATION->IncludeComponent(
        "april:catalog.section.list", 
        "aside", 
        array(
            "VIEW_MODE" => "LIST",
            "SHOW_PARENT_NAME" => "Y",
            "IBLOCK_TYPE" => "catalog",
           // "FILTER_NAME" => $arParams['FILTER_NAME'],
            "IBLOCK_ID" => CATALOG_IBLOCK_ID,
            "SECTION_ID" => "",
            "SECTION_CODE" => "",
            "SECTION_URL" => "",
            "COUNT_ELEMENTS" => "N",
            "TOP_DEPTH" => "3",
            "SECTION_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "ADD_SECTIONS_CHAIN" => "Y",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "7200",
            "CACHE_NOTES" => "",
            "CACHE_GROUPS" => "Y",
            "COMPONENT_TEMPLATE" => "aside"
        ),
        false
    );?>
		<div class="col-sm-9 col-md-10">
			<?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
                    "AREA_FILE_SUFFIX" => "",
                    "EDIT_TEMPLATE" => "",
                    "DIV_FOR_PAGE"=>"Y",
                    "CATALOG_SECTION" => "Y",
                    "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
                )
            );?>
            <? if(isset($_REQUEST['SEARCH'])){?>
            <p class="search-result">
            	<? $_REQUEST['q']=htmlspecialcharsbx($_REQUEST['q']);?>
				По вашему запросу <span>«<?=htmlspecialcharsbx($_REQUEST['q']);?>»</span><i class="count-results"></i>:
			</p>
            <?}?>
            <div id="prod-container-catalog" class="prod-container grid">
            <!-- <div class="grid-sizer"></div> -->
			<?
            //решаем баг с тем что page2 попадает в путь к разделу
            if(!isset($_REQUEST['BRAND'])&&!isset($_REQUEST['NOVINKI'])&&!isset($_REQUEST['RASPRODAZHA'])&&!isset($_REQUEST['SEARCH'])&&!isset($_REQUEST['tag'])){
                $arResult['VARIABLES']['SECTION_CODE_PATH']=preg_replace('/page+[0-9]{4}/i', '', $arResult['VARIABLES']['SECTION_CODE_PATH'], -1, $count);
                $arResult['VARIABLES']['SECTION_CODE_PATH']=preg_replace('/page+[0-9]{3}/i', '', $arResult['VARIABLES']['SECTION_CODE_PATH'], -1, $count);
                $arResult['VARIABLES']['SECTION_CODE_PATH']=preg_replace('/page+[0-9]{2}/i', '', $arResult['VARIABLES']['SECTION_CODE_PATH'], -1, $count);
                $arResult['VARIABLES']['SECTION_CODE_PATH']=preg_replace('/page+[0-9]{1}/i', '', $arResult['VARIABLES']['SECTION_CODE_PATH'], -1, $count);
                $arResult['VARIABLES']['SECTION_CODE_PATH']=trim($arResult['VARIABLES']['SECTION_CODE_PATH'],"/");
            }
            else{
				unset($arResult['VARIABLES']);
				if(isset($_REQUEST['tag'])){
					$arSelect = Array("ID", "NAME","CODE");
					$arFilter = Array("IBLOCK_ID"=>22, "ACTIVE"=>"Y","ID"=>$_REQUEST['tag']);
					$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nTopCount"=>1), $arSelect);
					while($ob = $res->GetNextElement())
					{
						$arFields = $ob->GetFields();
						$path=stristr($_SERVER['REQUEST_URI'], $arFields['CODE'], true);
						$url_words=explode("/",$path);
						$SECTION_ID="";
						foreach($url_words as $word){
							if(strlen($word)>0){
								$arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID,"CODE"=>$word,"IBLOCK_SECTION_ID"=>$SECTION_ID/*">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"CATALOG_AVAILABLE"=>"Y","!DETAIL_PICTURE"=>false*/);
								$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
								while ($arSection = $rsSections->GetNext())
								{
									$APPLICATION->AddChainItem($arSection["NAME"], $arSection["SECTION_PAGE_URL"]);
									$GLOBALS['CATALOG_FILTER']["SECTION_ID"]= array($arSection["ID"]);
									$GLOBALS['CATALOG_FILTER']["INCLUDE_SUBSECTIONS"]="Y";
									$SECTION_ID=$arSection["ID"];
									
								}
							}
						}
						$arSelect_meta = Array("ID", "NAME","CODE","PROPERTY_SEO_H1","PROPERTY_SEO_TITLE","PREVIEW_TEXT");
						$arFilter_meta = Array("IBLOCK_ID"=>23, "ACTIVE"=>"Y","PROPERTY_TAG"=>$_REQUEST['tag'],"PROPERTY_CATEGORY"=>$SECTION_ID);
						$res_meta = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter_meta, false, Array("nTopCount"=>1), $arSelect_meta);
						if($ob_meta = $res_meta->GetNextElement())
						{
							$arFields_meta = $ob_meta->GetFields();
							$arProps_meta = $ob_meta->GetProperties();
							$APPLICATION->SetPageProperty("description",$arFields_meta['PREVIEW_TEXT']); 
							$APPLICATION->SetPageProperty("title",$arFields_meta['PROPERTY_SEO_TITLE_VALUE']); 
							$APPLICATION->AddChainItem($arFields_meta['PROPERTY_SEO_H1_VALUE'], "");
							$APPLICATION->SetTitle($arFields_meta['PROPERTY_SEO_H1_VALUE']);
						}
						else{
							CHTTP::SetStatus("404 Not Found");
							@define("ERROR_404","Y");
						}
					}
					//$arResult['VARIABLES']['SECTION_CODE_PATH']=stristr($_SERVER['REQUEST_URI'], $arFields['CODE'], true);
				}
            }
            if(isset($_REQUEST['SEARCH'])){
                $found=false;
                function dropBackWordshl($word) { //тут мы обрабатываем одно слово
                    $reg = "/(ый|ой|ая|ое|ые|ому|ями|ем|ами|а|о|у|е|ого|ему|и|ство|ых|ох|ия|ий|ь|я|он|ют|ат)$/i"; //данная регулярная функция будет искать совпадения окончаний
                    $word = preg_replace($reg,'',$word); //убиваем окончания
                    return $word;
                }
                $searchquery=$_REQUEST["q"];
                $lat=str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789&");
                $category=trim(str_replace($lat,"",$searchquery));
                $designer=$searchquery;
                $words=explode(" ",$category);
                CModule::IncludeModule("iblock");
                CModule::IncludeModule("catalog");
                foreach($words as $word){
                    $designer=trim(str_replace($word,"",$designer));
                }
				$category=dropBackWordshl($category);
				$GLOBALS['CATALOG_FILTER']['IBLOCK_ID']=CATALOG_IBLOCK_ID;
                //если в поиске нет дизайнера ищем категории сначала
                if(strlen($designer)==0){
                    $arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID,"%NAME"=>$category,/*">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"CATALOG_AVAILABLE"=>"Y","!DETAIL_PICTURE"=>false*/);
                    $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
                    while ($arSection = $rsSections->GetNext())
                    {
                        $found=true;
                       $GLOBALS['CATALOG_FILTER']["SECTION_ID"][]= $arSection["ID"];
                    }
                }
                else if(strlen($category)==0){
                    $found=true;
					$GLOBALS['CATALOG_FILTER']["%PROPERTY_CML2_MANUFACTURER_VALUE"]=$designer;
					//$GLOBALS['CATALOG_FILTER']["%PROPERTY_CML2_ARTICLE"]=$designer;
                }
                else{
                   $arFilterCat=array("%PROPERTY_CML2_ARTICLE"=>$category,"IBLOCK_ID"=>CATALOG_IBLOCK_ID,"CATALOG_AVAILABLE"=>"Y" /*">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"CATALOG_AVAILABLE"=>"Y","!DETAIL_PICTURE"=>false*/);
                    $resCatItems = CIBlockElement::GetList(Array(), $arFilterCat,false,array("nPageSize"=>10), Array("NAME","ID","IBLOCK_SECTION_ID","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER"));
                    while($arCatItems=$resCatItems->GetNextElement())
                    {
                        $found=true;
                        $arFields=$arCatItems->GetFields();
                        $GLOBALS['CATALOG_FILTER']["ID"][]= $arFields["ID"];
                    }
                    $arFilterCat=array("%NAME"=>$category,"%PROPERTY_CML2_MANUFACTURER_VALUE"=>$designer,"IBLOCK_ID"=>CATALOG_IBLOCK_ID,/*">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"CATALOG_AVAILABLE"=>"Y","!DETAIL_PICTURE"=>false*/);
                    $resCatItems = CIBlockElement::GetList(Array(), $arFilterCat,false,array("nPageSize"=>10), Array("NAME","ID","IBLOCK_SECTION_ID","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER"));
                    while($arCatItems=$resCatItems->GetNextElement())
                    {
                        $found=true;
                        $arFields=$arCatItems->GetFields();
                       $GLOBALS['CATALOG_FILTER']["ID"][]= $arFields["ID"];
                    }
				}
                if($found===false){
					$GLOBALS['CATALOG_FILTER']["%PROPERTY_CML2_ARTICLE"]=$_REQUEST['q'];
					$GLOBALS['CATALOG_FILTER']["CATALOG_AVAILABLE"]="Y";
				}
                $count_search=0;
                $arSelect = Array("ID", "NAME");
                $res = CIBlockElement::GetList(Array(), $GLOBALS['CATALOG_FILTER'], false, Array(), $arSelect);
                if (!function_exists('declOfNumSearch')) {
                function declOfNumSearch($number, $titles,$forward=array("","",""))  
                    {  
                        $cases = array (2, 0, 1, 1, 1, 2);  
                        return $forward[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ]." ".$number." ".$titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];  
                    } 
                }
                while($ob = $res->GetNextElement())
                {
                    $count_search++;
				}
				if($count_search==0){
					$GLOBALS['CATALOG_FILTER']["%PROPERTY_CML2_MANUFACTURER_VALUE"]="";
					$GLOBALS['CATALOG_FILTER']["%PROPERTY_CML2_ARTICLE"]=$designer;
					$arSelect = Array("ID", "NAME");
					$res = CIBlockElement::GetList(Array(), array("%PROPERTY_CML2_ARTICLE"=>$designer,"CATALOG_AVAILABLE"=>"Y","IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y"), false, Array(), $arSelect);
					while($ob = $res->GetNextElement())
					{
						$arField=$ob->GetFields();
						$res_offers = CCatalogSKU::getOffersList(
							$arField['ID'],
							$arField['IBLOCK_ID'],
							$skuFilter = array("CATALOG_AVAILABLE"=>"Y","ACTIVE"=>"Y"),
							$fields = array("PROPERTY_RAZMER","PROPERTY_TSVET","NAME","PRICE"),
							$propertyFilter = array()
						);
						foreach($res_offers[$arField['ID']] as $good){
							$count_search++;
						}
					}
				}
				?>
                <script>$(".count-results").text("<?=declOfNumSearch($count_search, array('результат', 'результата', 'результатов'),array("найден","найдены","найдено")); ?>");</script>
                <?
            }
			$intSectionID = $APPLICATION->IncludeComponent(
				"april:catalog.section",
				"catalog",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
					"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
					"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
					"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
					"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_TITLE" => $arParams["SET_TITLE"],
                    "SHOW_ALL_WO_SECTION" => $arParams["SHOW_ALL_WO_SECTION"],
					"MESSAGE_404" => $arParams["~MESSAGE_404"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"SHOW_404" => $arParams["SHOW_404"],
					"FILE_404" => $arParams["FILE_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
					"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
					"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
					"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
					"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
					"LAZY_LOAD" => $arParams["LAZY_LOAD"],
					"MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
					"LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

					"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
					"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
					"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
					"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
					"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
					"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
					"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                    "SECTION_CODE_PATH" => $arResult["VARIABLES"]["SECTION_CODE_PATH"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
					'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

					'LABEL_PROP' => $arParams['LABEL_PROP'],
					'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
					'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
					'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
					'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
					'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
					'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
					'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
					'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
					'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
					'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
					'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

					'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
					'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
					'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
					'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
					'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
					'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
					'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
					'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
					'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
					'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
					'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
					'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
					'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
					'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
					'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
					'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
					'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

					'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
					'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
					'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

					'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"ADD_SECTIONS_CHAIN" => "Y",
					'ADD_TO_BASKET_ACTION' => $basketAction,
					'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
					'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
					'COMPARE_NAME' => $arParams['COMPARE_NAME'],
					'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
					'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
					'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
				),
				$component
			);
			if($intSectionID>0){
				$arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID,"ID"=>$intSectionID);
                $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter, false, array("UF_SEO_TITLE","UF_SEO_DESCRIPTION","UF_H1TEXT"));
                while ($arSection = $rsSections->GetNext())
                {
					if(strlen($arSection["UF_SEO_TITLE"])>0)
						$APPLICATION->SetPageProperty("title", $arSection["UF_SEO_TITLE"]);
					if(strlen($arSection["UF_H1TEXT"])>0)
                		$APPLICATION->SetTitle($arSection["UF_H1TEXT"], false);
					if(strlen($arSection["UF_SEO_DESCRIPTION"])>0)
						$APPLICATION->SetPageProperty("description", $arSection["UF_SEO_DESCRIPTION"]);
                }
			}
			?>
            </div>
            <div class="for-page pagination"><?=$GLOBALS['CATALOG_PAGER'];?></div>
            <? if(strlen($GLOBALS["SEO_TEXT"])>0){?>
            <div class="container">
				<div class="row center-xs">
					<div class="col-xs-12 seo">
						<!-- <?=$GLOBALS["SEO_TEXT"];?> -->
					</div>
                </div>
			</div>
            <?}?>
		</div>
	</div>
</div>
<div class="scrollToTop">
	<div>
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 489 489" style="enable-background:new 0 0 489 489;" xml:space="preserve"><g><g><path d="M417.4,71.6C371.2,25.4,309.8,0,244.5,0S117.8,25.4,71.6,71.6S0,179.2,0,244.5s25.4,126.7,71.6,172.9S179.2,489,244.5,489s126.7-25.4,172.9-71.6S489,309.8,489,244.5S463.6,117.8,417.4,71.6z M244.5,462C124.6,462,27,364.4,27,244.5S124.6,27,244.5,27S462,124.6,462,244.5S364.4,462,244.5,462z"></path><path d="M236.1,169.2c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l56.2,56.2L217,300.7c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4l65.7-65.7c5.3-5.3,5.3-13.8,0-19.1L236.1,169.2z"></path></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
	</div>
</div>