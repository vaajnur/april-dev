<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// номер текущей страницы
$curPage = (isset($_REQUEST["PAGEN_1"]))?$_REQUEST["PAGEN_1"]:"1";
//количество элементов в инфоблоке или категории по фильтру
$ID = urldecode(urldecode($arParams["ID"]));
$arsFilter = Array("USER_ID"=>IntVal($USER->GetID()), "LID" => SITE_ID);
$sql = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arsFilter);
$countorders=0;
while ($result = $sql->Fetch())
{
   $countorders++;
}
$count = $countorders;
$totalPages = $count/($arParams["ORDERS_PER_PAGE"]);
$totalPages=ceil($totalPages);
?>
<div class="col-sm-12 col-md-10 profile-right-column">
	<div class="row">
		<div class="col-xs-12">
			<table class="order-table">
				<thead class="order-thead">
					<th>Статус</th>
					<th>Состав</th>
					<th>Номер</th>
					<th>Стоимость</th>
					<th>Доставка</th>
					<th>Оплата</th>
					<th>Дата</th>
				</thead>
				<tbody class="order-tbody">
				<?
				if(count($arResult["ORDERS"])==0){?>
				<tr><td colspan=7>Заказы отсутствуют</td></tr>
				<?}?>
				<?
				foreach($arResult["ORDERS"] as $val):?>
					<tr <?php if($arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="NP"){ echo "class='warning'"; }?><?php if($arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="F"){ echo "class='success'"; }?>>
						<td class="order-status">
							<?php 
							$linktopay="";
							if($arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="OB"||$arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="P"||$arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="N")
								$iconname="hourglass.svg"; ?>
							<?php 
							if($arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="D")
								$iconname="shipped.svg"; ?>
							<?php 
							if($arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="F"){
								$iconname="success.svg";
							}?>
							<?php 
							if($arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="NP"){
								$iconname="warning.svg";
								if($val['ORDER']['PAYED']=="N")
									$linktopay="Y";
								else{
									$arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]["NAME"]="В обработке";
									$iconname="hourglass.svg";
								}				
							}
							if($arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]['ID']=="F"){
								$iconname="success.svg";	
							}
							echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/img/icons/".$iconname); ?>
							<p><?
								echo $arResult["INFO"]["STATUS"][$val["ORDER"]["STATUS_ID"]]["NAME"]?></p>
							<? 
							if($linktopay=="Y"){
								$order_id=$val['ORDER']['ID'];
								$arOrder = CSaleOrder::GetByID($order_id);
								$dbPaySysAction = CSalePaySystemAction::GetList(
									array(),
									array(
											"ID" => $arOrder['PAY_SYSTEM_ID']
										),
									false,
									false,
									array("NAME", "ACTION_FILE", "NEW_WINDOW", "PARAMS", "ENCODING", "LOGOTIP")
								);
								$arPaySysAction = $dbPaySysAction->Fetch();
								/*if($arOrder['PAYED']=="Y")
									header("Location: /personal/orders/");*/
								if($arOrder["USER_ID"]!=$USER->GetID()){
									//CHTTP::SetStatus("404 Not Found");
									//@define("ERROR_404","Y");
								}
								CSalePaySystemAction::InitParamArrays($arOrder, $order_id, $arPaySysAction["PARAMS"]);
								$pathToAction = $_SERVER["DOCUMENT_ROOT"].$arPaySysAction["ACTION_FILE"];
								$pathToAction = str_replace("//", "/", $pathToAction);
								while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
									$pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);
								if (file_exists($pathToAction))
								{
									if (is_dir($pathToAction) && file_exists($pathToAction."/payment.php"))
										$pathToAction .= "/payment.php";

									try
									{
										//echo $pathToAction;
										include $pathToAction;
									}
									catch(\Bitrix\Main\SystemException $e)
									{
										if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
											$arResult["ERROR"][] = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
										else
											$arResult["ERROR"][] = $e->getMessage();
										//print_r($arResult["ERROR"]);
									}
								}
							}
							?>
						</td>
						<td class="order-composition">
							<?
								$bNeedComa = False;
								foreach($val["BASKET_ITEMS"] as $vval)
								{
									$mxResult = CCatalogSku::GetProductInfo(
										$vval["PRODUCT_ID"]
									);
									if (is_array($mxResult))
									{
										$product_id=$mxResult['ID'];
									}
									else{
										$product_id=$vval["PRODUCT_ID"];
									}
									$arFilter = array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ID"=>$product_id);
									$res = CIBlockElement::GetList(array(), $arFilter, false, false, array());
									if($ob = $res->GetNextElement()){
									$arFields=$ob->GetFields();
									$arProps=$ob->GetProperties();
									$discount_price="";
									$vval["SIZE"]="";
									// Выведем все свойства элемента корзины с кодом $basketID
									$db_res = CSaleBasket::GetPropsList(
											array(
													"SORT" => "ASC",
													"NAME" => "ASC"
												),
											array("BASKET_ID" => $vval['ID'])
										);
									while ($ar_res = $db_res->Fetch())
									{
										if($ar_res["NAME"]=="Размер")
											$vval["SIZE"]=$ar_res["VALUE"];
									}
                                   	$arItems = CSaleBasket::GetByID($vval['ID']);
									$price=$arItems['PRICE'];
									?>
									<div class="order-item">
										<div>
											<?$myImg= CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width'=>300, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL , true);?>
											<img src="<?=$myImg['src'];?>"/>
										</div>
										<div>
											<a href="<?=$arFields["DETAIL_PAGE_URL"];?>" class="order-name">
												<p><?=explode(" ",$arFields["NAME"])[0];?></p>
												<p><?=$arProps['CML2_MANUFACTURER']['VALUE'];?></p>
											</a>
											<p>Артикул: <?=$arProps['CML2_ARTICLE']['VALUE'];?></p>
											<p>Цена:  <? if($arItems['QUANTITY']>1){ echo intval($arItems['QUANTITY'])." x "; }?><?=number_format($price, 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></p>
											<p>Размер: <span><?=$vval["SIZE"];?></span></p>
										</div>
									</div>
									<?
								}}
							?>
						</td>
						<td><?=$val['ORDER']['ID'];?></td>
						<td><?=number_format(str_replace(" ","",str_replace(" руб.","",$val["ORDER"]["FORMATED_PRICE"])), 0, ',', ' ')?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></td>
						<td class="order-delivery">

							<p><?=explode(":",$arResult["INFO"]['DELIVERY'][$val["ORDER"]["DELIVERY_ID"]]['NAME'])[0];?></p>
							<?//=date("d.m.Y", strtotime($val['ORDER']['delivery_date_time']));
							//выборка по нескольким свойствам (например, по LOCATION и ADDRESS):
							$dbOrderProps = CSaleOrderPropsValue::GetList(
									array("SORT" => "ASC"),
									array("ORDER_ID" => $val['ORDER']['ID'], "CODE"=>array("delivery_date_time"))
								);
								while ($arOrderProps = $dbOrderProps->GetNext()):
									if(strlen($arOrderProps['VALUE'])>0)
										echo "<p class='delivery-date'>".$arOrderProps['VALUE']."<br>14:00</p>";
								endwhile;
							?>
						</td>
						<td class="order-cash"><?=$arResult["INFO"]["PAY_SYSTEM"][$val["ORDER"]["PAY_SYSTEM_ID"]]["NAME"]?></td>
						<td class="order-date"><?=date("d.m", strtotime($val['ORDER']['DATE_STATUS']));?></td>
					</tr>
				<?endforeach;?>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?
            if(strpos($arResult['NAV_STRING'], '<a style="display:none;"')!==false)
                echo strstr($arResult['NAV_STRING'], '<a style="display:none;"', true);
            else
                echo $arResult['NAV_STRING'];
            
            ?>
		</div>
	</div>
</div>