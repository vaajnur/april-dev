<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?><?
if (0 < $arResult["SECTIONS_COUNT"])
{
?>
<?
	$GLOBALS['CATEGORIES_ARR']=array();
	switch ($arParams['VIEW_MODE'])
	{
		case 'LIST':
			$intCurrentDepth = 1;
			$boolFirst = true;
			$countr=0;
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				if($arSection['DEPTH_LEVEL']==1){
					$GLOBALS['CATEGORIES_ARR'][$countr]['TEXT']=$arSection['NAME'];
					$GLOBALS['CATEGORIES_ARR'][$countr]['ID']=$arSection['ID'];
					$parent1=$arSection['NAME'];
					$prev=1;
				}
				if($arSection['DEPTH_LEVEL']==2){
					$GLOBALS['CATEGORIES_ARR'][$countr]['TEXT']=$parent1."->".$arSection['NAME'];
					$GLOBALS['CATEGORIES_ARR'][$countr]['ID']=$arSection['ID'];
					$parent2=$arSection['NAME'];
					$prev=2;
				}
				if($arSection['DEPTH_LEVEL']==3){
					$GLOBALS['CATEGORIES_ARR'][$countr]['TEXT']=$parent1."->".$parent2."->".$arSection['NAME'];
					$GLOBALS['CATEGORIES_ARR'][$countr]['ID']=$arSection['ID'];
					$prev=3;
				}
				$countr++; 
				
			}
			unset($arSection);
			break;
	}
}
?>