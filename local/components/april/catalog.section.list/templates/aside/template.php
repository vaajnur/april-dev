<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

/*$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));*/

?><?
if (0 < $arResult["SECTIONS_COUNT"])
{
?>

<div class="col-sm-3 col-md-2 catalog-menu"><aside>
<?
$GLOBALS['podcategories']=array();
	switch ($arParams['VIEW_MODE'])
	{
		case 'LIST':
			$intCurrentDepth = 1;
			$boolFirst = true;
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
                $GLOBALS[$arParams['FILTER_NAME']];
                $arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'],"SECTION_ID"=>$arSection['ID'],"ACTIVE"=>"Y","INCLUDE_SUBSECTIONS"=>"Y");
                $arFilter = array_merge($arFilter, $GLOBALS[$arParams['FILTER_NAME']]);
                
                $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
                $count=0;
                while($ob = $res->GetNextElement())
                {
                    $count++;
                }
                if($count==0){
                    continue;
                }
				$rsParentSection = CIBlockSection::GetByID($arSection['ID']);
				if ($arParentSection = $rsParentSection->GetNext())
				{
				   $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL'],
				   "ACTIVE"=>"Y"); // выберет потомков без учета активности
				   $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
					$sections_count=0;
				   while ($arSect = $rsSect->GetNext())
				   {
					   $arSelect = Array("ID", "NAME");
						$arFilter = Array("SECTION_ID"=>$arSect['ID'],"ACTIVE"=>"Y","INCLUDE_SUBSECTIONS" => "Y");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
						$activeElements=0;
						if($ob = $res->GetNextElement())
						{
						 $sections_count=1;
						}
				   }
				}
                if (strstr($_SERVER['REQUEST_URI'], $arSection['CODE'])&&!strstr($_SERVER['REQUEST_URI'], "dizaynery")) {
					$class=" active";
					if($sections_count>0)
						$class.=" parent-item";
					$dich=true;
				}
                else{
                    $dich=false;
					$class="";
					if($sections_count>0)
						$class.=" parent-item";
                }
                if($arSection['CODE']=="sertifikaty") continue;
                if($arSection['DEPTH_LEVEL']==2&&$prev==2){
					echo "</div><div class='item{$class}'>";
				}
				if($arSection['DEPTH_LEVEL']==1&&$prev==2){
					echo "</div>";
				}
				if($arSection['DEPTH_LEVEL']==2&&$prev==1){
                    
					echo "<div class='item{$class}'>";
				}
				if($arSection['DEPTH_LEVEL']==1&&$prev==3){
					echo "</ul></div>";
				}
				if($arSection['DEPTH_LEVEL']==2&&$prev==3){
					echo "</ul></div><div class='item{$class}'>";
				}
				if($arSection['DEPTH_LEVEL']==3&&$prev==2){
					echo "<ul>";
				}
				if($arSection['DEPTH_LEVEL']==1){
					echo '<p class="h1 header">'.$arSection['NAME']."</p>";
					$parent1=$arSection['CODE'];
					$prev=1;
				}
				if($arSection['DEPTH_LEVEL']==2){
					 if (strstr($_SERVER['REQUEST_URI'], $arSection['CODE'])&&!strstr($_SERVER['REQUEST_URI'], "dizaynery")) {
						$class=" active";
                         $style="display:none;";
                         $stylea="";
						$dich=true;
					}
					else{
						$dich=false;
						$class="";
                         $style="";
                         $stylea="display:none;";
					}
                    $arSelect = Array("ID", "NAME");
                    $arFilter = Array("SECTION_ID"=>$arSection['ID'],"ACTIVE"=>"Y","INCLUDE_SUBSECTIONS" => "Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
                    $activeElements=0;
                    if($ob = $res->GetNextElement())
                    {
                     $activeElements=1;
                    }
                    $arFilter = Array(
                        "IBLOCK_ID"=>CATALOG_IBLOCK_ID,
						"SECTION_ID"=>$arSection['ID'],
						"ACTIVE"=>"Y"
                        );

                    // без подразделов
                    if(CIBlockSection::GetCount($arFilter)==0){
                        // $class=" active";
                         // $style="display:none;";
                         // $stylea="";
						$dich=true;
                    }
                    if($activeElements>0)
					   echo "<p style='".$stylea."' class='h2".$class."'>".$arSection['NAME']."</p><p style='".$style."' class='h2".$class." witha'><a href='/".$parent1."/".$arSection['CODE']."/'>".$arSection['NAME']."</a></p>";
					$parent2=$arSection['CODE'];
					$prev=2;
				}
				if($arSection['DEPTH_LEVEL']==3){
					 if (strstr($_SERVER['REQUEST_URI'], $parent2."/".$arSection['CODE'])) {
						$class=" active";
					}
					else{
						$class="";
					}
                    $arSelect = Array("ID", "NAME");
                    $arFilter = Array("SECTION_ID"=>$arSection['ID'],"ACTIVE"=>"Y","INCLUDE_SUBSECTIONS" => "Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
                    $activeElements=0;
                    if($ob = $res->GetNextElement())
                    {
                     $activeElements=1;
                    }
                    if($activeElements>0)
					   echo "<li class='".$class."'><a class='thirdlvl' href='/".$parent1."/".$parent2."/".$arSection['CODE']."/'>".$arSection['NAME']."</a></li>";
					if($dich){
						array_push($GLOBALS['podcategories'],"<li class='am-item ".$class."'><a href='/".$parent1."/".$parent2."/".$arSection['CODE']."/'>".$arSection['NAME']."</a></li>");
					}
					$prev=3;
				}
			}
			unset($arSection);
			if($prev==3){
				echo "</ul></div>"; 
			}
			if($prev==2){
				echo "</ul></div>";
			}
           
			break;
	}
?>
</aside></div>
    
<?
	echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
}
?>
<script>
$( ".am-item ul" ).each(function( index ) {
  if($(this).children().length==0){
	  //убираем пустые списки категории с пустыми подкатегориями
	  $(this).remove();
  }
	  
});
/*$( ".thirdlvl" ).click(function() {
	var el=$(this);
	var href="";
	$( this ).addClass("loading");
	if ( $( this ).parent().hasClass("active") ) {
		href=$( this ).parent().parent().parent().find("p a").attr("href");
	}
	else{
		href=$(this).attr("href");
	}
	window.history.replaceState(null, null, href);
	   $.ajax({
		 url: href,
		 type: "POST",
		 data:{
			 AJAX3LVL: 'Y'                  
			 }
		 }).done(function(html){
			 $(".thirdlvl").removeClass("loading");
			if ( el.parent().hasClass("active") ) {
				$(".thirdlvl").parent().removeClass("active");
			}
			else{
				$(".thirdlvl").parent().removeClass("active");
				$(".am-item").removeClass("active");
				el.parent().addClass("active");
				el.parent().parent().parent().addClass("active");
			}
			 var matches = html.match(/<title>(.*?)<\/title>/);
			 matches[0]=matches[0].replace("<title>", "");
			 matches[0]=matches[0].replace("</title>", "");
				document.title = matches[0];
				$(".catalog").html("");	
				$(".category_banner").remove();
				$(".catalog").append(html);
				$(".category_banner").remove();
				 var $elem = $('<div>').html(html);
				 var $elem = $elem.find('.category_banner');
                 jQuery("#main").append($elem);  
				$('.catalog-body').masonry({
					itemSelector: '.catalog-item',
					columnWidth: '.catalog-sizer',
					gutter: '.gutter-sizer',
					percentPosition: true
				});
				//lazy load каталог
				if($(".lazy-catalog-item").length>0){
					$('.lazy-catalog-item').lazy();
				}
		   });
	
	
  return false;
});*/
</script>