<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<form id="subscribe<?=$arParams['CODE_FORM'];?>" method="post" action="<?=$arResult["FORM_ACTION"]?>">
	<div class="input">
		<input type="hidden" name="do_subscribe" value="Y">
		<input type="hidden" name="rubric_id" value="<?=$arParams['RUBRIC_ID'];?>">
		<input type="text" id="popup_sub" class="emaill" name="email" value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>"  required/>
		<span>E-mail:</span>
		<p class="message">Не правильно введен E-mail</p>
	</div>
	<button>Подписаться</button>
</form>
<script>
$("form#subscribe<?=$arParams['CODE_FORM'];?>").submit(function(){
	$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Загрузка...");
        $.ajax({
            type: "POST",
            url: "<?=$arResult["FORM_ACTION"]?>",
            data:$(this).serialize(),
             success: function(response){
				 //console.log(response);
				if(response!="ok"){
					$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписаться");
					$("#subscribe<?=$arParams['CODE_FORM'];?> .input").addClass("error");
				}
				else{
					yaCounter45391296.reachGoal('subscribe');
					$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписка оформлена");
					$("#subscribe<?=$arParams['CODE_FORM'];?> .input").removeClass("error");
					$("#subscribe<?=$arParams['CODE_FORM'];?>").addClass("success");
					$(".popup.new-popup h2").html('Спасибо за подписку!');
					$(".popup").hide();
					$(".popup.new-popup").fadeIn();
					setTimeout(function() {
                        $(".popup.new-popup").hide();
					}, 20000);
					setTimeout(function() {
                        if($( ".popup.firsttime" ).hasClass( "opened_popup" )){
                            $(".popup.firsttime").fadeOut().removeClass('opened_popup');
                        }
						$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").val("");
						$("#subscribe<?=$arParams['CODE_FORM'];?>").removeClass("success");
						$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписаться");
					}, 5000);
				}		
			}
        });
		return false;
     });
</script>
