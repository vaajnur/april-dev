<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="subscribe-footer">
	<div class="subscribe-footer--description">Подпишитесь на нашу E-mail рассылку</div>
	<form class="group" id="subscribe<?=$arParams['CODE_FORM'];?>" method="post" action="<?=$arResult["FORM_ACTION"]?>">
		<input type="text" name="email" placeholder="E-mail">
		<input type="hidden" name="rubric_id" value="<?=$arParams['RUBRIC_ID'];?>">
		<input type="hidden" name="do_subscribe" value="Y">
		<span class="event-massage">Некорректный E-mail</span>
		<button class="btn btn--subscribe">Подписаться</button>
	</form>
</div>
<script>
$( document ).ready(function() {
    $("form#subscribe<?=$arParams['CODE_FORM'];?>").submit(function(){
		$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Загрузка...");
			$.ajax({
				type: "POST",
				url: "<?=$arResult["FORM_ACTION"]?>",
				data:$(this).serialize(),
				 success: function(response){
					if(response!="ok"){
						$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписаться");
						$(".group").addClass("error");
						$(".event-massage").text("Неверно введен адрес почты");
					}
					else{
						yaCounter45391296.reachGoal('subscribe');
						$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписка оформлена");
						$(".group").removeClass("error");
						$(".group").addClass("success");
						$(".popup.new-popup h2").html('Спасибо за подписку!');
						$(".popup.new-popup").fadeIn();
						setTimeout(function() {
							$(".popup.new-popup").hide();
						}, 20000);
						$(".event-massage").text("Подписка успешно оформлена");
						setTimeout(function() {
							$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").val("");
							$(".group").removeClass("success");
							$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписаться");
						}, 5000);
					}		
				}
			});
			return false;
     });
});

</script>
