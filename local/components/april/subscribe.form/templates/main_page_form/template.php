<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?><div class="form-tile">
	<div class="logotype">
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 767.2 336.8" style="enable-background:new 0 0 767.2 336.8;" xml:space="preserve">
			<g>
	<path d="M502.2,13.5V0h-83.6v2.3c4.9,2.8,5.8,8.1,5.8,13.8v320.7l77.9,0v-19.3h-2.3c-2.8,4.9-8.1,5.8-13.8,5.8l0,0l-48.2,0l0-188.1
		h54.9v-13.5l-54.9,0V13.5H502.2z"></path>
	<path d="M732.9,121.6h-35.7V0h-19.3v2.3c4.9,2.8,5.8,8.1,5.8,13.8l0,0v320.7l49.2,0c19,0,34.4-15.4,34.4-34.4V156
		C767.2,137,751.8,121.6,732.9,121.6z M753.7,302.4c0,11.5-9.3,20.8-20.8,20.8l-35.7,0l0-188.1l35.7,0c11.5,0,20.8,9.3,20.8,20.8
		V302.4z"></path>
	<path d="M96.7,320.7l0,0.1L79.8,0H11.1v2.3c4.7,2.7,6.1,6.9,5.7,13.3L0,336.8h20.2v-2.3c-4.9-2.8-6.1-8.1-5.8-13.8l0,0.1l5.5-105.6
		l57.6,0l6.4,121.6h19.3v-2.3C98.4,331.7,97,326.3,96.7,320.7z M20.6,201.6l9.9-188.1l36.5,0l9.9,188.1H20.6z"></path>
	<path d="M237.7,320.7L237.7,320.7V0h-97.8v2.3c4.9,2.8,5.8,8.1,5.8,13.8l0,0l0,320.7h19.3v-2.3c-4.9-2.8-5.8-8.1-5.8-13.8V13.5h65
		l0,323.3h19.3v-2.3C238.5,331.7,237.7,326.4,237.7,320.7z"></path>
	<path d="M342.1,0h-54.9v2.3c4.9,2.8,5.8,8.1,5.8,13.8v320.7h19.3v-2.3c-4.9-2.8-5.8-8.1-5.8-13.8V215.2h35.7
		c19,0,34.4-15.4,34.4-34.4V34.4C376.5,15.4,361.1,0,342.1,0z M363,180.8c0,11.5-9.3,20.8-20.8,20.8l-35.7,0l0-188.1l35.7,0
		c11.5,0,20.8,9.3,20.8,20.8V180.8z"></path>
	<path d="M636.5,320.7l0,0.1L619.7,0h-68.7v2.3c4.7,2.7,6.1,6.9,5.7,13.3l-16.8,321.2H560v-2.3c-4.9-2.8-6.1-8.1-5.8-13.8l0,0.1
		l16.1-307.2h36.5l16.9,323.3h19.3v-2.3C638.3,331.7,636.8,326.3,636.5,320.7z"></path>
			</g>
		</svg>
	</div>
	<p>Хотите быть в курсе<br>всех НОВИНОК, АКЦИЙ,<br>и получить доступ<br>к закрытым распродажам?</p>
	<p>Подпишитесь на нашу E-mail рассылку </p>
	<form class="group" id="subscribe<?=$arParams['CODE_FORM'];?>" method="post" action="<?=$arResult["FORM_ACTION"]?>">
		<input type="text" name="email" placeholder="E-mail"  value="<?=$arResult["EMAIL"]?>">
		<input type="hidden" name="rubric_id" value="<?=$arParams['RUBRIC_ID'];?>">
		<input type="hidden" name="do_subscribe" value="Y">
		<span class="event-massage">Неверно введен адрес почты</span>
		<button class="btn">ПОДПИСАТЬСЯ</button>
	</form>
</div>
<script>
$( document ).ready(function() {
    $("form#subscribe<?=$arParams['CODE_FORM'];?>").submit(function(){
		$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Загрузка...");
			$.ajax({
				type: "POST",
				url: "<?=$arResult["FORM_ACTION"]?>",
				data:$(this).serialize(),
				 success: function(response){
					if(response!="ok"){
						$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("ПОДПИСАТЬСЯ");
						$(".form-tile").addClass("error");
						$(".event-massage").text("Неверно введен адрес почты");
					}
					else{
						yaCounter45391296.reachGoal('subscribe');
						$(".popup.new-popup h2").html('Спасибо за подписку!');
						$(".popup.new-popup").fadeIn();
						setTimeout(function() {
							$(".popup.new-popup").hide();
						}, 20000);
						$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("Подписка оформлена");
						$(".form-tile").removeClass("error");
						$(".form-tile").addClass("success");
						$(".event-massage").text("Подписка успешно оформлена");
						setTimeout(function() {
							$("#subscribe<?=$arParams['CODE_FORM'];?> input[name='email']").val("");
							$(".form-tile").removeClass("success");
							$("#subscribe<?=$arParams['CODE_FORM'];?> button").text("ПОДПИСАТЬСЯ");
						}, 5000);
					}		
				}
			});
			return false;
     });
});

</script>