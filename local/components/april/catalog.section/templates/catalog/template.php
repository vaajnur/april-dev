<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */
if (!CModule::IncludeModule("sale")) return;
$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');

if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

$showTopPager = false;
$showBottomPager = false;
$showLazyLoad = false;

if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showTopPager = $arParams['DISPLAY_TOP_PAGER'];
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}


?>

<?
$index=0;
$tmp_script="";

//забиваем просмотренную категорию подписчику
if($arResult['ID']>0){
    if(isset($_COOKIE['BITRIX_SM_USER_EMAIL'])){
        $results = $DB->Query("SELECT * FROM `b_email_marketing_custom` WHERE `email`='".$DB->ForSQL($_COOKIE['BITRIX_SM_USER_EMAIL'])."'"); 
        while ($row = $results->Fetch())
        {
            $row['viewed_categories']=$row['viewed_categories'].$arResult['ID'].";";
            $arAr = Array(
                "viewed_categories"    => "'".$row['viewed_categories']."'",
             );
            $DB->Update("b_email_marketing_custom", $arAr, "WHERE `email`='".$DB->ForSQL($_COOKIE['BITRIX_SM_USER_EMAIL'])."'", $err_mess.__LINE__);
        }
    }
}
$tmp_script="";
shuffle($arResult["ITEMS"]);
foreach($arResult["ITEMS"] as $arElement):
$index++;
$nav = CIBlockSection::GetNavChain($arElement["IBLOCK_ID"], $arElement["IBLOCK_SECTION_ID"]);
$category="";
while ($arNav=$nav->GetNext()):
	$category.=$arNav["NAME"].'/';
endwhile;
?>
    <div class="item grid-item">
		<div class="slider-container">
			<div class="owlslidecontent">
			<?
			$imgs=array();
			$renderImage = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], Array("width" => 1000, "height" => 1000), BX_RESIZE_IMAGE_PROPORTIONAL, false);
			$imgs[]=$renderImage['src'];
			foreach($arElement["DISPLAY_PROPERTIES"]['MORE_PHOTO']['VALUE'] as $k=>$v)
			{
				$myImg= CFile::ResizeImageGet($v, array('width'=>1000, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL , false); 
				$arFile = CFile::GetFileArray($v);
				if(file_exists($_SERVER["DOCUMENT_ROOT"].$arFile['SRC'])){
					//optimize_image($myImg["src"]);
					$imgs[]=$myImg['src'];
				}
			}
			shuffle($imgs);
			foreach($imgs as $img){?>
			<div class="slide-item">
				<? 
				//optimize_image($renderImage['src']);
				if(strlen($arElement['DISPLAY_PROPERTIES']['RAZMER_SKIDKI']['VALUE'])>0){?><div class="status">SALE</div><?}?>
				<? if(strtotime($arElement['DATE_CREATE'])>=strtotime(date("d.m.Y"))-5184000&&strlen($arElement['DISPLAY_PROPERTIES']['RAZMER_SKIDKI']['VALUE'])==0){?><div class="status">NEW</div><?}?>
				<a href="<?=$arElement["DETAIL_PAGE_URL"];?>" class="item-img">
					<img class="lazy-catalog-item" src="<? if(isset($_REQUEST['is_ajax'])){ echo strlen($img)>0?$img:"/img/prod-sample.jpg";  } else{ echo "/img/lazyload.gif"; }?>" data-src="<?=strlen($img)>0?$img:"/img/prod-sample.jpg";?>" alt="<?=explode(" ",$arElement['NAME'])[0]." ".$arElement['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE']." ".$arElement['DISPLAY_PROPERTIES']['CML2_ARTICLE']['VALUE'];?>">
				</a>
			</div>
			<?
			}
			?>
			
		</div>
			<a href="<?=$arElement["DETAIL_PAGE_URL"];?>">
				<span><?=explode(" ",$arElement["NAME"])[0]?></span>
				<span><?=$arElement['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'];?></span>
			</a>
            <?
			$res_offers = CCatalogSKU::getOffersList(
				$arElement['ID'],
				$arElement['IBLOCK_ID'],
				$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
				$fields = array("PROPERTY_RAZMER","PROPERTY_TSVET","NAME","PRICE"),
				$propertyFilter = array()
			);
			foreach($res_offers[$arElement['ID']] as $good){
				$ar_price = GetCatalogProductPrice($good["ID"], 1);
				$offer_color=$good['PROPERTY_TSVET_VALUE'];
				$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
					$ar_price["ID"],
					$USER->GetUserGroupArray(),
					"N",
					SITE_ID
				);
				$discountPrice = CCatalogProduct::CountPriceWithDiscount(
						$ar_price["PRICE"],
						$ar_price["CURRENCY"],
						$arDiscounts
					);
				$new_price=$discountPrice; 
				if($new_price!=$ar_price["PRICE"]){
					$ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
					$ar_price["PRICE"] = $new_price;
				} 
			}

			?>
			<?
                $tmp_script.="{  'id': '".$arElement["ID"]."',
				  'name': '".explode(" ",$arElement['NAME'])[0]." ".$arElement['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE']." ".$arElement['DISPLAY_PROPERTIES']['CML2_ARTICLE']['VALUE']."', 
                  'list': '".$_SERVER['REQUEST_URI']."',
                  'price': '".number_format($ar_price['PRICE'],0,".","")."',		// Цена товара	
                  'brand': '".$arElement["PROPERTIES"]["CML2_MANUFACTURER"]["VALUE"]."',
                  'category': '".trim($category,"/")."',
                  'position': ".$index.",
                  'variant': '".$offer_color."'
               },";
            ?>
			<div class="options">
				<div class="price"> 
					<? if(strlen($ar_price["DISCOUNT_PRICE"])>0){?>
					<span class="old"><?=number_format($ar_price["DISCOUNT_PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
					<?}?>
					<span><?=number_format($ar_price['PRICE'], 0, ',', ' ');?> <span class="rub">i</span></span>
				</div>
				<button product-id="<?=$arElement['ID'];?>" class="btn action-quick-look">Быстрый просмотр</button>
				<button product-id="<?=$arElement['ID'];?>" class="btn action-to-cart">В корзину</button>
			</div>
		</div>
	</div>
<?endforeach;?>
<script>
  dataLayer.push({
    'ecommerce': {                     
      'impressions': 
      [
          <?=$tmp_script;?>
      ]
    }
  });
</script> 
<?$GLOBALS["SEO_TEXT"]=$arResult['DESCRIPTION']; ?>
<div class="after-ajax-insert" style="display:none;"></div>
<? echo strstr($arResult['NAV_STRING'], '<a style="display:none;"');?>
<?
$arResult['NAV_STRING']=str_replace("?is_ajax=y","",$arResult['NAV_STRING']);
if ($showBottomPager)
{
	?>
		<!-- pagination-container -->
        <script>
       $(document).ready(function () {
           <? if(strpos($arResult['NAV_STRING'], '<a style="display:none;"')!==false){?>
           $(".for-page").html(<?=json_encode(strstr($arResult['NAV_STRING'], '<a style="display:none;"', true));?>);
           <?} else{ ?>
           $(".for-page").html(<?=json_encode($arResult['NAV_STRING']);?>);
           <?}?>
           $(".title-footer .pagination-list").html($(".for-page .pagination-list").html());
       });
       </script>
		<!-- pagination-container -->
	<?
}

$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, 'catalog.section');
$signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.section');
?>
