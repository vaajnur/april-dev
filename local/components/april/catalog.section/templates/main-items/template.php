<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="non-catalog-items">
<?
foreach($arResult["ITEMS"] as $arElement):
?>
   <div class="item">
		<div class="slider-container">
            <? if(strpos($_SERVER['REQUEST_URI'],"personal/wishlist/")!==false){ ?>
                <button product_id="<?=$arElement['ID'];?>" class="wish-select"><!--?xml version="1.0" encoding="iso-8859-1"?-->
                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->

                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 78.369 78.369" style="enable-background:new 0 0 78.369 78.369;" xml:space="preserve">
                <g>
                    <path d="M78.049,19.015L29.458,67.606c-0.428,0.428-1.121,0.428-1.548,0L0.32,40.015c-0.427-0.426-0.427-1.119,0-1.547l6.704-6.704   c0.428-0.427,1.121-0.427,1.548,0l20.113,20.112l41.113-41.113c0.429-0.427,1.12-0.427,1.548,0l6.703,6.704   C78.477,17.894,78.477,18.586,78.049,19.015z" fill="#262626"></path>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                <g>
                </g>
                </svg>
                </button>
                <button onclick="delwish(<?=$arElement['ID'];?>);" class="wishdel wishdel<?=$arElement['ID'];?>"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 232.3 239.9" style="enable-background:new 0 0 232.3 239.9;" xml:space="preserve"><g><path d="M130.5,0.7v105.2h101.2v26.9H130.5v106.1h-28.7V132.8H0.7v-26.9h101.2V0.7H130.5z"></path></g></svg></button>
            <? } ?>
            <? 
            $renderImage = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], Array("width" => 1001, "height" => 1001), BX_RESIZE_IMAGE_PROPORTIONAL, false);
           // optimize_image($renderImage['src']);
			if(strlen($arElement['DISPLAY_PROPERTIES']['RAZMER_SKIDKI']['VALUE'])>0){?><div class="status">SALE</div><?}?>
			<? if(strtotime($arElement['DATE_CREATE'])>=strtotime(date("d.m.Y"))-5184000&&strlen($arElement['DISPLAY_PROPERTIES']['RAZMER_SKIDKI']['VALUE'])==0){?><div class="status">NEW</div><?}?>
			<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="item-img"><img  class="" src="<? /*if(isset($_REQUEST['q'])){*/ echo strlen($arElement["DETAIL_PICTURE"]['SRC'])>0?$renderImage['src']:"/img/prod-sample.jpg";  //}?>" data-src="<?=strlen($arElement["DETAIL_PICTURE"]['SRC'])>0?$renderImage['src']:"/img/prod-sample.jpg";?>" alt="<?=explode(" ",$arElement['NAME'])[0]." ".$arElement['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE']." ".$arElement['DISPLAY_PROPERTIES']['CML2_ARTICLE']['VALUE'];?>"></a>
			<a href="<?=$arElement["DETAIL_PAGE_URL"]?>">
							<span><?=explode(" ",$arElement["NAME"])[0]?></span>
							<span><?=$arElement['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'];?></span>
						</a>
			<div class="options">
				<div class="price">
					<?
					$res_offers = CCatalogSKU::getOffersList(
						$arElement['ID'],
						$arElement['IBLOCK_ID'],
						$skuFilter = array(),
						$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
						$propertyFilter = array()
					);
					foreach($res_offers[$arElement['ID']] as $good){
                        $ar_price = GetCatalogProductPrice($good["ID"], 1); 
                        $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                            $ar_price["ID"],
                            $USER->GetUserGroupArray(),
                            "N",
                            SITE_ID
                        );
                        $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                                $ar_price["PRICE"],
                                $ar_price["CURRENCY"],
                                $arDiscounts
                            );
                        $new_price=$discountPrice; 
                        if($new_price!=$ar_price["PRICE"]){
                            $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
                            $ar_price["PRICE"] = $new_price;
                        }
					}

					?>
					<? if(strlen($ar_price["DISCOUNT_PRICE"])>0){?>
					<span class="old"><?=number_format($ar_price["DISCOUNT_PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
					<?}?>
					<span><?=number_format($ar_price['PRICE'], 0, ',', ' ');?> ₽</span>
				</div>
				<? if($arParams['NOT_FAST_SHOW']!="Y"){?><button product-id="<?=$arElement['ID'];?>" class="btn action-quick-look">Быстрый просмотр</button><?}?>
				<button product-id="<?=$arElement['ID'];?>" class="btn action-to-cart">В корзину</button>
			</div>
		</div>
	</div>
<?endforeach;?>
</div>
