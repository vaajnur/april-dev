<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

					
						<? if(strpos($arResult['NAV_STRING'], '<a style="display:none;"')!==false)
							echo strstr($arResult['NAV_STRING'], '<a style="display:none;"', true);
						else
							echo $arResult['NAV_STRING'];?>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="blog-section">
		<div class="container-fluid">
			<div class="row">
			<?
			$index=0;
			if(isset($_REQUEST['is_ajax'])){ echo "</div></div>"; $APPLICATION->RestartBuffer(); }
			if(count($arResult["ITEMS"])==0){
				echo "<p>Записи отсутствуют</p>";
			}
			foreach($arResult["ITEMS"] as $arItem):
				$index++;
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				if($index==5){?>
				<div class="col-xs-12 col-sm-4 blog-article blog-subscribe">
						<div class="box-row">
							<div class="subscribe-header">
								<div class="subscribe-logo">
									<?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/apr-logo.svg"); ?>
								</div>
							</div>
							<div class="subscribe-body">
								<p class="subscribe-body--description">
									Хотите быть вкурсе всех <span>новинок, акций</span> и получить доступ к закрытым распродажам?
								</p>
							</div>
							<?$APPLICATION->IncludeComponent(
								"april:subscribe.form",
								"blog_list_form",
								Array(
									"CACHE_TIME" => "3600",
									"CACHE_TYPE" => "A",
									"CODE_FORM" => "BLOG",
									"PAGE" => "/ajax/subscribe.php",
									"RUBRIC_ID" => 1,
									"SHOW_HIDDEN" => "Y",
									"USE_PERSONALIZATION" => "Y"
								)
							);?>
						</div>
				</div>
				<?}
				?>
				<div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="col-xs-12 col-sm-4 blog-article">
					<div class="box-row">
						<div class="article-header">
							<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
								<div class="article-img">
									<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="article-image">
								</div>
								<h2 class="article-title"><?echo $arItem["NAME"]?></h2>
							</a>
						</div>
						<div class="article-body">
							<p><?echo $arItem["PREVIEW_TEXT"];?></p>
						</div>
						<div class="article-footer">
							<span class="article-post-date"><?=explode(" ",$arItem['DATE_CREATE'])[0];?></span>
						</div>
					</div>
				</div>
								
			<?endforeach;
			if(isset($_REQUEST['is_ajax'])){  die(); } 
			echo "<div style='display:none'>".$arResult['NAV_STRING']."</div>";
				?>
			</div>
		</div>
	</section>
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<?
                  if(strpos($arResult['NAV_STRING'], '<a style="display:none;"')!==false)
                        echo strstr($arResult['NAV_STRING'], '<a style="display:none;"', true);
                    else
                        echo $arResult['NAV_STRING'];
                    
                    ?>
				</div>
			</div>
		</div>
	</section>
	<? ?>
</main>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
