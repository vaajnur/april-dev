<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<? if(!isset($_REQUEST['AJAX'])){?>
<div class="row instashop-items <? if(isset($_REQUEST['AJAX'])) echo "loading";?>">
<?}?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	?>
	<div id="item<?echo $arItem["ID"]?>" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
		<div class="instashop-item lazy-insta-item" style="background-image:url(/img/lazyload.gif)" data-src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
			<a target="_blank" rel="nofollow" href="<?if($arItem['DISPLAY_PROPERTIES']['good']['VALUE']>0){echo "#";}else{echo $arItem['DISPLAY_PROPERTIES']['postlink']['VALUE'];}?>" class="instashop-link"></a>
			<?if($arItem['DISPLAY_PROPERTIES']['good']['VALUE']>0){
			$mxResult = CCatalogSku::GetProductInfo(
				$arItem['DISPLAY_PROPERTIES']['good']['VALUE']
				);
				if (is_array($mxResult))
				{
					$product_id=$mxResult['ID'];
				}
				else{
					$product_id=$arItem['DISPLAY_PROPERTIES']['good']['VALUE'];
				}
			$arFilter = array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ID"=>$product_id,"ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(array(), $arFilter, false, false, array());
				if($ob = $res->GetNextElement()){
					$arFields = $ob->GetFields();
					$arProps = $ob->GetProperties();
					$res_offers = CCatalogSKU::getOffersList(
						$product_id,
						CATALOG_IBLOCK_ID,
						$skuFilter = array("ID"=>$arItem['DISPLAY_PROPERTIES']['good']['VALUE']),
						$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
						$propertyFilter = array()
					);
					foreach($res_offers[$product_id] as $good){
						$counter++;
						$checked="";
						$offer_id=$good['ID'];
						$offer_size=$good['PROPERTY_RAZMER_VALUE'];
					}
					?>
					<div class="instashop-info">
						<button class="addtocart btn instashop-info--btn addtocart<?=$offer_id;?>"  product-id="<?=$offer_id;?>" size="<?=$offer_size;?>">Купить <?=$arFields['NAME'];?> <?=$arProps['CML2_MANUFACTURER']['VALUE'];?></button>
					</div>
			<?}}?>
		</div>
	</div>
<?endforeach;?>
<? if(!isset($_REQUEST['AJAX'])){?>
</div>
<?}?>
<script>
$(document).ready(function () {
	<? if(isset($_REQUEST['AJAX'])){?>$('.instashop-items').removeClass("loading");<?}?>
});
</script>
<div class="row center-xs show-more-btn" style="display:none;">
	<div class="col-xs-12">
		<?=$arResult["NAV_STRING"]?>
	</div>
</div>

