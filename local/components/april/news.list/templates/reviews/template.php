<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<? //echo"<br>".$curPage." - ".$totalPages;?>
<?if($arParams["AJAX"] != "Y"):?>
<?endif;?>
<?
// номер текущей страницы
$curPage = (isset($_REQUEST["PAGEN_1"]))?$_REQUEST["PAGEN_1"]:1;
//$count+1 потому что форма подписки прибавляет 1 элемент
$count=count($arResult["ITEMS"]);
$totalPages = ($count)/$arParams["ON_PAGE"];
$totalPages=ceil($totalPages);
$start=$arParams["ON_PAGE"]*($curPage-1);
$end=$start+$arParams["ON_PAGE"]-1;
if($_REQUEST['AJAX']=="Y")
	$end=count($arResult["ITEMS"]);
if($end>=$count){
	$GLOBALS['SHOW_AJAX']="N";
}
$GLOBALS['pagescount']=$totalPages;
if (count($arResult["ITEMS"])==0){
?>
<p class="empty-reviews">Отзывы отсутствуют.</p>
<?
}
for($is=$start;$is<=$end;$is++){
	if(isset($arResult["ITEMS"][$is])){
		$arItem=$arResult["ITEMS"][$is];
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
	<div class="review">
		<div class="review-who">
			<div class="review-image">
				<? if(strlen($arItem['PREVIEW_PICTURE']['SRC'])>0){?>
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>"/>
				<?}
				else if($arItem['CREATED_BY']>0){
					$rsUser = CUser::GetByID($arItem['CREATED_BY']);
					$arUser = $rsUser->Fetch();
					if($arUser['PERSONAL_PHOTO']>0){?>
						<img src="<?=CFile::GetPath($arUser['PERSONAL_PHOTO']);?>"/>
					<?}
				}
				else{
					echo mb_substr($arItem['NAME'],0,1);
				}?>
			</div>
			<p><?=$arItem['NAME'];?></p>
		</div>
		<div class="review-body">
			<div class="review-info">
				<div class="rating">
				<? for($i=0;$i<5;$i++){?>
					<svg class="<?if($i<intval($arItem['DISPLAY_PROPERTIES']['rating']['VALUE'])) echo "active";?>" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve"><path class="st0" d="M500.9,233c10.1-9.8,13.6-24.2,9.3-37.6c-4.4-13.4-15.7-23-29.6-25l-124-18c-5.3-0.8-9.8-4.1-12.2-8.9 L288.9,31.2c-6.2-12.6-18.9-20.5-32.9-20.5c-14.1,0-26.7,7.8-32.9,20.5l-55.4,112.3c-2.4,4.8-6.9,8.1-12.2,8.9l-124,18 c-13.9,2-25.3,11.6-29.6,25c-4.3,13.4-0.8,27.8,9.3,37.6l89.7,87.4c3.8,3.7,5.6,9.1,4.7,14.4L84.3,458.3 c-2.4,13.9,3.2,27.6,14.6,35.9c11.4,8.3,26.2,9.4,38.7,2.8l110.9-58.3c4.7-2.5,10.4-2.5,15.1,0L374.4,497c5.4,2.9,11.3,4.3,17.1,4.3 c7.6,0,15.1-2.4,21.6-7.1c11.4-8.3,17-22,14.6-35.9l-21.2-123.5c-0.9-5.3,0.9-10.6,4.7-14.4L500.9,233z"/></svg>
				<?}?>
				</div>
				<p><?=date("d.m.Y", strtotime($arItem['DATE_CREATE']));?></p>
			</div>
			<div class="review-text">
				<p><?=$arItem['PREVIEW_TEXT'];?></p>
			</div>
		</div>
	</div>
	<? /*if ($is==1){
		//выводим баннер/баннеры
		$arFilter = array("IBLOCK_ID"=>34);
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, array());
		$banners=array();
		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arProps =$ob->GetProperties();
			 $img = CFile::GetPath($arProps["background"]["VALUE"]);
			?>
			<div class="category_banner" style="background-image:url('<?=$img;?>');">
			<p class="header"><?=$arProps["head_text"]["VALUE"];?></p>
			<p><?=$arProps["text"]["VALUE"]["TEXT"];?></p>
			</div>
		<?
		}
	}*/
}}?>

