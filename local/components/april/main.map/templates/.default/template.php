<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!is_array($arResult["arMap"]) || count($arResult["arMap"]) < 1)
	return;

$arRootNode = Array();
foreach($arResult["arMap"] as $index => $arItem)
{
	if ($arItem["LEVEL"] == 0)
		$arRootNode[] = $index;
}
$allNum = count($arRootNode);
$colNum = ceil($allNum / $arParams["COL_NUM"]);

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<ul class="main-categories-list">

		<?
		$previousLevel = -1;
		$counter = 0;
		$column = 1;
		$cloth_counter=0;
		foreach($arResult["arMap"] as $index => $arItem):
			$arItem["FULL_PATH"] = htmlspecialcharsbx($arItem["FULL_PATH"], ENT_COMPAT, false);
			$arItem["NAME"] = htmlspecialcharsbx($arItem["NAME"], ENT_COMPAT, false);
			$arItem["DESCRIPTION"] = htmlspecialcharsbx($arItem["DESCRIPTION"], ENT_COMPAT, false);
		?>

			<?if ($arItem["LEVEL"] < $previousLevel):?>
				<?=str_repeat("</ul></li>", ($previousLevel - $arItem["LEVEL"]));?>
			<?endif?>


			<?if ($counter >= $colNum && $arItem["LEVEL"] == 0): 
					$allNum = $allNum-$counter;
					$colNum = ceil(($allNum) / ($arParams["COL_NUM"] > 1 ? ($arParams["COL_NUM"]-$column) : 1));
					$counter = 0;
					$column++;
			?>
				</ul><ul class="main-categories-list">
			<?endif?>

			<?if (array_key_exists($index+1, $arResult["arMap"]) && $arItem["LEVEL"] < $arResult["arMap"][$index+1]["LEVEL"]):
			
				$cloth_counter++;
				if($cloth_counter==1){?>
				</ul></div></div><div class="row"><div class="col-xs-12 col-sm-4"><ul class="main-cloth-list">
				<?}
				else{?>
				</ul></div><div class="col-xs-12 col-sm-4 no-pd-l"><ul class="main-cloth-list">
				<?}?>
				<li class="main-cloth-item"><a href="<?=$arItem["FULL_PATH"]?>"><?=$arItem["NAME"]?></a><?if ($arParams["SHOW_DESCRIPTION"] == "Y" && strlen($arItem["DESCRIPTION"]) > 0) {?><div><?=$arItem["DESCRIPTION"]?></div><?}?>
					<?php
						$url_words=explode("/",$arItem["FULL_PATH"]);
						$SECTION_ID="";
						foreach($url_words as $word){
							if(strlen($word)>0){
								$arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID,"CODE"=>$word,"IBLOCK_SECTION_ID"=>$SECTION_ID/*">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"CATALOG_AVAILABLE"=>"Y","!DETAIL_PICTURE"=>false*/);
								$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
								while ($arSection = $rsSections->GetNext())
								{
									$SECTION_ID=$arSection["ID"];
								}
							}
						}
						$arSelect_meta = Array("ID", "NAME","CODE","PROPERTY_SEO_H1","PROPERTY_TAG","PREVIEW_TEXT");
						$arFilter_meta = Array("IBLOCK_ID"=>23, "ACTIVE"=>"Y","PROPERTY_CATEGORY"=>$SECTION_ID);
						$res_meta = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter_meta, false, Array("nTopCount"=>100), $arSelect_meta);
						while($ob_meta = $res_meta->GetNextElement())
						{
							$arFields_meta = $ob_meta->GetFields();
							$arProps_meta = $ob_meta->GetProperties();
							$arFields_meta['PROPERTY_SEO_H1_VALUE'];
							$arSelect = Array("ID", "NAME","CODE");
							$arFilter = Array("IBLOCK_ID"=>22, "ACTIVE"=>"Y","ID"=>$arFields_meta['PROPERTY_TAG_VALUE']);
							$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nTopCount"=>1), $arSelect);
							echo "<ul>";
							while($ob = $res->GetNextElement())
							{
								$arFields = $ob->GetFields();
								$arSelect_tag = Array("ID", "NAME");
								$arFilter_tag = Array("SECTION_ID"=>$SECTION_ID,"ACTIVE"=>"Y","INCLUDE_SUBSECTIONS" => "Y","PROPERTY_TAGS"=>$arFields_meta['PROPERTY_TAG_VALUE']);
								$res_tag = CIBlockElement::GetList(Array(), $arFilter_tag, false, Array(), $arSelect_tag);
								$activeElements=0;
								while($ob_tag = $res_tag->GetNextElement())
								{
									$arFields_tag = $ob_tag->GetFields();
									$res_offers = CCatalogSKU::getOffersList(
										$arFields_tag['ID'],
										CATALOG_IBLOCK_ID,
										$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
										$fields = array("PROPERTY_RAZMER","PROPERTY_TSVET","NAME","PRICE"),
										$propertyFilter = array()
									);
									foreach($res_offers[$arFields_tag['ID']] as $good){
										$activeElements=1;
									}
									
								}
								if($activeElements>0){
									?><li class="cloth-categories-item-tag cloth-categories-item-tag-level-1"><a href="<?=$arItem["FULL_PATH"].$arFields['CODE']."/";?>"><?=$arFields_meta['PROPERTY_SEO_H1_VALUE'];?></a></li><?
								}
							}
							echo "</ul>";
						}
					?>
					<ul class="cloth-categories-list">

			<?else:?>
					<? if($arItem["LEVEL"]>0){?>
					<li class="cloth-categories-item  cloth-categories-item-level-2">
						<a class="cloth-categories-link" href="<?=$arItem["FULL_PATH"]?>"><?=$arItem["NAME"]?></a>
						<?if ($arParams["SHOW_DESCRIPTION"] == "Y" && strlen($arItem["DESCRIPTION"]) > 0) {?><div><?=$arItem["DESCRIPTION"]?></div><?}?>
					<?php
					$url_words=explode("/",$arItem["FULL_PATH"]);
					$SECTION_ID="";
					foreach($url_words as $word){
						if(strlen($word)>0){
							$arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID,"CODE"=>$word,"IBLOCK_SECTION_ID"=>$SECTION_ID/*">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"CATALOG_AVAILABLE"=>"Y","!DETAIL_PICTURE"=>false*/);
							$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
							while ($arSection = $rsSections->GetNext())
							{
								$SECTION_ID=$arSection["ID"];
							}
						}
					}
					$arSelect_meta = Array("ID", "NAME","CODE","PROPERTY_SEO_H1","PROPERTY_TAG","PREVIEW_TEXT");
					$arFilter_meta = Array("IBLOCK_ID"=>23, "ACTIVE"=>"Y","PROPERTY_CATEGORY"=>$SECTION_ID);
					$res_meta = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter_meta, false, Array("nTopCount"=>100), $arSelect_meta);
					while($ob_meta = $res_meta->GetNextElement())
					{
						$arFields_meta = $ob_meta->GetFields();
						$arProps_meta = $ob_meta->GetProperties();
						$arFields_meta['PROPERTY_SEO_H1_VALUE'];
						$arSelect = Array("ID", "NAME","CODE");
						$arFilter = Array("IBLOCK_ID"=>22, "ACTIVE"=>"Y","ID"=>$arFields_meta['PROPERTY_TAG_VALUE']);
						$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nTopCount"=>1), $arSelect);
						echo "<ul>";
						while($ob = $res->GetNextElement())
						{
							$arFields = $ob->GetFields();
							$arSelect_tag = Array("ID", "NAME");
							$arFilter_tag = Array("SECTION_ID"=>$SECTION_ID,"ACTIVE"=>"Y","INCLUDE_SUBSECTIONS" => "Y","PROPERTY_TAGS"=>array($arFields_meta['PROPERTY_TAG_VALUE']));
							$res_tag = CIBlockElement::GetList(Array(), $arFilter_tag, false, Array(), $arSelect_tag);
							$activeElements=0;
							while($ob_tag = $res_tag->GetNextElement())
							{
								$arFields_tag = $ob_tag->GetFields();
								$res_offers = CCatalogSKU::getOffersList(
									$arFields_tag['ID'],
									CATALOG_IBLOCK_ID,
									$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
									$fields = array("PROPERTY_RAZMER","PROPERTY_TSVET","NAME","PRICE"),
									$propertyFilter = array()
								);
								foreach($res_offers[$arFields_tag['ID']] as $good){
									$activeElements=1;
								}
							}
							if($activeElements>0){
								?><li class="cloth-categories-item-tag cloth-categories-item-tag-level-2"><a href="<?=$arItem["FULL_PATH"].$arFields['CODE']."/";?>"><?=$arFields_meta['PROPERTY_SEO_H1_VALUE'];?></a></li><?
							}
						}
						echo "</ul>";
					}
					$arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID,"SECTION_ID"=>$SECTION_ID,"ACTIVE"=>"Y"/*">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"CATALOG_AVAILABLE"=>"Y","!DETAIL_PICTURE"=>false*/);
					$rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
					echo "<ul>";
					while ($arSection = $rsSections->GetNext())
					{
						?><li class="cloth-categories-item-level cloth-categories-item-level-3"><a href="<?=$arSection["SECTION_PAGE_URL"];?>"><?=$arSection['NAME'];?></a></li><?
						$arSelect_meta = Array("ID", "NAME","CODE","PROPERTY_SEO_H1","PROPERTY_TAG","PREVIEW_TEXT");
						$arFilter_meta = Array("IBLOCK_ID"=>23, "ACTIVE"=>"Y","PROPERTY_CATEGORY"=>$arSection['ID']);
						$res_meta = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter_meta, false, Array("nTopCount"=>100), $arSelect_meta);
						while($ob_meta = $res_meta->GetNextElement())
						{
							$arFields_meta = $ob_meta->GetFields();
							$arProps_meta = $ob_meta->GetProperties();
							$arFields_meta['PROPERTY_SEO_H1_VALUE'];
							$arSelect = Array("ID", "NAME","CODE");
							$arFilter = Array("IBLOCK_ID"=>22, "ACTIVE"=>"Y","ID"=>$arFields_meta['PROPERTY_TAG_VALUE']);
							$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nTopCount"=>1), $arSelect);
							echo "<ul>";
							while($ob = $res->GetNextElement())
							{
								$arFields = $ob->GetFields();
								$arSelect_tag = Array("ID", "NAME");
								$arFilter_tag = Array("SECTION_ID"=>$arSection['ID'],"ACTIVE"=>"Y","INCLUDE_SUBSECTIONS" => "Y","PROPERTY_TAGS"=>array($arFields_meta['PROPERTY_TAG_VALUE']));
								$res_tag = CIBlockElement::GetList(Array(), $arFilter_tag, false, Array(), $arSelect_tag);
								$activeElements=0;
								while($ob_tag = $res_tag->GetNextElement())
								{
									$arFields_tag = $ob_tag->GetFields();
									$res_offers = CCatalogSKU::getOffersList(
										$arFields_tag['ID'],
										CATALOG_IBLOCK_ID,
										$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
										$fields = array("PROPERTY_RAZMER","PROPERTY_TSVET","NAME","PRICE"),
										$propertyFilter = array()
									);
									foreach($res_offers[$arFields_tag['ID']] as $good){
										$activeElements=1;
									}
								}
								if($activeElements>0){
									?><li class="cloth-categories-item-tag cloth-categories-item-tag-level-3"><a href="<?=$arSection["SECTION_PAGE_URL"].$arFields['CODE']."/";?>"><?=$arFields_meta['PROPERTY_SEO_H1_VALUE'];?></a></li><?
								}
							}
							echo "</ul>";
						}
					}
					echo "</ul>";
					
					?>
					</li>
					<?} else{
						if($arItem['IS_DIR']=="Y"){?>
						<li class="main-categories-item"><a href="<?=$arItem["FULL_PATH"]?>" class="main-categories-link"><?=$arItem["NAME"]?></a></li>
						<?} else{?>
					</ul></div><div class="col-xs-12 col-sm-4 no-pd-l"><ul class="main-cloth-list">
							<li class="main-cloth-item"><a href="<?=$arItem["FULL_PATH"]?>"><?=$arItem["NAME"]?></a></li>
						<?}?>
					<?}?>

			<?endif?>


			<?
				$previousLevel = $arItem["LEVEL"];
				if($arItem["LEVEL"] == 0)
					$counter++;
			?>

		<?endforeach?>

		<?if ($previousLevel > 1)://close last item tags?>
			<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
		<?endif?>

			</ul>
		</div>
	</div>
</div>