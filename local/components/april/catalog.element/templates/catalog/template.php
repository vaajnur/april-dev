<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Catalog\CatalogViewedProductTable as CatalogViewedProductTable;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?><?
$product_id = $arResult['ID'];
$user_id = CSaleBasket::GetBasketUserID();
if($user_id  > 0)
{
	CatalogViewedProductTable::refresh($product_id, $user_id);
}

$countwatches=0;
$results = $DB->Query("SELECT * FROM `b_view_today` WHERE `goodid`='".$arResult["ID"]."' AND date='".date("d.m.Y")."'"); 
//проверка существования записи с сегодняшней датой и этим товаром
while ($row = $results->Fetch())
{
	$countwatches=$row['count'];
	$arAr = Array(
		"count"    => "".($row['count']+1)."",
	 );
	 if($_SESSION['watched_'.$arResult["ID"]]!=1){
		$DB->Update("b_view_today", $arAr, "WHERE id=".$row['id']."", $err_mess.__LINE__);
	 }
	$_COOKIE["viewed"+$arResult["ID"]]="ok";
	$_SESSION['watched_'.$arResult["ID"]]=1;
	$find=true;
	$today=true;
}
if(!$today){
	$results = $DB->Query("SELECT * FROM `b_view_today` WHERE `goodid`='".$arResult["ID"]."'"); 
	//проверка существования записи с этим товаром
	while ($row = $results->Fetch())
	{
		$arAr = Array(
			"count" =>1,
			"date"	=> "'".date("d.m.Y")."'"
		 );
		 if($_SESSION['watched_'.$arResult["ID"]]!=1){
			$DB->Update("b_view_today", $arAr, "WHERE id=".$row['id']."", $err_mess.__LINE__);
		 }
		$_SESSION['watched_'.$arResult["ID"]]=1;
		$find=true;
	}
}
//если записи в бд нет, она создается
if(!$find){
	$arFields = array(
			"date"                    => "'".date("d.m.Y")."'",
			"goodid"                 => "'".$arResult["ID"]."'",
			"count"                 => "".(1)."",
			);
	$ID = $DB->Insert("b_view_today", $arFields, $err_mess.__LINE__);
}
//если подписан и просмотрел товары записываем
if(isset($_COOKIE['BITRIX_SM_USER_EMAIL'])&&$arResult['ID']>0){
    $results = $DB->Query("SELECT * FROM `b_email_marketing_custom` WHERE `email`='".$DB->ForSQL($_COOKIE['BITRIX_SM_USER_EMAIL'])."'"); 
    //проверка существования записи с сегодняшней датой и этим товаром
    while ($row = $results->Fetch())
    {
        $row['viewed_goods']=$row['viewed_goods'].$arResult['ID'].";";
        $arAr = Array(
            "viewed_goods"    => "'".$row['viewed_goods']."'",
         );
        $DB->Update("b_email_marketing_custom", $arAr, "WHERE `email`='".$DB->ForSQL($_COOKIE['BITRIX_SM_USER_EMAIL'])."'", $err_mess.__LINE__);
    }
}
//добавляем в крошки
$GLOBALS['chainelement']=$arResult['NAME']." ".$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'];

//ищем торговые предложения
$res_offers = CCatalogSKU::getOffersList(
	$arResult['ID'],
	$arResult['IBLOCK_ID'],
	$skuFilter = array("CATALOG_AVAILABLE"=>"Y","ACTIVE"=>"Y"),
	$fields = array("PROPERTY_RAZMER","CATALOG_QUANTITY","PROPERTY_TSVET","NAME","PRICE"),
	$propertyFilter = array()
);
$no_sizes=true;
$cnt_offers=0;
foreach($res_offers[$arResult['ID']] as $good){
	$cnt_offers++;
	if(strlen($good['PROPERTY_RAZMER_VALUE'])>0&&$good['CATALOG_QUANTITY']>0){
		$no_sizes=false;
	}
	if($cnt_offers==1){
	$offer_id=$good['ID'];
	$offer_color=$good['PROPERTY_TSVET_VALUE'];
	}
	$ar_price = GetCatalogProductPrice($good["ID"], 1); 
	$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
		$ar_price["ID"],
		$USER->GetUserGroupArray(),
		"N",
		SITE_ID
	);
	$discountPrice = CCatalogProduct::CountPriceWithDiscount(
			$ar_price["PRICE"],
			$ar_price["CURRENCY"],
			$arDiscounts
		);
	$new_price=$discountPrice; 
	if($new_price!=$ar_price["PRICE"]){
		$ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
		$ar_price["PRICE"] = $new_price;
	}
	
}
?>
<?php
if($arResult["DETAIL_PICTURE"] != false){
	$renderImage = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], Array("width" => 1001, "height" => 1001), BX_RESIZE_IMAGE_PROPORTIONAL, false); 
	//optimize_image($renderImage["src"]);
	$im = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'].$renderImage["src"]); // $_SERVER['DOCUMENT_ROOT'].$img - путь к изображению, для пнг нужно использовать imagecreatefrompng
	$rgb = imagecolorat($im, 50, 400); // 10,10 - x и y координаты пикселя
	$colors = imagecolorsforindex($im, $rgb);
}
?>
<script>
	//$(".section-item").css("background-color","rgb(<?=$colors['red'];?>,<?=$colors['green'];?>,<?=$colors['blue'];?>)");
</script>
<div class="col-xs-12 col-sm-3 d-flex xs-order-2">
	<div class="item-body">
		<div class="item-controller">
			<div class="item-controller-button item-controller-prev"></div>
				<div class="item-controller--content">
					<img class="lazy-image" src="/img/lazyload.gif" data-src="<?=strlen($renderImage["src"])>0?$renderImage["src"]:"/img/item/black-2.jpg";?>" alt="<?=$GLOBALS['chainelement'];?>">
					<?
					foreach($arResult["DISPLAY_PROPERTIES"]['MORE_PHOTO']['VALUE'] as $k=>$v)
					{
						$myImg= CFile::ResizeImageGet($v, array('width'=>1000, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL , false); 
						$arFile = CFile::GetFileArray($v);
						if(file_exists($_SERVER["DOCUMENT_ROOT"].$arFile['SRC'])){
							//optimize_image($myImg["src"]);
						?>
						<img class="lazy-image" src="/img/lazyload.gif" data-src="<?=$myImg['src']?>" alt="<?=$GLOBALS['chainelement'];?>" title=""/>
						<?
						}
						$i++;
					}
					?>
				</div>
				<div class="item-controller-button item-controller-next"></div>
		<div class="item-price visible-xs">
			<? if(strlen($ar_price["DISCOUNT_PRICE"])>0){?>
			<span class="old"><?=number_format($ar_price["DISCOUNT_PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
			<?}?>
			<? if(strlen($ar_price["PRICE"])>0){?><span class="new"><?=number_format($ar_price["PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span><?}?>
		</div>
	</div>
	<div class="item-suggestion hidden-xs">
		<ul class="item-suggestion--list">
               <? if(count($arResult['DISPLAY_PROPERTIES']['GOODS']['VALUE'])>0){
                   
               ?>
                    <?
                    $arSelect = Array("ID", "NAME","DETAIL_PAGE_URL","DETAIL_PICTURE","PROPERTY_CML2_MANUFACTURER","PROPERTY_RAZMER_SKIDKI_VALUE");
                    //в фильтре указываем что ищем товары которые были добавлены не более часа назад
                    $res = CIBlockElement::GetList(Array(), array("ID"=>$arResult['DISPLAY_PROPERTIES']['GOODS']['VALUE'],"!DETAIL_PICTURE"=>false), false, Array(), $arSelect);
                    $all_price=$ar_price['PRICE'];
                    while($ob = $res->GetNextElement())
                    {
						$GLOBALS["LOOKS_ITEMS"]=$arResult['DISPLAY_PROPERTIES']['GOODS']['VALUE'];
                    	// echo mydump($GLOBALS["LOOKS_ITEMS"]);
                    	// echo "111111111111";
                       $arFields = $ob->GetFields();
                       $arProps=$ob->GetProperties();
                       $res_offers2 = CCatalogSKU::getOffersList(
							$arFields['ID'],
							$arResult['IBLOCK_ID'],
							$skuFilter = array(),
							$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
							$propertyFilter = array()
						);
						foreach($res_offers2[$arFields['ID']] as $good){
							$ar_price2 = GetCatalogProductPrice($good["ID"], 1);
							$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
								$ar_price2["ID"],
								$USER->GetUserGroupArray(),
								"N",
								SITE_ID
							);
							$discountPrice = CCatalogProduct::CountPriceWithDiscount(
									$ar_price2["PRICE"],
									$ar_price2["CURRENCY"],
									$arDiscounts
								);
							$new_price=$discountPrice; 
							$ar_price2["DISCOUNT_PRICE"] = $ar_price2["PRICE"]; 
							$ar_price2["PRICE"] = $new_price; 
							$offerid=$good['ID'];
							$size=$good['PROPERTY_RAZMER_VALUE'];
						}
						$price_look = floor($ar_price2["PRICE"]);
                        $discount_price_look=$price_look;
                        $all_price+=$discount_price_look;
						
                        ?>
                        <li size="<?=$size;?>" product-id="<?=$offerid?>" class="item-suggestion--item"><a href="<?=$arFields['DETAIL_PAGE_URL'];?>" class="item-suggestion--link"><?=$arFields['NAME'];?>  <?=$arFields['PROPERTY_CML2_MANUFACTURER']['VALUE'];?> - <?=number_format($discount_price_look, 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></a></li>
                        <? 
                    }
                    ?>
				<? if(count($GLOBALS["LOOKS_ITEMS"])>0){?>
				<li class="item-suggestion--item item-suggestion--set">
					<a href="javascript:void(0);" button-buy-element="item-btn--cart" class="item-suggestion--link buy-all">Купить весь look за <?=number_format($all_price, 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></a>
				</li>
				<?}?>
            <?}?>
			</ul>
		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-4 relative xs-order-1">
	<div class="item-photo">
		<img itemprop="image"  class="item-photo--hidden" src="<?=strlen($renderImage["src"])>0?$renderImage["src"]:"/img/item/black-2.jpg";?>" alt="product"> 
		<figure class="zoo-item" data-zoo-image="<?=strlen($renderImage["src"])>0?$renderImage["src"]:"/img/item/black-2.jpg";?>">
		</figure>
	</div>
</div>
<div class="col-xs-12 col-sm-offset-1 col-sm-4 relative between-vertically xs-order-3">
	<div class="item-header">
         <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file", //Показывать информацию из файла
                "AREA_FILE_SUFFIX" => "",
                "CATALOG_ITEM" => "Y",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
            )
        );?>
        <h1 itemprop="name" class="item-name"><?=$arResult['NAME']." ".$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'];?></h1>
        <p class="item-code">Артикул: <span><?=$arResult['DISPLAY_PROPERTIES']['CML2_ARTICLE']['VALUE'];?></span></p>
		<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="item-price hidden-xs">
			<? if(strlen($ar_price["DISCOUNT_PRICE"])>0){?>
			<span class="old"><?=number_format($ar_price["DISCOUNT_PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
			<?}?>
			<? if(strlen($ar_price["PRICE"])>0){?><span class="new"><?=number_format($ar_price["PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
				<meta itemprop="price" content="<?=$ar_price["PRICE"];?>">
    			<meta itemprop="priceCurrency" content="RUB">
				<link itemprop="availability" href="http://schema.org/InStock">
			<?}?>
		</div>
	</div>
   <? 
	$res = CIBlockElement::GetByID($offer_id);
	if($obRes = $res->GetNextElement())
	{
	  $ar_res = $obRes->GetProperty("CML2_ATTRIBUTES");
	}	
	$dopsostav=false;
	if(count($arResult['DISPLAY_PROPERTIES']['CML2_ATTRIBUTES']['VALUE'])>0||strlen($arResult['DISPLAY_PROPERTIES']['SOSTAV']['VALUE'])>0||count($arResult['DISPLAY_PROPERTIES']['SOSTAV_DOP']['VALUE'])>0||count($ar_res)>0||strlen($arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'])>0){?>
	<div class="item-info description-dropdown">
		<span><i></i><a class="dashed">Информация о товаре</a></span>
		<div class="description-hider" itemprop="description">
				<? 
				if(strlen($arResult['DISPLAY_PROPERTIES']['SOSTAV']['VALUE'])>0){
					$arResult['DISPLAY_PROPERTIES']['SOSTAV']['VALUE'] = str_replace("%", "% ", $arResult['DISPLAY_PROPERTIES']['SOSTAV']['VALUE']);
					$returnValue = preg_replace('/([а-я])([0-9])/', '$1,$2', $arResult['DISPLAY_PROPERTIES']['SOSTAV']['VALUE'] , -1, $count);
					if($arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE']!="LaRoom") echo '<div class="ib-desc-item"><p>Состав</p><div></div><p>'.$returnValue."</p></div>";
				}
				$arParams = array("replace_space"=>"-","replace_other"=>"-");
                $trans = Cutil::translit($arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'],"ru",$arParams);			
				foreach ($ar_res['DESCRIPTION'] as $key=>$attribute){
					if($attribute!="Размер")
						echo '<div class="ib-desc-item"><p>'.$attribute."</p><div></div><p>".$ar_res['VALUE'][$key]."</p></div>";
				}
				/*
				foreach ($arProps['CML2_ATTRIBUTES']['VALUE'] as $key=>$attribute){
					echo '<div class="ib-desc-item"><p>'.$attribute."</p><div></div><p>".$arProps['CML2_ATTRIBUTES']['DESCRIPTION'][$key]."</p></div>";
				}*/?>
             <? foreach ($arResult['DISPLAY_PROPERTIES']['CML2_ATTRIBUTES']['VALUE'] as $key=>$attribute){
				 if($attribute=="Страна производитель"){
					 $dopsostav=true;
					foreach ($arResult['DISPLAY_PROPERTIES']['SOSTAV_DOP']['VALUE'] as $key2=>$attribute2){
						echo '<div class="ib-desc-item"><p>'.$attribute2."</p><div></div><p>".$arResult['DISPLAY_PROPERTIES']['SOSTAV_DOP']['DESCRIPTION'][$key2]."</p></div>";
					}
				 }
                echo '<div class="ib-desc-item"><p>'.$attribute."</p><div></div><p>".$arResult['DISPLAY_PROPERTIES']['CML2_ATTRIBUTES']['DESCRIPTION'][$key]."</p></div>";
			}
			if(strlen($arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'])>0){
				echo '<div class="ib-desc-item"><p>Дизайнер</p><div></div><p><a href="/dizaynery/'.$trans.'/">'.$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE']."</a></p></div>";
			}
			?>
	   </div>
	</div>
        <?}?>
     <? if(strlen($arResult['DISPLAY_PROPERTIES']['HEIGHT_MODEL']['VALUE'])>0&&strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_0']['VALUE'])>0){?>
		<p class="about">Фотомодель: рост <?=$arResult['DISPLAY_PROPERTIES']['HEIGHT_MODEL']['VALUE'];?> см., размер одежды <?=$arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_0']['VALUE'];?></p>
	<?}?>
	<? if($no_sizes===false){?>
	<div class="item-options">
		<div class="item-options-size">
			<div class="size">
				<span>Размер:</span>
				<?
				$counter=0;
				foreach($res_offers[$arResult['ID']] as $good){
					$counter++;
					$checked="";
					if($counter==1){
						$offer_id=$good['ID'];
						$offer_size=$good['PROPERTY_RAZMER_VALUE'];
						$checked="checked";
					}
					if(strlen($good['PROPERTY_RAZMER_VALUE'])>0){
				?>
				<div class="radio">
					<input class="<? if($counter==1){ echo 'must-be-clicked'; }?>" <?=$checked;?> offer-id="<?=$good['ID'];?>" size-id="<?=$good['PROPERTY_RAZMER_VALUE'];?>" type="radio" id="item-<?=$good['PROPERTY_RAZMER_VALUE'];?>" name="radio-group">
					<label for="item-<?=$good['PROPERTY_RAZMER_VALUE'];?>"><?=$good['PROPERTY_RAZMER_VALUE'];?></label>
				</div>
				<?}}?>
			</div>
			<div class="size-info">
                <?
                $groups = CIBlockElement::GetElementGroups($arResult["ID"], true);
                $ar_groups = Array();
                while($ar_group = $groups->Fetch()){
                    $SECTION_ID=$ar_group["ID"];

                    $ids[]=$SECTION_ID;
                    $res = CIBlockSection::GetByID($ar_group["ID"]);
                    if($ar_res = $res->GetNext())
                      $ids[]=$ar_res['IBLOCK_SECTION_ID'];
                }	
                foreach ($ids as $id){
                    $nav = CIBlockSection::GetNavChain(false, $id);
                    foreach($nav->arResult as $navres){
                        if(!in_array($navres['ID'],$ids))
                            array_push($ids,$navres['ID']);
                    }
                }
                $arFilter = array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ID"=>$ids);
                $res = CIBlockSection::GetList(array(), $arFilter, false, array("ID","NAME","UF_SIZES_GRID"), array());
                
                while($arSection = $res->Fetch()){
                    $pieces = explode('"', $arSection['UF_SIZES_GRID']);
                    if(count($arSection['UF_SIZES_GRID'])>1)
                       $sizes_grid=$arSection['UF_SIZES_GRID'];
                }
                ?>
                <? if(count($sizes_grid)>0){?>
				<a href="#" class="table-size">Информация о размерах</a>
                <?}?>
			</div>
		</div>
		<a class="btn addtocart<?=$offer_id;?> addtocart item-btn--cart" product-id="<?=$offer_id;?>" size="<?=$offer_size;?>" href="javascript:void(0);">Добавить в корзину</a>
		<div class="wishlist">
            <?
            $arSelect = Array();
            $arFilter = Array("IBLOCK_ID"=>12,"CREATED_BY"=>$USER->GetID(),"PROPERTY_GOOD"=>$arResult['ID']);
            $res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
            $found=0;
            while($ob = $res->GetNextElement())
            {
               $found=1;
            }
            ?>
			<? if ($USER->IsAuthorized()){?>
			<a class="btn-wishlist <? if ($found==1) echo "disabled";?>"  product-id="<?=$arResult['ID'];?>" product-name="<?=$arResult['NAME'];?> <?=$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'];?>" href="<? if ($found==1) echo "/personal/wishlist/"; else echo "javascript:void(0);";?>">
			<span><?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/star.svg"); ?></span>
			<span class="texwish">
			<? if($found==1) echo "Товар в WishListе"; else echo "Отложить в WishList";?>
				</span>
			</a>
			<?}?>
		</div>
	</div>
	<?}
	else{?>
	<div class="item-options">
		<div class="item-options-size">
			<div class="size-info">
                <?
                $groups = CIBlockElement::GetElementGroups($arResult["ID"], true);
                $ar_groups = Array();
                while($ar_group = $groups->Fetch()){
                    $SECTION_ID=$ar_group["ID"];

                    $ids[]=$SECTION_ID;
                    $res = CIBlockSection::GetByID($ar_group["ID"]);
                    if($ar_res = $res->GetNext())
                      $ids[]=$ar_res['IBLOCK_SECTION_ID'];
                }	
                foreach ($ids as $id){
                    $nav = CIBlockSection::GetNavChain(false, $id);
                    foreach($nav->arResult as $navres){
                        if(!in_array($navres['ID'],$ids))
                            array_push($ids,$navres['ID']);
                    }
                }
                $arFilter = array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ID"=>$ids);
                $res = CIBlockSection::GetList(array(), $arFilter, false, array("ID","NAME","UF_SIZES_GRID"), array());
                
                while($arSection = $res->Fetch()){
                    $pieces = explode('"', $arSection['UF_SIZES_GRID']);
                    if(count($arSection['UF_SIZES_GRID'])>1)
                       $sizes_grid=$arSection['UF_SIZES_GRID'];
                }
                ?>
                <? if(count($sizes_grid)>0){?>
				<a href="#" class="table-size">Информация о размерах</a>
                <?}?>
			</div>
		</div>
		<a class="btn item-btn--cart disabled" href="javascript:void(0);">Нет в наличии</a>
		<div class="wishlist">
            <?
            $arSelect = Array();
            $arFilter = Array("IBLOCK_ID"=>12,"CREATED_BY"=>$USER->GetID(),"PROPERTY_GOOD"=>$arResult['ID']);
            $res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
            $found=0;
            while($ob = $res->GetNextElement())
            {
               $found=1;
            }
            ?>
			<? if ($USER->IsAuthorized()){?>
			<a class="btn-wishlist <? if ($found==1) echo "disabled";?>"  product-id="<?=$arResult['ID'];?>" product-name="<?=$arResult['NAME'];?> <?=$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'];?>" href="<? if ($found==1) echo "/personal/wishlist/"; else echo "javascript:void(0);";?>">
			<span><?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/star.svg"); ?></span>
			<span class="texwish">
			<? if($found==1) echo "Товар в WishListе"; else echo "Отложить в WishList";?>
				</span>
			</a>
			<?}?>
		</div>
	<?}?>

</div>


<?
$nav = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"], $arResult["IBLOCK_SECTION_ID"]);
$category="";
while ($arNav=$nav->GetNext()):
	$category.=$arNav["NAME"].'/';
endwhile;
$category=trim($category,"/");
?>

<script>
// Measure a view of product details. This example assumes the detail view occurs on pageload,
// and also tracks a standard pageview of the details page.
dataLayer.push({
  'event': 'Product View',
  'ecommerce': {
    'detail': {
      'actionField': {},    // 'detail' actions have an optional list property.
      'products': [{
        'name': '<?=explode(" ",$arResult['NAME'])[0];?> <?=$arResult['PROPERTIES']['CML2_MANUFACTURER']["VALUE"];?> <?=$arResult['PROPERTIES']['CML2_ARTICLE']["VALUE"];?>',         // Name or ID is required.
        'id': '<?=$arResult['ID'];?>',
        'price': <?=number_format($ar_price['PRICE'],0,".","")?>,
		'brand': '<?=$arResult['PROPERTIES']['CML2_MANUFACTURER']["VALUE"];?>',
		'category': '<?=$category;?>',
		'variant':'<?=$offer_color;?>'
       }]
     }
   }
});
ga('send',  'event',  'Ecommerce',  'Product View');   
</script>
<!-- Open Graph Metadata -->

 <?$APPLICATION->AddHeadString('<meta property="og:title" content="'.$arResult['NAME']." ".$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'].'">',true)?>
 <?$APPLICATION->AddHeadString('<meta property="og:description" content="'.$arResult['NAME']." ".$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'].' в магазине Апрель">',true)?>
 <?$APPLICATION->AddHeadString('<meta property="og:url" content="https://'.SITE_SERVER_NAME.$arResult['DETAIL_PAGE_URL'].'">',true)?>
 <?$APPLICATION->AddHeadString('<meta property="og:image" content="https://'.SITE_SERVER_NAME.$renderImage["src"].'">',true)?>
 <?$APPLICATION->AddHeadString('<meta property="product:brand" content="'.$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'].'">',true)?>
 <? if($no_sizes===false){?>
 <?$APPLICATION->AddHeadString('<meta property="product:availability" content="in stock">',true)?>
 <?} else{?>
	<?$APPLICATION->AddHeadString('<meta property="product:availability" content="out of stock">',true)?>
 <?}?>
 <?$APPLICATION->AddHeadString('<meta property="product:condition" content="new">',true)?>
 <?$APPLICATION->AddHeadString('<meta property="product:price:amount" content="'.$ar_price["PRICE"].'">',true)?>
 <?$APPLICATION->AddHeadString('<meta property="product:price:currency" content="RUB">',true)?>
 <?$APPLICATION->AddHeadString('<meta property="product:retailer_item_id" content="'.$arResult['ID'].'">',true)?>
<!--
<script>
fbq('track', 'ViewContent', {
  content_name: '<?=$arResult['NAME']." ".$arResult['DISPLAY_PROPERTIES']['CML2_MANUFACTURER']['VALUE'];?>',
  content_category: '<?=$category;?>',
  value: <?=$ar_price["PRICE"];?>,
  currency: 'RUB'
 });
 </script>
-->

<? if($no_sizes===false){?>
<script>
  fbq('track', 'ViewContent', {
    value: <?=$ar_price["PRICE"];?>,
    currency: 'RUB',
    content_ids: '<?=$arResult['ID']?>',
    content_type: 'product',
  });
</script>


<?}?>

<!-- End Open Graph Metadata -->
<? 
	
//размерная сетка
if(count($sizes_grid)>0){
    // включаем буфер
ob_start();
?>

<!-- Размерная сетка  -->
<!-- <div class="popup popup-table-size" style="display: none">
	<div class="popup-container">
		<div class="popup-body">
			<div class="sizes sizes--table">
				<div class="close-icon close-popup">
					<?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/close.svg"); ?>
				</div>
                <? 
                if(strlen($arResult["DISPLAY_PROPERTIES"]['id_size_grid']['VALUE'])>0){
                    $arFilter = Array("IBLOCK_ID"=>14,"ID"=>$arResult["DISPLAY_PROPERTIES"]['id_size_grid']['VALUE'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
                    while($ob = $res->GetNextElement())
                    {
                        $arFields = $ob->GetFields();
                        $activecode=$arFields['CODE'];
                    }
                }
                foreach ($sizes_grid as $sizes_g){
                    $pieces = explode(",", $sizes_g); 
                    $code=$pieces[0];
                    ?>
                    <div class="sizes-line <? if($code==$activecode) echo "active";?>">
                        <? foreach($pieces as $piece){
                        ?>
                        <p><?=$piece;?></p>
                        <?
                        }?>
                    </div>
                <?
                }
                ?>
			</div>
            <?
                $dop_popup=0;
             for($index=0;$index<8;$index++){
                 if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_'.$index]['VALUE'])>0){
                    $dop_popup=1;   
                 }
             }
            ?>
            <? if($dop_popup>0){?>
			<div class="sizes sizes--info">
                <? for($index=0;$index<8;$index++){ 
                    if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_'.$index]['VALUE'])>0){
                        ?>
                        <div class="sizes-line">
                            <p><?=$arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_'.$index]['NAME'];?></p>
                            <p><?=$arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_'.$index]['VALUE'];?></p>
                        </div>
                        <?
                    }
                }
                ?>
			</div>
			<? if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_1']['VALUE'])>0||strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_2']['VALUE'])>0||strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_3']['VALUE'])>0||strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_4']['VALUE'])>0||strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_5']['VALUE'])>0||strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_6']['VALUE'])>0||strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_7']['VALUE'])>0){?>
			<div class="how-to-size">
                 <?
                if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_1']['VALUE'])>0){
                    ?>
                <div class="go-length-no">
					<p>Длина изделия</p>
					<span>Длина от крайней верхней до крайней нижней точки изделия по центру лицевой стороны</span>
				</div>
                    <?
                }
                ?>
                <?
                if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_2']['VALUE'])>0){
                    ?>
                <div class="go-arm-length">
					<p>Длина рукава</p>
					<span>Длина рукава от плечевого шва до самого длинного края рукава</span>
				</div>
                    <?
                }
                ?>
                <?
                if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_3']['VALUE'])>0){
                    ?>
                <div class="go-chest">
					<p>Обхват груди</p>
					<span>Лента должна проходить через лопатки и самую выступающую часть груди</span>
				</div>
                    <?
                }
                ?>
                <?
                if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_4']['VALUE'])>0){
                    ?>
                <div class="go-waist">
					<p>Обхват талии</p>
					<span>Лента должна проходить по самой узкой части торса</span>
				</div>
                    <?
                }
                ?>
                <?
                if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_5']['VALUE'])>0){
                    ?>
                <div class="go-thigh">
					<p>Обхват бедер</p>
					<span>Лента должна проходить по самым выступающим частям ягодиц</span>
				</div>
                    <?
                }
                ?>
                <?
                if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_6']['VALUE'])>0){
                    ?>
                <div class="go-rise">
					<p>Высота пояса</p>
					<span>Высота посадки брюк от пояса до паха</span>
				</div>
                    <?
                }
                ?>
                <?
                if(strlen($arResult['DISPLAY_PROPERTIES']['SIZE_GUIDE_7']['VALUE'])>0){
                    ?>
                <div class="go-inside-leg-no no-hover-div">
					<p>Внутренний шов</p>
					<span>Длина внутреннего бокового шва от паха до нижней точки изделия</span>
				</div>
                    <?
                }
                ?>
            </div>
			<?}?>
            <?}?>
		</div>
	</div>
</div>
 -->
<?

// сохраняем всё что есть в буфере в переменную $content
$GLOBALS['POPUP_SIZE'] = ob_get_contents();
 
// отключаем и очищаем буфер
ob_end_clean();
}?>

<?
unset($actualItem, $itemIds, $jsParams);