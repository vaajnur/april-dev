<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

CUtil::InitJSCore(array('fx'));
?>
<main class="sidebar-version">
	<?$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);?>
	<section class="blog-banner-bg">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 blog-banner">
					<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
					width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
					height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
					alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
					title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>" class="img-responsive">
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<p class="h1 catalog-title blog-title"><?=$arResult['NAME'];?></p>
				<div class="blog-description">
					<p>
					<?if(strlen($arResult["DETAIL_TEXT"])>0):?>
						<?echo $arResult["DETAIL_TEXT"];?>
					<?else:?>
						<?echo $arResult["PREVIEW_TEXT"];
					endif;?>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="blog-promo">
		<div class="container">
			<div class="row center-xs">
				<div class="col-xs-12 col-sm-6">
					<div class="blog-logo">
						<?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/apr-logo.svg"); ?>
					</div>
				</div>
			</div>
			<div class="row center-xs">
				<div class="col-xs-12 col-sm-6">
					<h2 class="blog-promo--title">Понравилась статья?</h2>
					<p class="blog-promo--description">Получайте свежие новости, советы от стилистов, информацию о закрытых распродажах и акциях на почту!</p>
					<?$APPLICATION->IncludeComponent(
						"april:subscribe.form",
						"blog_form",
						Array(
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "A",
							"CODE_FORM" => "BLOG",
							"PAGE" => "/ajax/subscribe.php",
							"RUBRIC_ID" => 1,
							"SHOW_HIDDEN" => "Y",
							"USE_PERSONALIZATION" => "Y"
						)
					);?>
				</div>
			</div>
		</div>
	</div>
	<div class="blog-social">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<h2 class="blog-social--title">Полезная статья? Отправьте друзьям:</h2>
				</div>
				<div class="col-xs-12 col-sm-12 blog-social-links">
					<a class="bt" href="http://vk.com/share.php?url=https://aprilmoscow.ru<?=$_SERVER['REQUEST_URI'];?>"><img src="/img/icons/vk.svg" alt="social-img" width="36" height="36"></a>
					<a class="bt" href="http://www.facebook.com/sharer.php?u=https://aprilmoscow.ru<?=$_SERVER['REQUEST_URI'];?>"><img src="/img/icons/fb2.svg" alt="social-img" width="36" height="36"></a>
					<a class="bt" href="https://twitter.com/intent/tweet?url=https://aprilmoscow.ru<?=$_SERVER['REQUEST_URI'];?>"><img src="/img/icons/twitter.svg" alt="social-img" width="36" height="36"></a>
					<a class="bt" href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=https://aprilmoscow.ru<?=$_SERVER['REQUEST_URI'];?>"><img src="/img/icons/odnoklassniki.svg" alt="social-img" width="36" height="36"></a>
				</div>
			</div>
        </div>
        <div class="container-fluid blog-main">
			<div class="row blog-main around-xs">
			<?$APPLICATION->IncludeComponent("april:news.list","",Array(
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => "news",
					"IBLOCK_ID" => "1",
					"NEWS_COUNT" => "3",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => Array("ID","DATE_CREATE"),
					"PROPERTY_CODE" => Array("DESCRIPTION"),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_LAST_MODIFIED" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "Y",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_BASE_LINK_ENABLE" => "Y",
					"SET_STATUS_404" => "Y",
					"SHOW_404" => "Y",
					"MESSAGE_404" => "",
					"PAGER_BASE_LINK" => "",
					"PAGER_PARAMS_NAME" => "arrPager",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
			);?>
			</div>
		</div>
	</div>
</main>

