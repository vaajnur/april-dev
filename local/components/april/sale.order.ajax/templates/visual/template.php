<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->SetAdditionalCSS($templateFolder."/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
use \Bitrix\Catalog;


$context = Main\Application::getInstance()->getContext();
$request = $context->getRequest();


CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));

$_SESSION['order_props'] = $request->getPostList();
/*if (strlen($request->get('ORDER_ID')) > 0)
{
  include(Main\Application::getDocumentRoot().$templateFolder.'/confirm.php');
}elseif ($arParams['DISABLE_BASKET_REDIRECT'] === 'Y' && $arResult['SHOW_EMPTY_BASKET'])
{
  include(Main\Application::getDocumentRoot().$templateFolder.'/empty.php');
}else{*/

?>
<style>
    #wait_order_form_content{
        display:none;
	}
	div[id^="wait_"]{ display: none;}
</style>
<a name="order_form"></a>

<div id="order_form_div" class="order-checkout" style="position:relative">
<!-- анимация загрузки ниже, срабатывает если есть класс loading у родителя выше -->
<div class="load-container">
    <div class="load-circle"></div>
    <div class="load-circle"></div>
</div>
<NOSCRIPT>
	<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>

<?
if (!function_exists("getColumnName"))
{
	function getColumnName($arHeader)
	{
		return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
	}
}
?>

<div class="bx_order_make">
	<?
	if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
	{
		if(!empty($arResult["ERROR"]))
		{
			foreach($arResult["ERROR"] as $v){
              echo $v;
            }
				
		}
		elseif(!empty($arResult["OK_MESSAGE"]))
		{
			foreach($arResult["OK_MESSAGE"] as $v)
				echo ShowNote($v);
		}

		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
	}
	else
	{
		
		if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
		{
			if(strlen($arResult["REDIRECT_URL"]) > 0)
			{
				?>
				<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';

				</script>
				<?
				die();
			}
			else
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
			}
		}
		else
		{
			?>
			<script type="text/javascript">
			$(document).on("blur", '#ORDER_PROP_6_val', function(event) { 
				
			})
			function submitForm(val,button,coupon)
			{
				setCookie("order_email",$("#ORDER_PROP_2").val(),{path: "/"});
				setCookie("order_phone",$("#ORDER_PROP_3").val(),{path: "/"});
				setCookie("order_name",$("#ORDER_PROP_1").val(),{path: "/"});
				setCookie("order_city",$("#ORDER_PROP_6").val(),{path: "/"});
				setCookie("order_street",$("#ORDER_PROP_8").val(),{path: "/"});
				setCookie("order_house",$("#ORDER_PROP_9").val(),{path: "/"});
				setCookie("order_flat",$("#ORDER_PROP_10").val(),{path: "/"});
                if(button!="Y"&&coupon!=1){
					loading_animation("#order_form_content");
					BX.addClass(BX('order_form_div'),"loading-animation");
				}
                    
                else if(coupon!=1){
					$("#ORDER_PROP_2").removeAttr("disabled");
					$("#ORDER_PROP_3").removeAttr("disabled");
                    $("#order_form_content button").text("Загрузка...");
                    $("#order_form_content button").attr("disabled","disabled");
                }
				if(val != 'Y')
					BX('confirmorder').value = 'N';

				var orderForm = BX('ORDER_FORM');
				
                if($('#ORDER_PROP_7').length>0)
                {
					// $('#ORDER_PROP_7').val('г. '+$('#ORDER_PROP_6_val').val()+", ул. "+$('#ORDER_PROP_8').val()+", д. "+$('#ORDER_PROP_9').val()+", кв. "+$('#ORDER_PROP_10').val());
					// $('#ORDER_PROP_7').val('г. '+$('#ORDER_PROP_6_val').val()+", ул. "+$('#ORDER_PROP_8').val());
                }
				$("#ORDER_PROP_3").mask("+7(999) 999-99-99",{placeholder:"+7(___) ___-__-__"});
				
				BX.ajax.submitComponentForm(orderForm, 'order_form_content', true);
				BX.submit(orderForm);
				
				$('select').selectric('refresh');
				return true;
			}
			$(document).ready(function () {
				$(".hide-with-coupon").html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
				submitForm("","",1);
			});
			function nonAuthorized(){
				$(".popup.new-popup h2").html('<p>Пользователь с таким E-mail уже существует.</p><a class="link-enter" href="javascript:void(0);">Чтобы продолжить оформление заказа, авторизуйтесь</a>');
				$(".popup.new-popup").fadeIn();
			}
			function SetContact(profileId)
			{
				BX("profile_change").value = "Y";
				submitForm();
			}
			</script>
			<?if($_POST["is_ajax_post"] != "Y")
			{
				?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" class="cart-form" enctype="multipart/form-data">
				<?=bitrix_sessid_post()?>
                <p class="h1">Оформление заказа</p>
				<div id="order_form_content">
				<?
			}
			else
			{
				$APPLICATION->RestartBuffer();
			}
			$GLOBALS['ERRORS_ORDER']=array();
		
			if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
			{
				foreach($arResult["ERROR"] as $v){
                    echo ShowError($v);
                    $GLOBALS['ERRORS_ORDER'][]=str_replace(" обязательно для заполнения","",$v);
                }
				?>
				<script type="text/javascript">
					//top.BX.scrollToNode(top.BX('ORDER_FORM'));
				</script>
				<?
			}

			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");
			if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d")
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
			}
			else
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
			}

			

			//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
			if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
				echo $arResult["PREPAY_ADIT_FIELDS"];
			?>
			</div>
			<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
			<input type="hidden" name="profile_change" id="profile_change" value="N">
			<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
			<? foreach($arResult['JS_DATA']['COUPON_LIST'] as $coupon){
                    if($coupon['JS_STATUS']=="APPLIED"){
                        $class='success';
                        $coupon=$coupon['COUPON'];
                    } 
			}
			?>
			
            <div class="promocode <?=$class;?>">
				<? if($class=="success"){?>
				<div class="cancel-promocode close-icon close-popup" onclick="$('#coupon').val('_'); enterCoupon(); $('#coupon').val(''); $('#order_form_div').addClass('loading-animation');  loading_animation('#order_form_content');">
				<?php include($_SERVER["DOCUMENT_ROOT"]."/img/icons/close.svg"); ?>
				</div>
				<?}?>
				<input type="text" id="coupon" class="<?=$class;?>" <? if($class=="success") echo "disabled";?> name="COUPON" value="<?=$coupon;?>" placeholder="Промокод">
				<button <? if($class=="success") echo "disabled";?> onclick="if($('#coupon').val().length==0){ $('#coupon').val('_'); enterCoupon(); $('#coupon').val(''); } $('#order_form_div').addClass('loading-animation'); loading_animation('#order_form_content');  $(this).text('Загрузка...'); enterCoupon(); return false;" class="btn">ПРИМЕНИТЬ</button>
			</div> 
			<div class="cart-info">
				<p>Доставка:</p>
				<? if(strlen($_SESSION['manager_calculate'])>0){?>
					<p class="hide-with-coupon"><?=$_SESSION['manager_calculate'];?></p>
				<?} else{?>
				<p class="hide-with-coupon"><?=$arResult['JS_DATA']['TOTAL']['DELIVERY_PRICE']==0?"Бесплатно":number_format($arResult['JS_DATA']['TOTAL']['DELIVERY_PRICE'], 0, ',', ' ');?><i> <?php if($arResult['JS_DATA']['TOTAL']['DELIVERY_PRICE']>0) include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></i></p>
                <?}?>
				<? if(intval(($arResult['JS_DATA']['TOTAL']['PRICE_WITHOUT_DISCOUNT_VALUE']-$arResult['JS_DATA']['TOTAL']['ORDER_PRICE'])*100/$arResult['JS_DATA']['TOTAL']['PRICE_WITHOUT_DISCOUNT_VALUE'])>0){?>
				<p>Скидка:</p>
				<p class="hide-with-coupon"><?=number_format($arResult['JS_DATA']['TOTAL']['PRICE_WITHOUT_DISCOUNT_VALUE']-$arResult['JS_DATA']['TOTAL']['ORDER_PRICE'], 0, ',', ' ');?><i> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></i> (-<?=intval(($arResult['JS_DATA']['TOTAL']['PRICE_WITHOUT_DISCOUNT_VALUE']-$arResult['JS_DATA']['TOTAL']['ORDER_PRICE'])*100/$arResult['JS_DATA']['TOTAL']['PRICE_WITHOUT_DISCOUNT_VALUE']);?>%)</p>
                <?}?>
				<p>Итого:</p>
				<p class="hide-with-coupon"><?=number_format($arResult['JS_DATA']['TOTAL']['ORDER_TOTAL_PRICE'], 0, ',', ' ');?><i> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></i></p>
			</div>
            <div>
                <button onClick="submitForm('Y','Y'); return false;" class="btn h2 full-width">ОФОРМИТЬ ЗАКАЗ</button>
            </div>
			<?if($_POST["is_ajax_post"] != "Y")
			{
				?>
                    <script>
                    document.getElementsByName('ORDER_PROP_6_val')[0].placeholder='Город';
                    </script>
				</form>
				<?
				if($arParams["DELIVERY_NO_AJAX"] == "N")
				{
					$APPLICATION->AddHeadScript("/bitrix/js/main/cphttprequest.js");
					$APPLICATION->AddHeadScript("/bitrix/components/bitrix/sale.ajax.delivery.calculator/templates/.default/proceed.js");
				}
			}
			else
			{
				?>
				<div class="popup new-popup" style="display:none">
					<div class="popup-container">
						<div class="popup-content">
							<div class="close-icon close-popup">
								<?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/close.svg"); ?>
							</div>
							<h2>Спасибо за ... </h2>
						</div>
					</div>
				</div>
                    <script src="/js/jquery.min.js"></script>
                    <script src="/js/selectric.js"></script>
					<script type="text/javascript">
                       
                        $(document).ready(function () {
                            document.getElementsByName('ORDER_PROP_6_val')[0].placeholder='Город';
							$("#order_form_div").removeClass("loading-animation");
                        });
						<? if($_SESSION['USER_EXISTS']=="Y"){
							$_SESSION['USER_EXISTS']="N";
							?>
							$(document).ready(function () {
								$(document).find(".popup.new-popup h2").html('<p>Пользователь с таким E-mail уже существует.</p><a class="link-enter" href="javascript:void(0);">Чтобы продолжить оформление заказа, авторизуйтесь</a>');
								$(document).find(".popup.new-popup").show();
							});
						<?}?>
                        top.BX.removeClass(
                         "order_form_div",
                         "loading"
                        );
						top.BX.removeClass(
                         "order_form_div",
                         "loading-animation"
                        );
						top.BX('confirmorder').value = 'Y';
						top.BX('profile_change').value = 'N';
					</script>
				<?
				die();
			}
		}
	}
	?>
	</div>
</div>
<? //} ?>