<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $deliveryId = 0)
	{
        global $USER;
        $errors=array();
        if(!empty($arResult["ERROR"]))
		{
			foreach($arResult["ERROR"] as $v){
                $errors[]=str_replace(" обязательно для заполнения","",$v);
            }
				
		}
		if (!empty($arSource))
		{
			?>
				<div>
					<?
					foreach($arSource as $arProperties)
					{
						if ($arProperties["TYPE"] == "CHECKBOX")
						{
							?>
							<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">

							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r1x3 pt8"><input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y"<?if ($arProperties["CHECKED"]=="Y") echo " checked";?>></div>

							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXT")
						{
							?>
							<!-- <div class="group"> -->
							<div class="group">
								<?
								if(isset($GLOBALS['niceadres'])){
									switch ($arProperties["CODE"]) {
										case "CITY":
											$arProperties["VALUE"]=$GLOBALS['niceadres']['city'];
											break;
										case "street":
											$arProperties["VALUE"]=$GLOBALS['niceadres']['street'];
											break;
										case "house":
											$arProperties["VALUE"]=$GLOBALS['niceadres']['house'];
											break;
										case "flat":
											$arProperties["VALUE"]=$GLOBALS['niceadres']['flat'];
											break;
									}
								}
								if($arProperties["ID"]=="2"&&isset($_COOKIE['order_email'])&&$_COOKIE['order_email']!="undefined"){
									$arProperties["VALUE"]=$_COOKIE['order_email'];
								}
								if($arProperties["ID"]=="1"&&isset($_COOKIE['order_name'])&&$_COOKIE['order_name']!="undefined"){
									$arProperties["VALUE"]=$_COOKIE['order_name'];
								}
								if($arProperties["ID"]=="3"&&isset($_COOKIE['order_phone'])&&$_COOKIE['order_phone']!="undefined"){
									$arProperties["VALUE"]=$_COOKIE['order_phone'];
								}
								if($arProperties["ID"]=="8"&&isset($_COOKIE['order_street'])&&$_COOKIE['order_street']!="undefined"){
									$arProperties["VALUE"]=$_COOKIE['order_street'];
								}
								if($arProperties["ID"]=="9"&&isset($_COOKIE['order_house'])&&$_COOKIE['order_house']!="undefined"){
									$arProperties["VALUE"]=$_COOKIE['order_house'];
								}
								if($arProperties["ID"]=="10"&&isset($_COOKIE['order_flat'])&&$_COOKIE['order_flat']!="undefined"){
									$arProperties["VALUE"]=$_COOKIE['order_flat'];
								}
								if($USER->IsAuthorized()&&$arProperties["ID"]=="1"){
									$arProperties["VALUE"]=$USER->GetFirstName();
								}
								if($USER->IsAuthorized()&&$arProperties["ID"]=="2"){
									$arProperties["VALUE"]=$USER->GetEmail();
								}
								if($USER->IsAuthorized()&&$arProperties["ID"]=="3"){
									$rsUser = CUser::GetByID($USER->GetID());
									$arUser = $rsUser->Fetch();
									$arProperties["VALUE"]=$arUser["PERSONAL_PHONE"];
								}
								?>
								<input type="text" <? //if ($USER->IsAuthorized()&&(($arProperties["ID"]=="2"&&strlen($arProperties["VALUE"])>0)||($arProperties["ID"]=="3"&&strlen($arProperties["VALUE"])>0))) echo "disabled";?> maxlength="250" class="lined-input  <? if(in_array($arProperties["NAME"],$GLOBALS['ERRORS_ORDER'])) echo 'error';?>" size="<?=$arProperties["SIZE1"]?>" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" class="<? if(in_array($arProperties["NAME"],$errors)) echo "error";?>" placeholder="<?=$arProperties["NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>">
								<?php
								if(in_array($arProperties["NAME"],$GLOBALS['ERRORS_ORDER']))
								{
								?>
								<?php
								}
								?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "SELECT")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?
									endforeach;
									?>
								</select>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "MULTISELECT")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?
									endforeach;
									?>
								</select>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXTAREA")
						{
							$rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
							?>
							<textarea rows="<?=$rows?>" cols="<?=$arProperties["SIZE1"]?>" placeholder="<?=$arProperties["NAME"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"><?=$arProperties["VALUE"]?></textarea>
							<?
						}
						elseif ($arProperties["TYPE"] == "LOCATION")
						{
							
							?>
                            <div class="location_select" style="<? if($deliveryId!=2&&$deliveryId!=23&&$deliveryId!=17) echo "display:none;"?>"><?
							$value = 0;
							if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
							{
								foreach ($arProperties["VARIANTS"] as $arVariant)
								{
									if ($arVariant["SELECTED"] == "Y")
									{
										$value = $arVariant["ID"];
										if($arProperties["ID"]=="6"&&isset($_COOKIE['order_city'])){
											//$value=$_COOKIE['order_city'];
										}
										break;
									}
								}
							}
							$arLocs = CSaleLocation::GetByID($value, LANGUAGE_ID);
                            $GLOBALS['niceadres']['city']= $arLocs["CITY_NAME"];
                            /*if ($USER->IsAuthorized()){
                                $arFilter = array("ID" => $USER->GetID());
                                $arParams["SELECT"] = array("UF_ADRESSES");
                                $arRes = CUser::GetList($by,$desc,$arFilter,$arParams);
                                if ($res = $arRes->Fetch()) {
                                    $adresses=true;
                                    if (isset($_COOKIE["default_address"])){
                                        $defaultaddress=$_COOKIE["default_address"];
                                        if($_COOKIE["default_address"]=="new"){
                                            $defaultaddress="Добавить адрес доставки";
                                        }
                                    }
                                    else{
                                        $pieces = explode(";", $res["UF_ADRESSES"][0]);
                                        $arLocs = CSaleLocation::GetByID($pieces[0], LANGUAGE_ID);
                                        $city= $arLocs["CITY_NAME"];
                                        $defaultaddress=$pieces[4]."";									
                                        }
                                    ?>
                                    <select class="choose_adres">
                                            <?
                                            $counterr=0;
                                            foreach ($res["UF_ADRESSES"] as $key=>$address){
                                                $counterr++;
                                                $pieces = explode(";", $address);
                                                $arLocs = CSaleLocation::GetByID($pieces[0], LANGUAGE_ID);
                                                $city= $arLocs["CITY_NAME"];

                                                ?>
                                                <option <? if($key==$_COOKIE["default_address"]-1) echo "selected"; ?> adresname="<?=$pieces[4];?>" cityname=<?=$city;?> city="<?=$pieces[0];?>" street="<?=$pieces[1];?>" house="<?=$pieces[2];?>" flat="<?=$pieces[3];?>" value="<?=$counterr;?>"><?=$pieces[4]." (г. ".$city.", ул. ".$pieces[1].", д. ".$pieces[2].", кв. ".$pieces[3].")"?></option>
                                                <?
                                                if(trim($pieces[4])==trim($defaultaddress)){ 
                                                ?>
                                                    <?
                                                    $GLOBALS['niceadres']['num']=$counterr;
                                                    $GLOBALS['niceadres']['city']=$pieces[0];
                                                    $GLOBALS['niceadres']['street']=$pieces[1];
                                                    $GLOBALS['niceadres']['house']=$pieces[2];
                                                    $GLOBALS['niceadres']['flat']=$pieces[3];
                                                    $GLOBALS['niceadres']['adresname']=$pieces[4];
                                                    $arProperties["VALUE"]=$GLOBALS['niceadres']['city'];
                                                    ?>

                                                    <?
                                                }?>
                                                <?} ?>

                                        <option <? if("new"==$_COOKIE["default_address"]) echo "selected"; ?> value="new">Добавить новый...</option></select>
                                    <? 
                                    if($_COOKIE["default_address"]!="new"&&is_array($res["UF_ADRESSES"])){
                                        ?>
                                        <style>
                                        #ORDER_PROP_6_val,#ORDER_PROP_6,#ORDER_PROP_8,#ORDER_PROP_9,#ORDER_PROP_10{
                                            display:none;
                                        }
                                        </style>
                                        <?
                                    }
                                }
                                ?>
                                <?
                            }*/
							?>
                                <style>
								#ORDER_PROP_5,
								#ORDER_PROP_7{
									/*display:none;*/
								}
							</style>
								<?
								$GLOBALS["APPLICATION"]->IncludeComponent(
									"bitrix:sale.ajax.locations",
									$locationTemplate,
									array(
										"AJAX_CALL" => "N",
                                        "PLACEHOLDER"=>$arProperties["NAME"],
										"COUNTRY_INPUT_NAME" => "COUNTRY",
										"REGION_INPUT_NAME" => "REGION",
										"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
										"CITY_OUT_LOCATION" => "Y",
										"LOCATION_VALUE" => $value,
										// "LOCATION_VALUE" => '',
										"ORDER_PROPS_ID" => $arProperties["ID"],
										"ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
										"SIZE1" => $arProperties["SIZE1"],
									),
									null,
									array('HIDE_ICONS' => 'Y')
								);
								?>
							 </div><?
						}
						elseif ($arProperties["TYPE"] == "RADIO")
						{
							?>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<?
								if (is_array($arProperties["VARIANTS"]))
								{
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<input
											type="radio"
											name="<?=$arProperties["FIELD_NAME"]?>"
											id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"
											value="<?=$arVariants["VALUE"]?>" <?if($arVariants["CHECKED"] == "Y") echo " checked";?> />

										<label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label></br>
									<?
									endforeach;
								}
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "FILE")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<?=showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"])?>
							</div>

							<div style="clear: both;"></div><br/>
							<?
						}
					}
					?>
				</div>
			<?
		}
	}
}
?>