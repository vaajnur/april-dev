<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<ul itemscope itemtype="http://schema.org/BreadcrumbList">';



$strReturn .= '';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($title=="April Style") $arResult[$index]["LINK"]="/blog/";
	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	$arrow = ($index > 0? '<i>/</i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">'.$arrow.'
		<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="item"><span itemprop="name">
					'.$title.'</span><meta itemprop="position" content="'.$index.'">
				</a></li>';
	}
	else
	{
		$strReturn .= '<li>'.$arrow.' '.$title.'</li>';
	}
}

$strReturn .= '';

return $strReturn;
