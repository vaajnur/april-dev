<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<?foreach($arResult as $arItem):?>
<li class="profile-item <? if($arItem["LINK"]==$_SERVER['REQUEST_URI']) echo "active"; ?>"><p>
	<a class="profile-link <? if(strpos($_SERVER['REQUEST_URI'],$arItem["LINK"])!==false) echo "active"; ?>" href="<?=$arItem["LINK"]?>">
	<?php
	if($arItem["TEXT"]=="Мой профиль")
		echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/img/icons/woman.svg");
	if($arItem["TEXT"]=="Мои заказы")
		echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/img/icons/cart.svg");
	if($arItem["TEXT"]=="Мой Wish List")
		echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/img/icons/star.svg");
    if($arItem["TEXT"]=="Мои подписки") 
		echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/img/icons/email1.svg");
	?> 
	<?=$arItem["TEXT"]?></a>
</p></li>
<?endforeach?>
<?endif?>
