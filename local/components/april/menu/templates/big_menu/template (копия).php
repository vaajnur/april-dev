<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class='big-menu big-menu123'>
    <div class='big-menu__inner'>
        <div class="row center-xs">

<?
$previousLevel = 0;
foreach($arResult as $arItem):

	/*echo "<pre>";
	print_r($arItem);
	echo "</pre>";*/
	?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?//=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
		<?//=str_repeat("</ul></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>


	<? if($arItem["DEPTH_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
			<!-- </ul></li></ul></div> -->
	<? endif;?>


	<? if(!empty($arItem['PARAMS']) && $arItem['PARAMS']['FROM_IBLOCK'] == '1' && $arItem['PARAMS']['IS_PARENT'] == '1' && $arItem['PARAMS']['DEPTH_LEVEL'] == '1'):?>
		<div class="col-xs">
            <ul class='big-menu__list'>
	<? endif;?>

	<? if(empty($arItem['PARAMS']) && !empty($previousITEM['PARAMS'])):?>
		</ul></li></ul></div>
		<div class="col-xs">
            <ul class='big-menu__list'>
	<? endif;?>


	<?if ($arItem["IS_PARENT"]):?>


		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                    <li>
                        <div><?=$arItem["TEXT"]?><div class="expand-icon"></div></div>
                        <ul class='big-menu__list-inner'>
		<?else:?>
					<li>
                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                    </li>
		<?endif?>

	<?else:?>



			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
                    <li>
                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                    </li>

			<?else:?>
			<?endif?>


	<?endif?>


	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
	<?$previousITEM = $arItem;?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?//=str_repeat("</ul></li>1111", ($previousLevel-1) );?>
<?endif?>

        </div>
    </div>
</div>
<?endif?>