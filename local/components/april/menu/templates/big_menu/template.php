<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class='big-menu'>
    <div class='big-menu__inner'>
        <div class="row center-xs">


            <div class="col-xs">
                <ul class='big-menu__list'>
                	<? foreach($arResult['data'] as $parent):?>
                    <li>
                        <div><?=$parent['NAME'];?><div class="expand-icon"></div></div>
                        <ul class='big-menu__list-inner'>
                            <li>
                                <a href="<?=$parent['SECTION_PAGE_URL'];?>"><span class='text-bold'>Посмотреть все</b></a>
                            </li>
		                	<? foreach($parent['SUBSECTIONS'] as $subsection):?>
                            <li>
                                <a href="<?=$subsection['SECTION_PAGE_URL'];?>"><?=$subsection['NAME'];?></a>
                            </li>
			            	<? endforeach;?>	
                        </ul>
                    </li>
	            	<? endforeach;?>	
                </ul>
            </div>
            <? foreach ($arResult['static'] as $key => $static_pages) {?>
            <div class="col-xs">
                <ul class="big-menu__list">
                	<? foreach ($static_pages as $key => $page) {?>
                    <li>
                        <a href="<?=$page['LINK'];?>"><?=$page['TEXT'];?></a>
                    </li>
                	<?}?>
                </ul>
            </div>
            <?}?>


            
        </div>
    </div>
</div>