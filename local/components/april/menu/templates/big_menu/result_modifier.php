<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$IBLOCK_ID = $arParams['IBLOCK_ID'];
$res1 = ciblocksection::getlist([], ['IBLOCK_ID' => $IBLOCK_ID, 'SECTION_ID' => false, 'DEPTH_LEVEL' => 1, 'ACTIVE' => 'Y'], false, ['ID', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL']);
global $sections;
$sections = [];
while ($ob = $res1->getnext()) {
	// pr($ob);
	$sections[$ob['ID']] = $ob;
	$res2 = ciblocksection::getlist([], ['IBLOCK_ID' => $IBLOCK_ID, 'SECTION_ID' => $ob['ID'], 'ACTIVE' => 'Y'], false, ['NAME', 'IBLOCK_ID', 'ID', 'SECTION_PAGE_URL']);
	while ($ob2 = $res2->getnext()) {
		$sections[$ob['ID']]['SUBSECTIONS'][] = $ob2;
	}
}

$arResult['data'] = $sections;
$arResult['static'] = array_filter($arResult, function($item){
	return ( (isset($item['PARAMS']) && empty($item['PARAMS']) )  || ( isset($item['PARAMS']) &&  $item['PARAMS']['FROM_IBLOCK'] == false ) );
});

$arResult['static'] = array_chunk($arResult['static'], round(count($arResult['static'])/2) );

// pr($sections);
// file_put_contents(__DIR__.'/filename.log', var_export($arResult['static'], true));

