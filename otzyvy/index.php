<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
?>

<main class="sidebar-version">
	<?$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"WITHOUT_FOOTER"=>"Y",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);?>
	<section class="section-reviews">
		<div class="container-fluid">
			<div class="row">
			<div class="col-xs-12">
			<div class="reviews-wrapper">
			<div class="review-items">
			<? if ($_REQUEST['AJAX']=="Y"){
				$APPLICATION->RestartBuffer();
			}?>
			<?$APPLICATION->IncludeComponent(
				"april:news.list", 
				"reviews", 
				array(
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"AJAX" => $_REQUEST["AJAX"],
					"IBLOCK_TYPE" => "info",
					"IBLOCK_ID" => "9",
					"ON_PAGE" => "5",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "rating",
						2 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"FIELD_CODE" => array(
						0 => "DETAIL_PICTURE",
						1 => "DATE_CREATE",
						2 => "CREATED_BY",
						3 => "",
					),
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_LAST_MODIFIED" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => $_REQUEST["SECTION_CODE"],
					"INCLUDE_SUBSECTIONS" => "Y",
					"CACHE_TYPE" => "A", 
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"PAGER_BASE_LINK_ENABLE" => "Y",
					"SET_STATUS_404" => "Y",
					"SHOW_404" => "N",
					"MESSAGE_404" => "",
					"PAGER_BASE_LINK" => "",
					"PAGER_PARAMS_NAME" => "arrPager",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"COMPONENT_TEMPLATE" => "reviews",
					"NEWS_COUNT" => "100",
					"FILTER_NAME" => ""
				),
				false
			);?>
			<? if ($_REQUEST['AJAX']=="Y"){
				die();
			}?>
			</div>
			<div class="morereviews">
				<? if($GLOBALS['pagescount']>1){?>
				<button pagenum="1" maxpages="2" class="btn-white loadmorereviews">Показать все отзывы</button>
				<?}?>
				<div class="load_circle-container">
					<div class="load_circle-anim"></div>
				</div>
			</div>
			<div class="reviews-form">
				<p class="header">Оставьте Ваш отзыв:</p>
				<div class="rate-us">
					<p>Оцените наш магазин:</p>
					<div class="rating">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve"><path class="st0" d="M500.9,233c10.1-9.8,13.6-24.2,9.3-37.6c-4.4-13.4-15.7-23-29.6-25l-124-18c-5.3-0.8-9.8-4.1-12.2-8.9 L288.9,31.2c-6.2-12.6-18.9-20.5-32.9-20.5c-14.1,0-26.7,7.8-32.9,20.5l-55.4,112.3c-2.4,4.8-6.9,8.1-12.2,8.9l-124,18 c-13.9,2-25.3,11.6-29.6,25c-4.3,13.4-0.8,27.8,9.3,37.6l89.7,87.4c3.8,3.7,5.6,9.1,4.7,14.4L84.3,458.3 c-2.4,13.9,3.2,27.6,14.6,35.9c11.4,8.3,26.2,9.4,38.7,2.8l110.9-58.3c4.7-2.5,10.4-2.5,15.1,0L374.4,497c5.4,2.9,11.3,4.3,17.1,4.3 c7.6,0,15.1-2.4,21.6-7.1c11.4-8.3,17-22,14.6-35.9l-21.2-123.5c-0.9-5.3,0.9-10.6,4.7-14.4L500.9,233z"/></svg>
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve"><path class="st0" d="M500.9,233c10.1-9.8,13.6-24.2,9.3-37.6c-4.4-13.4-15.7-23-29.6-25l-124-18c-5.3-0.8-9.8-4.1-12.2-8.9 L288.9,31.2c-6.2-12.6-18.9-20.5-32.9-20.5c-14.1,0-26.7,7.8-32.9,20.5l-55.4,112.3c-2.4,4.8-6.9,8.1-12.2,8.9l-124,18 c-13.9,2-25.3,11.6-29.6,25c-4.3,13.4-0.8,27.8,9.3,37.6l89.7,87.4c3.8,3.7,5.6,9.1,4.7,14.4L84.3,458.3 c-2.4,13.9,3.2,27.6,14.6,35.9c11.4,8.3,26.2,9.4,38.7,2.8l110.9-58.3c4.7-2.5,10.4-2.5,15.1,0L374.4,497c5.4,2.9,11.3,4.3,17.1,4.3 c7.6,0,15.1-2.4,21.6-7.1c11.4-8.3,17-22,14.6-35.9l-21.2-123.5c-0.9-5.3,0.9-10.6,4.7-14.4L500.9,233z"/></svg>
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve"><path class="st0" d="M500.9,233c10.1-9.8,13.6-24.2,9.3-37.6c-4.4-13.4-15.7-23-29.6-25l-124-18c-5.3-0.8-9.8-4.1-12.2-8.9 L288.9,31.2c-6.2-12.6-18.9-20.5-32.9-20.5c-14.1,0-26.7,7.8-32.9,20.5l-55.4,112.3c-2.4,4.8-6.9,8.1-12.2,8.9l-124,18 c-13.9,2-25.3,11.6-29.6,25c-4.3,13.4-0.8,27.8,9.3,37.6l89.7,87.4c3.8,3.7,5.6,9.1,4.7,14.4L84.3,458.3 c-2.4,13.9,3.2,27.6,14.6,35.9c11.4,8.3,26.2,9.4,38.7,2.8l110.9-58.3c4.7-2.5,10.4-2.5,15.1,0L374.4,497c5.4,2.9,11.3,4.3,17.1,4.3 c7.6,0,15.1-2.4,21.6-7.1c11.4-8.3,17-22,14.6-35.9l-21.2-123.5c-0.9-5.3,0.9-10.6,4.7-14.4L500.9,233z"/></svg>
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve"><path class="st0" d="M500.9,233c10.1-9.8,13.6-24.2,9.3-37.6c-4.4-13.4-15.7-23-29.6-25l-124-18c-5.3-0.8-9.8-4.1-12.2-8.9 L288.9,31.2c-6.2-12.6-18.9-20.5-32.9-20.5c-14.1,0-26.7,7.8-32.9,20.5l-55.4,112.3c-2.4,4.8-6.9,8.1-12.2,8.9l-124,18 c-13.9,2-25.3,11.6-29.6,25c-4.3,13.4-0.8,27.8,9.3,37.6l89.7,87.4c3.8,3.7,5.6,9.1,4.7,14.4L84.3,458.3 c-2.4,13.9,3.2,27.6,14.6,35.9c11.4,8.3,26.2,9.4,38.7,2.8l110.9-58.3c4.7-2.5,10.4-2.5,15.1,0L374.4,497c5.4,2.9,11.3,4.3,17.1,4.3 c7.6,0,15.1-2.4,21.6-7.1c11.4-8.3,17-22,14.6-35.9l-21.2-123.5c-0.9-5.3,0.9-10.6,4.7-14.4L500.9,233z"/></svg>
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve"><path class="st0" d="M500.9,233c10.1-9.8,13.6-24.2,9.3-37.6c-4.4-13.4-15.7-23-29.6-25l-124-18c-5.3-0.8-9.8-4.1-12.2-8.9 L288.9,31.2c-6.2-12.6-18.9-20.5-32.9-20.5c-14.1,0-26.7,7.8-32.9,20.5l-55.4,112.3c-2.4,4.8-6.9,8.1-12.2,8.9l-124,18 c-13.9,2-25.3,11.6-29.6,25c-4.3,13.4-0.8,27.8,9.3,37.6l89.7,87.4c3.8,3.7,5.6,9.1,4.7,14.4L84.3,458.3 c-2.4,13.9,3.2,27.6,14.6,35.9c11.4,8.3,26.2,9.4,38.7,2.8l110.9-58.3c4.7-2.5,10.4-2.5,15.1,0L374.4,497c5.4,2.9,11.3,4.3,17.1,4.3 c7.6,0,15.1-2.4,21.6-7.1c11.4-8.3,17-22,14.6-35.9l-21.2-123.5c-0.9-5.3,0.9-10.6,4.7-14.4L500.9,233z"/></svg>
					</div>
				</div>
				<?$APPLICATION->IncludeComponent(
				"april:iblock.element.add.form", 
				"addreview", 
				array(
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"SEF_MODE" => "Y",
					"IBLOCK_TYPE" => "info",
					"IBLOCK_ID" => "9",
					"PROPERTY_CODES" => array(
						0 => "66",
						1 => "NAME",
						2 => "PREVIEW_TEXT",
						3 => "PREVIEW_PICTURE",
					),
					"PROPERTY_CODES_REQUIRED" => array(
						1 => "NAME",
						2 => "PREVIEW_TEXT",
					),
					"GROUPS" => array(
						0 => "2",
					),
					"STATUS_NEW" => "NEW",
					"STATUS" => "ANY",
					"LIST_URL" => "",
					"ELEMENT_ASSOC" => "PROPERTY_ID",
					"ELEMENT_ASSOC_PROPERTY" => "",
					"MAX_USER_ENTRIES" => "100000",
					"MAX_LEVELS" => "100000",
					"LEVEL_LAST" => "Y",
					"USE_CAPTCHA" => "N",
					"USER_MESSAGE_EDIT" => "",
					"USER_MESSAGE_ADD" => "Спасибо! Ваш отзыв добавлен и появится после проверки модератором",
					"DEFAULT_INPUT_SIZE" => "30",
					"RESIZE_IMAGES" => "Y",
					"MAX_FILE_SIZE" => "0",
					"CUSTOM_TITLE_NAME" => "Укажите Ваше имя:",
					"CUSTOM_TITLE_TAGS" => "",
					"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
					"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
					"CUSTOM_TITLE_IBLOCK_SECTION" => "",
					"CUSTOM_TITLE_PREVIEW_TEXT" => "Напишите Ваше мнение о нашем магазине и покупках:",
					"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
					"CUSTOM_TITLE_DETAIL_TEXT" => "",
					"CUSTOM_TITLE_DETAIL_PICTURE" => "",
					"SEF_FOLDER" => "/",
					"COMPONENT_TEMPLATE" => "addreview",
					"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
					"DETAIL_TEXT_USE_HTML_EDITOR" => "N"
				),
				false
			);?>
			</div>
			</div>
			</div>
			</div>
		</div>
	</section>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>