<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
if(isset($_REQUEST['ORDER_ID'])){?>
    <?
    $order_id=$_REQUEST['ORDER_ID'];
    $arOrder = CSaleOrder::GetByID($order_id);
	$arBaketItems=array();
	$arBaketItemIds=array(0);
	$rsBasket = CSaleBasket::GetList(
		array(),
		array(
			'ORDER_ID' => $order_id
		),
		false,
		false,
		array("ID","NAME","QUANTITY","PRICE","PRODUCT_ID")
	);
	
	$BASKET_IDS = "";
	$QUANTITIES = "";
	while($arBasket = $rsBasket->GetNext())
	{
		$BASKET_IDS .= $arBasket["ID"]."%2C";
	   	$QUANTITIES .= $arBasket["QUANTITY"]."%2C";
	   	$arBaketItems[$arBasket["PRODUCT_ID"]]=array(
	   		"ID" => $arBasket["PRODUCT_ID"],
	   		"PRICE" => $arBasket["PRICE"],
	   		"QUANTITY" => $arBasket["QUANTITY"]
		);
		$arBaketItemIds[]=$arBasket["PRODUCT_ID"];
	}
	$resBItem=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>21,"ID"=>$arBaketItemIds),false,false,array("ID","NAME","IBLOCK_SECTION_ID","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE"));
	while($arBItem=$resBItem->GetNext())
	{	
		$resBItemSection=CIBlockSection::GetById(intval($arBItem["IBLOCK_SECTION_ID"]));
		$arBItemSection=$resBItemSection->GetNext();
		$arBaketItems[$arBItem["ID"]]["NAME"]=explode(" ",$arBItem["NAME"])[0]." ".$arBItem["PROPERTY_CML2_MANUFACTURER_VALUE"]." ".$arBItem["PROPERTY_CML2_ARTICLE_VALUE"];
		$arBaketItems[$arBItem["ID"]]["BRAND"]=$arBItem["PROPERTY_CML2_MANUFACTURER_VALUE"];
		$arBaketItems[$arBItem["ID"]]["CATALOG_ID"]=$arBItem["ID"];
		$arBaketItems[$arBItem["ID"]]["SECTION"]=$arBItemSection["NAME"];
    }
    $couponList = \Bitrix\Sale\Internals\OrderCouponsTable::getList(array(
        'select' => array('COUPON'),
        'filter' => array('=ORDER_ID' => $order_id)
    ));
    while ($coupon = $couponList->fetch())
    {
        if(strlen($coupon['COUPON'])>0){
            $coupon_text=trim($coupon['COUPON']);
        }
    }
    ?>
	<script>
		dataLayer.push({
			"event": "Call Back",
			"ecommerce": {
			"purchase": {
			"actionField": {	
			"id" : '<?=$order_id?>',		
        },
        "shipping": '<?=$arOrder['PRICE_DELIVERY']?>',
		"products": [
		<? foreach($arBaketItems as $key=>$basketItem){
			$mxResult = CCatalogSku::GetProductInfo(
				$key
			);
			if (is_array($mxResult))
			{
				$product_id=$mxResult['ID'];
			}
			$resItem=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>intval($product_id)),false,false,array("ID","IBLOCK_ID","NAME","IBLOCK_SECTION_ID","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE"));
                $arItem=$resItem->GetNext();
                $nav = CIBlockSection::GetNavChain($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
                $category="";
                while ($arNav=$nav->GetNext()):
                    $category.=$arNav["NAME"].'/';
                endwhile;
                $category=trim($category,"/");  
				$id=$arItem['ID'];
				$resBItemSection=CIBlockSection::GetById(intval($arItem["IBLOCK_SECTION_ID"]));
				$arBItemSection=$resBItemSection->GetNext();
                $designer = $arItem['PROPERTY_CML2_MANUFACTURER_VALUE'];
			 	$article = $arItem['PROPERTY_CML2_ARTICLE_VALUE'];
                $arReturn['name']=explode(" ",$arItem['NAME'])[0]." ".$designer." ".$article;
			?>			
			{ 
				"id": '<?=$id?>',
				"name": '<?=$arReturn['name']?>',
				"brand": '<?=$designer?>',
				"price": '<?=number_format($basketItem["PRICE"],0,".","")?>',
				'category': '<?=$category?>',
                "quantity": '<?=intval($basketItem["QUANTITY"])?>',
                
				"coupon": '<?=$coupon_text?>'
			},
		<? }?>
		]
		}
		}
		});
		//ga('send',  'event',  'Ecommerce',  'Checkout Success');      
	</script>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>