<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<script type="text/javascript" src="https://april.server.paykeeper.ru/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="https://april.server.paykeeper.ru/js/jquery.cookie.js"></script>
<script type="text/javascript" src="https://april.server.paykeeper.ru/js/order.js"></script>
<script type="text/javascript" src="https://april.server.paykeeper.ru/js/jquery.maskedinput-1.3.1.min.js"></script>
<script type="text/javascript" src="https://april.server.paykeeper.ru/js/jquery.validationEngine.js"></script>
<link rel="stylesheet" type="text/css" href="https://april.server.paykeeper.ru/css/order.css">
<main class="sidebar-version">
	<?$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"WITHOUT_FOOTER"=>"Y",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);?>
<section class="contact-section">
		<div class="container-fluid">
			<div class="row">
<?
if(strlen($_SESSION['ORDER_TO_PAY'])>0)
	$order_id=$_SESSION['ORDER_TO_PAY'];
else
	$order_id=$_REQUEST["ORDER_ID"];
$_SESSION['ORDER_TO_PAY']="";
$_SESSION['ORDER_TO_FINISH']=$order_id;
$arOrder = CSaleOrder::GetByID($order_id);
$dbPaySysAction = CSalePaySystemAction::GetList(
	array(),
	array(
			"ID" => $arOrder['PAY_SYSTEM_ID']
		),
	false,
	false,
	array("NAME", "ACTION_FILE", "NEW_WINDOW", "PARAMS", "ENCODING", "LOGOTIP")
);
$arPaySysAction = $dbPaySysAction->Fetch();
if($arOrder['PAYED']=="Y")
	header("Location: /personal/orders/");
if($arOrder["USER_ID"]!=$USER->GetID()){
	//CHTTP::SetStatus("404 Not Found");
	//@define("ERROR_404","Y");
}
CSalePaySystemAction::InitParamArrays($arOrder, $order_id, $arPaySysAction["PARAMS"]);
$pathToAction = $_SERVER["DOCUMENT_ROOT"].$arPaySysAction["ACTION_FILE"];
$pathToAction = str_replace("//", "/", $pathToAction);
while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
	$pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);
if (file_exists($pathToAction))
{
	if (is_dir($pathToAction) && file_exists($pathToAction."/payment.php"))
		$pathToAction .= "/payment.php";

	try
	{
		include_once($pathToAction);
	}
	catch(\Bitrix\Main\SystemException $e)
	{
		if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
			$arResult["ERROR"][] = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
		else
			$arResult["ERROR"][] = $e->getMessage();
	}
}?>
</div></div></section></main>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>