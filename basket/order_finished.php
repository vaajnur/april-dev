<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("Заказ оформлен");
?>
<?
$_SESSION['new_user']="N";
if(strlen($_SESSION['ORDER_TO_FINISH'])>0)
	$order_id=$_SESSION['ORDER_TO_FINISH'];
else
	$order_id=htmlspecialcharsbx($_REQUEST["ORDER_ID"]);
$_SESSION['ORDER_TO_FINISH']="";
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

//считаем кол-во заказов
$arFilter_countorders = Array(
      "USER_ID" => $USER->GetID(),
      "STATUS_ID"=>"F"
  );
 $rsSales_countorders = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter_countorders);
  $countorders=0;
 while ($arSales_countorders = $rsSales_countorders->Fetch())
 {
    $countorders++;
 }
//после оформления заказа в емейл маркетинге ставим что нет просмотренных категорий и товаров и обновляем дату для дальнейшего маркетинга
if(isset($_COOKIE['BITRIX_SM_USER_EMAIL'])){
     $results = $DB->Query("SELECT * FROM `b_email_marketing_custom` WHERE `email`='".$DB->ForSQL($_COOKIE['BITRIX_SM_USER_EMAIL'])."'"); 
     while ($row = $results->Fetch())
     {
         $row['viewed_categories']=$row['viewed_categories'].$props_array['ID'].";";
         $arAr = Array(
             "count_orders"=>"'".$countorders."'",
             "add_to_cart"    => "0",
             "sended"    => "0",
             "viewed_goods"    => "",
             "viewed_categories"    => "",
             "date_of_sub"    => "".strtotime("now"), 
          );
         $DB->Update("b_email_marketing_custom", $arAr, "WHERE `email`='".$DB->ForSQL($_COOKIE['BITRIX_SM_USER_EMAIL'])."'", $err_mess.__LINE__);
     }
}
if($countorders>1){
	AddMessage2Log("письмо 3.2 - ".$USER->GetEmail());
    //письмо 3.2 Сразу после оформления каждого повторного заказа
   $arEventFields = array( 
         "EMAIL" => '"'.$USER->GetEmail().'"' 
     ); 
     if (CModule::IncludeModule("main")): 
       /* if (CEvent::Send("EMAIL_MARKETING_CUSTOM", "s1", $arEventFields,"Y",62)): 
           echo ""; 
        endif; */
     endif;
}
//если первый заказ, то сегментируем в заказчики
if ($countorders<2){
    require_once($_SERVER["DOCUMENT_ROOT"]."/includes/unisenderApi.php"); //подключаем файл класса
    $apikey=UNISENDER_API_KEY; //API-ключ к вашему кабинету
    $user_list = UNISENDER_LIST;
    $uni=new UniSenderApi($apikey); //создаем экземляр класса, с которым потом будем работать
    $uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$USER->GetEmail(),"guest"=>"Нет","customer"=>"Да","potential_client"=>"Нет"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d"))); 
}
?>
<main class="sidebar-version order-finished-version">
	<?$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);?>
	<section class="section-order-finished">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 finish-text col-sm-8 col-md-5">
					<div class="order-finished">
                        <?
                         //массив с товарами-сертификатами, если такие найдутся
                        $sertarray=array();
                        $sertarrayprops=array();
                        if (!($arOrder = CSaleOrder::GetByID($order_id)))
                        {
                            if($_REQUEST['payed']=="Y"){
                                if($arOrder['STATUS_ID']!="P"){
                                    $arFieldss = array(
                                        "STATUS_ID" => "P"
                                    );
                                    CSaleOrder::Update($order_id, $arFieldss);
                                    $phone=$GLOBALS['contacts']['PROPERTY_PHONE_ADMIN_VALUE'];
                                    $message="На сайте Апрель оплачен заказ №".$order_id."!";
                                    AddMessage2Log($message);
                                    $sessionId=file_get_contents("https://integrationapi.net/rest/user/sessionid?login=AprilFashion&password=Lempicka369");
                                    $sessionId=trim($sessionId,'"');
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL,"https://integrationapi.net/rest/Sms/Send");
                                    curl_setopt($ch, CURLOPT_POST, 1);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS,"SessionId=".$sessionId."&DestinationAddress=".$phone."&Data=".$message."&Validity=0&SourceAddress=APRIL");
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                
                                    $server_output = curl_exec ($ch);
                                    curl_close ($ch);
                                    echo "<div class='finished-header'><h2>Ваш заказ оплачен!</h2><a href='/personal/orders/'>Мои заказы</a></div>";
                                }
                            }
                            else
                                echo "Заказ с кодом ".$order_id." не найден";
                        }
                        else
                        {
                            $order_id=$order_id;
                            $dbBasketItems = CSaleBasket::GetList(
                             array(
                                  "NAME" => "ASC",
                                  "ID" => "ASC"
                                  ),
                             array(
                                "ORDER_ID" => $order_id
                                  ),
                                false,
                                false,
                                array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT",  "NAME")
                            );
                            $sertindex=0;
                            $counter=0;
                            if(!isset($_REQUEST['SERTIFICAT'])){
                                while ($arItems = $dbBasketItems->Fetch())
                                {
                                    $counter++;
                                    $pos=strpos($arItems['NAME'],"сертификат");
                                    if($pos!==false){
                                        array_push($sertarray,$arItems);
                                        $db_res = CSaleBasket::GetPropsList(
                                                array(
                                                        "SORT" => "ASC",
                                                        "NAME" => "ASC"
                                                    ),
                                                array("BASKET_ID" => $arItems['ID'])
                                            );
                                        while ($ar_res = $db_res->Fetch())
                                        {
                                           $sertarrayprops[$sertindex][]=$ar_res;
                                        }
                                       CSaleBasket::Delete($arItems['ID']);
                                       $sertindex++;
                                    }
                                }
                            }
                            if(count($sertarray)>0){

                                 //CSaleBasket::DeleteAll($arOrder['USER_ID'], False);
                                 
                                $sertindex=0;
                                foreach($sertarray as $serti){
                                    $arFieldsItem = array(
                                        'PRODUCT_ID' => $serti['PRODUCT_ID'],
                                        'PRICE' => $serti['PRICE'],
                                        'CURRENCY' => 'RUB',
                                        'QUANTITY' => $serti['QUANTITY'],
                                        'LID' => 's1',
                                        'NAME' => $serti['NAME'],
                                        'ORDER_ID' =>  $order_id_new
                                        );
                                     $arFieldsItem["PROPS"] = $sertarrayprops[$sertindex];
                                    $el = new CIBlockElement;
                                    $PROP = array();
                                    //CSaleBasket::Add($arFieldsItem);
                                    $order_id_new = CSaleOrder::Add(
                                    array(
                                       'LID' => SITE_ID,
                                       'PERSON_TYPE_ID' => 1,
                                       'PAYED' => "N",
                                       'CANCELED' => "N",
                                       'STATUS_ID' => "N",
                                       'ALLOW_DELIVERY' => 'N',
                                       'PRICE' => $sum,
                                       'CURRENCY' => "RUB",
                                       'USER_ID' => $arOrder['USER_ID'],
                                       'PAY_SYSTEM_ID' => 3,
                                       'DELIVERY_ID' => 3,
                                       'USER_DESCRIPTION' => 'ЗАКАЗ Сертификаты',
                                       'ADDITIONAL_INFO' => 'ЗАКАЗ Сертификаты',
                                       )
                                    );
                                    if ($order_id_new > 0){
                                         $arFields = array(
                                            'PRODUCT_ID' => $serti['PRODUCT_ID'],
                                            'PRICE' => $serti['PRICE'],
                                            "CURRENCY" => "RUB",
                                            "WEIGHT" => $ar_res['PRODUCT']['WEIGHT'],
                                            'QUANTITY' => $serti['QUANTITY'],
                                            "DELAY" => "N",
                                            'LID' => 's1',
                                            "CAN_BUY" => "Y",
                                            "ORDER_ID" => $order_id_new,
                                            'NAME' => $serti['NAME'],
                                            "MODULE" => "catalog",
                                            "NOTES" => "",
                                            "DETAIL_PAGE_URL" => $ar_res['DETAIL_PAGE_URL'],
                                            "PROPS" => $arProps
                                        );
                                        $add = CSaleBasket::Add($arFields);
                                    }
                                    foreach($sertarrayprops[$sertindex] as $sertprop){
                                        switch ($sertprop['CODE']){
                                           case "ADDRESS_RECIPIENT":
                                               $PROP[103]=$sertprop['VALUE'];
                                               break;
                                           case "INDEX_RECIPIENT":
                                               $PROP[104]=$sertprop['VALUE'];
                                               break;
                                           case "RECIPIENT":
                                               $PROP[102]=$sertprop['VALUE'];
                                               break;
                                           case "SENDER":
                                               $PROP[101]=$sertprop['VALUE'];
                                               break;
                                           case "MESSAGE":
                                               $PROP[109]=$sertprop['VALUE'];
                                               break;
                                           case "TYPE_SERTIFICATE":
                                               $PROP[100]=$sertprop['VALUE'];
                                               break;
                                           case "EMAIL_RECIPIENT":
                                               $PROP[107]=$sertprop['VALUE'];
                                               break;
                                           case "DATE_OF_RECEIVING":
                                               $PROP[108]=$sertprop['VALUE'];
                                               break;
                                        }

                                    }
                                    $PROP[106]=$serti['PRICE']; 
                                    $arFilter_с = Array("IBLOCK_ID"=>16);
                                    $res_count = CIBlockElement::GetList(Array(), $arFilter_с, Array(), false, Array());
                                    $arLoadProductArray = Array(
                                      "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
                                      "IBLOCK_SECTION_ID" => false,       // элемент лежит в корне раздела
                                      "IBLOCK_ID"      => 16,
                                      "PROPERTY_VALUES"=> $PROP,
                                      "NAME"           => "SERT-000".$res_count,
                                      "ACTIVE"         => "Y",            // активен
                                      );

                                    if($PRODUCT_ID = $el->Add($arLoadProductArray)){
                                       //echo "New ID: ".$PRODUCT_ID; 
                                    }   
                                    else{
                                       //echo "Error: ".$el->LAST_ERROR; 
                                    }

                                    $discid=3;
                                    if(intval($serti['PRICE'])==10000)
                                       $discid=4; 
                                    if(intval($serti['PRICE'])==15000)
                                       $discid=5; 
                                    $time = strtotime(date("d.m.Y h:i:s"));
                                    $date_from = new \Bitrix\Main\Type\DateTime(date("d.m.Y h:i:s"));
                                    $final = new \Bitrix\Main\Type\DateTime(date("d.m.Y h:i:s", strtotime("+3 month", $time)));
                                    $result = \Bitrix\Sale\Internals\DiscountCouponTable::add(array(
                                        "COUPON"=>"SERT-0000".$res_count,
                                        "DISCOUNT_ID"=>$discid,
                                        "ACTIVE"=>"Y",
                                        "TYPE"=>2,
                                        "ACTIVE_FROM"=>$date_from,
                                        "ACTIVE_TO"=>$final,
                                    ));
                                    if ($result->isSuccess())
                                    {
                                      // echo "Ss";
                                    }
                                    else
                                    {
                                     //  print_r($result->getErrorMessages());
                                    }
                                    $sertindex++;
                                }

                                CSaleBasket::OrderBasket($order_id_new, $arOrder['USER_ID'], 's1');
                                $orderprops = CSaleOrderPropsValue::GetList(
                                    array(),
                                    array(
                                        "ORDER_ID"=>$arOrder['ID']
                                    )
                                );
                                while($arVals = $orderprops->Fetch()){
                                    $arFields = array(
                                       "ORDER_ID" => $order_id_new,
                                       "ORDER_PROPS_ID" => $arVals["ORDER_PROPS_ID"],
                                       "NAME" => $arVals["NAME"],
                                       "CODE" => $arVals["CODE"],
                                       "VALUE" => $arVals["VALUE"]
                                    );
                                    CSaleOrderPropsValue::Add($arFields);
                                }
                               $inorder=CSaleBasket::GetList(false, array("ORDER_ID" => $order_id),false,false,array("ID"))->SelectedRowsCount();
                                if($inorder==0){
                                    CSaleOrder::Delete($order_id);
                                }
                               header("Location: /basket/order_finished.php?SERTIFICAT=Y&ORDER_ID=".$order_id_new);
                            }
                        ?>
						<div class="finished-header">
							<h2>
                            <?
                            $rsUser = CUser::GetByID($arOrder['USER_ID']); 
                            $arUser = $rsUser->Fetch(); 
                            $arUser['NAME'] = $_SESSION['order_props']['ORDER_PROP_1']; 
                            // pr($_SESSION['order_props']);
                            ?><?=$arUser['NAME'];?>, спасибо за покупку!<br>
								Ваш заказ <span class="order-number">№<?=$order_id;?></span><br>успешно оформлен!
							</h2>
						</div>
                        <?
                         //время работы магазина
                        $arSelect = Array("ID","NAME","PROPERTY_TIMEBACKEND",);
                        $rs = CIBlockElement::GetList (
                        Array("RAND" => "ASC"),
                        Array("IBLOCK_ID" => 8, "ID"=>3278),
                        false,
                        Array (),
                        $arSelect
                        );
                        if($arOrder["USER_ID"]!=$USER->GetID()){
                            /*CHTTP::SetStatus("404 Not Found");
                            @define("ERROR_404","Y");*/
                        }
                        $props_shop=$rs->GetNext();
                        $pieces = explode(";", $props_shop['PROPERTY_TIMEBACKEND_VALUE']);
                        $times = explode(":", $pieces[0]);
                        $nowhour=date('H');
                        if($nowhour<$times[1]&&$nowhour<23){
                            $data=date("d.m.Y")." в ".$times[0]." часов"; 
                        }
                        else{
                            $data=date("d.m.Y", time() + 86400)." в ".$times[0]." часов"; 
                        }
                        if($nowhour>=$times[0]&&$nowhour<=$times[1]&&$arOrder["DELIVERY_ID"]==3)
                            $message= "В ближайшее время Вам позвонит менеджер<br>для утверждения заказа.";
                        else if($nowhour>=$times[0]&&$nowhour<=$times[1])
                            $message= "В ближайшее время Вам позвонит менеджер<br>для согласования удобного времени доставки.";
                        else
                            $message= "Менеджер позвонит Вам сразу после начала работы магазина ".$data."!";
                         ?>
						<div class="finished-body">
							<p>
								<!--<?=$message;?>-->В ближайшее время Вы получите письмо - подтверждение.
							</p>
						</div>
                        <?}?>
					</div>
				</div>
			</div>
		</div>
	</section>

</main>
<?
if(isset($_REQUEST['ORDER_ID'])){?>
    <?
    $order_id=$_REQUEST['ORDER_ID'];
    $arOrder = CSaleOrder::GetByID($order_id);
	$arBaketItems=array();
	$arBaketItemIds=array(0);
	$rsBasket = CSaleBasket::GetList(
		array(),
		array(
			'ORDER_ID' => $order_id
		),
		false,
		false,
		array("ID","NAME","QUANTITY","PRICE","PRODUCT_ID")
	);
	
	$BASKET_IDS = "";
	$QUANTITIES = "";
	while($arBasket = $rsBasket->GetNext())
	{
		$BASKET_IDS .= $arBasket["ID"]."%2C";
	   	$QUANTITIES .= $arBasket["QUANTITY"]."%2C";
	   	$arBaketItems[$arBasket["PRODUCT_ID"]]=array(
	   		"ID" => $arBasket["PRODUCT_ID"],
	   		"PRICE" => $arBasket["PRICE"],
	   		"QUANTITY" => $arBasket["QUANTITY"]
		);
		$arBaketItemIds[]=$arBasket["PRODUCT_ID"];
	}
	$resBItem=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>21,"ID"=>$arBaketItemIds),false,false,array("ID","NAME","IBLOCK_SECTION_ID","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE"));
	while($arBItem=$resBItem->GetNext())
	{	
		$resBItemSection=CIBlockSection::GetById(intval($arBItem["IBLOCK_SECTION_ID"]));
		$arBItemSection=$resBItemSection->GetNext();
		$arBaketItems[$arBItem["ID"]]["NAME"]=explode(" ",$arBItem["NAME"])[0]." ".$arBItem["PROPERTY_CML2_MANUFACTURER_VALUE"]." ".$arBItem["PROPERTY_CML2_ARTICLE_VALUE"];
		$arBaketItems[$arBItem["ID"]]["BRAND"]=$arBItem["PROPERTY_CML2_MANUFACTURER_VALUE"];
		$arBaketItems[$arBItem["ID"]]["CATALOG_ID"]=$arBItem["ID"];
		$arBaketItems[$arBItem["ID"]]["SECTION"]=$arBItemSection["NAME"];
    }
    $couponList = \Bitrix\Sale\Internals\OrderCouponsTable::getList(array(
        'select' => array('COUPON'),
        'filter' => array('=ORDER_ID' => $order_id)
    ));
    while ($coupon = $couponList->fetch())
    {
        if(strlen($coupon['COUPON'])>0){
            $coupon_text=trim($coupon['COUPON']);
        }
    }
    ?>
	<script>
		dataLayer.push({
			"event": "Checkout Success",
			"ecommerce": {
			"purchase": {
			"actionField": {	
			"id" : '<?=$order_id?>',
            "shipping": '<?=$arOrder['PRICE_DELIVERY']?>',
            "coupon": '<?=$coupon_text?>',		
        },
		"products": [
		<? foreach($arBaketItems as $key=>$basketItem){
			$mxResult = CCatalogSku::GetProductInfo(
				$key
			);
			if (is_array($mxResult))
			{
				$product_id=$mxResult['ID'];
			}
			$resItem=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>intval($product_id)),false,false,array("ID","IBLOCK_ID","NAME","IBLOCK_SECTION_ID","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE"));
                $arItem=$resItem->GetNext();
                $nav = CIBlockSection::GetNavChain($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
                $category="";
                while ($arNav=$nav->GetNext()):
                    $category.=$arNav["NAME"].'/';
                endwhile;
                $category=trim($category,"/");  
				$id=$arItem['ID'];
				$resBItemSection=CIBlockSection::GetById(intval($arItem["IBLOCK_SECTION_ID"]));
				$arBItemSection=$resBItemSection->GetNext();
                $designer = $arItem['PROPERTY_CML2_MANUFACTURER_VALUE'];
			 	$article = $arItem['PROPERTY_CML2_ARTICLE_VALUE'];
                $arReturn['name']=explode(" ",$arItem['NAME'])[0]." ".$designer." ".$article;
			?>			
			{ 
				"id": '<?=$id?>',
				"name": '<?=$arReturn['name']?>',
				"brand": '<?=$designer?>',
				"price": '<?=number_format($basketItem["PRICE"],0,".","")?>',
				'category': '<?=$category?>',
                "quantity": '<?=intval($basketItem["QUANTITY"])?>',
                
				
			},
		<? }?>
		]
		}
		}
		});
		//ga('send',  'event',  'Ecommerce',  'Checkout Success');      
	</script>
<?}?>
<?
CCatalogDiscountCoupon::ClearCoupon();
?>
	<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>