<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Корзина покупок Aprilmoscow.ru");
$APPLICATION->SetTitle("Оформление заказа");
?><?
if(isset($_REQUEST['ORDER_ID'])){
    LocalRedirect("/basket/order_finished.php?ORDER_ID=".$_REQUEST['ORDER_ID']);
}
?>
<main class="sidebar-version">
	<?$APPLICATION->IncludeComponent(
		  "bitrix:main.include",
		  "",
		  Array(
			"AREA_FILE_SHOW" => "file", //Показывать информацию из файла
			"AREA_FILE_SUFFIX" => "",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/include/breadcrumbs.php" //Указываем путь к файлу
		  )
	);?>
    <section class="section-cart">
		<div class="container-fluid">
			<div class="row">
             <? if(isset($_REQUEST["AJAX"]))
                $APPLICATION->RestartBuffer(); ?>   
            <?$APPLICATION->IncludeComponent("april:sale.basket.basket", "", array(
                "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                "COLUMNS_LIST" => array(
                    0 => "NAME",
                    1 => "DISCOUNT",
                    2 => "PRICE",
                    3 => "QUANTITY",
                    4 => "SUM",
                    5 => "PROPS",
                    6 => "DELETE",
                    7 => "DELAY",
                ),
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "PATH_TO_ORDER" => "/personal/order/make/",
                "HIDE_COUPON" => "N",
                "QUANTITY_FLOAT" => "N",
                "PRICE_VAT_SHOW_VALUE" => "Y",
                "TEMPLATE_THEME" => "site",
                "SET_TITLE" => "Y",
                "AJAX_OPTION_ADDITIONAL" => "",
                "OFFERS_PROPS" => array(
                    0 => "SIZES_SHOES",
                    1 => "SIZES_CLOTHES",
                    2 => "COLOR_REF",
                ),
                ),
                false
            );?>
            <? if(isset($_REQUEST["AJAX"]))
                die(); ?>
            </div>
        </div>
    </section>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>