<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//тег каноникал для пагинации
if(strpos($_SERVER['REQUEST_URI'],"page")!==false){
	$url=preg_replace('/page+[0-9]{4}/i', '', $_SERVER['REQUEST_URI'], -1, $count);
    $url=preg_replace('/page+[0-9]{3}/i', '', $_SERVER['REQUEST_URI'], -1, $count);
    $url=preg_replace('/page+[0-9]{2}/i', '', $_SERVER['REQUEST_URI'], -1, $count);
	$url=preg_replace('/page+[0-9]{1}/i', '', $_SERVER['REQUEST_URI'], -1, $count);
	$url=trim($url,"/");
	$APPLICATION->AddHeadString('<link rel="canonical" href="https://aprilmoscow.ru/'.$url.'/" />',true);
}
$SET404="Y";
if(isset($_REQUEST['PAGEN_1'])){
     $SET404="N";
}
//фильтр по цене
$GLOBALS['CATALOG_FILTER']=array(/*">CATALOG_PRICE_2"=>0,"CATALOG_AVAILABLE"=>"Y","!DETAIL_PICTURE"=>false*/);
	/*if(!isset($_COOKIE['SORT_FIELD'])){
		$sortarray=array("CATALOG_PRICE_1","ID","sort","timestamp_x","name","shows");
		$number = rand(0, count($sortarray) - 1);
		setcookie("SORT_FIELD", $sortarray[$number], time() - 3600, "/");
	}
	if(!isset($_COOKIE['SORT_ORDER'])){
		$sortarray=array("asc","desc");
		$number = rand(0, count($sortarray) - 1);
		setcookie("SORT_ORDER", $sortarray[$number], time() - 3600, "/");
	}*/
	$sort="timestamp_x";
	$sort_order="desc";
//если страница Поиск
if(isset($_REQUEST['SEARCH'])){
    $SET404="N";
    $APPLICATION->AddChainItem("Поиск", "");
    $APPLICATION->SetTitle("Поиск");
    if(isset($_REQUEST['q'])){
        $_SESSION['SEARCH'.$url]=$_REQUEST['q'];
    }
    if(isset($_SESSION['SEARCH'.$url])){
        $_REQUEST['q']=$_SESSION['SEARCH'.$url];
    }
}
//если страница Тег
$url_words=explode("/",$_SERVER['REQUEST_URI']);
foreach($url_words as $word){
	if(strlen($word)>0){
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_ID"=>22, "ACTIVE"=>"Y","CODE"=>$word);
		$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nTopCount"=>1), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$_REQUEST['tag']=$arFields['ID'];
		
		}
	}
}

//если страница Новинки
if(isset($_REQUEST['NOVINKI'])){
	$sort="id";
	$sort_order="desc";
     $SET404="N";
    //TODO поменять дату на date("d.m.Y H:i:s",strtotime("-7 days"));
	//$GLOBALS['CATALOG_FILTER'][">DATE_CREATE"]= date("d.m.Y H:i:s",strtotime("-120 days"));
	$arSelect = Array("ID", "NAME");
	$arFilter = Array("IBLOCK_ID"=>13,"CODE"=>array("cyrille-gassiline","joseph","rag-bone","jbrand","equipment"),"ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	$found=false;
	while($ob = $res->GetNextElement())
	{
		
		$arFi=$ob->GetFields();
		if(strlen($arFi['NAME'])>0)
			$found=true;
		$GLOBALS['CATALOG_FILTER']["PROPERTY_CML2_MANUFACTURER_VALUE"][]= $arFi['NAME'];

	}
    $APPLICATION->AddChainItem("Новинки", "");
    $APPLICATION->SetTitle("Новинки");
	$APPLICATION->SetPageProperty("description", "Скидки и распродажа женской одежды в Aprilmoscow.ru .  Только лучшая и оригинальная одежда от лучших мировых дизайнеров. Удобный бутик в центре Москвы.");
	$APPLICATION->SetPageProperty("title", "Распродажа женской одежды - интернет магазин Aprilmoscow.ru | Женская одежда со скидкой");
}
//если страница Распродажа
if(isset($_REQUEST['RASPRODAZHA'])){
	//$sort="desc";
     $SET404="N";
    //TODO поменять дату на date("d.m.Y H:i:s",strtotime("-7 days"));
    /* ФИЛЬТР НА SALE поставить потом или по скидкам */
	$GLOBALS['CATALOG_FILTER'][">PROPERTY_RAZMER_SKIDKI_VALUE"]= 0;
    $APPLICATION->AddChainItem("Распродажа", "");
    $APPLICATION->SetTitle("Распродажа");
	$APPLICATION->SetPageProperty("description", "Скидки и распродажа женской одежды в Aprilmoscow.ru .  Только лучшая и оригинальная одежда от лучших мировых дизайнеров. Удобный бутик в центре Москвы.");
	$APPLICATION->SetPageProperty("title", "Распродажа женской одежды - интернет магазин Aprilmoscow.ru | Женская одежда со скидкой");
}
//если страница бренда
if(isset($_REQUEST['BRAND'])){
	if($_REQUEST['BRAND']=="cyrille-gassiline"){?><!--	<div class="info-line">
		<div class="container">
			<p>Все модели в наличии в разных размерах, уточняйте у менеджера</p>
		</div>
	</div>
	<?}
  $SET404="N";
  $arSelect = Array("ID", "NAME");
  $arFilter = Array("IBLOCK_ID"=>13,"CODE"=>$_REQUEST['BRAND'],"ACTIVE"=>"Y");
  $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
  $found=false;
  while($ob = $res->GetNextElement())
  {
      
      $arFi=$ob->GetFields();
      if(strlen($arFi['NAME'])>0)
          $found=true;
      $GLOBALS['CATALOG_FILTER']["PROPERTY_CML2_MANUFACTURER_VALUE"]= $arFi['NAME'];
      $APPLICATION->AddChainItem($arFi['NAME'], "");
      $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(
           13, // ID инфоблока
           $arFi["ID"] // ID элемента
       );
        $arElMetaProp = $ipropValues->getValues();
      if(strlen($arElMetaProp['ELEMENT_META_TITLE'])>0){
         $APPLICATION->SetTitle($arElMetaProp['ELEMENT_META_TITLE']); 
      }
      else{
          $APPLICATION->SetTitle($arFi['NAME']);
      }
      if(strlen($arElMetaProp['ELEMENT_META_DESCRIPTION'])>0){
         $APPLICATION->SetPageProperty("description",$arElMetaProp['ELEMENT_META_DESCRIPTION']); 
      }
      if(strlen($arElMetaProp['ELEMENT_PAGE_TITLE'])>0){
         $APPLICATION->SetPageProperty("title",$arElMetaProp['ELEMENT_PAGE_TITLE']); 
      }
  }
    if($found==false){
        $SET404="Y";
    }
}
//если страница тега
if(isset($_REQUEST['tag'])){
	$SET404="N";
    
	$GLOBALS['CATALOG_FILTER']["PROPERTY_TAGS"]= array(intval($_REQUEST['tag']));
}

?>
<?
if(!isset($_REQUEST['SEARCH'])){
	//делал чтоб отображать в поиске товары без фото для теста
	//$GLOBALS['CATALOG_FILTER']["!DETAIL_PICTURE"]= false;
}
$GLOBALS['CATALOG_FILTER']["!DETAIL_PICTURE"]= false;
if(strpos($_SERVER['REQUEST_URI'],"cyrille_gassiline")!==false){?>
	<div class="info-line">
		<div class="container">
			<p>Данная модель в наличии в разных размерах, уточняйте у менеджера <a class="wup h3" href="whatsapp://+79264810639">
						<span class="watsup"></span>
						<span class="watsuptext">+7 926 481-06-39<span>
					</span></span></a></p>
		</div>
	</div>
<?}

$APPLICATION->IncludeComponent("april:catalog", "", array(
	"IBLOCK_TYPE" => "1c_catalog",
	"IBLOCK_ID" => CATALOG_IBLOCK_ID,
	"TEMPLATE_THEME" => "site",
	"HIDE_NOT_AVAILABLE" => "N",
	"HIDE_NOT_AVAILABLE_OFFERS" => "N",
    "SHOW_ALL_WO_SECTION" => "Y",
	"BASKET_URL" => "/personal/cart/",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "Y",
	"CACHE_GROUPS" => "Y",
	"SET_TITLE" => "Y",
	"ADD_SECTION_CHAIN" => "Y",
	"ADD_ELEMENT_CHAIN" => "N",
	"SET_STATUS_404" => $SET404,
	"DETAIL_DISPLAY_NAME" => "N",
	"USE_ELEMENT_COUNTER" => "Y",
	"USE_FILTER" => "Y",
	"FILTER_NAME" => "CATALOG_FILTER",
	"FILTER_VIEW_MODE" => "VERTICAL",
	"FILTER_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"FILTER_PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"FILTER_PRICE_CODE" => array(
		0 => "BASE",
	),
	"FILTER_OFFERS_FIELD_CODE" => array(
		0 => "PREVIEW_PICTURE",
		1 => "DETAIL_PICTURE",
		2 => "",
	),
	"FILTER_OFFERS_PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"USE_REVIEW" => "Y",
	"MESSAGES_PER_PAGE" => "10",
	"USE_CAPTCHA" => "Y",
	"REVIEW_AJAX_POST" => "Y",
	"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
	"FORUM_ID" => "11",
	"URL_TEMPLATES_READ" => "",
	"SHOW_LINK_TO_FORUM" => "Y",
	"USE_COMPARE" => "N",
	"PRICE_CODE" => array(
		0 => "BASE",
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "Y",
	"CONVERT_CURRENCY" => "N",
	"QUANTITY_FLOAT" => "N",
	"OFFERS_CART_PROPERTIES" => array(
		0 => "SIZES_SHOES",
		1 => "SIZES_CLOTHES",
		2 => "COLOR_REF",
	),
	"SHOW_TOP_ELEMENTS" => "N",
	"SECTION_COUNT_ELEMENTS" => "N",
	"SECTION_TOP_DEPTH" => "1",
	"SECTIONS_VIEW_MODE" => "TILE",
	"SECTIONS_SHOW_PARENT_NAME" => "N",
	"PAGE_ELEMENT_COUNT" => "12",
	"LINE_ELEMENT_COUNT" => "3",
	"ELEMENT_SORT_FIELD" => $sort,
	"ELEMENT_SORT_ORDER" => $sort_order,
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER2" => "desc",
	"LIST_PROPERTY_CODE" => array(
		0 => "CML2_MANUFACTURER",
		1 => "SALELEADER",
		2 => "SPECIALOFFER",
		3 => "RAZMER_SKIDKI",
		4 => "CML2_ARTICLE",
		5 => "MORE_PHOTO"
	),
	"INCLUDE_SUBSECTIONS" => "Y",
	"LIST_META_KEYWORDS" => "UF_KEYWORDS",
	"LIST_META_DESCRIPTION" => "UF_META_DESCRIPTION",
	"LIST_BROWSER_TITLE" => "UF_BROWSER_TITLE",
	"LIST_OFFERS_FIELD_CODE" => array(
		0 => "NAME",
		1 => "PREVIEW_PICTURE",
		2 => "DETAIL_PICTURE",
		3 => "",
	),
	"LIST_OFFERS_PROPERTY_CODE" => array(
		0 => "SIZES_SHOES",
		1 => "SIZES_CLOTHES",
		2 => "COLOR_REF",
		3 => "MORE_PHOTO",
		4 => "ARTNUMBER",
		5 => "",
	),
	"LIST_OFFERS_LIMIT" => "0",
	"SECTION_BACKGROUND_IMAGE" => "UF_BACKGROUND_IMAGE",
	"DETAIL_PROPERTY_CODE" => array(
		0 => "CML2_MANUFACTURER",
		1 => "CML2_ARTICLE",
		2 => "CML2_ATTRIBUTES",
        3 => "SIZE_GUIDE_0",
        4 => "SIZE_GUIDE_1",
        5 => "SIZE_GUIDE_2",
        6 => "SIZE_GUIDE_3",
        7 => "SIZE_GUIDE_4",
        8 => "SIZE_GUIDE_5",
        9 => "SIZE_GUIDE_6",
        10 => "SIZE_GUIDE_7",
        11 => "id_size_grid",
        12 => "GOODS",
		13 => "RAZMER_SKIDKI",
		14 => "SOSTAV",
		15 => "HEIGHT_MODEL",
		16 => "MORE_PHOTO",
		17 => "SOSTAV_DOP"
	),
	"DETAIL_META_KEYWORDS" => "KEYWORDS",
	"DETAIL_META_DESCRIPTION" => "META_DESCRIPTION",
	"DETAIL_BROWSER_TITLE" => "TITLE",
	"DETAIL_OFFERS_FIELD_CODE" => array(
		0 => "NAME",
		1 => "",
	),
	"DETAIL_OFFERS_PROPERTY_CODE" => array(
		0 => "ARTNUMBER",
		1 => "SIZES_SHOES",
		2 => "SIZES_CLOTHES",
		3 => "COLOR_REF",
		4 => "MORE_PHOTO",
		5 => "",
	),
	"DETAIL_BACKGROUND_IMAGE" => "BACKGROUND_IMAGE",
	"LINK_IBLOCK_TYPE" => "",
	"LINK_IBLOCK_ID" => "",
	"LINK_PROPERTY_SID" => "",
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
	"USE_ALSO_BUY" => "Y",
	"ALSO_BUY_ELEMENT_COUNT" => "4",
	"ALSO_BUY_MIN_BUYES" => "1",
	"OFFERS_SORT_FIELD" => "sort",
	"OFFERS_SORT_ORDER" => "desc",
	"OFFERS_SORT_FIELD2" => "id",
	"OFFERS_SORT_ORDER2" => "desc",
	"PAGER_TEMPLATE" => "modern",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
	"PAGER_SHOW_ALL" => "N",
	"ADD_PICT_PROP" => "MORE_PHOTO",
	"LABEL_PROP" => array(
		0 => "NEWPRODUCT",
	),
	"PRODUCT_DISPLAY_MODE" => "Y",
	"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
	"OFFER_TREE_PROPS" => array(
		0 => "SIZES_SHOES",
		1 => "SIZES_CLOTHES",
		2 => "COLOR_REF",
		3 => "",
	),
	"SHOW_DISCOUNT_PERCENT" => "Y",
	"SHOW_OLD_PRICE" => "Y",
	"MESS_BTN_BUY" => "Купить",
	"MESS_BTN_ADD_TO_BASKET" => "В корзину",
	"MESS_BTN_COMPARE" => "Сравнение",
	"MESS_BTN_DETAIL" => "Подробнее",
	"MESS_NOT_AVAILABLE" => "Нет в наличии",
	"DETAIL_USE_VOTE_RATING" => "Y",
	"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
	"DETAIL_USE_COMMENTS" => "Y",
	"DETAIL_BLOG_USE" => "Y",
	"DETAIL_VK_USE" => "N",
	"DETAIL_FB_USE" => "Y",
	"AJAX_OPTION_ADDITIONAL" => "",
	"USE_STORE" => "Y",
	"BIG_DATA_RCM_TYPE" => "personal",
	"FIELDS" => array(
		0 => "STORE",
		1 => "SCHEDULE",
		2 => "DATE_CREATE",
	),
	"USE_MIN_AMOUNT" => "N",
	"STORE_PATH" => "/store/#store_id#",
	"MAIN_TITLE" => "Наличие на складах",
	"MIN_AMOUNT" => "10",
	"DETAIL_BRAND_USE" => "Y",
	"DETAIL_BRAND_PROP_CODE" => "BRAND_REF",
	"SIDEBAR_SECTION_SHOW" => "Y",
	"SIDEBAR_DETAIL_SHOW" => "Y",
	"SIDEBAR_PATH" => "/catalog/sidebar.php",
	"SEF_URL_TEMPLATES" => array(
		"sections" => "",
		"section" => "#SECTION_CODE_PATH#/",
		"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
		"compare" => "compare/",
	)
	),
	false
);?>
<!-- <? if(!isset($_COOKIE['firsttimecancel_2'])){?>
    <script>
    $(document).ready(function () {
        //для 3го сценария, если уводим мышь из тела страницы и открыта 1 вкладка сайта, то показываем соответствующее сообщение
        $(document).mouseleave(function(e) {
            if(getCookie("tabs")==1&&getCookie("firsttimecancel_2")==""){
                setTimeout(function() { 
                    $(".popup.popup_sub").fadeIn();
                    var d = new Date();
                       d.setDate(d.getDate() + 1);
                    setCookie("firsttimecancel_2","1",{expires: d,path: "/"});
                }, 2000);
                
            }
        });
    });
    </script>
    <?}?> -->







<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>