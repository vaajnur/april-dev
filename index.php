<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Хотите быть в курсе всех НОВИНОК, АКЦИЙ, и получить доступ к закрытым распродажам? Продажа брендовой одежды для женщин. Удобный бутик в центре Москвы. Удобный каталог и таблицы размеров.");
$APPLICATION->SetPageProperty("title", "Aprilmoscow.ru - интернет магазин женской одежды (официальный сайт)");
$APPLICATION->SetTitle("\"Апрель\"");
?>
	<ul class="slider-on-main">
	<?
	//слайдер на главной
	$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE","PROPERTY_URL");
	$arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nTopCount"=>5), $arSelect);
	while($ob = $res->GetNextElement())
	{
	 $arFields = $ob->GetFields();
	 ?>
	<li><a href="<?=$arFields['PROPERTY_URL_VALUE'];?>"><img src="<?=CFile::GetPath($arFields['PREVIEW_PICTURE']);?>" alt=""></a></li>
	 <?
	}
	?>
	</ul>
	<div class="container-fluid no-padding banners">
		<div class="row">
			<?
			//баннеры на главной
			$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE","PROPERTY_URL","PROPERTY_CELLS");
			$arFilter = Array("IBLOCK_ID"=>4, "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nTopCount"=>5), $arSelect);
			while($ob = $res->GetNextElement())
			{
			 $arFields = $ob->GetFields();
			 //optimize_image(CFile::GetPath($arFields['PREVIEW_PICTURE']));
			 ?>
			 <div class="col-sm-6 no-padding">
			 	<a href="<?=$arFields['PROPERTY_URL_VALUE'];?>">
			 		<img src="<?=CFile::GetPath($arFields['PREVIEW_PICTURE']);?>" alt="" class="img-responsive"></a>
			 	</div>
			 <?
			}
			?>
			<div class="col-sm-6 no-padding relative form-block">
				<!-- error или success на form-tile (поле вернется в первоночальное состояние только если убрать класс)  -->
				<?$APPLICATION->IncludeComponent(
					"april:subscribe.form",
					"main_page_form",
					Array(
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"CODE_FORM" => "MAINPAGE",
						"PAGE" => "/ajax/subscribe.php",
						"RUBRIC_ID" => 3,
						"SHOW_HIDDEN" => "Y",
						"USE_PERSONALIZATION" => "Y"
					)
				);?>
			</div>
		</div>
	</div>
	<div class="container-fluid no-padding">
		<!-- это инстаграм для пк, для мобил ниже. они прячутся соответственно.  -->
		<div class="inst-tiles row hide-on-mobile">
			<div class="col-sm-4 no-padding">
				
				<?
				$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE","PROPERTY_good","PROPERTY_postlink","PROPERTY_likes","PROPERTY_comments","PROPERTY_date");
				$arFilter = Array("IBLOCK_ID"=>6, "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array("PROPERTY_date"=>DESC), $arFilter, false, Array("nTopCount"=>7), $arSelect);
				$photos=array();
				while($ob = $res->GetNextElement())
				{
				 $arFields = $ob->GetFields();
				 $photos[]=$arFields;
				}
				?>
				<? 
				$index=0;
				foreach($photos as $photo){
					$index++;
					if($index==1){?>
					<div class="container-fluid no-padding first-items-container">
					<div class="row">
						<div class="col-xs-6 no-padding first-items">
						
					<?}
					if($index==2){?>
						</div><div class="col-xs-6 no-padding first-items">
					<?}
					if($index==3){?>
						</div></div></div><div class="inst-header">
					<img src="img/backgrounds/inst-tiles.jpg" class="img-responsive" alt="">
					<p>April в Instagram:<br><a target="_blank" href="<?=$GLOBALS['contacts']['PROPERTY_INSTALINK_VALUE'];?>">@aprilmoscow</a></p>
				</div></div><div class="col-sm-2 no-padding">
					<?}
					if($index==5){?>
						</div><div class="col-sm-4 no-padding">
					<?}
					if($index==6){?>
						</div><div class="col-sm-2 no-padding">
					<?}?>
					<div class="item" style="background-image: url(<?=CFile::GetPath($photo['PREVIEW_PICTURE']);?>)">
						<!-- <a href=""><img src="" class="img-responsive" alt=""></a> -->
						<div class="options">
							<div class="info">
								<div>
									<?php include("img/icons/heart.svg"); ?>
									<span><?=$photo['PROPERTY_LIKES_VALUE'];?></span>
								</div>
								<div>
									<?php include("img/icons/comment.svg"); ?>
									<span><?=$photo['PROPERTY_COMMENTS_VALUE'];?></span>
								</div>
							</div>
							<? if(strlen($photo['PROPERTY_GOOD_VALUE'])>0){?>
							<div class="buy">
								<?php include("img/icons/cart.svg"); ?>
								<a href=""></a>
							</div>
							<?}?>
						</div>
					</div>
				<?}?>
				</div>
		
	   </div>
        <!-- инста для мобил -->
		<div class="inst-tiles hide-on-desktop">
			<? foreach($photos as $photo){?>
			<div class="item">
				<a href="">
					<img src="<?=CFile::GetPath($photo['PREVIEW_PICTURE']);?>" class="img-responsive" alt="">
				</a>
				<div class="options">
					<div class="info">
						<div>
							<?php include("img/icons/heart.svg"); ?>
							<span><?=$photo['PROPERTY_LIKES_VALUE'];?></span>
						</div>
						<div>
							<?php include("img/icons/comment.svg"); ?>
							<span><?=$photo['PROPERTY_COMMENTS_VALUE'];?></span>
						</div>
					</div>
					<? if(strlen($photo['PROPERTY_GOOD_VALUE'])>0){?>
					<div class="buy">
						<?php include("img/icons/cart.svg"); ?>
						<a href=""></a>
					</div>
					<?}?>
				</div>
			</div>
			<?}?>
		</div>
    </div>
	<p class="h1 header">Сейчас просматривают на сайте</p>
	<div id="prod-container-on-main" class="prod-container">
		<?
		if($_SESSION["watched"]<4||$_SESSION["watched"]==0)
			$_SESSION["watched"]+=2;
		else
			$_SESSION["watched"]=0;
		//$results = $DB->Query("SELECT * FROM b_view_today ORDER BY count DESC LIMIT ".$_SESSION["watched"].", 2");
		$results = $DB->Query("SELECT * FROM b_view_today WHERE date='".date("d.m.Y")."' AND goodid<>8549 AND goodid<>8550 AND goodid<>8549 ORDER BY count DESC LIMIT 0, 6");
		//проверка существования записи с сегодняшней датой и этим товаром
		$ids=array();
		while ($row = $results->Fetch())
		{
			array_push($ids,$row['goodid']);
		}
        if(count($ids)<4){
            $results = $DB->Query("SELECT * FROM b_iblock_element WHERE ACTIVE='Y' AND IBLOCK_ID='".CATALOG_IBLOCK_ID."' AND id<>8550 AND id<>8551 AND id<>8549 ORDER BY RAND() DESC LIMIT 0, 4");
            while ($row = $results->Fetch())
            {
                array_push($ids,$row['ID']);
            }
        }
		$GLOBALS["FILTER_MAIN_CATALOG"]=array("ID"=>$ids,"!DETAIL_PICTURE"=>false);
		?>
		<?
		$APPLICATION->IncludeComponent(
			"april:catalog.section",
			"main-items",
			Array(
				"TEMPLATE_THEME" => "blue",
				"PRODUCT_DISPLAY_MODE" => "N",
				"OFFER_ADD_PICT_PROP" => "FILE",
				"OFFER_TREE_PROPS" => array("-"),
				"PRODUCT_SUBSCRIPTION" => "N",
				"SHOW_DISCOUNT_PERCENT" => "N",
				"SHOW_OLD_PRICE" => "N",
				"SHOW_CLOSE_POPUP" => "Y",
				"SEF_MODE" => "N",
				"IBLOCK_TYPE" => "books",
				"IBLOCK_ID" => CATALOG_IBLOCK_ID,
				"SECTION_ID" => $_REQUEST["SECTION_ID"],
				"SECTION_CODE" => "",
				"SECTION_USER_FIELDS" => array(),
				"ELEMENT_SORT_FIELD" => $sort,
				"ELEMENT_SORT_ORDER" => $asc,
				"ELEMENT_SORT_FIELD2" => "name",
				"ELEMENT_SORT_ORDER2" => "asc",
				"FILTER_NAME" => "FILTER_MAIN_CATALOG",
				"INCLUDE_SUBSECTIONS" => "Y",
				"SHOW_ALL_WO_SECTION" => "Y",
				"SECTION_URL" => "",
				"DETAIL_URL" => "",
				"BASKET_URL" => "/personal/basket.php",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"ADD_SECTIONS_CHAIN" => "N",
				"DISPLAY_COMPARE" => "N",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"BROWSER_TITLE" => "-",
				"SET_META_KEYWORDS" => "N",
				"META_KEYWORDS" => "",
				"SET_META_DESCRIPTION" => "N",
				"META_DESCRIPTION" => "",
				"SET_LAST_MODIFIED" => "Y",
				"USE_MAIN_ELEMENT_SECTION" => "Y",
				"SET_STATUS_404" => "N",
				"PAGE_ELEMENT_COUNT" => "4",
				"LINE_ELEMENT_COUNT" => "1",
				"PROPERTY_CODE" => array("RAZMER_SKIDKI","RAZMER_TOVARA","TSVET","CML2_MANUFACTURER","TSENA_V_OFITS_BUTIKE","IZNOS","SOSTAV"),
				"OFFERS_FIELD_CODE" => array(),
				"OFFERS_PROPERTY_CODE" => array(),
				"OFFERS_SORT_FIELD" => "sort",
				"OFFERS_SORT_ORDER" => "asc",
				"OFFERS_SORT_FIELD2" => "active_from",
				"OFFERS_SORT_ORDER2" => "desc",
				"OFFERS_LIMIT" => "5",
				"BACKGROUND_IMAGE" => "-",
				"PRICE_CODE" => array(),
				"USE_PRICE_COUNT" => "Y",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"PRODUCT_PROPERTIES" => array(),
				"USE_PRODUCT_QUANTITY" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",

				"HIDE_NOT_AVAILABLE" => "Y",
				"OFFERS_CART_PROPERTIES" => array(),
				"CONVERT_CURRENCY" => "Y",
				"CURRENCY_ID" => "RUB",
				"ADD_TO_BASKET_ACTION" => "ADD",
				"PAGER_BASE_LINK_ENABLE" => "Y",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
				"MESSAGE_404" => "",
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",
				"PAGER_BASE_LINK" => "",
				"PAGER_PARAMS_NAME" => "arrPager"
			)
		);?>
	</div>
	<!--<p class="h1 header">Блог</p>
	<div class="container-fluid blog-main">
		<div class="row around-xs">
			<?$APPLICATION->IncludeComponent("april:news.list","",Array(
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => "news",
					"IBLOCK_ID" => "1",
					"NEWS_COUNT" => "3",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => Array("ID","DATE_CREATE"),
					"PROPERTY_CODE" => Array("DESCRIPTION"),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_LAST_MODIFIED" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_DESC_NUMBERING" => "Y",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_BASE_LINK_ENABLE" => "Y",
					"SET_STATUS_404" => "Y",
					"SHOW_404" => "Y",
					"MESSAGE_404" => "",
					"PAGER_BASE_LINK" => "",
					"PAGER_PARAMS_NAME" => "arrPager",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				)
			);?>
		</div>
	</div>-->
	<div class="container-fluid about-main">
		<p class="h1 header">Магазин Апрель</p>
		<div class="row">
			<div class="col-sm-4 col-sm-offset-1">
				<img src="img/about-main-sample.png" alt="" class="img-responsive">
			</div>
			<div class="col-sm-5 col-sm-offset-1 about-main-block">
				<span>Апрель - это … </span>
				<span>Элегантность, беспечный шик, великолепие, изысканность, ум и стиль<br>Сдержанная сексуальность, элегантность и красота</span>
				<span>Утонченная классика, чистые, простые, высоко-качественные и идеально скроенные</span>
				<span>Комфортные, мягкие, гладкие, каждодневные и удобные</span>
				<span>Многогранный, простой в носке, уместный и днем, и вечером, стиль, который сразу становится Вашим</span>
				<span>Уверенная, позитивная, отзывчивая, положительная и гармоничная</span>
				<!-- <p>Всё – для Вас!<br><br>Трудимся, вдохновляемся, постоянно ищем – все самое модное, самое красивое, самое изысканное и
					женственное, самое передовое и необычное: были у нас и юбки с рукавами, и платья из перьев страуса, белоснежные смокинги
					(носить на голое тело!), платья со шлейфом и без, вышиванки и ушанки, кашемир (из горных коз, собранный руками невинных
					девушек с кустов высокогорного розмарина) и тонкое кружево для томных вечеров.<br><br>Мода – удивительный инструмент,
					способный менять мир. Часто женщине достаточно красивого платья, летящей юбки в нежный цветочек, делового костюма с
					иголочки, чтобы почувствовать себя счастливой. А если все вместе!..<br><br>Красота спасет мир. А еще улыбка счастливой
					женщины!
					<br><br>С любовью,<br>Ваш Апрель!</p> -->
			</div>

		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>