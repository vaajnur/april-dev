<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Соответствие размеров");
?>
<section class="section-suggestion">
    <div class="container-fluid">
        <div class="row center-xs">
            <div class="col-xs-12">
                <p class="h1 header">Соответствие размеров</p>
                <!-- Размерная сетка  -->
                <div class="popup-table-size-down">
                    <div class="popup-body-down">
                        <div class="sizes sizes--table">
                            <div class="sizes-line-tb ">
                                <p>INT</p>
                                <p>XXS</p>
                                <p>XS</p>
                                <p>XS</p>
                                <p>S</p>
                                <p>S</p>
                                <p>M</p>
                                <p>M</p>
                                <p>L</p>
                                <p>L</p>
                                <p>XL</p>
                                <p>XL</p>
                                <p>XXL</p>
                                <p>XXL</p>
                            </div>
                            <div class="sizes-line-tb">
                                <p>JEA</p>
                                <p>23</p>
                                <p>24</p>
                                <p>25</p>
                                <p>26</p>
                                <p>27</p>
                                <p>28</p>
                                <p>29</p>
                                <p>30</p>
                                <p>31</p>
                                <p>32</p>
                                <p>33</p>
                                <p>34</p>
                                <p>35</p>
                            </div>
                            <div class="sizes-line-tb ">
                                <p>EU</p>
                                <p>34</p>
                                <p>34</p>
                                <p>36</p>
                                <p>36</p>
                                <p>38</p>
                                <p>38</p>
                                <p>40</p>
                                <p>40</p>
                                <p>42</p>
                                <p>42</p>
                                <p>44</p>
                                <p>46</p>
                                <p>46</p>
                            </div>
                            <div class="sizes-line-tb ">
                                <p>IT</p>
                                <p>36</p>
                                <p>38</p>
                                <p>38</p>
                                <p>40</p>
                                <p>40</p>
                                <p>42</p>
                                <p>44</p>
                                <p>44</p>
                                <p>46</p>
                                <p>46</p>
                                <p>48</p>
                                <p>50</p>
                                <p>50</p>
                            </div>
                            <div class="sizes-line-tb ">
                                <p>RUS</p>
                                <p>38</p>
                                <p>40</p>
                                <p>40</p>
                                <p>42</p>
                                <p>42</p>
                                <p>44</p>
                                <p>44</p>
                                <p>46</p>
                                <p>46</p>
                                <p>48</p>
                                <p>48</p>
                                <p>50</p>
                                <p>54</p>
                            </div>
                            <div class="sizes-line-tb ">
                                <p>US</p>
                                <p>0</p>
                                <p>0</p>
                                <p>2</p>
                                <p>2</p>
                                <p>4</p>
                                <p>6</p>
                                <p>8</p>
                                <p>8</p>
                                <p>10</p>
                                <p>10</p>
                                <p>12</p>
                                <p>12</p>
                                <p>14</p>
                            </div>
                            <div class="sizes-line-tb ">
                                <p>UK</p>
                                <p>4</p>
                                <p>6</p>
                                <p>6</p>
                                <p>8</p>
                                <p>8</p>
                                <p>10</p>
                                <p>12</p>
                                <p>12</p>
                                <p>14</p>
                                <p>16</p>
                                <p>16</p>
                                <p>18</p>
                                <p>18</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>