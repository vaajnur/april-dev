$(document).ready(function () {
    if($("#ORDER_PROP_3").length>0){
        $("#ORDER_PROP_3").mask("+7(999) 999-99-99",{placeholder:"+7(___) ___-__-__"});
    }
    if($("#ORDER_PROP_6_val").length>0){
        $("#ORDER_PROP_6_val").attr("autocomplete","off");
    }
	if($(".must-be-clicked").length>0)
	{
		$(".must-be-clicked").trigger("click");
		$(".must-be-clicked").removeClass("must-be-clicked");
	}
    //проверка, если куки отсутствуют значит задаем куки и делаем таймер с проверкой
	console.log(getCookie("firsttime"));

    // if(getCookie("firsttime").length==0){
    //     deleteCookie("firsttime");
    //     deleteCookie("firsttimecancel");
    //     var d = new Date();
    //     d.setDate(d.getDate() + 7);
    //     setCookie("firsttime","1",{expires: d,path: "/"});
    //     gotimerfirsttime();
    // }
    
    $( document ).on( "click", ".cookie .close-popup", function() {
        var d = new Date();
        d.setDate(d.getDate() + 30);
        setCookie("COOKIE_ALERT","1",{expires: d,path: "/"});
    });
    //галочки в вишлисте
    $( document ).on( "click", ".wish-select", function() {
		if($(this).hasClass("active"))
            $(this).removeClass("active");
        else
            $(this).addClass("active");
        if($(".wish-select.active").length>0){
            $(".wish-selector").show();
        }
        else{
            $(".wish-selector").hide();
        }
	});
    //поделиться вишлистом
    $( document ).on( "click", ".wish-selector", function() {
		$(".popup.share_wishlist").fadeIn();
	});
    //отправка поделиться вишлистом
    $( document ).on( "click", ".share_wishlist button", function() {
        var oldtxt=$(".share_wishlist button").text();
        resetvalidate(".popup.share_wishlist input");
		$(".share_wishlist button").text("Загрузка...");
        var ids="";
        $( ".wish-select.active" ).each(function() {
          ids=ids+$(this).attr("product_id")+";";
        });
		$.ajax({
			type:"POST",
			url:"/ajax/sharewishlist.php",
			data:{ 
				MALE:$(".popup.share_wishlist input[name=radio-group-wish]:checked").val(),
                EMAIL_SHARE:$(".popup.share_wishlist .email_share").val(),
				NAME_SHARE:$(".popup.share_wishlist .name_share").val(),
                AJAXSHARE:"Y",
                IDS:ids
            },
			success: function(response){
				console.log(response);
				if(response.indexOf("accepted")!=-1){
                    validate(".popup.share_wishlist input",0);
                         $(".popup.share_wishlist").hide();
                         if($(".popup.share_wishlist input[name=radio-group-wish]:checked").val()=="Дорогой"){
                            $(".popup.new-popup h2").text("Вы успешно отправили свой WishList своему мужчине.");
                         }
                         else{
                            $(".popup.new-popup h2").text("Вы успешно отправили свой WishList своей подруге.");
                        }
	                    $(".popup.new-popup").fadeIn();
                        setTimeout(function() {
                            $(".popup.new-popup").hide();
                            resetvalidate(".popup.share_wishlist input");
                        }, 20000);
                    }
				else{
					if(response.indexOf("emailerror")!=-1)
						validate(".popup.share_wishlist .email_share",1);
					if(response.indexOf("nameerror")!=-1)
						validate(".popup.share_wishlist .name_share",1);
				}
                $(".share_wishlist button").text(oldtxt);	
			}
		});
		return false;
	});
	//выбор размера в каталоге и карточке товара
    $( document ).on( "click", '.size input[type="radio"]', function() {
		if($(".addtocart").length>0){
			$(".addtocart").attr("product-id",$(this).attr("offer-id"));
			$(".addtocart").attr("size",$(this).attr("size-id"));
			if($(".quickbuy").length>0){
				$(".quickbuy").attr("size",$(this).attr("size-id"));
				$(".quickbuy").attr("product-id",$(this).attr("offer-id"));
			}
			$(".addtocart").addClass("addtocart"+$(this).attr("offer-id"));
		}
		if($(".quick-form .product_id").length>0){
			$(".quick-form .product_id").val($(this).attr("offer-id"));
		}
	});
    //lazy load каталог
	if($(".lazy-catalog-item").length>0){
		$('.lazy-catalog-item').lazyload();
    }
    if($(".lazy-image").length>0){
		$('.lazy-image').lazyload();
	}
    var xhr="";
    $(".fancybox").fancybox();
     // sertificares
    $('.choose-format div').click(function() {
        var thisDiv = $(this),
            index = thisDiv.index();
        if (!thisDiv.hasClass('active')) {
            thisDiv.siblings().removeClass('active');
            thisDiv.addClass('active');

            $('.sertificates-form').eq(index).siblings().hide();
            $('.sertificates-form').eq(index).show();
        }
    });
    $('.choose-cost button').click(function() {
        var thisDiv = $(this),
            index = thisDiv.index();
        if (!thisDiv.hasClass('active')) {
            thisDiv.siblings().removeClass('active');
            thisDiv.addClass('active');

            $('.sertificates-preview a').eq(index).siblings().hide();
            $('.sertificates-preview a').eq(index).show();
        }
    });
    $('.sert-faq-item').click(function() {
        var thisDiv = $(this);
        if (!thisDiv.hasClass('active')) {
            thisDiv.addClass('active');
            thisDiv.find('div').slideToggle();
        } else {
            thisDiv.removeClass('active');
            thisDiv.find('div').slideToggle();
        }
    });
     $( document ).on( "click", ".go-preview-mail" , function() {
       $('.popup.sertpreview-popup').fadeIn();
         return false;
	});
    //клик по открытому пункту меню в каталоге
    $( document ).on( "click", ".item.open .h2" , function() {
       //if($(this).attr("true-link").length>0)
           //alert($(this).attr("true-link"));
            //location.replace($(this).attr("true-link"));
	});
    //поиск в вишлисте
    $( document ).on( "change", ".wishlist-search input" , function() {
        loading_animation("#prod-container-on-main");
		$(".item-suggestion .col-xs-12").load("?q="+$(".wishlist-search input").val()+" #prod-container-on-main",function() {
		});
	});
	//копируем пагинацию в заказах в верхний блок с крошками
	if($(".profile-right-column .pagination-list").length>0){
		$(".title-footer .pagination-list").html($(".profile-right-column .pagination-list").html());
	}
    //подгрузка в каталоге
    var loading = false;
    $(window).scroll(function() {
        if ($('#infinity-next-page').length>0 && !loading) {
            var scrollvalue=300;
            if($(".seo").length>0){
                scrollvalue=scrollvalue+$(".seo").height();
            }
            if ($(window).scrollTop()+scrollvalue >= $(document).height()-$(window).height()) {
                loading = true;
                loading_animation("#prod-container-catalog");
                $.get($('#infinity-next-page').attr('href'), {is_ajax: 'y'}, function(data){
                    $('#infinity-next-page').after(data);
                    $(".owlslidecontent:not(.slick-initialized)").slick({
                        dots: false,
                        arrows: false,
                        infinite: true,
                        autoplaySpeed: 100,
                        speed: 800,
                        fade: true,
                        slidesToShow: 1
                    });
                    doneResizing();
                    delete_loading();
                    $('#infinity-next-page').remove();
                    $('.after-ajax-insert:eq(1)').remove();
                    loading = false;
                });
            }
        }
    }); 
    //быстрый просмотр
    
	$( document ).on( "click", ".action-quick-look", function() {
        var oldtxt=$(this).text();
        var elem=$(this);
        $(this).text("Загрузка...");
        $(".popup-table-size").parent().remove();
        loading_animation(".quick-look");
        $.ajax({
           url: '/ajax/product.preview.php',
           data:{
                PRODUCT_ID:$(this).attr("product-id")
           },
           success: function(ans){
                elem.text(oldtxt);
               $(".quick-look").html(ans);
               $('.quick-look').closest('.popup').fadeIn();
               $('body').append($('<div></div>').load($(".quick-look .table-size").attr("link")+" .popup-table-size"));
               
           }
       });
    });
    //попап добавления в корзину с размерами в мобиле
	$( document ).on( "click", ".action-to-cart", function() {
        loading_animation(".sizes-popup");
        $.ajax({
           url: '/ajax/product.sizes.php',
           data:{
                PRODUCT_ID:$(this).attr("product-id")
           },
           success: function(ans){
              $(".sizes-popup").html(ans); 
           }
       });
    });
    //заказ в 1 клик
    $(document).on("click", '.quickbuy', function(event) { 
        loading_animation(".quick-buy");
        $.ajax({
           url: '/ajax/product.oneclick.php',
           data:{
                PRODUCT_ID:$(this).attr("product-id"),
                SIZE:$(this).attr("size")
           },
           success: function(ans){
			 
			   // yaCounter45391296.reachGoal('call_back');
              $(".quick-buy").html(ans); 
               $('.your_phone').mask("+7(999) 999-99-99",{placeholder:"+7(___) ___-__-__"});
              $('.quick-buy').closest('.popup').fadeIn();
           }
       });
        
    });
     $( document ).on( "click", ".quick-buy .go-quick-buy", function() {
       var oldtxt=$(this).text();
       $(".quick-buy input").removeClass("error");
       resetvalidate(".quick-buy input");
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax/form.oneclick.php',
           data:{
                product:$(".quick-buy .product_id").val(),
                phone:$(".quick-buy .your_phone").val(),
           },
           success: function(ans){
              console.log(ans);
               if(ans.indexOf("name-error")!=-1){
                   validate(".quick-buy .your_name",1);
               }
               if(ans.indexOf("phone-error")!=-1){
                   validate(".quick-buy .your_phone",1);
               }
               if(ans.indexOf("okidoki")!=-1){

                   validate(".quick-buy .your_phone",0);
                   $(".quick-buy button").text("Заказ оформлен");
                   dataLayer.push({
                    'event': 'Call Back',
                    'ecommerce': {
                    }
                  });
                   //deleteCookie("Cookiescript");
				   $(".quick-buy button").attr("disabled","disabled");
                   setTimeout(function() {
                            $(".popup.oneclick button").text(oldtxt);
                   }, 10000);
               }
               else{
                   $(".quick-buy button").text(oldtxt);
               }
           }
       });
         return false;
    });
     //добавление в вишлист
	$( document ).on( "click", ".wishlist-link:not(.disabled), .btn-wishlist:not(.disabled)", function() {
		var product=$(this).attr("product-id");
        var product_name=$(this).attr("product-name");
        loading_animation(".wishlister .drop");
        var oldtxt=$(this).text();
        //$(this).text("Загрузка...");
		$.ajax({
			type:"POST",
			url:"/ajax/addtowishlist.php",
			data:{
				product: product
            },
			success: function(response){
                $(".wishlist-link, .texwish").text("Товар в WishListe");
                $(".wishlist-link, .btn-wishlist").addClass('disabled');
                $(".wishlist-link, .btn-wishlist").attr('href',"/personal/wishlist/");
                $(".wishlister").load("/personal/wishlist/ .wishlister .header-option",function() {
			     });
			}
		});
        return false;
	});
    //добавление в корзину
    $(document).on("click", '.addtocart', function(event) { 
        
        addtocart($(this).attr("product-id"),$(this).attr("size"));
    });
    //добавление лука в корзину
    $(document).on("click", '.buy-all', function(event) { 
        $( "li.item-suggestion--item:not(.item-suggestion--set)" ).each(function( index ) {
            if($(this).attr("product-id").length>0){
                addtocart($(this).attr("product-id"),$(this).attr("size"));
            }
        });
        $("."+$(".buy-all").attr("button-buy-element")).trigger("click");
        $(".buy-all").text("Лук в корзине");
        $(".buy-all").attr("href","/basket/");
        $(".buy-all").removeClass("buy-all");
        return false;
    });
	// RATING
    $('.reviews-form .rating svg').mouseenter(function() {
        var thisRateIndex = $(this).index() + 1,
            thisRateParent = $(this).parent().find('svg');
        for (var i = 0; i < thisRateIndex; i++) {
            thisRateParent.eq(i).addClass('hovered');
        }
    });
    $('.reviews-form .rating svg').mouseleave(function() {
        $(this).removeClass('hovered');
        $(this).siblings().removeClass('hovered');
    });
    $('.reviews-form .rating svg').on('click', function() {
        var thisRateIndex = $(this).index() + 1,
            thisRateParent = $(this).parent().find('svg');
		$('input[name="PROPERTY[66][0]"]').val(thisRateIndex);
        if ($(this).parent().hasClass('checked')) {
            thisRateParent.each(function() {
                $(this).removeClass('active');
            });
            for (var i = 0; i < thisRateIndex; i++) {
                thisRateParent.eq(i).addClass('active');
            }
        } else {
            $(this).parent().addClass('checked');
            for (var i = 0; i < thisRateIndex; i++) {
                thisRateParent.eq(i).addClass('active');
            }
        }
    });
	// mask
	if($('#personal-phone, input[name="USER_PHONE"], .your_phone').length>0)
    	$('#personal-phone, input[name="USER_PHONE"], .your_phone').mask("+7(999) 999-99-99",{placeholder:"+7(___) ___-__-__"});
	if($('#personal-birth').length>0)
		$('#personal-birth').mask("99.99.9999",{placeholder:"__.__.____"});
	//показать все инсташоп
	$( document ).on( "click", ".show-all.btn", function() {
        $(this).text("Загрузка...");
        var elem=$(this);
		if($(".instashop-items").length>0)
			loading_animation(".instashop-items");
		if($(".instashop-content").length>0){
            $.ajax({
                type:"POST",
                url:"/instashop/?PAGEN_1="+$(".show-all.btn").attr("page-for-load")+"&AJAX=Y",
                data:{},
                success: function(response){
                    $(".instashop-items").append(response);
                    elem.remove();
                    elem=$(".show-more-btn");
                    $(".instashop-content").append(elem);
                    delete_loading(".instashop-items");
                }
            });
		}
		$(this).attr("disabled","disabled");
		
		return false;
    });
    $(window).scroll(function() {
        if ($('.show-more-btn').length>0 && !loading) {
            var elem=$('.show-more-btn');
            if ($(window).scrollTop()+300 >= $(document).height()-$(window).height()&&$(".show-all.btn").length>0) {
                loading = true;
                if($(".instashop-items").length>0)
			        loading_animation(".instashop-items");
                if($(".instashop-content").length>0){
                    $.ajax({
                        type:"POST",
                        url:"/instashop/?PAGEN_1="+$(".show-all.btn").attr("page-for-load")+"&AJAX=Y",
                        data:{},
                        success: function(response){
                            console.log($(".show-all.btn").attr("page-for-load"));
                            $(".instashop-items").append(response);
                            elem.remove();
                            elem=$(".show-more-btn");
                            $(".instashop-content").append(elem);
                            $(".bubblingG").remove();
                            $('.lazy-insta-item').lazyload();
                            loading = false;
                        }
                    });
                }
            }
        }
    }); 
    //lazy load инсташоп
	if($(".lazy-insta-item").length>0){
		$('.lazy-insta-item').lazyload();
	}
	//вход в лк
	$( document ).on( "click", ".reg-and-enter-popup.enter button", function() {
       var oldtxt=$(this).text();
       resetvalidate(".reg-and-enter-popup.enter input");
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax/form.auth.php',
           data:{
                login:$(".reg-and-enter-popup.enter input[name=login]").val(),
                password:$(".reg-and-enter-popup.enter  input[name=password]").val()
           },
           success: function(ans){
              console.log(ans);
               if(ans.indexOf("error-login")!=-1){
				   validate(".reg-and-enter-popup.enter input[name='login']",1);
                   $(".reg-and-enter-popup.enter button").text(oldtxt);
               }
               if(ans.indexOf("error-password")!=-1){
                validate(".reg-and-enter-popup.enter input[name='password']",1);
                $(".reg-and-enter-popup.enter button").text(oldtxt);
            }
               if(ans.indexOf("okidoki")!=-1){
                  location.reload();
               }
           }
       });
         return false;
    });
	//регистрация
    $( document ).on( "click", ".reg-and-enter-popup.reg button", function() {
       var oldtxt=$(this).text();
		resetvalidate(".reg-and-enter-popup.reg  input");
       $(this).text("Загрузка...");
       $.ajax({
           url: '/ajax/form.reg.php',
           data:{
                USER_NAME:$(".reg-and-enter-popup.reg input[name=USER_NAME]").val(),
                USER_EMAIL:$(".reg-and-enter-popup.reg input[name=USER_EMAIL]").val(),
                USER_PHONE:$(".reg-and-enter-popup.reg input[name=USER_PHONE]").val(),
                USER_BIRTHDAY:$(".reg-and-enter-popup.reg input[name=USER_BIRTHDAY]").val(),
           },
           success: function(ans){
              console.log(ans);
               if(ans.indexOf("name-error")!=-1){
				   validate(".reg-and-enter-popup.reg input[name='USER_NAME']",1);
               }
               if(ans.indexOf("email-error")!=-1){
				   validate(".reg-and-enter-popup.reg input[name='USER_EMAIL']",1);
               }
               if(ans.indexOf("date-error")!=-1){
				   validate(".reg-and-enter-popup.reg input[name='USER_BIRTHDAY']",1);
               }
			   if(ans.indexOf("phone-error")!=-1){
				   validate(".reg-and-enter-popup.reg input[name='USER_PHONE']",1);
               }
               if(ans.indexOf("okidoki")!=-1){
                   location.reload();
               }
               else{
                    $(".reg-and-enter-popup.reg button").text(oldtxt);
                }
           }
           
       });
       return false;
    }); 
    //диалоговое окно на выход
	$( document ).on( "click", ".link-exit", function() {
		$(".popup-agreement .popup-warning").text("Вы уверены что хотите выйти?");
		$(".popup.popup-agreement").fadeIn();
		$(".popup-agreement .popup-yes").addClass("go-exit");
		$(".popup-agreement .popup-no").addClass("go-exit");
	});
    $( document ).on( "click", ".popup-no.go-exit", function() {
		$(".popup.popup-agreement").fadeOut();
	});
	$( document ).on( "click", ".popup-yes.go-exit", function() {
		$( ".popup-no.go-exit" ).remove();
		$(".popup-yes.go-exit").text("Загрузка...");
        $(".popup-yes.go-exit").css("width","100%");
		$(".popup-yes.go-exit").attr("disabled","disabled");
		$.ajax({
			type:"POST",
			url:'/ajax/form.exit.php',
			data:{
				EXIT: 'Y',
            },
			success: function(response){
				location.replace("/"); 
			}
		});
	});
    //забыли пароль

     $( document ).on( "click", ".popup.forget-popup .btn", function() {
         resetvalidate(".popup.forget-popup input");
         resetvalidate(".popup.forget-popup .g-recaptcha");
         var oldtxt=$(".popup.forget-popup .btn").text();
         $(".popup.forget-popup .btn").text("Загрузка...");
        var form=$(".popup.forget-popup form");
		$.ajax({
			type:"POST",
			url:"/ajax/form.forgot.php",
            data:form.serialize(),
            recaptcha:grecaptcha.getResponse(),
			success: function(response){
                console.log(response);
                grecaptcha.reset();
                $(".popup.forget-popup button").text(oldtxt);
                if(response.indexOf("login-error")!=-1){
					validate(".popup.forget-popup input",1);
                }
                if(response.indexOf("recaptcha-error")!=-1){
					validate(".popup.forget-popup .g-recaptcha",1);
                }
                if(response.indexOf("login-error")==-1&&response.indexOf("recaptcha-error")==-1){
                    validate(".popup.forget-popup input",0);
                    $(".popup.forget-popup button").text("Перейти на "+response);
                    $(".popup.forget-popup input").val("");
                    $(".popup.forget-popup button").attr("onclick","location.replace('https://"+response+"')");
                }
                
			}
		});
		return false;
	});
	//редактировать и отмена сохранений в лк
	$( document ).on( "click", ".personal-edit", function() {
		$( "input" ).each(function( index ) {
			$(this).attr("oldval",$(this).val());
		});
	});
	$( document ).on( "click", ".personal-edit--close", function() {
		$( "input" ).each(function( index ) {
			$(this).val($(this).attr("oldval"));
		});
	});
	//чекбокс в личном кабинете
	$( document ).on( "click", "#personal-sms", function() {
		if(!$("#personal-sms").prop('checked')){
			$.ajax({
				url: '/ajax/form.profile.php',
				data: {
					NAME:"UF_SMS",
					VALUE: "",
				},
				success: function(ans){
					console.log(ans);
				}
			});
		}
		else{
			$.ajax({
				url: '/ajax/form.profile.php',
				data: {
					NAME:"UF_SMS",
					VALUE: "Y",
				},
				success: function(ans){
					console.log(ans);
				}
			});
		}
	});
	//фото пользователя
	$( document ).on( "change", "#personal-upload" , function() {
		$( "#my_form" ).trigger( "submit" );
	});
	$('#my_form').on('submit', function(e){
		e.preventDefault();
		var $that = $(this),
		formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
		$.ajax({
		  url: $that.attr('action'),
		  type: $that.attr('method'),
		  contentType: false, // важно - убираем форматирование данных по умолчанию
		  processData: false, // важно - убираем преобразование строк по умолчанию
		  data: formData,
		  dataType: 'html',
		  success: function(response){
			  console.log(response);
			$(".personal-img.profile-photo").css("background-image","url('"+response+"')");
		  }
		});
	});
	$( document ).on( "click", ".personal-save", function() {
		changeuserfield('NAME', 'personal-name');
		changeuserfield('EMAIL', 'personal-email');
		changeuserfield('PERSONAL_BIRTHDAY', 'personal-birth');
		changeuserfield('PERSONAL_PHONE', 'personal-phone');
		changeuserfield('UF_GROWTH', 'personal-height');
		changeuserfield('UF_WAIST', 'personal-waist');
		changeuserfield('UF_GROWTH', 'personal-height');
		changeuserfield('UF_HIPGIRTH', 'personal-hip');
		changeaddress();
		
	});
	//смена адреса в личном кабинете
	$( document ).on( "click", ".lk-regions .region", function() {
		$(".lk-regions .region").removeClass("selected");
		$(this).addClass("selected");
		$(".selectedadres").val($(this).attr("index"));
		$(".lk-body #personal-country").val($(this).attr("country"));
		$(".lk-body #personal-city").val($(this).attr("city"));
		$(".lk-body #personal-street").val($(this).attr("street"));
		$(".lk-body #personal-house").val($(this).attr("house"));
		$(".lk-body #personal-housing").val($(this).attr("housing"));
		$(".lk-body #personal-building").val($(this).attr("flat"));
		$(".lk-body #address-name").val($(this).attr("adresname"));
		return false;
	});
	//вызываем искусственный клик по первому адресу
	if($(".lk-regions .region").length>0)
		$(".lk-regions .region").first().trigger('click');
	//изменение адресов
	$( ".address-inputs input" ).keyup(function() {
		changeaddress();
	});
	$( document ).on( "blur", ".lk-body #address-name", function() {
		$( "button.region" ).each(function( index ) {
		  if($(this).text().length==0){
			  $(this).parents(".region-item").remove();
		  }
		});
	});
	$( document ).on( "click", ".popup-agreement .popup-yes.delete-address", function() {
		vall=$(this).attr("index");
		$.ajax({
			type:"POST",
			url:"/ajax/form.profile.php",
			data:{
				DELETE: 'Y',
				ADRES:vall,
			},
			success: function(response){
				console.log(response);
				location.reload();
			}
		});
	});
	$( document ).on( "click", ".popup-agreement .popup-no.delete-address", function() {
        $(".popup.popup-agreement").fadeOut();
        return false;
    }); 
    //изменение адреса в корзине
    if($(".choose_adres option").length>0){
        var element = $(".choose_adres option:selected");
        if(element.attr("value")=="new"){
			setCookie("default_address","new",{path: "/"});
		}
		else{
			setCookie("default_address",element.attr("value"),{path: "/"});
        }
        //submitForm();
    }
    $( document ).on( "change", ".choose_adres", function() {
      var element = $(".choose_adres option:selected");
      if(element.attr("value")=="new"){
			setCookie("default_address","new",{path: "/"});
			$("#ORDER_PROP_8").show();
			$("#ORDER_PROP_9").show();
            $("#ORDER_PROP_10").show();
            $("#ORDER_PROP_6_val").show();
			$(".bx-ui-sls-fake").val("");
			$(".dropdown-field.town").val("");
			$("#ORDER_PROP_8").val("");
			$("#ORDER_PROP_9").val("");
			$("#ORDER_PROP_10").val("");
		}
		else{
			setCookie("default_address",element.attr("value"),{path: "/"});
			$(".location-block-wrapper").hide();
			$("#ORDER_PROP_8").hide();
			$("#ORDER_PROP_9").hide();
			$("#ORDER_PROP_10").hide();
            $("#ORDER_PROP_6_val").hide();
            $("#ORDER_PROP_5").val(element.attr("cityname"));
			$("#ORDER_PROP_6").val(element.attr("city"));
			$("#ORDER_PROP_8").val(element.attr("street"));
			$("#ORDER_PROP_9").val(element.attr("house"));
			$("#ORDER_PROP_10").val(element.attr("flat"));
        }
        submitForm();
    });
	//изменение размера в корзине
     $( document ).on( "change", ".cart-count-size select", function() {
         var id=$(this).parents(".item").attr("product-id");
         var quantity=1;
         var size=$(this).parents(".item").find("select option:selected").val();
         recalc_in_basket(id,quantity,size);
     });
    //изменение количества в корзине
     $( document ).on( "change", ".counter input", function() {
         var id=$(this).parents(".item").attr("product-id");
         var quantity=$(this).val();
         var size=$(this).parents(".item").find("select option:selected").text();
         recalc_in_basket(id,quantity,size);
     });
    //удаление из корзины
     $( document ).on( "click", ".cart-items .remove, .top-cart  .delete", function() {
         dataLayer.push({
            'event': 'Remove from Cart',
            'ecommerce': {
              'remove': {                               // 'remove' actionFieldObject measures.
                'products': [{                          //  removing a product to a shopping cart.
                    'name': $(this).attr("name-gtm"),
                    'id': $(this).attr("id-gtm"),
                    'price': $(this).attr("price-gtm"),
                    'brand': $(this).attr("brand-gtm"),
                    'category': $(this).attr("category-gtm"),
                    'quantity': 1
                }]
              }
            }
          });
         var id=$(this).parents(".item").attr("product-id");
         var quantity=0;
         var size="";
         recalc_in_basket(id,quantity,size);
     });
    //поиск в шапке
    $( document ).on( "keyup", ".header-options .search-field input", function(e) {
         var code = e.keyCode || e.which;
         if(code == 13) { //Enter keycode
           location.replace("/search/?q="+$(this).val());
         }
		$(".header-options .search-field input").val($(this).val());
		if(xhr){ 
			xhr.abort();
		 }
		 if($(this).val().length>2){
			 xhr = $.ajax({
				url: '/ajax/head.search.php',
				data: {
					AJAXSEARCH:"Y",
					SEARCHQUERY:$(this).val()
				},
				success: function(ans){
					$(".header-options .search-field .results").html(ans);
					if(ans.length==0){
                        $(".header-options .search-field .results").hide();
						$(".header-options .search-field .results").addClass("empty");
                        $(".header-options .search-field .results").removeClass("active");
					}
					else{
                        $(".header-options .search-field .results").show();
                        $(".header-options .search-field .results").addClass("active");
						$(".header-options .search-field .results").removeClass("empty");
					}
				}
			});
		}
		else{
			$(".search_results").addClass("empty");
			$(".search_results").html("");
		}
	});
    
});
//удаление из вишлиста
function delwish(id){
    loading_animation(".wishlister .drop");
    if($(".wishlist-section .prod-container").length>0)
        loading_animation(".wishlist-section .prod-container");
	$.ajax({
		type:"POST",
		url:"/personal/wishlist/",
		data:{
			AJAX: 'Y',
			DELETEWISH:"Y",
			ID:id,
        },
		success: function(response){
            if($(".wishlist-section .prod-container").length>0)
			    $(".wishlist-section .prod-container").html(response);
            $(".wishlister").load("/personal/wishlist/ .wishlister .header-option",function() {
			});
		}
	});
}
//изменение значения обычного поля
function changeuserfield(name,id){
	$(".lk-body #"+id).removeClass("error");
	$.ajax({
		url: '/ajax/form.profile.php',
		data: {
			NAME:name,
			VALUE: $(".lk-body #"+id).val(),
		},
		success: function(ans){
			//console.log(ans);
			if(ans=="error"){
				$(".lk-body #"+id).addClass("error");
			}
			else{
				$(".lk-body #"+id).addClass("success");
				setTimeout(function() {
					$(".lk-body #"+id).removeClass("success");
				}, 2000);
			}
		}
	});
}
//проверка города
function profilecity(){
	$(".lk-body #personal-city").removeClass("error");
	$.ajax({
		url: '/ajax/city.profile.php',
		data: {
			CITY: $(".lk-body #personal-city").val(),
		},
		success: function(ans){
			console.log(ans);
			if(ans=="error"){
				$(".lk-body #personal-city").addClass("error");
			}
			else{
				$(".lk-body #personal-city").addClass("success");
				setTimeout(function() {
					$(".lk-body #personal-city").removeClass("success");
				}, 2000);
			}
		}
	});
}
//проверка пароля
function checkpass(){
	$.ajax({
		url: '/ajax/form.profile.php',
		data: {
			NAME:"password",
			VALUE: $(".active_password").val(),
		},
		success: function(ans){
			$(".active_password").parents(".form-group").removeClass("error");
			$(".active_password").parents(".form-group").removeClass("success");
			if(ans=="1"){
				$(".active_password").parents(".form-group").addClass("success");
				$(".active_password").attr("disabled","disabled");
				$(".new_pass1").removeAttr("disabled","disabled");
				$(".new_pass2").removeAttr("disabled","disabled");
				$('.new_pass1').trigger( "focus" );
			}
			else{
				$(".active_password").parents(".form-group").addClass("error");
			}
		}
	});
}
//проверка нового пароля
function checknewpass(){
	$.ajax({
		url: '/ajax/form.profile.php',
		data: {
			NAME:"password",
			VALUE1: $(".new_pass1").val(),
			VALUE2: $(".new_pass2").val(),
			VALUE3:$(".active_password").val()
		},
		success: function(ans){
			$(".new_pass1").parents(".form-group").removeClass("success");
			$(".new_pass2").parents(".form-group").removeClass("success");
			$(".new_pass1").parents(".form-group").removeClass("error");
			$(".new_pass2").parents(".form-group").removeClass("error");
			if(ans=="1"){
				$(".new_pass1").parents(".form-group").addClass("success");
				$(".new_pass2").parents(".form-group").addClass("success");
			}
			else{
				$(".new_pass1").parents(".form-group").addClass("error");
				$(".new_pass2").parents(".form-group").addClass("error");
			}
			//console.log(ans);
		}
	});
}
//изменения поля адреса
function changeaddress(){
    $(".form-group").removeClass("error");
	$.ajax({
		url: '/ajax/form.profile.php',
		data: {
			ADRES:$(".selectedadres").val(),
			CITY:$(".lk-body #personal-city").val(),
			STREET:$(".lk-body #personal-street").val(),
			HOUSE:$(".lk-body #personal-house").val(),
			HOUSING:$(".lk-body #personal-housing").val(),
			FLAT:$(".lk-body #personal-building").val(),
			ADRESNAME:$(".lk-body #address-name").val()
		},
		success: function(ans){
            if(ans.indexOf('city-error')!=-1){
                $("#personal-city").parents(".form-group").addClass("error");
            }
			$("button.region.selected").attr("city",$(".lk-body #personal-city").val());
			$("button.region.selected").attr("street",$(".lk-body #personal-street").val());
			$("button.region.selected").attr("house",$(".lk-body #personal-house").val());
			$("button.region.selected").attr("flat",$(".lk-body #personal-building").val());
			$("button.region.selected").attr("housing",$(".lk-body #personal-housing").val());
			$("button.region.selected").attr("adresname",$(".lk-body #address-name").val());
			$("button.region.selected").text($(".lk-body #address-name").val());
			
			$(".lk-custom_double.addresses").addClass("success");
			setTimeout(function() {
				$(".lk-custom_double.addresses").removeClass("success");
			}, 2000);
			console.log(ans);
		}
	});
}
function deleteaddress(index){
	$(".popup.popup-agreement").fadeIn();
	$(".popup-agreement .popup-yes").attr("index",index);
	$(".popup-agreement .popup-yes").addClass("delete-address");
	$(".popup-agreement .popup-no").addClass("delete-address");
}
function addaddress(){
	
	$("button.region").removeClass("selected");
	$("<div class='region-item'><button index='"+(parseInt($("button.region").last().attr("index"))+1)+"' class='region selected'>Мой новый адрес</button><button onclick='deleteaddress("+(parseInt($("button.region").last().attr("index"))+1)+")' class='region-delete'><svg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 232.3 239.9' style='enable-background:new 0 0 232.3 239.9;' xml:space='preserve'><g><path d='M130.5,0.7v105.2h101.2v26.9H130.5v106.1h-28.7V132.8H0.7v-26.9h101.2V0.7H130.5z'></path></g></svg></button></div>").insertBefore($(".add-region"));
	$(".selectedadres").val($("button.selected.region").attr("index"));
	$(".lk-body #personal-city").val("");
	$(".lk-body #personal-street").val("");
	$(".lk-body #personal-house").val("");
	$(".lk-body #personal-building").val("");
	$(".lk-body #personal-housing").val("");
	$(".lk-body #address-name").val("");
}
function delchar(input) { 
	var value = input.value; 
	var rep = /[-;,":'a-zA-Zа-яА-Я\\=`ё/\*++!@#$%\^&_№?><]/;
	if (rep.test(value)) { 
		value = value.replace(rep, ''); 
		input.value = value; 
	}
}
function validate(input,error){
	if(error==1){
		$(input).parents(".input").removeClass("success");
		$(input).parents(".input").addClass("error");
	}
	else{
		$(input).parents(".input").removeClass("error");
		$(input).parents(".input").addClass("success");
	}
		
}
function resetvalidate(input){
	$(input).parents(".input").removeClass("success");
	$(input).parents(".input").removeClass("error");		
}
function myalert(message,type){
    $(".popup.alert-popup .popup-template").removeClass("popup-full-width");
    if(type!="image"){
        $(".popup.alert-popup .popup-template").addClass("popup-full-width");
        $(".popup.alert-popup .img").hide();
        $(".popup.alert-popup .info").html(message);
    }
    else{
        $(".popup.alert-popup .popup-template-content").html(message);
    }
	$(".popup.alert-popup").fadeIn();
}
function loading_animation(selector){
    if($(selector+" .bubblingG").length==0)
	    $(selector).prepend('<div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>');
}
function delete_loading(){
	$(".bubblingG").remove();
}
// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : "";
}
//задавать свои куки
function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}
//удаление кук
function deleteCookie(name) {
  setCookie(name, "", {
    expires: -1,
	path: "/",
  })
}
function recalc_in_basket(id,quantity,size){
    if($(".section-cart .row .item"+id).length>0){
        loading_animation(".section-cart .row .item"+id);
        $(".section-cart .row .item"+id).addClass("loading-animation");
        $("#order_form_div").addClass("loading-animation");
        loading_animation("#order_form_content");
        setCookie("order_email",$("#ORDER_PROP_2").val(),{path: "/"});
        setCookie("order_phone",$("#ORDER_PROP_3").val(),{path: "/"});
        setCookie("order_name",$("#ORDER_PROP_1").val(),{path: "/"});
        setCookie("order_city",$("#ORDER_PROP_6").val(),{path: "/"});
        setCookie("order_street",$("#ORDER_PROP_8").val(),{path: "/"});
        setCookie("order_house",$("#ORDER_PROP_9").val(),{path: "/"});
        setCookie("order_flat",$("#ORDER_PROP_10").val(),{path: "/"});
         
    }
        
    
    loading_animation(".top-cart .drop");
    $.ajax({
        url: '/ajax/recalcbasket.php',
        data: {
            id:id,
            quantity:quantity,
            size:size
        },
        success: function(ans){
			console.log(ans);
            if($(".section-cart .row").length>0){
                $( ".section-cart .row " ).load( "?AJAX=Y", function() {
                    $.ajax({
                        url: '/ajax/addtocart.php',
                        data: {
                            cmd:"getHeadBasket"
                        },
                        success: function(ans){
                            $(".top-cart").html(ans);
                        }
                    });
                  $('select').selectric();
                  $(".hide-with-coupon").html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
                  submitForm("","",1);
                  tippy('.tooltip-icon', {
                    theme: 'april',
                    arrow: true
                });
                 
                });
                
            }
            else{
                $.ajax({
                   url: '/ajax/addtocart.php',
                   data: {
                       cmd:"getHeadBasket"
                   },
                   success: function(ans){
                       $(".top-cart").html(ans);
                   }
                });
            }
        }
    });
}
function addtocart(product_id,size){
    if($(".cart-products").length>0){
        $(".cart-products .products-container").addClass("loading");
        $(".cart-info").addClass("loading");
    }
    var oldtxt=$(".addtocart"+product_id).text();
    $(".addtocart"+product_id).text("Загрузка...");
    $.ajax({
        url: '/ajax/addtocart.php',
        data: {
           cmd:"addP2B",
           product_id:product_id,
           size:size
        },
        success: function(ans){
            if(ans.indexOf("disabled")!=-1){
                $(".addtocart"+product_id).addClass("disabled");
                $(".addtocart"+product_id).text(oldtxt);
            }
            else{
                ans=JSON.parse(ans);
                // yaCounter45391296.reachGoal('add_cart');
                
                //ga('send',  'event',  'Ecommerce',  'Add to Cart');
                //gtag('event', 'Add to Cart', {'event_category': 'Ecommerce'});      
                dataLayer.push({
                     'event': 'Add to Cart',
                     'ecommerce': {
                         'currencyCode': 'RUB',
                         'actionField': {'list':window.location.pathname},    // Если добавление произошло в списке товаров, необходимо передать в каком листе.
                         'add': {
                              'products': [
                             {
                                'name': ans.name,     // Название продукта 
                                'id': ans.id, 			// ID товара
                                'price': ans.price,			// Цена товара
                                'brand': ans.brand,			// Бренд товара
                                'category': ans.category    //Тип акции на товар, если 2, то через / 
                             }
                             ]
                        }
                     }
                }); 
                 $.ajax({
                    url: '/ajax/addtocart.php',
                    data: {
                        cmd:"getHeadBasket"
                    },
                    success: function(ans){
                        //scenes - Проверяем не заданы ли куки на сценарии
                        if(getCookie("scenes")==""){
                            deleteCookie("change");
                            deleteCookie("scene1");
                            deleteCookie("scene2");
                            deleteCookie("scene3");
                            deleteCookie("reload");
                            var d = new Date();
                            d.setDate(d.getDate() + 1);
                            setCookie("scene3","1",{path: "/"});
                            setCookie("scenes","1",{expires: d,path: "/"});
                            gotimer();
                        }
                        $(".addtocart"+product_id).text("Перейти в корзину");
                        $(".addtocart"+product_id).addClass("in-cart");
                        $(".addtocart"+product_id).removeClass("addtocart");
                        $(".addtocart"+product_id).attr("onclick","location.replace('/basket/')");
						$(".addtocart"+product_id).attr("href","/basket/");
						$(".addtocartid"+product_id).each(function() {
							$(this).text("Перейти в корзину");
							$(this).attr("onclick","location.replace('/basket/')");
							$(this).attr("href","/cart/");
						});
                        $(".top-cart").html(ans);
                    }
                });
                if($(".cart-products").length>0){
                    $.ajax({
                        url: '',
                        data: {
                            UPDATE_BASKET:""
                        },
                        success: function(ans){
                            $(".cart-products .products-container").removeClass("loading");
                            $(".for-ul-basket").html(ans);
                        }
                    });
                    $.ajax({
                        url: '',
                        data: {
                            UPDATE_CART_INFO:""
                        },
                        success: function(ans){
                            $(".cart-info").removeClass("loading");
                            $(".cart-info").html(ans);
                        }
                    });
                }
            }
        }
    });
}