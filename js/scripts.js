// animate fun
// use $('#yourElement').animateCss('bounce');
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

var mobile = false;
var iphone = false;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    mobile = true;
}
if (/iPhone/i.test(navigator.userAgent)) {
    iphone = true;
}
var btrxPanel= $('#bx-panel');
$(window).on('load', function () {
    // detect bitrix admin panel on a page and set padding for content
        btrxPanel = $('#bx-panel');
        btrxPanelHeight = btrxPanel.height();
        if (btrxPanel.length > 0 ) {
            $('.desktop-menu').css('padding-top', '39px');
            $('.footer-liner').css('padding-top', '16rem')
            if (mobile) {
                $('.footer-liner').css('padding-top', '11rem')
            }
        }
});
function doneResizing() {
    if(Modernizr.mq('screen and (min-width:768px)')) {
      $('.owlslidecontent').each(function(){
        var slickProduct = $(this);
        slickProduct.hover(
          function() {
            slickProduct.slick('slickPlay');
          }, function() {
            slickProduct.slick('slickPause');
            console.log('Hover off');
            
            slickProduct.slick('slickGoTo', 0);
          }
        );  
      })
    }
    else if(Modernizr.mq('screen and (max-width:767px)')) {
    }
  }
$(document).ready(function(){
    $(".owlslidecontent").slick({
        dots: false,
        arrows: false,
        infinite: true,
        autoplaySpeed: 100,
        speed: 2800,
        fade: true,
        slidesToShow: 1
    });
  
    $('.owlslidecontent').hover(
     function(e){},
     function(e) {
     e.preventDefault();
     $('.slider-nav').slick('slickGoTo', 0);
    });
    var id;
    $(window).resize(function() {
        clearTimeout(id);
        id = setTimeout(doneResizing, 0);
    });
  
    doneResizing();
    
    // personal info button edit
    $('.personal-edit').on('click', function() {
        $(this).parents('.personal-parent').find('input').removeAttr('disabled').first().focus();
        $(this).parents('.personal-parent').find('.personal-save').addClass('active');
        $(this).parents('.personal-parent').find('.lk-regions').addClass('active');
        $(this).parents('.personal-parent').addClass('active');
        $(this).hide();
        $(this).siblings('.personal-edit--close').show();
        if ($(this).parents('.personal-password')) {
            $(this).parents('.personal-password').find('input').attr('type', 'password');
        }
    })

    $('.personal-edit--close').on('click', function() {
        $(this).parents('.personal-parent').find('input').attr('disabled', 'disabled');
        $(this).parents('.personal-parent').find('.personal-save').removeClass('active');
        $(this).parents('.personal-parent').removeClass('active');
        $(this).parents('.personal-parent').find('.lk-regions').removeClass('active');
        $(this).hide()
        $(this).siblings('.personal-edit').show();
        if ($(this).parents('.personal-password')) {
            $(this).parents('.personal-password').find('input').attr('type', 'text');
        }
    })

    $('.personal-save').on('click', function() {
        $(this).parents('.personal-parent').find('input').attr('disabled', 'disabled');
        $(this).parents('.personal-parent').find('.personal-save').removeClass('active');
        $(this).parents('.personal-parent').removeClass('active');
        $(this).parents('.personal-parent').find('.lk-regions').removeClass('active');
        $(this).removeClass('active');
        $(this).parents('.personal-parent').find('.personal-edit--close').hide();
        $(this).parents('.personal-parent').find('.personal-edit').show();
        if ($(this).parents('.personal-password')) {
            $(this).parents('.personal-password').find('input').attr('type', 'text');
        }
    });

    // subscription page
    $('.subscription-link').on('click', function() {
        $(this).toggleClass('active');
    })

    // site-map sublist show
    $('.cloth-categories-link').on('click', function () {
        $(this).toggleClass('active');
        $(this).parents('.cloth-categories-item').find('.cloth-sublist').toggleClass('active');
    })

    $(".slider-on-main").lightSlider({
        item: 1,
        autoWidth: false,
        slideMargin: 0,
        speed: 400,
        auto: true,
        loop: true,
        pause: 4000,
        controls: false,
        pager: true,
        addClass: 'slider-on-main-container'
    });
    
    if (mobile || window.innerWidth <= 768){
        $(".inst-tiles.hide-on-desktop").lightSlider({
            item: 2,
            autoWidth: false,
            slideMargin: 0,
            speed: 400,
            auto: true,
            loop: true,
            pause: 2000,
            controls: false,
            pager: true,
            addClass: 'slider-on-main-instagram',
            responsive : [{
                breakpoint: 450,
                settings: {
                    item: 1
                }
            }]
        });
    }
    if (mobile || window.innerWidth <= 1024){
        $("#prod-container-on-main").lightSlider({
            item: 2,
            autoWidth: false,
            slideMargin: 0,
            speed: 400,
            //auto: true,
            loop: true,
            pause: 2000,
            controls: false,
            pager: true,
            addClass: 'prod-container-on-main',
            responsive : [{
                breakpoint:580,
                settings: {
                    item: 1
                }
            }]
        });
        $(".blog-main .row").lightSlider({
            item: 2,
            autoWidth: false,
            // slideMargin: 0,
            speed: 400,
            auto: true,
            loop: true,
            pause: 2000,
            controls: false,
            pager: true,
            addClass: 'slider-on-main-blog',
            responsive : [{
                breakpoint:580,
                settings: {
                    item: 1
                }
            }]
        });
        $(".blog-main .row").removeClass('row');
    }
    $( document ).on( "click", ".quick-look .preview img", function() {
        var thisAttrSrc = 'background-image:url(' + $(this).attr('src') + ')';
        $('.quick-look .main-image').attr('style', thisAttrSrc);
    });
    // if(!mobile){ // если не мобила, то даем падинг сверху для дэсктопного меню
    //     $('.footer-liner').attr('style', 'padding-top:' + $('.desktop-menu').height() + 'px');
    // }
    $(window).scroll(function() {
        var thisTop = $(window).scrollTop();
        if (thisTop >= 100) {
            $('.mobile-menu').addClass('active');
        } else {
            $('.mobile-menu').removeClass('active');
        }

        if ( thisTop >= 89) {
            // $('.footer-liner').addClass('float-menu');
            $('.footer-liner').addClass('floating-menu');

            if (btrxPanel.length > 0 ) {
                $('.desktop-menu').css('padding-top', '0');
                $('.footer-liner').css('padding-top', '12rem')
                if (!mobile) {
                    $('.footer-liner').css('padding-top', '0')
                }
            }
        } else {
            // $('.footer-liner').removeClass('float-menu');
            $('.footer-liner').removeClass('floating-menu');
            if (btrxPanel.length > 0 ) {
                $('.desktop-menu').css('padding-top', '0');
                $('.desktop-menu').css('padding-top', '39px');
                $('.footer-liner').css('padding-top', '16rem');
                if (mobile) {
                    $('.footer-liner').css('padding-top', '11rem')
                }
            }
        }
    });
    $(document).on("click", '.close-popup, .popup', function(event) { 
        $(this).parents('.popup').fadeOut();
        // $('body').toggleClass('no-scroll'); 
        // Не забудь тоглить скрол при показе попапа
    });

    $('.popup-container').on('click', function(e) {
	    if (!$(e.target).closest(".popup-container > div").length) {
            $(this).parents('.popup').fadeOut();
        }
    });

    $('.open-hamburger').on('click', function(){
        $('.pusher').toggleClass('active');
        $('body').toggleClass('no-scroll');
    });
    $('.hamburger-menu .mm-parent span').on('click', function(){
        $(this).parent().addClass('active');
    });
    $('.hamburger-menu .mm-parent li p').on('click', function(){
        $(this).closest('.active').removeClass('active');
    });
    $('.close-hamburger').on('click', function(){
        $('.pusher').toggleClass('active');
        $('body').toggleClass('no-scroll');
    });

    $(document).on('click', function(evt){  
        if($(evt.target).is('.search-field *') || $(evt.target).is('.search-field') ){
            $('.search-field').addClass('open-search');
            $('.search-field .results').show();
        }else{
            $('.search-field').removeClass('open-search');
            $('.search-field .results').hide();
        }
    });

    var focused = $('.search-input:first');

    $('.search-field').on('click', function () {
        $(this).hover();
        $(this).find('.search-input').focus();
        focused.next('input').trigger('touchstart');
    });

    $('.search-field').on('touchstart', function() {
        $(this).hover();
        $(this).find('.search-input').focus();
    });

    if (!mobile) {
            // FLOATING MENU
    if ($('aside').length && $('.prefooter').length) {
        var bottomOfAside = $('aside').offset().top + $('aside').height(),
            endOfScroll = $('.prefooter').offset().top,
            windowH = $(window).height();
        $('body').append('<div class="scrollToTop"><div><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 489 489" style="enable-background:new 0 0 489 489;" xml:space="preserve"><g><g><path d="M417.4,71.6C371.2,25.4,309.8,0,244.5,0S117.8,25.4,71.6,71.6S0,179.2,0,244.5s25.4,126.7,71.6,172.9S179.2,489,244.5,489s126.7-25.4,172.9-71.6S489,309.8,489,244.5S463.6,117.8,417.4,71.6z M244.5,462C124.6,462,27,364.4,27,244.5S124.6,27,244.5,27S462,124.6,462,244.5S364.4,462,244.5,462z"/><path d="M236.1,169.2c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l56.2,56.2L217,300.7c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4l65.7-65.7c5.3-5.3,5.3-13.8,0-19.1L236.1,169.2z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div></div>');
        $(window).scroll(function() {
            if ($(window).scrollTop() >= bottomOfAside) {
                $('.scrollToTop').addClass('active');
            } else {
                $('.scrollToTop').removeClass('active');
            }

            var theLine = $('.prefooter').offset().top - $(window).height();
            if ($(window).scrollTop() >= theLine) {
                var curbottom = $(window).scrollTop() - theLine;
                $('.scrollToTop').css('bottom', curbottom)
            } else {
                $('.scrollToTop').css('bottom', 0)
            }
        });
        $(document).on("click", ".scrollToTop", function() {
            var body = $("html, body");
            body.stop().animate({
                scrollTop: 0
            }, '500', 'swing', function() {});
        });
    }
    }
    
    //var el = document.querySelector('.description-hider');
    //SimpleScrollbar.initEl(el);
    // $('.description-dropdown').addClass('active');
    $( document ).on( "click", ".description-dropdown span", function() {
       // $(this).parent().toggleClass('active');
		if ($('.description-hider').is(":visible")) {
			$(this).children().filter('i').css('transform', 'rotate(90deg)');
			$(this).parent().children().filter('div').slideToggle('slow');
		} else {
			$(this).children().filter('i').css('transform', 'rotate(45deg)');
			$(this).parent().children().filter('div').slideToggle('slow');
		}


         //$(this).toggleClass('active');
    });

    $( document ).on( "click", ".link-enter", function() {
        $('.enter').closest('.popup').fadeIn();
    });
    $('.forget-link').click(function(){
        $(this).parents('.popup').fadeOut();
        $('.forget-popup').fadeIn();
    });
    $('.reg-and-enter-body .link-enter').click(function(){
        $(this).parents('.popup').fadeOut();
    });
    $('.link-register').click(function(){
        $('.reg').closest('.popup').fadeIn();
    });
	$( document ).on( "click", ".action-to-cart", function() {
        $('.sizes-popup').closest('.popup').fadeIn();
    });
    $( document ).on( "click", ".table-size", function(e) {
        e.preventDefault();
        $('.popup-table-size').fadeIn();
    });
    // http://selectric.js.org/
    $('select').selectric();
    $( document ).on( "click", ".counter span", function() {
        var input = $(this).siblings('input');
        if($(this).attr('class') == 'plus'){
            input.val(parseInt(input.val()) + 1);
            input.trigger("change");
        }else{
            if(input.val() >= 2){
                input.val(parseInt(input.val()) - 1);
                input.trigger("change");
            }
        }
        
    });
    $('.catalog-menu .item p').click(function(){
        window.location.href=$(this).find('a').attr('href')
        /*if( $(this).children().length == 0 ){
            $(".catalog-menu .item.parent-item p").show();
            $(".catalog-menu .item.parent-item p.witha").hide();
            $(this).parent().addClass('open');
            $(this).next("p").show();
            $(this).hide();
            $(this).parent().siblings().removeClass('open');
        }*/
    });
	$('.cookie .close-icon.close-popup').click(function(){
		$('.cookie').fadeOut('slow');
	});
    // $('.grid').masonry({
    //     // set itemSelector so .grid-sizer is not used in layout
    //     itemSelector: '.grid-item',
    //     // use element for option
    //     columnWidth: 235
    // })

    // if mobile width, change location of ".item-price" in item_single.php
    if (window.innerWidth <= 736) {
        //$(".item-price").insertAfter(".item-photo");
        var divForAppend =  $(".item-single .xs-order-1");
        $(".item-header").prependTo(divForAppend);
    }
		if( $('.promocode').hasClass('success') ) {
			$(this).children().filter('input').attr('disabled');
		}
    // Scaling main background image in item_single.php
    $('.zoo-item').ZooMove({
        cursor: 'true',
        scale: '1.8',
        over: 'true'
    });


    // lightSlider for controller with img in item_single.php
    if (window.innerWidth >= 737) {
        var slider = $('.item-controller--content').lightSlider({
            item: 3,
			//slideMove: 1,
            auto: false,
            vertical: true,
            verticalHeight: 300,
            enableDrag: true,
            slideMargin: 0,
            freeMove: false,
            pager: false,
            controls: false
    });
} else if (window.innerWidth <= 736) {
	//slider.destroy(); 
	//$('.item-controller--content').hide();
	/*$('.item-controller--content').lightSlider({
			item: 6,
            autoWidth: true,
            slideMargin: 0,
            //speed: 400,
            //auto: true,
            //loop: true,
            pause: 2000,
            controls: false,
            pager: false,
            responsive : [{
                breakpoint:580,
                settings: {
                    item: 4
                }
            }]
    });*/
}

$(document).mouseup(function (e) {
    var container = $(".popup-body");
    if (container.has(e.target).length === 0){
        container.parent().parent().fadeOut('slow');
    }
});
$(document).mouseup(function (e) {
    var container = $(".popup-content");
    if (container.has(e.target).length === 0){
        container.parent().parent().fadeOut('slow');
    }
});
    // custom scroll buttons for lightslider in item_single.php
    $('.item-controller .item-controller-prev').click(function() {
        slider.goToPrevSlide();
    });
    $('.item-controller .item-controller-next').click(function() {
        slider.goToNextSlide();
    });

    // change background img on click in item_single.php
    $('.item-controller--content img').on('click', function() {
        var thisAttrSrc = $(this).attr('src')
            $('.zoo-img').attr('style', 'background-image: url(' + thisAttrSrc + ');');
    });

    $('.inst_main-container').masonry({
        itemSelector: '.inst-item',
        columnWidth: '.inst-sizer',
        gutter: '.gutter-sizer',
        percentPosition: true
    });

    tippy('.tooltip-icon', {
        theme: 'april',
        arrow: true
    });

});


/* - - - -*/

$(document).on('click', '.js-menu-button', function() {

    if ($(this).hasClass('active')) {
        $('body').css('overflow', '')
        $(this).removeClass('active')
        $('.big-menu').fadeOut(300)
        $('.footer-liner.floating-menu .desktop-menu').removeClass('menu-open')
        $('.header-top.standing').css('background', '#fff');

        $('.mobile-menu').removeClass('open')
        $('.header-top ').removeClass('open')
        
    } else {
        $('body').css('overflow', 'hidden')
        $(this).addClass('active')
        $('.big-menu').fadeIn(300)
        $('.footer-liner.floating-menu .desktop-menu').addClass('menu-open')
        $('.header-top.standing').css('background', '#CDCDCD');

        $('.mobile-menu').addClass('open')
        $('.header-top ').addClass('open')
    }    
})

$(document).on('click', '.big-menu__list li div', function() {
    $(this).siblings("ul").slideToggle();
    $(this).children('.expand-icon').toggleClass("expanded");
    event.stopPropagation();
})

// scroll info size table

$('.table-size').click(() => {
    $('html, body').animate({
        scrollTop: $('.title-size-popup').offset().top
    }, 500);
});