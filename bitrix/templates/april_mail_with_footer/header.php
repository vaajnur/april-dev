<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
		@media screen and (max-width: 480px){
			.four-td-layout > td {
				width: 100%!important;
				display: inline-block;
				padding: 0 0 10px!important;
				text-align: center;
			}
			.h1 {
				font-size: 36px!important;
			}
			.h2 {
				font-size: 24px!important;
			}
			.button {
				max-width: 90%!important;
				box-sizing: border-box;
			}
		}
	</style>
</head>
<body style="padding:0;margin:0;">
	<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
		<tbody>
			<tr style="text-align:center;background-color:#ffe8e6;max-width:600px;">
				<td style="max-width:300px;margin:auto;padding:20px 0;text-align:center;"><a href="https://aprilmoscow.ru/?utm_source=email&utm_medium=cpc&utm_campaign=shapka" target="_blank"><img style="max-width:100%;" src="https://aprilmoscow.ru/img/logo1.png" alt=""></a></td>
			</tr>
			<tr style="max-width:600px;margin:auto;text-align:center;">
				<td style="width:100%;text-align:center;max-width:600px;padding:20px 0 0;">
					<a href="https://aprilmoscow.ru/novinki/?utm_source=email&utm_medium=cpc&utm_campaign=shapka" style="font-size:16px;text-decoration:none;color:black;font-family:sans-serif;" target="_blank">Новинки</a>
					<a href="https://aprilmoscow.ru/zhenskaya_odezhda/?utm_source=email&utm_medium=cpc&utm_campaign=shapka" style="font-size:16px;text-decoration:none;color:black;font-family:sans-serif;padding:5px;" target="_blank">Женская одежда</a>
					<a href="https://aprilmoscow.ru/rasprodazha/?utm_source=email&utm_medium=cpc&utm_campaign=shapka" style="font-size:16px;text-decoration:none;color:black;font-family:sans-serif;padding:5px;" target="_blank">SALE</a>
					<a href="https://aprilmoscow.ru/dizaynery/?utm_source=email&utm_medium=cpc&utm_campaign=shapka" style="font-size:16px;text-decoration:none;color:black;font-family:sans-serif;padding:5px;" target="_blank">Дизайнеры</a>
					<a href="https://aprilmoscow.ru/instashop/?utm_source=email&utm_medium=cpc&utm_campaign=shapka" style="font-size:16px;text-decoration:none;color:black;font-family:sans-serif;" target="_blank">InstaShop</a>
				</td>
			</tr>
		</tbody>
	</table>
