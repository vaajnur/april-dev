	<table border="0" cellpadding="0" cellspacing="0" style="width:100%;background-color:#ffe8e6;border-width:2px 0;border-style:solid;border-color:#d59b69;">
		<tbody>
			<tr style="max-width:600px;display:block;margin:auto;padding:25px 10px;text-align:left;box-sizing:border-box;">
				<td style="width:50%;vertical-align:top;text-align:left;padding-left:25px;">
					<a href="https://aprilmoscow.ru/dostavka-i-oplata/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="line-height: 25px;font-size:11px;text-decoration:none;color:black;font-family:sans-serif;" target="_blank">Доставка и оплата</a><br>
					<a href="https://aprilmoscow.ru/programma-loyalnosti/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="line-height: 25px;font-size:11px;text-decoration:none;color:black;font-family:sans-serif;" target="_blank">Программа лояльности</a><br>
					<a href="https://aprilmoscow.ru/vozvrat-tovara/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="line-height: 25px;font-size:11px;text-decoration:none;color:black;font-family:sans-serif;" target="_blank">Возврат товара</a><br>
					<a href="https://aprilmoscow.ru/o-kompanii/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="line-height: 25px;font-size:11px;text-decoration:none;color:black;font-family:sans-serif;" target="_blank">О компании</a><br>
					<a href="https://aprilmoscow.ru/otzyvy/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="line-height: 25px;font-size:11px;text-decoration:none;color:black;font-family:sans-serif;" target="_blank">Отзывы</a><br>
					<a href="https://aprilmoscow.ru/kontakty/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="line-height: 25px;font-size:11px;text-decoration:none;color:black;font-family:sans-serif;" target="_blank">Контакты</a>
				</td>
				<td style="width:50%;vertical-align:top;text-align:right;padding-right:25px;">
					<span style="font-size:11px;color:black;font-family:sans-serif;">+7 (499) 248-08-80</span><br>
					<a href="mailTo:love@aprilmoscow.ru" style="text-decoration:none;font-size:11px;color:black;font-family:sans-serif;line-height: 25px;" target="_blank">love@aprilmoscow.ru</a><br><br>
					<span style="font-size:11px;color:black;font-family:sans-serif;">г. Москва.<br>Большой Саввинский переулок, 16/14, строение 7.</span>
				</td>
			</tr>
			<tr style="max-width:600px;margin:auto;text-align:center;padding:25px 10px;">
				<td style="vertical-align:top;width:33%;text-align:center;padding-bottom:25px;">
					<span style="font-size:11px;color:black;font-family:sans-serif;display:block;">Присоединяйтесь к нам в социальных сетях</span>
					<span style="text-align:left;">
						<a style="font-size:11px;color:black;font-family:sans-serif;" href="https://www.instagram.com/aprilmoscow/"><img src="https://aprilmoscow.ru/img/icon-inst.png" alt="" style="padding-top:5px;"></a>
						<a style="font-size:11px;color:black;font-family:sans-serif;padding-left:5px;" href="https://www.facebook.com/aprilmoscow/"><img src="https://aprilmoscow.ru/img/icon-fb.png" alt="" style="padding-top:5px;"></a>
					</span><br>
					<span style="font-size:11px;color:black;font-family:sans-serif;text-align:left;">Удобная оплата заказов:</span>
					<span><br>
						<img style="height:11px;display:inline-block;vertical-align:middle;padding-top:5px;" src="https://aprilmoscow.ru/img/icon-visa.png" alt="">
						<img style="height:14px;display:inline-block;vertical-align:middle;padding-top:5px;padding-left:5px;padding-right:5px;" src="https://aprilmoscow.ru/img/icon-mastercard.png" alt="">
						<img style="height:14px;display:inline-block;vertical-align:middle;padding-top:5px;" src="https://aprilmoscow.ru/img/icon-apple.png" alt="">
					</span>
				</td>
			</tr>
		</tbody>
	</table>
	<table border="0" cellpadding="0" cellspacing="0" style="width:100%;background-color:#322a35;">
		<tbody>
			<tr style="text-align:center;max-width:600px;">
				<td style="max-width:300px;margin:auto;padding:25px 0 0;text-align:center;"><a href="https://aprilmoscow.ru/?utm_source=email&utm_medium=cpc&utm_campaign=footer" target="_blank"><img style="max-width:100%;" src="https://aprilmoscow.ru/img/logo2.png" alt=""></a></td>
			</tr>
			<tr style="max-width:600px;margin:auto;text-align:center;">
				<td style="width: 100%;display: block;text-align:center;padding: 25px 0;">
					<span style="font-size:14px;text-decoration:none;color:white;font-family:sans-serif;">© 2018 магазин дизайнерской одежды АПРЕЛЬ<br>ИНН: 7704409265 ОГРН: 1177746436680</span>
				</td>
			</tr>
			<tr style="max-width:600px;margin:auto;text-align:center;">
				<td style="width: 100%;display: block;text-align:center;padding: 0 0 25px;">
					<a href="https://aprilmoscow.ru/sitemap/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="font-size:14px;text-decoration:none;color:white;font-family:sans-serif;" target="_blank">Карта сайта</a>
					<a href="https://aprilmoscow.ru/politika-konfidencialnosti/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="font-size:14px;text-decoration:none;color:white;font-family:sans-serif;padding:0 10px;" target="_blank">Политика конфиденциальности</a>
					<a href="https://aprilmoscow.ru/publichnaya-oferta/?utm_source=email&utm_medium=cpc&utm_campaign=footer" style="font-size:14px;text-decoration:none;color:white;font-family:sans-serif;" target="_blank">Публичная оферта</a>
				</td>
			</tr>
			<tr style="max-width:600px;margin:auto;text-align:center;">
				<td style="width: 100%;display: block;text-align:center;">
					<span style="font-size:11px;text-decoration:none;color:white;font-family:sans-serif;" target="_blank">Вы получили это письмо, потому что подписались на рассылку на сайте www.aprilmoscow.ru</span>
				</td>
			</tr>
			<tr style="max-width:600px;margin:auto;text-align:center;">
				<td style="width: 100%;display: block;text-align:center;padding: 0 0 25px;">
					<a href="https://aprilmoscow.ru/unsubscribe/?email=#EMAIL_RECEPIENT#&hash=#EMAIL_RECEPIENT_MD5#&utm_source=email&utm_medium=cpc&utm_campaign=unsubscribe" style="font-size:11px;text-decoration:none;color:white;font-family:sans-serif;" target="_blank">Отписаться от рассылки</a>
				</td>
			</tr>
		</tbody>
	</table>
</body>