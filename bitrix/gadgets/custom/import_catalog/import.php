<?
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

\Bitrix\Main\Loader::includemodule('iblock');
\Bitrix\Main\Loader::includemodule('sale');

global $show_details, $translit_params, $ibl_id, $sku_ibl_id, $prop_tsvet_id, $prop_razmer_id, $prop_cml2_manufacturer_id;

// =============== BOY

$ibl_id = 20;
$sku_ibl_id = 21;
$prop_tsvet_id = 148;
$prop_razmer_id = 149;
$prop_cml2_manufacturer_id = 147;
// ============== TEST
// $ibl_id = 24;
// $sku_ibl_id = 25;
// $prop_tsvet_id = 172;
// $prop_razmer_id = 173;
// $prop_cml2_manufacturer_id = 183;

$show_details = $_POST['show_details']; 
$translit_params = array(
      "max_len" => "100", 
      "change_case" => "L", 
      "replace_space" => "_", 
      "replace_other" => "_", 
      "delete_repeat_replace" => "true", 
      "use_google" => "false", 
   );
// echo "file name";
// var_dump($uploads_dir.'/'.$name);
$file_csv = $uploads_dir.'/'.$name;
// var_dump($file_csv);

// ////////////////////////////////// деактивация всех элементов
$elems = ciBlockElement::getlist([], ['IBLOCK_ID' => $ibl_id, 'IBLOCK_TYPE' => '1c_catalog'], false, false, ['ID', 'IBLOCK_ID']);
$el_1 = new ciblockelement;
while ($OB1 = $elems->getnext())
	$res = $el_1->Update($OB1['ID'], ['ACTIVE' => 'N']);


/**
 * [$file1 description]
 * @var [type]
 */
$file1 = $file_csv;
if (($handle = fopen($file1, "r")) !== FALSE) {
	$counter = 0;
    while (($data = fgetcsv($handle, 10000000, ";")) !== FALSE) {
		$counter++;
		if($counter == 1) continue;
		// ограничение
		// if($counter == 13) break;
		// enum list
		$manufac_vals = get_enum_list('CML2_MANUFACTURER', $ibl_id);
		$res1 = ciblockelement::getlist(array(), array(
			'IBLOCK_ID' => $ibl_id, 
			'IBLOCK_TYPE' => '1c_catalog', 
			// 'NAME' => $data[2], 
			// 'PROPERTY_CML2_MANUFACTURER' => $manufac_vals[$data[0]], 
			'PROPERTY_CML2_ARTICLE' => $data[1]
		), false, false, array('ID', 'IBLOCK_ID', 'NAME'));
		if($ob = $res1->GetNext()){
			if($show_details == true)
				echo "elem finded!<br>";
			$el = new ciblockelement;
			// activate
			$res = $el->Update($ob['ID'], ['ACTIVE' => 'Y']);
			add_offer($data, $ob['ID']);
		}else{
			add_good($data);
		}
    }
    fclose($handle);
    // удаляю файл
    unlink($file_csv);
}

/** 
 * [get_section description]
 * @param  [type] $data [description]
 * @return [type]       [description]
 */
function get_section($data){
	global $show_details, $translit_params, $ibl_id, $sku_ibl_id;
	$res1 = ciblocksection::getlist([], ['NAME' => $data[10], 'IBLOCK_ID' => $ibl_id ], false, ['ID']);
	if($ob1 = $res1->getnext()){
		if($show_details == true)
			echo "sect finded<br>";
		$id_sect = $ob1['ID'];
	}else{
		$sect = new ciblocksection;

		$id_sect = $sect->add(
			[
				'ACTIVE' => 'Y',
				'IBLOCK_ID' => $ibl_id,
				'NAME' => $data[10],
				'CODE' => CUtil::translit($data[10], "ru", $translit_params)
			]
		);
		if($id_sect == true)
			if($show_details == true)
				echo "sect added<br>";
		else
			echo $sect->LAST_ERROR;	
	}
	return $id_sect;
}


/** 
 * [add_good description]
 * @param [type] $data [description]
 */
function add_good($data){
	global $show_details, $translit_params, $ibl_id, $sku_ibl_id;

		// find section
		$id_sect = get_section($data);

		$ciBlockElement = new CIBlockElement;

		// get enum list
		$manufac_vals = get_enum_list('CML2_MANUFACTURER', $ibl_id);

		$arLoadProductArray = array(
		    "IBLOCK_ID"      => $ibl_id, // IBLOCK торговых предложений
		    "NAME"           => $data[2],
		    "ACTIVE"         => "Y",
		    'CODE' => CUtil::translit($data[2], "ru", $translit_params).$data[1].CUtil::translit($data[4], "ru", $translit_params).$data[5],
		    // "IBLOCK_SECTION_ID"         => $id_sect,
		    "IBLOCK_SECTION"         => [$id_sect],
		    'PROPERTY_VALUES' => array(
		        'CML2_ARTICLE' => $data[1],
		        'CML2_MANUFACTURER' => $manufac_vals[$data[0]]
		    )
		    // Прочие параметры товара 
		);
		$product_id = $ciBlockElement->Add($arLoadProductArray);
		if($product_id != false){
			if($show_details == true)
				echo "elem added!<br>";
			$res = CCatalogProduct::Add(
			    array(
			        "ID" => $product_id,
			        "QUANTITY" => $data[6]
			    )
			);
			add_offer($data, $product_id);
		}else{
			echo $ciBlockElement->LAST_ERROR;
		}
}

/** 
 * [get_enum_list description]
 * @param  [type] $code      [description]
 * @param  [type] $iblock_id [description]
 * @return [type]            [description]
 */
function get_enum_list($code, $iblock_id){
	global $show_details, $translit_params, $ibl_id, $sku_ibl_id;
	// тип список
	$enumlist = ciblockpropertyenum::getlist([], ['CODE' => $code, 'IBLOCK_ID' => $iblock_id]);
	$enum_vals = [];
	while ($ob2 = $enumlist->getnext()) {
		$enum_vals[$ob2['VALUE']] = $ob2['ID'];
	}
	return $enum_vals;
}

/** 
 * [add_offer description]
 * @param [type] $data       [description]
 * @param [type] $product_id [description]
 */
function add_offer($data, $product_id){
	global $show_details, $translit_params, $ibl_id, $sku_ibl_id, $prop_tsvet_id, $prop_razmer_id, $prop_cml2_manufacturer_id;
	$ciBlockElement = new CIBlockElement;
		$tsvet_vals = get_enum_list('TSVET', $sku_ibl_id);
		$razmer_vals = get_enum_list('RAZMER', $sku_ibl_id);
		$vendor_vals = get_enum_list('CML2_MANUFACTURER', $sku_ibl_id);

	// =========== поиск ску
	$sku_arr = ccatalogsku::getofferslist($product_id, $ibl_id, [
		'PROPERTY_TSVET' => $tsvet_vals[$data[4]],
		'PROPERTY_RAZMER' => $razmer_vals[$data[5]], 
		'PROPERTY_CML2_ARTICLE' => $data[1], 
		'PROPERTY_CML2_MANUFACTURER' => $vendor_vals[$data[0]], 
	], ['ID', 'IBLOCK_ID', 'NAME']);

	// если ску уже есть обновляю кол-во и цену
	if(!empty($sku_arr)){
		// var_dump($sku_arr);
		$sku_id = key($sku_arr[key($sku_arr)]);
		$el = new ciblockelement;
		// activate
		$res = $el->Update($sku_id, ['ACTIVE' => 'Y']);
		if($show_details == true)
			echo "sku is ".$sku_id.'<br>';
		$PRICE_TYPE_ID = 1;
		$arFields = array('QUANTITY' => $data[6]);//  количество
		if(CCatalogProduct::Update($sku_id, $arFields)){
			if($show_details == true)
				echo "quant updated";
		}
		else{
			if($show_details == true)
				echo "qnt not up";
		}

		$arFields = Array(
		    "PRODUCT_ID" => $sku_id,
		    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
		    "PRICE" => preg_replace('/[\s ]+/', '', $data[7]),
		    "CURRENCY" => "RUB"
		);

		$res1 = CPrice::GetList(array(), array("PRODUCT_ID" => $sku_id, "CATALOG_GROUP_ID" => $PRICE_TYPE_ID ) );

		// обновление цены
		if ($arr = $res1->Fetch()) {
		    if(CPrice::Update($arr["ID"], $arFields)){
		    	if($show_details == true)
			    	echo "price upped";
		    }
		    else{
		    	if($show_details == true)
			    	echo "price not up";
		    }
		} else {
		    CPrice::Add($arFields);
		}
	}else{

		$PROPS = array(
		        'CML2_LINK' => $product_id, 
		        'CML2_ARTICLE' => $data[1],
		    );
		$ibpenum = new CIBlockPropertyEnum;

		// +++++++++++++++++++++++++++++ цвет
		if(isset($tsvet_vals[$data[4]]))
			$PROPS['TSVET'] = $tsvet_vals[$data[4]];
		else{
			$PropID = null;
			if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=> $prop_tsvet_id, 'VALUE'=>$data[4]))){
				if($show_details == true)
					 echo 'New ID:'.$PropID;
				$PROPS['TSVET'] = $PropID;
			}else{
				if($show_details == true)
					echo "prop not added";
			}
		}
		// +++++++++++++++++++++++++++++  размер
		if(isset($razmer_vals[$data[5]]))
			$PROPS['RAZMER'] = $razmer_vals[$data[5]];
		else{
			$PropID = null;
			if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=> $prop_razmer_id, 'VALUE'=>$data[5]))){
			 	if($show_details == true)
				 	echo 'New ID:'.$PropID;
				$PROPS['RAZMER'] = $PropID;
			}else{
				if($show_details == true)
					echo "prop not added";
			}
		}

		// +++++++++++++++++++++++++++++  Производитель
		if(isset($vendor_vals[$data[0]]))
			$PROPS['CML2_MANUFACTURER'] = $vendor_vals[$data[0]];
		else{
			$PropID = null;
			if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=> $prop_cml2_manufacturer_id, 'VALUE'=>$data[0]))){
				if($show_details == true)
					 echo 'New ID:'.$PropID;
				$PROPS['CML2_MANUFACTURER'] = $PropID;
			}else{
				if($show_details == true)
					echo "prop not added";
			}
		}


		$arLoadProductArray = array(
		    "IBLOCK_ID"      => $sku_ibl_id, // IBLOCK торговых предложений
		    "NAME"           => $data[2] . "(цвет {$data[4]}, Размер {$data[5]})",
		    "ACTIVE"         => "Y",
		    'PROPERTY_VALUES' => $PROPS
		);
		$product_offer_id = $ciBlockElement->Add($arLoadProductArray);
		if($product_offer_id == true){
			if($show_details == true)
				echo "offer added<br>";
			$res = CCatalogProduct::Add(
			    array(
			        "ID" => $product_offer_id,
			        "QUANTITY" => $data[6]
			    )
			);
			// var_dump($res) . PHP_EOL;
			$res1 = CPrice::Add(
			    array(
			        "CURRENCY" => "RUB",
			        "PRICE" => preg_replace('/[\s ]+/', '', $data[7]),
			        "CATALOG_GROUP_ID" => 1,
			        "PRODUCT_ID" => $product_offer_id,
			    )
			);
			// var_dump($res, $res1) . PHP_EOL;
		}else{
			echo $product_offer_id->LAST_ERROR;
		}

		global $APPLICATION;
		if($ex = $APPLICATION->GetException()){
			echo $ex->Getstring();
		}
	}
}


?>


<?

// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");

?>