<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
function dropBackWordshl($word) { //тут мы обрабатываем одно слово
	$reg = "/(ый|ой|ая|ое|ые|ому|ями|ем|ами|а|о|у|е|ого|ему|и|ство|ых|ох|ия|ий|ь|я|он|ют|ат)$/i"; //данная регулярная функция будет искать совпадения окончаний
	$word = preg_replace($reg,'',$word); //убиваем окончания
	return $word;
}
$searchquery=$_REQUEST["SEARCHQUERY"];
$lat=str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789&");
$category=trim(str_replace($lat,"",$searchquery));
$designer=$searchquery;
$words=explode(" ",$category);
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
foreach($words as $word){
	$designer=trim(str_replace($word,"",$designer));
}
$category=dropBackWordshl($category);
    //если в поиске нет дизайнера ищем категории сначала
    if(strlen($designer)==0){
        $arFilter = array('IBLOCK_ID' => CATALOG_IBLOCK_ID,"%NAME"=>$category);
        $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
        while ($arSection = $rsSections->GetNext())
        {
        ?>
            <li class="title"><a class="s_title" href="<?=$arSection['SECTION_PAGE_URL'];?>"><?=$arSection['NAME'];?></a></li>
            <?
            
            ?>
        <?}
    }
    else if(strlen($category)==0){
        $arSelect = Array("ID", "NAME","CODE");
        $arFilter = Array("IBLOCK_ID"=>13,"%NAME"=>$designer,"ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>100000), $arSelect);
        while($ob = $res->GetNextElement())
        {
            $arFi=$ob->GetFields();
        ?>
           <li class="title"><a class="s_title" href="/dizaynery/<?=$arFi['CODE'];?>/"><?=$arFi['NAME'];?></a></li>
        <?
        }
        $arFilterCat=array("%PROPERTY_CML2_MANUFACTURER_VALUE"=>$designer,"IBLOCK_ID"=>CATALOG_IBLOCK_ID /*,">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"!DETAIL_PICTURE"=>false */ );
        $resCatItems = CIBlockElement::GetList(Array(), $arFilterCat,false,array("nPageSize"=>100000), Array("NAME","ID","IBLOCK_SECTION_ID","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER"));
		while($arCatItems=$resCatItems->GetNextElement())
		{
            $arFields=$arCatItems->GetFields();
		?>
           <li><a href="<?=$arFields['DETAIL_PAGE_URL'];?>"><?=explode(" ",trim($arFields['NAME']))[0]." ".$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];?></a></li>
        <?
		}
    }
    else{
        $arFilterCat=array("%PROPERTY_CML2_ARTICLE"=>$category,"IBLOCK_ID"=>CATALOG_IBLOCK_ID,/*,">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"!DETAIL_PICTURE"=>false */ );
        $resCatItems = CIBlockElement::GetList(Array(), $arFilterCat,false,array("nPageSize"=>100000), Array("NAME","ID","IBLOCK_SECTION_ID","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER"));
		while($arCatItems=$resCatItems->GetNextElement())
		{
            $arFields=$arCatItems->GetFields();
		?>
           <li><a href="<?=$arFields['DETAIL_PAGE_URL'];?>"><?=explode(" ",trim($arFields['NAME']))[0]." ".$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];?></a></li>
        <?
		}
        $arFilterCat=array("%NAME"=>$category,"%PROPERTY_CML2_MANUFACTURER_VALUE"=>$designer,"IBLOCK_ID"=>CATALOG_IBLOCK_ID,/*,">CATALOG_PRICE_2"=>0,">CATALOG_QUANTITY"=>0,"!DETAIL_PICTURE"=>false */ );
        $resCatItems = CIBlockElement::GetList(Array(), $arFilterCat,false,array("nPageSize"=>100000), Array("NAME","ID","IBLOCK_SECTION_ID","DETAIL_PAGE_URL","PROPERTY_CML2_MANUFACTURER"));
		while($arCatItems=$resCatItems->GetNextElement())
		{
            $arFields=$arCatItems->GetFields();
		?>
           <li><a href="<?=$arFields['DETAIL_PAGE_URL'];?>"><?=explode(" ",trim($arFields['NAME']))[0]." ".$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];?></a></li>
        <?
		}
    }
?>