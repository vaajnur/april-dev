<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

$arSelect = Array();
$mxResult = CCatalogSku::GetProductInfo(
	$_REQUEST['PRODUCT_ID']
);
if (is_array($mxResult))
{
	$product_id=$mxResult['ID'];
}
else{
	$product_id=$_REQUEST['PRODUCT_ID'];
}
$res_offers = CCatalogSKU::getOffersList(
	$product_id,
	CATALOG_IBLOCK_ID,
	$skuFilter = array("CATALOG_AVAILABLE"=>"Y","ACTIVE"=>"Y"),
	$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
	$propertyFilter = array()
);
$no_sizes=true;
foreach($res_offers[$product_id] as $good){
	if(strlen($good['PROPERTY_RAZMER_VALUE'])>0){
		$no_sizes=false;
	}
	$offer_id=$good['ID'];
	$ar_price = GetCatalogProductPrice($good["ID"], 1); 
	$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
		$ar_price["ID"],
		$USER->GetUserGroupArray(),
		"N",
		SITE_ID
	);
	$discountPrice = CCatalogProduct::CountPriceWithDiscount(
			$ar_price["PRICE"],
			$ar_price["CURRENCY"],
			$arDiscounts
		);
	$new_price=$discountPrice; 
	if($new_price!=$ar_price["PRICE"]){
		$ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
		$ar_price["PRICE"] = $new_price;
	}
	$attributes=$good['PROPERTY_CML2_ATTRIBUTES'];
}
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$_REQUEST['PRODUCT_ID']);
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arProps=$ob->GetProperties();
    ?><div class="close-icon close-popup">
	<?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/close.svg"); ?>
</div>
<div class="img">
	<!-- Скидывай сюда фотки в полном разрешении, js-кой переключаются и берется основная   -->
    <?
    $myImg_main=CFile::ResizeImageGet($arFields['DETAIL_PICTURE'], array('width'=>1100, 'height'=>1100), BX_RESIZE_IMAGE_PROPORTIONAL , true);
    ?>
	<div class="preview" ss-container>
		<img src="<?=strlen($myImg_main['src'])>0?$myImg_main['src']:"/img/prod-sample.jpg";?>" alt="">
		<?
				foreach($arProps['MORE_PHOTO']['VALUE'] as $k=>$v)
				{
				$myImg= CFile::ResizeImageGet($v, array('width'=>1100, 'height'=>1100), BX_RESIZE_IMAGE_PROPORTIONAL , true);
				$arFile = CFile::GetFileArray($v);
				if(file_exists($_SERVER["DOCUMENT_ROOT"].$arFile['SRC'])){
				?>
				<img src="<?=$myImg['src']?>" alt="" title=""/>
				<?
				}
				$i++;
			}
		?>
	</div>
	<div class="main-image" style="background-image:url(<?=strlen($myImg_main['src'])>0?$myImg_main['src']:"/img/prod-sample.jpg";?>)"></div>
</div>
<div class="info">
	<div class="top">
		<div class="title"><?=$arFields['NAME'];?> <?=$arProps['CML2_MANUFACTURER']['VALUE'];?></div>
		<p>Артикул: <?=$arProps['CML2_ARTICLE']['VALUE'];?></p>
        <?
            $rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arFields['ID'], 'CATALOG_GROUP_ID' => 1)); 
	       if ($arPrice = $rsPrices->Fetch()) $price = floor($arPrice["PRICE"]);
        ?>
		<div class="price">
			<? if(strlen($ar_price["DISCOUNT_PRICE"])>0){?><span class="old"><?=number_format($ar_price["DISCOUNT_PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span><?}?>
			<span><?=number_format($ar_price['PRICE'], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
		</div>
	</div>
    <? if(count($arProps['CML2_ATTRIBUTES']['VALUE'])>0){?>
	<div class="description-dropdown">
		<span><i></i><a class="dashed">Информация о товаре</a></span>
		<!-- сюда можно заливать хоть весь контент, главное, чтобы контейнером был div description-hider -->
		<div class="description-hider">
             <? 
			if(strlen($arProps['SOSTAV']['VALUE'])>0){
			$arProps['SOSTAV']['VALUE'] = str_replace("%", "% ", $arProps['SOSTAV']['VALUE']);
			$returnValue = preg_replace('/([а-я])([0-9])/', '$1,$2', $arProps['SOSTAV']['VALUE'] , -1, $count);
			if($arProps['CML2_MANUFACTURER']['VALUE']!="LaRoom") echo '<div class="ib-desc-item"><p>Состав</p><div></div><p>'.$returnValue."</p></div>";
				
			}
			$res = CIBlockElement::GetByID($offer_id);
			if($obRes = $res->GetNextElement())
			{
			  $ar_res = $obRes->GetProperty("CML2_ATTRIBUTES");
			}	 			
			foreach ($ar_res['DESCRIPTION'] as $key=>$attribute){
				if($attribute!="Размер")
					echo '<div class="ib-desc-item"><p>'.$attribute."</p><div></div><p>".$ar_res['VALUE'][$key]."</p></div>";
			}
			foreach ($arProps['CML2_ATTRIBUTES']['VALUE'] as $key=>$attribute){
                echo '<div class="ib-desc-item"><p>'.$attribute."</p><div></div><p>".$arProps['CML2_ATTRIBUTES']['DESCRIPTION'][$key]."</p></div>";
            }
			$arParams = array("replace_space"=>"-","replace_other"=>"-");
            $trans = Cutil::translit($arProps['CML2_MANUFACTURER']['VALUE'],"ru",$arParams);	
			if(strlen($arProps['CML2_MANUFACTURER']['VALUE'])>0)
				echo '<div class="ib-desc-item"><p>Дизайнер</p><div></div><p><a href="/dizaynery/'.$trans.'/">'.$arProps['CML2_MANUFACTURER']['VALUE']."</a></p></div>";
			?>
		</div>
	</div>
    <?}?>
    <? if($no_sizes===false){?>
	<div class="bottom">
		<? 
		if(strlen($arProps['HEIGHT_MODEL']['VALUE'])>0&&strlen($arProps['SIZE_GUIDE_0']['VALUE'])>0){?>
		<p class="about">Фотомодель: рост <?=$arProps['HEIGHT_MODEL']['VALUE'];?> см., размер одежды <?=$arProps['SIZE_GUIDE_0']['VALUE'];?></p>
		<?}?>
		<div class="group">
			<div class="size">
				<span>Размер:</span>
				<div class="sizecontent">
				<?
				$counter=0;
				foreach($res_offers[$product_id] as $good){
					$counter++;
					$checked="";
					if($counter==1){
						$offer_id=$good['ID'];
						$offer_size=$good['PROPERTY_RAZMER_VALUE'];
						$checked="checked";
					}
					if(strlen($good['PROPERTY_RAZMER_VALUE'])>0){
				?>
				<div class="radio">
					<input <?=$checked;?> offer-id="<?=$good['ID'];?>" size-id="<?=$good['PROPERTY_RAZMER_VALUE'];?>" type="radio" id="item-<?=$good['PROPERTY_RAZMER_VALUE'];?>" name="radio-group">
					<label for="item-<?=$good['PROPERTY_RAZMER_VALUE'];?>"><?=$good['PROPERTY_RAZMER_VALUE'];?></label>
				</div>
				<?}}?>
			</div>
			</div>
			<?
            $groups = CIBlockElement::GetElementGroups($product_id, true);
            $ar_groups = Array();
            while($ar_group = $groups->Fetch()){
                $SECTION_ID=$ar_group["ID"];

                $ids[]=$SECTION_ID;
                $res = CIBlockSection::GetByID($ar_group["ID"]);
                if($ar_res = $res->GetNext())
                  $ids[]=$ar_res['IBLOCK_SECTION_ID'];
            }	
            foreach ($ids as $id){
                $nav = CIBlockSection::GetNavChain(false, $id);
                foreach($nav->arResult as $navres){
                    if(!in_array($navres['ID'],$ids))
                        array_push($ids,$navres['ID']);
                }
            }
            $arFilter = array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ID"=>$ids);
            $res = CIBlockSection::GetList(array(), $arFilter, false, array("ID","NAME","UF_SIZES_GRID"), array());
            
            while($arSection = $res->Fetch()){
                $pieces = explode('"', $arSection['UF_SIZES_GRID']);
                if(count($arSection['UF_SIZES_GRID'])>1)
                   $sizes_grid=$arSection['UF_SIZES_GRID'];
            }
            ?>
            <? if(count($sizes_grid)>0){?>
			<a link="<?=$arFields['DETAIL_PAGE_URL'];?>" href="javascript:void(0);" class="table-size">
				<span class="dashed">Размерная сетка</span>
			</a>
            <?}?>
			
		</div>
		<div class="group">
			<button product-id="<?=$offer_id;?>" size="<?=$offer_size;?>" class="btn addtocart addtocart<?=$offer_id;?>">Добавить в корзину</button>
			<button product-id="<?=$offer_id;?>" size="<?=$offer_size;?>" class="btn quickbuy">Купить в 1 клик</button>
		</div>
        <?
        $arSelect = Array();
        $arFilter = Array("IBLOCK_ID"=>12,"CREATED_BY"=>$USER->GetID(),"PROPERTY_GOOD"=>$arFields['ID']);
        $res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
        $found=0;
        while($ob = $res->GetNextElement())
        {
           $found=1;
        }
        ?>
		<? if ($USER->IsAuthorized()){?>
		<div class="group group-center">
			<a href="<? if($found==1){ '/personal/wishlist/';  }?>" product-id="<?=$arFields['ID'];?>" product-name="<?=$arFields['NAME'];?> <?=$arProps['CML2_MANUFACTURER']['VALUE'];?>" class="dashed wishlist-link <? if ($found==1) echo "disabled";?>">
			<span><?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/star.svg"); ?></span><span class="texwish"><? if($found==1) echo "Товар в WishListе"; else echo "Отложить в WishList";?></span>
			</a>
		</div>
		<?}?>
	</div>
	<?}?>
</div>
<?
}


?>