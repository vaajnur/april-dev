<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

$arSelect = Array();
$mxResult = CCatalogSku::GetProductInfo(
	$_REQUEST['PRODUCT_ID']
);
if (is_array($mxResult))
{
	$product_id=$mxResult['ID'];
}
else{
	$product_id=$_REQUEST['PRODUCT_ID'];
}
$res_offers = CCatalogSKU::getOffersList(
	$product_id,
	CATALOG_IBLOCK_ID,
	$skuFilter = array("CATALOG_AVAILABLE"=>"Y","ACTIVE"=>"Y"),
	$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
	$propertyFilter = array()
);
foreach($res_offers[$arResult['ID']] as $good){
	$ar_price = GetCatalogProductPrice($good["ID"], 1); 
}
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$_REQUEST['PRODUCT_ID']);
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arProps=$ob->GetProperties();
    ?>
<div class="close-icon close-popup">
	<?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/close.svg"); ?>
</div>
<span class="h2">Выберите размер:</span>
<div class="size">
	<div class="sizes-wrap">
		<?
		$counter=0;
		foreach($res_offers[$product_id] as $good){
			$counter++;
			$checked="";
			if($counter==1){
				$offer_id=$good['ID'];
				$offer_size=$good['PROPERTY_RAZMER_VALUE'];
				$checked="checked";
			}
		?>
		<div class="radio">
			<input offer-id="<?=$good['ID'];?>" size-id="<?=$good['PROPERTY_RAZMER_VALUE'];?>" type="radio" id="one-item-<?=$good['PROPERTY_RAZMER_VALUE'];?>" name="radio-group-one" <?=$checked;?>>
			<label for="one-item-<?=$good['PROPERTY_RAZMER_VALUE'];?>"><?=$good['PROPERTY_RAZMER_VALUE'];?></label>
		</div>
		<?}?>
	</div>
</div>
<button product-id="<?=$offer_id;?>" size="<?=$offer_size;?>" class="btn addtocart addtocart<?=$offer_id;?>" class="btn">Купить</button>
<?
}


?>