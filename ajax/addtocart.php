<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");
if(isset($_REQUEST["cmd"])){
    switch($_REQUEST["cmd"])
    {
        case "addP2B":
            //количество товаров в корзине (без цикла) 
            $cntBasketItems = CSaleBasket::GetList(
               array(),
               array( 
                  "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                  "LID" => SITE_ID,
                  "ORDER_ID" => "NULL",
                  "PRODUCT_ID"=> $_REQUEST['product_id']
               ), 
               array()
            );
            $product=CCatalogProduct::GetByID(
             $_REQUEST['product_id']
            );
            /*if($cntBasketItems>=$product['QUANTITY']){
                echo "disabled";
            }
            else{*/
            $mxResult = CCatalogSku::GetProductInfo(
			$_REQUEST["product_id"]
			);
			if (is_array($mxResult))
			{
			 $product_id=$mxResult['ID'];
			}
			$resItem=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>intval($product_id)),false,false,array("ID","IBLOCK_ID","NAME","IBLOCK_SECTION_ID","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE"));
                $arItem=$resItem->GetNext();
                $nav = CIBlockSection::GetNavChain($arItem["IBLOCK_ID"], $arItem["IBLOCK_SECTION_ID"]);
                $category="";
                while ($arNav=$nav->GetNext()):
                    $category.=$arNav["NAME"].'/';
                endwhile;
                $category=trim($category,"/"); 
                $res_offers = CCatalogSKU::getOffersList(
					$product_id,
					CATALOG_IBLOCK_ID,
					$skuFilter = array(),
					$fields = array("PROPERTY_TSVET","NAME","PRICE"),
					$propertyFilter = array()
				);
                foreach($res_offers[$product_id] as $good){
                    $color=$good['PROPERTY_TSVET_VALUE'];
                    $ar_price = GetCatalogProductPrice($good["ID"], 1); 
                    $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                        $ar_price["ID"],
                        $USER->GetUserGroupArray(),
                        "N",
                        SITE_ID
                    );
                    $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                            $ar_price["PRICE"],
                            $ar_price["CURRENCY"],
                            $arDiscounts
                        );
                    $new_price=$discountPrice; 
                    if($new_price!=$ar_price["PRICE"]){
                        $ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
                        $ar_price["PRICE"] = $new_price;
                    } 
				}
                $designer = $arItem['PROPERTY_CML2_MANUFACTURER_VALUE'];
			 	$article = $arItem['PROPERTY_CML2_ARTICLE_VALUE'];
                $arReturn['name']=explode(" ",$arItem['NAME'])[0]." ".$designer." ".$article;
                $arReturn['id']=$product_id;
                $arReturn['price']=number_format($ar_price['PRICE'],0,".","");
                $arReturn['brand']=$designer; 
                $arReturn['category']=$category;
				
				
                Add2BasketByProductID(
                     $_REQUEST['product_id'],
                     1,
                    array(),
                     array(
                             array("NAME" => "Размер", "CODE" => "SIZE", "VALUE" => $_REQUEST['size']),array("NAME" => "Дизайнер", "CODE" => "designer", "VALUE" => $designer),array("NAME" => "Цвет", "CODE" => "color", "VALUE" => $color),
                         )
                 );
                //записываем что пользователь добавил в корзину для емейл маркетинга
               if(isset($_COOKIE['BITRIX_SM_USER_EMAIL'])){
                    $results = $DB->Query("SELECT * FROM `b_email_marketing_custom` WHERE `email`='".$DB->ForSQL($_COOKIE['BITRIX_SM_USER_EMAIL'])."'"); 
                    //проверка существования записи с сегодняшней датой и этим товаром
                    while ($row = $results->Fetch())
                    {
                       if($row['count_orders']==0){
                           //сегмент "потенциальный покупатель"
                            require_once($_SERVER["DOCUMENT_ROOT"]."/includes/unisenderApi.php"); //подключаем файл класса
                            $apikey=UNISENDER_API_KEY; //API-ключ к вашему кабинету
                            $user_list = UNISENDER_LIST;
                            $uni=new UniSenderApi($apikey); //создаем экземляр класса, с которым потом будем работать
                            CModule::IncludeModule("subscribe");
                            // поиск подписчика по mail, что бы получить код потверждения
                            $subscription = CSubscription::GetByEmail($_COOKIE['BITRIX_SM_USER_EMAIL']);              
                            if($arSub = $subscription->Fetch()){
                                 $aSubscrRub = CSubscription::GetRubricArray($arSub['ID']);
                                array_push($aSubscrRub,4);
                                $subscr = new CSubscription;
                                $subscr->Update($arSub['ID'],array('RUB_ID'=>$aSubscrRub),"s1");
                            } 
                            $uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$_COOKIE['BITRIX_SM_USER_EMAIL'],"potential_client"=>"Да","guest"=>"Нет"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d"))); 
                        } 
                        $arAr = Array(
                            "date_of_sub"=>"".strtotime("now"),
                            "add_to_cart"    => '1',
                            "fuser_id"=>"'".CSaleBasket::GetBasketUserID()."'"
                         );
                        $DB->Update("b_email_marketing_custom", $arAr, "WHERE `email`='".$_COOKIE['BITRIX_SM_USER_EMAIL']."'", $err_mess.__LINE__);
                    }
                }
                echo json_encode($arReturn);
                
            //}
        break;
        case "getHeadBasket":
            $APPLICATION->IncludeComponent("april:sale.basket.basket.line", "top_cart_wc", Array(
                "PATH_TO_BASKET" => "/personal/basket.php",	
                "PATH_TO_PERSONAL" => "/personal/",	
                "SHOW_PERSONAL_LINK" => "Y",
                ),
                false 
            );
        break;
        case "getHeadWishlist":
            $APPLICATION->IncludeComponent("april:sale.basket.basket.line", "top_cart_wc", Array(
                "PATH_TO_BASKET" => "/personal/basket.php",	
                "PATH_TO_PERSONAL" => "/personal/",	
                "SHOW_PERSONAL_LINK" => "Y",
                ),
                false 
            );
        break;
    }
}
?>