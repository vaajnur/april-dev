<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
global $APPLICATION, $USER;
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

$arSelect = Array();
$mxResult = CCatalogSku::GetProductInfo(
	$_REQUEST['PRODUCT_ID']
);
if (is_array($mxResult))
{
	$product_id=$mxResult['ID'];
}
else{
	$product_id=$_REQUEST['PRODUCT_ID'];
}
$res_offers = CCatalogSKU::getOffersList(
	$product_id,
	CATALOG_IBLOCK_ID,
	$skuFilter = array("CATALOG_AVAILABLE"=>"Y","ACTIVE"=>"Y"),
	$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
	$propertyFilter = array()
);
foreach($res_offers[$product_id] as $good){
	$ar_price = GetCatalogProductPrice($good["ID"], 1);
	$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
		$ar_price["ID"],
		$USER->GetUserGroupArray(),
		"N",
		SITE_ID
	);
	$discountPrice = CCatalogProduct::CountPriceWithDiscount(
			$ar_price["PRICE"],
			$ar_price["CURRENCY"],
			$arDiscounts
		);
	$new_price=$discountPrice; 
	if($new_price!=$ar_price["PRICE"]){
		$ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
		$ar_price["PRICE"] = $new_price;
	}
}
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>$product_id);
$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arProps=$ob->GetProperties();
    ?>
<div class="close-icon close-popup">
	<?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/close.svg"); ?>
</div>
<div class="img">
	<div class="main-image" style="background-image:url(/img/prod-sample.jpg)"></div>
</div>
<div class="info">
	<h2 class="popup-title"><?=$arFields['NAME'];?> <?=$arProps['CML2_MANUFACTURER']['VALUE'];?></h2>
	<div class="quick-size size">
			<span>Размер:</span>
			
			<?
			$counter=0;
			foreach($res_offers[$product_id] as $good){
				$counter++;
				$checked="";
				if($good['PROPERTY_RAZMER_VALUE']==$_REQUEST['SIZE']){
					$offer_id=$good['ID'];
					$offer_size=$good['PROPERTY_RAZMER_VALUE'];
					$checked="checked";
				}
			?>
			<div class="radio">
				<input <?=$checked;?> offer-id="<?=$good['ID'];?>" size-id="<?=$good['PROPERTY_RAZMER_VALUE'];?>" type="radio" id="one-item-<?=$good['PROPERTY_RAZMER_VALUE'];?>" name="radio-group-one">
				<label for="one-item-<?=$good['PROPERTY_RAZMER_VALUE'];?>"><?=$good['PROPERTY_RAZMER_VALUE'];?></label>
			</div>
			<?}?>
	</div>
	<div class="quick-price price">
		<? 
		if(strlen($ar_price["DISCOUNT_PRICE"])>0){?><span class="old"><?=number_format($ar_price["DISCOUNT_PRICE"], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span><?}?>
			<span><?=number_format($ar_price['PRICE'], 0, ',', ' ');?> <?php include($_SERVER['DOCUMENT_ROOT']."/img/icons/rouble.svg"); ?></span>
	</div>
	<form action="" class="quick-form">
		<p class="quick-label">
			Заполните форму, и наши менеджеры свяжутся с Вами в течении часа
		</p>
		<div class="input">
             <?
                 $rsUser = CUser::GetByID($USER->GetID()); 
                 $arUser = $rsUser->Fetch();
              ?>
			<input type="phone" class="your_phone" name="phone" placeholder="Телефон" value="<?=$arUser['PERSONAL_PHONE'];?>" required>
			
			<p class="message">Неверный формат телефона</p>
             <input type="hidden" class="product_id" name="product-id" value="<?=$offer_id;?>">
		</div>
		<button class="btn go-quick-buy">Жду Звонка</button>
	</form>
</div>
<?
}


?>