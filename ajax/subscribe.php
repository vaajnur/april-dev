<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?
CModule::IncludeModule("subscribe");
require_once($_SERVER["DOCUMENT_ROOT"]."/includes/unisenderApi.php"); //подключаем файл класса
$apikey=UNISENDER_API_KEY; //API-ключ к вашему кабинету
$user_list = UNISENDER_LIST;
$uni=new UniSenderApi($apikey); //создаем экземляр класса, с которым потом будем работать
function checkEmail($email)
{
		if (!preg_match ( "/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/", $email)) {
			return FALSE;
		}	
		return TRUE;		
}
$submited=0;
$ERROR='';
$_POST['email']=htmlspecialcharsbx($_POST['email']);
if($_POST['do_subscribe']=="Y")
{
	if(!check_email($_POST['email']))
	{
		$ERROR='Введите корректный E-mail<br>';
	}else{
		$subscription = CSubscription::GetByEmail($_POST['email']);
		$subscription->ExtractFields("str_");
		if($str_ID){ 
			$ERROR='Ваш E-mail уже находится в списке рассылок<br>';
            $aSubscrRub = CSubscription::GetRubricArray($str_ID);
            if(!in_array($_POST['rubric_id'],$aSubscrRub)){
                array_push($aSubscrRub,$_POST['rubric_id']);
                $arFields_sub = Array(
                    "RUB_ID" => $aSubscrRub
                );
                $subscr = new CSubscription;
                if($subscr->Update($str_ID,$arFields_sub,"s1"))
                { $submited=1; echo 'ok';  die(); }
            }
		}
        if(strlen($ERROR)>0){
             $ERROR="";
            setcookie("BITRIX_SM_USER_EMAIL", $_POST['email'], time()+3600000, "/");
            //добавляем емейл в таблице емейл маркетинга
            $arFields = array(
                "email"                 => "'".$_POST['email']."'",
                "date_of_sub"           => "".strtotime("now"),
                );
           $ID = $DB->Insert("b_email_marketing_custom", $arFields, $err_mess.__LINE__);
            //если подписаны на блог - ставим сегмент подписчик блога
            if(intval($_POST['rubric_id'])==1){
                 $uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$_POST['email'],"blog_subscriber"=>"Да"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d")));
                if($str_ID){ 
                     $aSubscrRub = CSubscription::GetRubricArray($str_ID);
                    array_push($aSubscrRub,1);
                    $arFields_sub = Array(
                        "RUB_ID" => $aSubscrRub
                    );
                    if(CSubscription::Update($str_ID, $arFields_sub))
                    {}
                }
            }   
            echo "ok";
            die();
        }
	}
	if(!$ERROR)
	{
		$arFields = Array(
			"USER_ID" => ($USER->IsAuthorized()? $USER->GetID():false),
			"FORMAT" => ($FORMAT <> "html"? "text":"html"),
			"EMAIL" => $_POST['email'],
			"CONFIRMED" => "Y",
      		"SEND_CONFIRM" => "Y",
			"ACTIVE" => "Y",
			"RUB_ID" => isset($_POST['rubric_id'])?array($_POST['rubric_id']):array(1)
		);
		$subscr = new CSubscription;
		$ID = $subscr->Add($arFields);
        setcookie("BITRIX_SM_USER_EMAIL", $_POST['email'], time()+3600000, "/");
        $results = $DB->Query("SELECT * FROM `b_email_marketing_custom` WHERE `email`='".$_POST['email']."'"); 
        //проверка существования записи с сегодняшней датой и этим товаром
        if ($row = $results->Fetch())
        {

        }
        else{
            //добавляем емейл в таблице емейл маркетинга
            $arFields = array(
                "email"                 => "'".$_POST['email']."'",
                "date_of_sub"           => "".strtotime("now"),
                );
            $IDD = $DB->Insert("b_email_marketing_custom", $arFields, $err_mess.__LINE__);
        }
        
		//если подписаны на блог - ставим сегмент подписчик блога
        if(intval($_POST['rubric_id'])==1)
            $uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$_POST['email'],"guest"=>"Да","blog_subscriber"=>"Да"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d")));
        else
            $uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$_POST['email'],"guest"=>"Да"),"overwrite"=>2,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d")));  
		if($ID>0)
			CSubscription::Authorize($ID);
		else
			$strWarning .= "Ошибка при добавлении рассылки: ".$subscr->LAST_ERROR."";
		$submited=1;
	}
}
echo $ERROR;
if($_REQUEST['unsubscribe']=='Y')
{
	$res = CSubscription::Delete($str_ID);
	echo 'Рассылка удалена.';
}

if($submited){
	echo 'ok';
}