<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("subscribe");
if($_REQUEST['AJAXSHARE']){
    $APPLICATION->RestartBuffer();
    // email checking
	$errors=0;
    if(strlen($_REQUEST['EMAIL_SHARE'])==0||!check_email($_REQUEST['EMAIL_SHARE'])){
        echo "emailerror";
		$errors++;
    }
	if(strlen($_REQUEST['NAME_SHARE'])==0){
		echo "nameerror";
		$errors++;
	}
	if($errors==0){
		$count=9;
		$goods=array();
		$arSelect = Array("ID", "NAME");
		$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y","ID"=>explode(";",$_REQUEST['IDS']));
		$res = CIBlockElement::GetList(Array("created"=>"DESC"), $arFilter, false, Array("nPageSize"=>$count), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$goods[]=$arFields['ID'];
		}
		$goods=array_unique($goods);
		$advanced_goods=array();
		$my_index=0;
		foreach($goods as $good){
			$arSelect = Array("ID", "NAME","DETAIL_PAGE_URL","DETAIL_PICTURE","PROPERTY_CML2_MANUFACTURER","PROPERTY_CML2_ARTICLE");
			//в фильтре указываем что ищем товары которые были добавлены не более часа назад
		   $arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ACTIVE"=>"Y","ID"=>$good);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(""), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$arProps=$ob->GetProperties();
				$rsPrices = CPrice::GetList(array(), array('PRODUCT_ID' => $arFields['ID'], 'CATALOG_GROUP_ID' => 1)); 
				if ($arPrice = $rsPrices->Fetch()) $price = floor($arPrice["PRICE"]);
				$discount_price="";
				$arDiscounts = CCatalogDiscount::GetDiscountByProduct($arFields['ID']);
				if($arDiscounts[0]['VALUE_TYPE']=="P"&&strlen($arDiscounts[0]["COUPON"])==0){
						$discount_price=$price-($arDiscounts[0]['VALUE']/100)*$price;
				}
				if($arDiscounts[0]['VALUE_TYPE']=="F"&&strlen($arDiscounts[0]["COUPON"])==0){
					$discount_price=$price-$arDiscounts[0]['VALUE'];
				}
				if(strlen($discount_price)>0)
				   $discount_price=$discount_price;
				else{
					$discount_price=$price;
				}
				$res_offers = CCatalogSKU::getOffersList(
					$arFields['ID'],
					$arFields['IBLOCK_ID'],
					$skuFilter = array("CATALOG_AVAILABLE"=>"Y"),
					$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
					$propertyFilter = array()
				);
				$no_sizes=false;
				foreach($res_offers[$arFields['ID']] as $good){
					if(strlen($good['PROPERTY_RAZMER_VALUE'])==0){
						$no_sizes=true;
					}
					$offer_id=$good['ID'];
					$ar_price = GetCatalogProductPrice($good["ID"], 1); 
					$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
						$ar_price["ID"],
						$USER->GetUserGroupArray(),
						"N",
						SITE_ID
					);
					$discountPrice = CCatalogProduct::CountPriceWithDiscount(
							$ar_price["PRICE"],
							$ar_price["CURRENCY"],
							$arDiscounts
						);
					$new_price=$discountPrice; 
					$ar_price["DISCOUNT_PRICE"] = $ar_price["PRICE"]; 
					$ar_price["PRICE"] = $new_price;
				}
				$advanced_goods[$my_index]['LINK']=$arFields['DETAIL_PAGE_URL'];
				$advanced_goods[$my_index]['IMAGE']=$arFields['DETAIL_PICTURE'];
				$advanced_goods[$my_index]['NAME']=$arFields['NAME'];
				$advanced_goods[$my_index]['DESIGNER']=$arFields['PROPERTY_CML2_MANUFACTURER_VALUE'];
				$advanced_goods[$my_index]['ARTICLE']=$arFields['PROPERTY_CML2_ARTICLE_VALUE'];
				$advanced_goods[$my_index]['PRICE']=$ar_price['PRICE']; 
				$advanced_goods[$my_index]['DISCOUNT_PRICE']=$ar_price['DISCOUNT_PRICE']; 
			}
			$my_index++;
		}
		$goods='<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
				<tbody>
					<tr style="max-width:850px;display:block;margin:auto;text-align:center;vertical-align:top;">';
		$counter=0;
		print_r($advanced_goods);
		foreach($advanced_goods as $good){
			$counter++;
			if($counter%3==0){
				$goods.='</tr><tr style="max-width:850px;display:block;margin:auto;text-align:center;vertical-align:top;">';
			}
							$goods.='<td style="display: inline-block;width: 250px;padding: 0 10px 25px;">
						<img src="'.CFile::GetPath($good['IMAGE']).'" style="width: 100%;" alt="">
						<table border="0" cellpadding="0" cellspacing="0" style="width:100%;padding: 20px 0;">
							<tbody>
								<tr>
									<td style="text-align:left;font-family:sans-serif;color:#1f1f1f;font-size:16px;">'.$good['NAME'].' '.$good['DESIGNER'].'</td>';
									if($good['DISCOUNT_PRICE']!=$good['PRICE']){
										$goods.='<td style="text-align:right;font-family:sans-serif;color:#c75e03;font-size:14px;text-decoration: line-through;">'.number_format($good['DISCOUNT_PRICE'], 0, ',', ' ').' руб.</td>';
									}
									$goods.='</tr>
								<tr>
									<td style="text-align:left;font-family:sans-serif;color:#1f1f1f;font-size:14px;">Артикул: '.$good['ARTICLE'].'</td>
									<td style="text-align:right;font-family:sans-serif;color:#1f1f1f;font-size:16px;">'.number_format($good['PRICE'], 0, ',', ' ').' руб.</td>
								</tr>
							</tbody>
						</table>
						<a href="'.$good['LINK'].'?utm_source=email&utm_medium=cpc&utm_campaign=content" style="background: #ff948b;text-decoration: none;color: white;width: 100%;display: block;line-height: 40px;font-family: sans-serif;font-size: 14px;">КУПИТЬ</a>
					</td>';
		}
		 $goods.='</tr>
			</tbody>
		</table>';
        //генерим короткий урл
		$uri = "/wishlist/?ids=".$_REQUEST['IDS']."&USER_ID=".$USER->GetID();
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/short_uri.php");
		$url = CBXAllShortUri::GetShortUri($uri);
		$shortlink = $url;
         $arEventFields = array( 
            "EMAIL" => $_REQUEST['EMAIL_SHARE'],
			"NAME_WHO" => $_REQUEST['NAME_SHARE'],
			 "MALE" => $_REQUEST['MALE'],
			 "NAME_FROM"=> $USER->GetFirstName(),
            "GOODS" => $goods
        ); 
        if (CEvent::Send("SHARE_WISHLIST", "s1", $arEventFields,"Y",75)): 
              echo "accepted"; 
         endif; 
        //генерация ссылки и отправка письма
    }
    die();
}
?>