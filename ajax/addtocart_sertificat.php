<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");

global $APPLICATION, $USER;

CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale"); 
function is_email($email) { 
    if (! preg_match( '/^[A-Za-z0-9!#$%&\'*+-=?^_`{|}~]+@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)+[A-Za-z]$/', $email)) { 
    return false; 
    } else { 
    return true;} 
}
$ERRORS=array();
$SUCCESS=array();
if($_REQUEST['type']=="Электронный"){
    if(strlen($_REQUEST['sender'])==0)
        $ERRORS["error-sender"]="error-sender";
    else
        $ERRORS["error-sender"]="";
    if(strlen($_REQUEST['recipient'])==0)
        $ERRORS["error-recipient"]="error-recipient";
    else
        $ERRORS["error-recipient"]="";
    if(strlen($_REQUEST['email_recipient'])==0)
        $ERRORS["error-email_recipient"]="error-email_recipient";
    else
        $ERRORS["error-email_recipient"]="";
    if(strlen($_REQUEST['date_of_receiving'])==0)
        $ERRORS["error-date_of_receiving"]="error-date_of_receiving";
    else
        $ERRORS["error-date_of_receiving"]="";
    if(strlen($_REQUEST['message'])==0)
        $ERRORS["error-message"]="error-message";
    else
        $ERRORS["error-message"]="";
    if (!is_email($_REQUEST['email_recipient'])) $ERRORS["error-email_recipient-incorrect"]= "error-email_recipient-incorrect"; 
    else
        $ERRORS["error-email_recipient-incorrect"]= "";
    $errored=false;
    foreach($ERRORS as $error){
        if(strlen($error)>0)
            $errored=true;
    }
    if($errored==false){
        //добавляем со всеми параметрами
        if(
			Add2BasketByProductID(
				intval($_REQUEST["product_id"]),
				intval(1),
				array(
					0=>array("NAME" => "От кого", "VALUE" => trim($_REQUEST["sender"]), "CODE" => "SENDER"),
                    1=>array("NAME" => "Кому", "VALUE" => trim($_REQUEST["recipient"]), "CODE" => "RECIPIENT"),
                    2=>array("NAME" => "Email получателя", "VALUE" => trim($_REQUEST["email_recipient"]), "CODE" => "EMAIL_RECIPIENT"),
                    3=>array("NAME" => "Дата получения", "VALUE" => trim($_REQUEST["date_of_receiving"]), "CODE" => "DATE_OF_RECEIVING"),
                    4=>array("NAME" => "Текст поздравления", "VALUE" => trim($_REQUEST["message"]), "CODE" => "MESSAGE"),
                    5=>array("NAME" => "Тип сертификата", "VALUE" => trim($_REQUEST['type']), "CODE" => "TYPE_SERTIFICATE"),
				)
			)
		)
		{
			$arReturn["error"]=0;
		}
        $ERRORS['success']="success";
    }
    else{ 
        $ERRORS['success']="";
    }
    $ERRORS["error-address_recipient"]="";
    $ERRORS["error-index_recipient"]="";
    echo json_encode($ERRORS);
}
if($_REQUEST['type']=="Классический"){
    if(strlen($_REQUEST['sender'])==0)
        $ERRORS["error-sender"]="error-sender";
    else
        $ERRORS["error-sender"]="";
    if(strlen($_REQUEST['recipient'])==0)
        $ERRORS["error-recipient"]="error-recipient";
    else
        $ERRORS["error-recipient"]="";
    if(strlen($_REQUEST['address_recipient'])==0)
        $ERRORS["error-address_recipient"]="error-address_recipient";
    else
        $ERRORS["error-address_recipient"]="";
    if(strlen($_REQUEST['index_recipient'])==0)
        $ERRORS["error-index_recipient"]="error-index_recipient";
    else
        $ERRORS["error-index_recipient"]="";
    if(strlen($_REQUEST['message'])==0)
        $ERRORS["error-message"]="error-message";
    else
        $ERRORS["error-message"]="";
    $errored=false;
    foreach($ERRORS as $error){
        if(strlen($error)>0)
            $errored=true;
    }
    if($errored==false){
        //добавляем со всеми параметрами
        if(
			Add2BasketByProductID(
				intval($_REQUEST["product_id"]),
				intval(1),
				array(
					0=>array("NAME" => "От кого", "VALUE" => trim($_REQUEST["sender"]), "CODE" => "SENDER"),
                    1=>array("NAME" => "Кому", "VALUE" => trim($_REQUEST["recipient"]), "CODE" => "RECIPIENT"),
                    2=>array("NAME" => "Адрес получателя", "VALUE" => trim($_REQUEST["address_recipient"]), "CODE" => "ADDRESS_RECIPIENT"),
                    3=>array("NAME" => "Индекс получателя", "VALUE" => trim($_REQUEST["index_recipient"]), "CODE" => "INDEX_RECIPIENT"),
                    4=>array("NAME" => "Текст поздравления", "VALUE" => trim($_REQUEST["message"]), "CODE" => "MESSAGE"),
                    5=>array("NAME" => "Тип сертификата", "VALUE" => trim($_REQUEST['type']), "CODE" => "TYPE_SERTIFICATE"),
				)
			)
		)
		{
			$arReturn["error"]=0;
		}
        $ERRORS['success']="success";
    }
    else{
        
        $ERRORS['success']="";
    }
    $ERRORS["error-email_recipient-incorrect"]= "";
    $ERRORS["error-email_recipient"]="";
    $ERRORS["error-date_of_receiving"]="error-date_of_receiving";
    echo json_encode($ERRORS);
}
?>