<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
//если 1, значит галка доп фотки и проверять емейл еще нужно
$errors=0;

if(strlen($_REQUEST['phone'])==0){
    echo "phone-error";
    $errors++;
}
//все ок, добавляем в инфоблок и отправляем письмо
if($errors==0){
    if(CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")){        
			$product_id = intval($_REQUEST['product']);
			echo $product_id; 
			
			
			$mxResult = CCatalogSku::GetProductInfo(
				$_REQUEST['product']
			);
			if (is_array($mxResult))
			{
				$product_id=$mxResult['ID'];
			}
			else{
				$product_id=$_REQUEST['PRODUCT_ID'];
			}
			$res_offers = CCatalogSKU::getOffersList(
				$product_id,
				CATALOG_IBLOCK_ID,
				$skuFilter = array(),
				$fields = array("PROPERTY_RAZMER","NAME","PRICE"),
				$propertyFilter = array()
			);
			
			$ar_res = CCatalogProduct::GetByIDEx($product_id);
			foreach($res_offers[$product_id] as $good){
				$ar_price = GetCatalogProductPrice($good["ID"], 1); 
				$ar_res['NAME']=$good['NAME'];
			}
			$price=$ar_price['PRICE'];
			if ($ar_res){
				$is_error=false;
				//id пользователя Быстрый заказ
				$user_id = 7;
				if ($user_id > 0){
					//PAY_SYSTEM_ID, PRICE_DELIVERY, DELIVERY_ID, DISCOUNT_VALUE, TAX_VALUE можно не указывать
					$arFields = array(
						"LID" => SITE_ID,
						"PERSON_TYPE_ID" => 1,
						//Вместо ### укажите конкретный ID цены
						"PRICE" => $price,
						"PAYED" => "N",
						"CANCELED" => "N",
                        "PAY_SYSTEM_ID"=>1,
                        "DELIVERY_ID"=>3,
						"STATUS_ID" => "N",                 
						"CURRENCY" => "RUB",
						"USER_DESCRIPTION" => "",
						"USER_ID" => $user_id
					);
					$ORDER_ID = IntVal(CSaleOrder::Add($arFields));
					if ($ORDER_ID > 0){
						
						 $arFields = array(
							"PRODUCT_ID" => $_REQUEST['product'],
							"PRICE" => $price,
							"CURRENCY" => "RUB",
							"WEIGHT" => $ar_res['PRODUCT']['WEIGHT'],
							"QUANTITY" => 1,
							"DELAY" => "N",
							"LID" => $ar_res['LID'],
							"CAN_BUY" => "Y",
							"ORDER_ID" => $ORDER_ID,
							"NAME" => $ar_res['NAME'],
							"MODULE" => "catalog",
							"NOTES" => "",
							"DETAIL_PAGE_URL" => $ar_res['DETAIL_PAGE_URL'],
							"PROPS" => $arProps
						);
						$add = CSaleBasket::Add($arFields);
						if (intval($add) > 0){
							$arFields = array( 
							"ORDER_ID" => $ORDER_ID,
							'NAME' => "ФИО",
							"ORDER_PROPS_ID" =>1, 
							"CODE"=>"fio",
							"VALUE" => $_REQUEST["name"] 
							);
							CSaleOrderPropsValue::Add($arFields);
							$arFields = array( 
							"ORDER_ID" => $ORDER_ID, 
							'NAME' => "Телефон",
							"ORDER_PROPS_ID" =>3, 
							"CODE"=>"phone",
							"VALUE" => str_replace("+7","8",$_REQUEST["phone"])
							);
							CSaleOrderPropsValue::Add($arFields);
							$result="ok";
							//тут отправка письма по идее
						}
					}
				}
			}
		}
    echo "okidoki";
}
?>