<?
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include.php");
$USER=new Cuser();
CModule::IncludeModule("iblock");
?>
<?
function isUserPassword($userId, $password)
{
    $userData = CUser::GetByID($userId)->Fetch();

    $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));

    $realPassword = substr($userData['PASSWORD'], -32);
    $password = md5($salt.$password);

    return ($password == $realPassword);
}
if(isset($_REQUEST['NAME'])&&$_REQUEST['NAME']!="password"){
	$error=0;
	if(strlen($_REQUEST['VALUE'])==0){
		echo "error";
		$error++;
	} 
	if($_REQUEST['NAME']=="UF_GROWTH"){
		if(strlen($_REQUEST['VALUE'])>3){
			echo "error";
			$error++;
		}
		if(!is_numeric($_REQUEST['VALUE'])){
			echo "error";
			$error++;
		}
	}
	if($_REQUEST['NAME']=="UF_WAIST"){
		if(strlen($_REQUEST['VALUE'])>2){
			echo "error";
			$error++;
		}
		if(!is_numeric($_REQUEST['VALUE'])){
			echo "error";
			$error++;
		}
	}
	if($_REQUEST['NAME']=="UF_CHEST"){
		if(strlen($_REQUEST['VALUE'])>2){
			echo "error";
			$error++;
		}
		if(!is_numeric($_REQUEST['VALUE'])){
			echo "error";
			$error++;
		}
	}
	if($_REQUEST['NAME']=="UF_HIPGIRTH"){
		if(strlen($_REQUEST['VALUE'])>2){
			echo "error";
			$error++;
		}
		if(!is_numeric($_REQUEST['VALUE'])){
			echo "error";
			$error++;
		}
	}
	if($_REQUEST['NAME']=="PERSONAL_BIRTHDAY"){
		
		$val_explode=explode(".",$_REQUEST['VALUE']);
		if(intval($val_explode[0])>31||intval($val_explode[0])<1){
			echo "error";
			$error++;
		}
		if(intval($val_explode[1])>12||intval($val_explode[0])<1){
			echo "error";
			$error++;
		}
		if(intval($val_explode[2])>2007||intval($val_explode[2])<1930){
			echo "error";
			$error++;
		}
		
	}
	if($error==0){
		echo htmlspecialcharsbx($_REQUEST['NAME']);
		echo htmlspecialcharsbx($_REQUEST['VALUE']);
		if($_REQUEST['NAME']=="PERSONAL_BIRTHDAY"){
			$_REQUEST['VALUE']=$_REQUEST['VALUE'];
		}
		if($USER->IsAuthorized()){
			$fields=array(
			$_REQUEST['NAME']=>$_REQUEST['VALUE']
			);
			$USER->Update($USER->GetID(), $fields);
		}
		$rsUser = CUser::GetByID($USER->GetID());
		$arUser = $rsUser->Fetch();
		require_once($_SERVER["DOCUMENT_ROOT"]."/includes/unisenderApi.php"); //подключаем файл класса
		$apikey=UNISENDER_API_KEY; //API-ключ к вашему кабинету
		$user_list = UNISENDER_LIST;
		$uni=new UniSenderApi($apikey); //создаем экземляр класса, с которым потом будем работать
		//обхват груди, рост и т.д в юнисендер
		//$uni->subscribe(array("list_ids"=>array($user_list),"fields"=>array("email"=>$arUser['EMAIL'],"Name"=>$arUser['NAME'],"growth"=>$arUser['UF_GROWTH'],"chest"=>$arUser['UF_CHEST'],"waist"=>$arUser['UF_WAIST'],"hipgirth"=>$arUser['UF_HIPGIRTH']),"overwrite"=>0,"double_optin"=>3,"confirm_ip"=>$_SERVER['REMOTE_ADDR'],"confirm_time"=>date("Y-m-d")));
	}
	
}
if(isset($_REQUEST['NAME'])&&$_REQUEST['NAME']=="password"){
	if(isset($_REQUEST['VALUE'])){
		echo isUserPassword($USER->GetID(), $_REQUEST['VALUE']);
	}
	if(isset($_REQUEST['VALUE1'])&&isset($_REQUEST['VALUE2'])){
		if(strlen($_REQUEST['VALUE1'])>0&&strlen($_REQUEST['VALUE1'])>0){
			if($_REQUEST['VALUE1']==$_REQUEST['VALUE2']){
				echo "1";
				$fields=array(
					"PASSWORD"          => $_REQUEST['VALUE1'],
					"CONFIRM_PASSWORD"  => $_REQUEST['VALUE2'],
				);
				$USER->Update($USER->GetID(), $fields);
				//письмо об изменении пароля
				$arEventFields = array( 
					"NAME" => $USER->GetFirstName(),
					"EMAIL" => $USER->GetEmail()
				); 
				if (CModule::IncludeModule("main")): 
				  if (CEvent::Send("REGISTER_INFO", "s1", $arEventFields,"Y",79)): 
					  echo ""; 
				   endif;
				endif;
				echo $USER->LAST_ERROR;
				//тут меняем пароль на новый
			}
		}
	}
}

if(isset($_REQUEST['ADRES'])){
	$_REQUEST['ADRES']=intval($_REQUEST['ADRES']);
	CModule::IncludeModule("sale");
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	if(isset($_REQUEST['CITY'])){
		$db_vars = CSaleLocation::GetList(
			array(
					"SORT" => "ASC",
					"COUNTRY_NAME_LANG" => "ASC",
					
				),
			array("LID" => LANGUAGE_ID,"%CITY_NAME" => $_REQUEST['CITY']),
			false,
			false,
			array()
		);
	   while ($vars = $db_vars->Fetch()):
		$cityid=$vars["ID"];
	   endwhile;
	   if(strlen($cityid)==0){
			echo "city-error";
	   }
	}
   if($_REQUEST['DELETE']=="Y"){
	   $arUser['UF_ADRESSES'][$_REQUEST['ADRES']]="";
   }
   else if($_REQUEST['ADD']=="Y"){
	   
	   if(strlen($_REQUEST['ADRESNAME'])>0){
		   array_push($arUser['UF_ADRESSES'],$cityid.";".$_REQUEST['STREET'].";".$_REQUEST['HOUSE'].";".$_REQUEST['FLAT'].";".$_REQUEST['ADRESNAME'].";".$_REQUEST['HOUSING']);
	   }
   }
   else{
		if(strlen($cityid)>0){
			$arUser['UF_ADRESSES'][$_REQUEST['ADRES']]=$cityid.";".$_REQUEST['STREET'].";".$_REQUEST['HOUSE'].";".$_REQUEST['FLAT'].";".$_REQUEST['ADRESNAME'].";".$_REQUEST['HOUSING'];
		}
	}
	foreach($arUser['UF_ADRESSES'] as $key=>$value){
		$val_explode=explode(";",$value);
		if(strlen($val_explode[4])==0)
			unset($arUser['UF_ADRESSES'][$key]);
	}
	$fields=array(
		'UF_ADRESSES'=>$arUser['UF_ADRESSES']
	);
	print_r($arUser['UF_ADRESSES']);
	$USER->Update($USER->GetID(), $fields);
} 
function getExtension($str)
{
     $i = strrpos($str,".");
     if (!$i) { return ""; }
     $l = strlen($str) - $i;
     $ext = substr($str,$i+1,$l);
     return $ext;
}

if(isset($_FILES['avatar'])){
  $valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
  //get the extension of the file in a lower case format
  $filename = stripslashes($_FILES['avatar']['name']);
   $ext = getExtension($filename);
   $ext = strtolower($ext);
  if(in_array($ext,$valid_formats))
  {
	  move_uploaded_file($_FILES["avatar"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["avatar"]["name"]);
	  $arFile = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/'. $_FILES["avatar"]["name"]);
      $arFile['del'] = "Y";           
      $arFile["MODULE_ID"] = "main";
      $fields['PERSONAL_PHOTO'] = $arFile;
		echo '/upload/tmp/'. $_FILES["avatar"]["name"];
		$USER->Update($USER->GetID(), $fields);
	}
}
?>