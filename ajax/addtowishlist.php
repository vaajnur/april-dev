<?require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
if($USER->IsAuthorized()){
    CModule::IncludeModule("catalog");
    CModule::IncludeModule("iblock");
    //Добавляем элемент в инфоблок
    $el = new CIBlockElement;
    $PROP = array();
    $PROP[87] = $_REQUEST['product'];
    $arLoadProductArray = Array(
      "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
      "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
      "IBLOCK_ID"      => 12,
      "PROPERTY_VALUES"=> $PROP,
      "NAME"           => "Товар в вишлисте ".$_REQUEST['product'],
      "ACTIVE"         => "Y"
    );

    if($PRODUCT_ID = $el->Add($arLoadProductArray))
       echo "okidoki";
    else
      echo "Error: ".$el->LAST_ERROR;
}
else{
    echo "non-authorized";
}
?>