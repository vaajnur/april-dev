<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


CModule::IncludeModule("iblock");   
CModule::IncludeModule("catalog"); 
CModule::IncludeModule("sale");
$arBasketItems = array();
$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC",
			"ID" => "ASC"
			),
		array(
			"CAN_BUY" => "Y",
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"ID" => $_REQUEST['id'],
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
			),
		false,
		false,
		array()
	);
$cntbsk=0;
while ($arItems = $dbBasketItems->Fetch())
{
	$real_product=$arItems['PRODUCT_ID'];
}
$mxResult = CCatalogSku::GetProductInfo(
	$real_product
);
if (is_array($mxResult))
{
 $product_id=$mxResult['ID'];
}
else{
	$product_id=$real_product;
}
CSaleBasket::Delete(
 $_REQUEST['id']
);
if($_REQUEST['quantity']>0){
	$resItem=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"ID"=>intval($product_id)),false,false,array("ID","IBLOCK_ID","NAME","IBLOCK_SECTION_ID","PROPERTY_CML2_MANUFACTURER"));
	$arItem=$resItem->GetNext(); 
	$designer = $arItem['PROPERTY_CML2_MANUFACTURER_VALUE'];
	$arReturn['name']=explode(" ",$arItem['NAME'])[0]." ".$designer;
	$arReturn['id']=$_REQUEST["product_id"];
	$arReturn['price']=$price;
	$arReturn['brand']=$designer; 
	$arReturn['category']=$category;
	$res_offers = CCatalogSKU::getOffersList(
		$product_id,
		CATALOG_IBLOCK_ID,
		$skuFilter = array(),
		$fields = array("PROPERTY_TSVET","PROPERTY_RAZMER","NAME","PRICE","CATALOG_QUANTITY"),
		$propertyFilter = array()
	);
	foreach($res_offers[$product_id] as $good){
		if($good['PROPERTY_RAZMER_VALUE']==$_REQUEST['size']){
			$real_product=$good['ID'];
			if($_REQUEST['quantity']>$good['CATALOG_QUANTITY']){
				$_REQUEST['quantity']=$good['CATALOG_QUANTITY'];
			}
		}
		$color=$good['PROPERTY_TSVET_VALUE'];
	}
	 Add2BasketByProductID(
		  $real_product,
		  $_REQUEST['quantity'],
		array(),
		  array(
				  array("NAME" => "Размер", "CODE" => "SIZE", "VALUE" => $_REQUEST['size']),array("NAME" => "Дизайнер", "CODE" => "designer", "VALUE" => $designer),array("NAME" => "Цвет", "CODE" => "color", "VALUE" => $color),
		  )
	);
}
?>